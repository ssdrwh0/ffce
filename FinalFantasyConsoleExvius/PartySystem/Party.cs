﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using FinalFantasyConsoleExvius.Properties;

namespace FinalFantasyConsoleExvius
{
    public class Party
    {
        public static readonly int MAX_PARTY_SIZE = 5;
        public static readonly int MAX_ESPER_POINTS = 10;

        public static int gil = 0;
        public static int ep = 0;
        public static int epRefund = 0;
        public static int deathCount = 0;

        public static Vision selectedVision = null;

        public static List<SaveData> saves = new List<SaveData>();
        public static List<Unit> currentParty = new List<Unit>();
        public static List<Unit> players = new List<Unit>();
        public static List<Vision> visions = new List<Vision>();
        public static List<Esper> esperData = new List<Esper>();
        public static List<string> unlockedContent = new List<string>();

        public static Dictionary<string, int> itemsData = new Dictionary<string, int>();
        public static Dictionary<Equipment, int> equipmentData = new Dictionary<Equipment, int>();
        public static Dictionary<Equipment, int> equipmentDataUnsellable = new Dictionary<Equipment, int>();
        public static Dictionary<Materia, int> materiaData = new Dictionary<Materia, int>();
        public static Dictionary<Materia, int> materiaDataUnsellable = new Dictionary<Materia, int>();

        public static void loadPreset(string dataName, out string assetName, out EngineState state)
        {
            if (GameEngine.debug) Console.WriteLine("From preset data " + dataName + "...");
            load(AssetLoader.presetData[dataName], out assetName, out state);
        }

        public static void loadCurrentData(int target, out string assetName, out EngineState state)
        {
            if (GameEngine.debug) Console.WriteLine("From save files...");
            load(saves[target], out assetName, out state);
        }

        public static bool hasCharacter(string name)
        {
            foreach(Character unit in currentParty)
                if(unit.DataName.Equals(name))
                    return true;
            return false;
        }

        public static void save(int i, string assetName, EngineState state)
        {
            try
            {/**/
                List<LoadoutData> visionsData = new List<LoadoutData>();
                List<LoadoutData> partyData = new List<LoadoutData>();
                List<string> items = new List<string>();
                List<int> itemsQty = new List<int>();
                List<string> equips = new List<string>();
                List<int> equipsQty = new List<int>();
                List<string> equipsUnsellable = new List<string>();
                List<int> equipsQtyUnsellable = new List<int>();
                List<string> materias = new List<string>();
                List<int> materiasQty = new List<int>();
                List<string> materiasUnsellable = new List<string>();
                List<int> materiasQtyUnsellable = new List<int>();
                List<string> esperNamesData = new List<string>();
                List<int> esperLevelsData = new List<int>();
                List<string> unlockedContent = new List<string>();

                using (StreamWriter file = File.CreateText(ResourcePath.SaveFilePath))
                {
                    foreach (Character unit in visions)
                    {
                        LoadoutData detail = new LoadoutData(unit.ChainId, unit.UnitLevel, unit.LBLevel, unit.Exp, unit.Name, unit.getEsperName(), unit.getEsperLevel(), unit.getWeaponSlotA(),
                            unit.getWeaponPassivesA(), unit.getWeaponSlotB(), unit.getWeaponPassivesB(), unit.getEquipmentNames(), unit.getMateriaNames());
                        visionsData.Add(detail);
                    }
                    foreach (Character unit in players)
                    {
                        LoadoutData detail = new LoadoutData(unit.ChainId, unit.UnitLevel, unit.LBLevel, unit.Exp, unit.Name, unit.getEsperName(), unit.getEsperLevel(), unit.getWeaponSlotA(),
                            unit.getWeaponPassivesA(), unit.getWeaponSlotB(), unit.getWeaponPassivesB(), unit.getEquipmentNames(), unit.getMateriaNames());
                        partyData.Add(detail);
                    }
                    foreach (string item in itemsData.Keys) items.Add(item);
                    foreach (int qty in itemsData.Values) itemsQty.Add(qty);
                    foreach (Equipment equip in equipmentData.Keys) equips.Add(equip.EquipData.Name);
                    foreach (int qty in equipmentData.Values) equipsQty.Add(qty);
                    foreach (Equipment equip in equipmentDataUnsellable.Keys) equipsUnsellable.Add(equip.EquipData.Name);
                    foreach (int qty in equipmentDataUnsellable.Values) equipsQtyUnsellable.Add(qty);
                    foreach (Materia mat in materiaData.Keys) materias.Add(mat.Name);
                    foreach (int qty in materiaData.Values) materiasQty.Add(qty);
                    foreach (Materia mat in materiaDataUnsellable.Keys) materiasUnsellable.Add(mat.Name);
                    foreach (int qty in materiaDataUnsellable.Values) materiasQtyUnsellable.Add(qty);
                    foreach (Esper esper in esperData)
                    {
                        esperNamesData.Add(esper.Name);
                        esperLevelsData.Add(esper.EsperLevel);
                    }

                    string saveState = "";
                    switch (state)
                    {
                        case EngineState.COMBAT_STATE: saveState = "COMBAT_STATE"; break;
                        case EngineState.TOWN_STATE: saveState = "TOWN_STATE"; break;
                        default: saveState = "STORY_STATE"; break;
                    }

                    string visionName = "";
                    if (selectedVision != null) visionName = selectedVision.DataName;

                    SaveData save = new SaveData(visionsData, partyData, items, itemsQty, equips, equipsQty, equipsUnsellable, equipsQtyUnsellable, materias, materiasQty, materiasUnsellable,
                        materiasQtyUnsellable, esperNamesData, esperLevelsData, unlockedContent, gil, assetName, saveState, visionName);
                    if (i > -1 && i < saves.Count) saves[i] = save;
                    else saves.Add(save);

                    JsonSerializer serializer = new JsonSerializer();
                    //Formats the objects to be more readable
                    serializer.Formatting = Newtonsoft.Json.Formatting.Indented;
                    //serialize object directly into file stream
                    serializer.Serialize(file, saves);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Message: " + ex.Message + "\n\n" + ex.StackTrace);// + "\n\nInner: " + ex.InnerException + "\n\nSource: " + ex.Source);
                Console.ReadLine();
                System.Environment.Exit(0);
            }/**/
        }

        //Similar to load, but does not "clear" all current data
        public static void unlock(SaveData save)
        {
            foreach (LoadoutData data in save.visionData) addVision(data);
            foreach (LoadoutData data in save.partyData) addCharacter(data);
            int i = 0;
            foreach (string name in save.esperNamesData) esperData.Add(new Esper(AssetLoader.esperData[name], save.esperLevelsData[i++], false));

            if (selectedVision != null)
            {
                foreach (Vision vision in visions)
                    if (vision.DataName.Equals(save.selectedVision))
                    {
                        selectedVision = vision;
                        break;
                    }
            }

            for (i = 0; i < save.itemsData.Count; i++)
            {
                if (itemsData.Keys.Contains(save.itemsData[i]))
                    itemsData[save.itemsData[i]] += save.itemsQtyData[i];
                else itemsData.Add(save.itemsData[i], save.itemsQtyData[i]);
            }

            for (i = 0; i < save.equipmentData.Count; i++)
            {
                Equipment equip = new Equipment(AssetLoader.equipData[save.equipmentData[i]], false);
                if (equipmentData.Keys.Contains(equip)) equipmentData[equip] += save.equipmentQtyData[i];
                else equipmentData.Add(equip, save.equipmentQtyData[i]);
            }

            for (i = 0; i < save.equipmentDataUnsellable.Count; i++)
            {
                Equipment equip = new Equipment(AssetLoader.equipData[save.equipmentDataUnsellable[i]], false);
                if (equipmentDataUnsellable.Keys.Contains(equip)) equipmentDataUnsellable[equip] += save.equipmentQtyDataUnsellable[i];
                else equipmentDataUnsellable.Add(equip, save.equipmentQtyDataUnsellable[i]);
            }

            for (i = 0; i < save.materiaData.Count; i++)
            {
                Materia mat = new Materia(AssetLoader.materiaData[save.materiaData[i]], false);
                if (materiaData.Keys.Contains(mat)) materiaData[mat] += save.materiaQtyData[i];
                else materiaData.Add(mat, save.materiaQtyData[i]);
            }

            for (i = 0; i < save.materiaDataUnsellable.Count; i++)
            {
                Materia mat = new Materia(AssetLoader.materiaData[save.materiaDataUnsellable[i]], false);
                if (materiaDataUnsellable.Keys.Contains(mat)) materiaDataUnsellable[mat] += save.materiaQtyDataUnsellable[i];
                else materiaDataUnsellable.Add(mat, save.materiaQtyDataUnsellable[i]);
            }

            foreach(string content in save.unlockedContent)
            {
                bool flag = true;
                foreach(string check in save.unlockedContent)
                    if(content.Equals(check))
                    {
                        flag = false;
                        break;
                    }
                if(flag)unlockedContent.Add(content);
            }

            gil += save.gil;
        }

        public static bool hasItem(string itemName)
        {
            foreach (string item in itemsData.Keys)
            {
                int value = itemsData[item];
                string lowerName = AssetLoader.itemsData[item].DisplayName.ToLower();
                if (lowerName.Equals(itemName))
                {
                    if(value > 0) return true;
                    else
                    {
                        Console.WriteLine("You do not have anymore of this item! Try again!");
                        return false;
                    }
                }
            }
            Console.WriteLine("Item does not exist! Try again!");
            return false;
        }

        public static void listAvailableItems()
        {
            List<string> consoleList = new List<string>();
            foreach (string item in itemsData.Keys)
            {
                int value = itemsData[item];
                string lowerName = AssetLoader.itemsData[item].DisplayName.ToLower();
                string description = AssetLoader.itemsData[item].Description;
                if (value > 0) consoleList.Add(lowerName + " - " + value.ToString() + " left; " + description);
            }

            Console.WriteLine("\nitems available:\n");
            foreach (string description in consoleList) Console.WriteLine(description);
        }

        public static Ability getItemAbility(string displayName)
        {
            foreach (string item in itemsData.Keys)
            {
                string lowerName = AssetLoader.itemsData[item].DisplayName.ToLower();
                if (lowerName.Equals(displayName)) return AssetLoader.itemsData[item].AbilityEffect;
            }
            return null;
        }

        public static void itemUsed(string itemName)
        {
            if (itemsData.Keys.Contains(itemName)) itemsData[itemName]--;
        }

        private static void load(SaveData save, out string assetName, out EngineState state)
        {
            //Clears existing data
            selectedVision = null;
            itemsData.Clear();
            equipmentData.Clear();
            equipmentDataUnsellable.Clear();
            materiaData.Clear();
            materiaDataUnsellable.Clear();
            esperData.Clear();
            visions.Clear();
            players.Clear();

            Console.WriteLine("\n=== VISIONS ===");
            foreach (LoadoutData data in save.visionData) visions.Add(addVision(data));
            Console.WriteLine("\n=== IN PARTY ===");
            foreach (LoadoutData data in save.partyData) players.Add(addCharacter(data));
            int i = 0;
            foreach (string name in save.esperNamesData) esperData.Add(new Esper(AssetLoader.esperData[name], save.esperLevelsData[i++], false));

            foreach (Vision vision in visions)
                if (vision.DataName.Equals(save.selectedVision))
                {
                    selectedVision = vision;
                    break;
                }

            for (i = 0; i < save.itemsData.Count; i++)
            {
                if (AssetLoader.itemsData.ContainsKey(save.itemsData[i])) itemsData.Add(save.itemsData[i], save.itemsQtyData[i]);
                else
                {
                    if(GameEngine.debug) Console.WriteLine("Item " + save.itemsData[i] + " does not exist!");
                }
            }
            for (i = 0; i < save.equipmentData.Count; i++)
                equipmentData.Add(new Equipment(AssetLoader.equipData[save.equipmentData[i]], false), save.equipmentQtyData[i]);
            for (i = 0; i < save.equipmentDataUnsellable.Count; i++)
                equipmentDataUnsellable.Add(new Equipment(AssetLoader.equipData[save.equipmentDataUnsellable[i]], false), save.equipmentQtyDataUnsellable[i]);
            for (i = 0; i < save.materiaData.Count; i++)
                materiaData.Add(new Materia(AssetLoader.materiaData[save.materiaData[i]], false), save.materiaQtyData[i]);
            for (i = 0; i < save.materiaDataUnsellable.Count; i++)
                materiaDataUnsellable.Add(new Materia(AssetLoader.materiaData[save.materiaDataUnsellable[i]], false), save.materiaQtyDataUnsellable[i]);

            gil = save.gil;
            unlockedContent = save.unlockedContent;

            assetName = save.assetName;
            switch(save.state)
            {
                case "COMBAT_STATE": state = EngineState.COMBAT_STATE; break;
                case "TOWN_STATE": state = EngineState.TOWN_STATE; break;
                default: state = EngineState.STORY_STATE; break;
            }
            if (GameEngine.debug) Console.WriteLine("Done Loading current data!!!");
        }

        private static void characterDetails(Character newUnit, LoadoutData loadout)
        {
            newUnit.loadCurrentStats(loadout.xp, loadout.level, loadout.lb);

            if (!loadout.weaponSlotA.Equals(""))
            {
                List<Passive> passives = new List<Passive>();
                foreach (string passive in loadout.weaponPassivesA)
                    passives.Add(AssetLoader.passivesData[passive]);
                newUnit.weaponSet.Add(new Weapon(AssetLoader.equipData[loadout.weaponSlotA], passives, true));
            }

            if (!loadout.weaponSlotB.Equals(""))
            {
                List<Passive> passives = new List<Passive>();
                foreach (string passive in loadout.weaponPassivesB)
                    passives.Add(AssetLoader.passivesData[passive]);
                newUnit.weaponSet.Add(new Weapon(AssetLoader.equipData[loadout.weaponSlotB], passives, true));
            }

            foreach (string equip in loadout.armor) newUnit.equipmentSet.Add(new Equipment(AssetLoader.equipData[equip], true));
            foreach (string materia in loadout.materia) newUnit.materiaSet.Add(new Materia(AssetLoader.materiaData[materia], true));

            newUnit.resetStats(true);

            if (GameEngine.debug)
            {
                //newUnit.testStat();
                Console.WriteLine("\n" + newUnit.Name + " Hp: " + newUnit.MaxHP.ToString() + " (" + newUnit.HpBoost.ToString() + ")");
                Console.WriteLine(newUnit.Name + " Mp: " + newUnit.MaxMP.ToString() + " (" + newUnit.MpBoost.ToString() + ")");
                Console.WriteLine(newUnit.Name + " Atk: " + newUnit.Atk.ToString() + " (" + newUnit.AtkBoost.ToString() + " - " + newUnit.EquipAtk.ToString() + ")");
                Console.WriteLine(newUnit.Name + " Mag: " + newUnit.Mag.ToString() + " (" + newUnit.MagBoost.ToString() + " - " + newUnit.EquipMag.ToString() + ")");
                Console.WriteLine(newUnit.Name + " Def: " + newUnit.Def.ToString() + " (" + newUnit.DefBoost.ToString() + " - " + newUnit.EquipDef.ToString() + ")");
                Console.WriteLine(newUnit.Name + " Spr: " + newUnit.Spr.ToString() + " (" + newUnit.SprBoost.ToString() + " - " + newUnit.EquipSpr.ToString() + ")");/**/
            }
        }

        private static Character addCharacter(LoadoutData loadout)
        {
            Esper esper = null;
            if (!loadout.esperName.Equals("")) esper = new Esper(AssetLoader.esperData[loadout.esperName], loadout.esperLevel, true);
            Character newUnit = new Character("", 1, loadout.id, AssetLoader.unitData[loadout.name], esper);
            characterDetails(newUnit, loadout);
            return newUnit;
        }

        private static Vision addVision(LoadoutData loadout)
        {
            Esper esper = null;
            if (!loadout.esperName.Equals("")) esper = new Esper(AssetLoader.esperData[loadout.esperName], loadout.esperLevel, true);
            Vision newUnit = new Vision("", 1, loadout.id, AssetLoader.unitData[loadout.name], esper);
            characterDetails(newUnit, loadout);
            return newUnit;
        }
    }
}
