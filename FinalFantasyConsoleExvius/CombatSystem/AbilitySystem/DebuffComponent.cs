﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    public class DebuffComponent : AbilityComponent
    {
        private bool dispelable;
        private bool hex;
        private int duration;
        private double ignore;
        private DebuffType debuffType;
        private DamageType damageType;
        private AttackType attackType;
        private List<ParameterType> parameterTypes;
        private List<StatusType> statusTypes;

        public DebuffQueue queue;

        public bool Dispelable { get { return dispelable; } }
        public bool Hex { get { return hex; } }
        public int Duration { get { return duration; } }
        public double Ignore { get { return ignore; } }
        public DebuffType DebuffType { get { return debuffType; } }
        public DamageType DamageType { get { return damageType; } }
        public AttackType AttackType { get { return attackType; } }
        public IList<ParameterType> ParameterTypes { get { return parameterTypes.AsReadOnly(); } }
        public IList<StatusType> StatusTypes { get { return statusTypes.AsReadOnly(); } }

        public DebuffComponent(string name, int castTime, int sacrifice, double effect, double scale, double limitScale, double ignore, ActionType actionType, TargetType targetType, List<int> frameData,
            bool dispelable, bool hex, int duration,  DebuffType debuffType, AttackType attackType, DamageType damageType, List<ParameterType> parameterTypes, List<StatusType> statusTypes,
            List<ElementalType> elementalTypes, List<ScalingType> scalingTypes)
            : base(name, castTime, sacrifice, effect, scale, limitScale, actionType, frameData, targetType, scalingTypes, elementalTypes)
        {
            this.dispelable = dispelable;
            this.hex = hex;
            this.duration = duration;
            this.ignore = ignore;
            this.debuffType = debuffType;
            this.attackType = attackType;
            this.damageType = damageType;
            this.statusTypes = statusTypes;
            this.parameterTypes = parameterTypes;
        }

        public bool timePassed() { return --duration < 1; }

        public bool removeImperil(ElementalType elem)
        {
            foreach (ElementalType element in elementalTypes)
            {
                if (elem == element)
                {
                    elementalTypes.Remove(elem);
                    break;
                }
            }
            return elementalTypes.Count > 0;
        }

        public bool removeParameter(ParameterType parameter)
        {
            foreach (ParameterType param in parameterTypes)
            {
                if(param == parameter)
                {
                    parameterTypes.Remove(param);
                    break;
                }
            }
            return parameterTypes.Count > 0;
        }

        public bool removeStatus(StatusType status)
        {
            foreach (StatusType stat in statusTypes)
            {
                if (stat == status)
                {
                    statusTypes.Remove(stat);
                    break;
                }
            }
            return statusTypes.Count > 0;
        }
    }
}
