﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    public class OffensiveComponent : AbilityComponent
    {
        private bool jump;
        private bool duplicate;
        private double ignore;
        private DrainType drainType;
        private DamageType damageType;
        private AttackType attackType;
        private List<double> weight;

        public bool Jump { get { return jump; } }
        public bool Duplicate { get { return duplicate; } }
        public double Ignore { get { return ignore; } }
        public DrainType DrainType { get { return drainType; } }
        public DamageType DamageType { get { return damageType; } }
        public AttackType AttackType { get { return attackType; } }
        public IList<double> Weight { get { return weight.AsReadOnly(); } }

        public OffensiveComponent(string name, bool duplicate, bool jump, int castTime, int sacrifice, double effect, double scale, double limitScale, double ignore, ActionType actionType,
            TargetType targetType, List<int> frameData, List<ScalingType> scalingTypes, AttackType attackType, DamageType damageType, DrainType drainType, List<double> weight,
            List<ElementalType> elementalTypes)
            : base(name, castTime, sacrifice, effect, scale, limitScale, actionType, frameData, targetType, scalingTypes, elementalTypes)
        {
            this.duplicate = duplicate;
            this.jump = jump;
            this.attackType = attackType;
            this.damageType = damageType;
            this.drainType = drainType;
            this.weight = weight;
            this.ignore = ignore;
        }
    }
}
