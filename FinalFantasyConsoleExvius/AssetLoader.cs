﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinalFantasyConsoleExvius.Properties;

namespace FinalFantasyConsoleExvius
{
    public class AssetLoader
    {
        public static List<Race> killerTypes = new List<Race>()
        {
            Race.BEAST,
            Race.PLANT,
            Race.DEMON,
            Race.HUMAN,
            Race.BIRD,
            Race.MACHINE,
            Race.STONE,
            Race.SPIRIT,
            Race.BUG,
            Race.UNDEAD,
            Race.DRAGON,
            Race.AQUATIC
        };
        public static List<string> killerStrings = new List<string>() { "BEAST", "PLANT", "DEMON", "HUMAN", "BIRD", "MACHINE", "STONE", "SPIRIT", "BUG", "UNDEAD", "DRAGON", "AQUATIC" };
        
        public static Dictionary<string, Ability> abilitiesData = new Dictionary<string, Ability>();
        public static Dictionary<string, Passive> passivesData = new Dictionary<string, Passive>();
        public static Dictionary<string, UnitData> unitData = new Dictionary<string, UnitData>();
        public static Dictionary<string, ItemData> itemsData = new Dictionary<string, ItemData>();
        public static Dictionary<string, EquipmentData> equipData = new Dictionary<string, EquipmentData>();
        public static Dictionary<string, MateriaData> materiaData = new Dictionary<string, MateriaData>();
        public static Dictionary<string, EsperData> esperData = new Dictionary<string, EsperData>();
        public static Dictionary<string, TownData> townData = new Dictionary<string, TownData>();
        public static Dictionary<string, ExploreData> exploreData = new Dictionary<string, ExploreData>();
        public static Dictionary<string, DungeonData> dungeonData = new Dictionary<string, DungeonData>();
        public static Dictionary<string, SaveData> presetData = new Dictionary<string, SaveData>();
        public static Dictionary<string, List<StoryData>> storyData = new Dictionary<string, List<StoryData>>();
        public static Dictionary<string, List<EnemySpawnData>> enemySpawnData = new Dictionary<string, List<EnemySpawnData>>();
        public static Random random = new Random();

        public static void init()
        {
            // read JSON directly from a file
            using (StreamReader file = File.OpenText(ResourcePath.PassiveDataPath))
            using (JsonTextReader reader = new JsonTextReader(file))
            {
                JArray a = (JArray)JToken.ReadFrom(reader);
                if(a == null)
                {
                    if (GameEngine.debug) Console.WriteLine("Passives Json file did not load properly!!!");
                }
                else
                {
                    JToken token = a.First;
                    while (token != null)
                    {
                        passivesData.Add((string)token["name"], createPassive(token));
                        token = token.Next;
                    }
                    if (GameEngine.debug) Console.WriteLine("Done Loading passives!!!");
                }
            }

            // read JSON directly from a file
            using (StreamReader file = File.OpenText(ResourcePath.AbilitiesDataPath))
            using (JsonTextReader reader = new JsonTextReader(file))
            {
                JArray a = (JArray)JToken.ReadFrom(reader);
                if (a == null)
                {
                    if (GameEngine.debug) Console.WriteLine("Abilities Json file did not load properly!!!");
                }
                else
                {
                    JToken token = a.First;
                    while (token != null)
                    {
                        abilitiesData.Add((string)token["name"], createAbility(token));
                        token = token.Next;
                    }
                    if (GameEngine.debug) Console.WriteLine("Done Loading abilities!!!");
                }
            }

            // read JSON directly from a file
            using (StreamReader file = File.OpenText(ResourcePath.ItemsDataPath))
            using (JsonTextReader reader = new JsonTextReader(file))
            {
                JArray a = (JArray)JToken.ReadFrom(reader);
                if (a == null)
                {
                    if (GameEngine.debug) Console.WriteLine("Equipment Json file did not load properly!!!");
                }
                else
                {
                    JToken token = a.First;
                    while (token != null)
                    {
                        itemsData.Add((string)token["name"], createItem(token));
                        token = token.Next;
                    }
                    if (GameEngine.debug) Console.WriteLine("Done Loading items!!!");
                }
            }

            // read JSON directly from a file
            using (StreamReader file = File.OpenText(ResourcePath.EquipmentDataPath))
            using (JsonTextReader reader = new JsonTextReader(file))
            {
                JArray a = (JArray)JToken.ReadFrom(reader);
                if (a == null)
                {
                    if (GameEngine.debug) Console.WriteLine("Equipment Json file did not load properly!!!");
                }
                else
                {
                    JToken token = a.First;
                    while (token != null)
                    {
                        equipData.Add((string)token["name"], createEquipment(token));
                        token = token.Next;
                    }
                    if (GameEngine.debug) Console.WriteLine("Done Loading equipment!!!");
                }
            }

            // read JSON directly from a file
            using (StreamReader file = File.OpenText(ResourcePath.MateriaDataPath))
            using (JsonTextReader reader = new JsonTextReader(file))
            {
                JArray a = (JArray)JToken.ReadFrom(reader);
                if (a == null)
                {
                    if (GameEngine.debug) Console.WriteLine("Materia Json file did not load properly!!!");
                }
                else
                {
                    JToken token = a.First;
                    while (token != null)
                    {
                        materiaData.Add((string)token["name"], createMateria(token));
                        token = token.Next;
                    }
                    if (GameEngine.debug) Console.WriteLine("Done Loading materia!!!");
                }
            }

            // read JSON directly from a file
            using (StreamReader file = File.OpenText(ResourcePath.EsperDataPath))
            using (JsonTextReader reader = new JsonTextReader(file))
            {
                JArray a = (JArray)JToken.ReadFrom(reader);
                if (a == null)
                {
                    if (GameEngine.debug) Console.WriteLine("Espers Json file did not load properly!!!");
                }
                else
                {
                    JToken token = a.First;
                    while (token != null)
                    {
                        esperData.Add((string)token["name"], createEsper(token));
                        token = token.Next;
                    }
                    if (GameEngine.debug) Console.WriteLine("Done Loading espers!!!");
                }
            }

            // read JSON directly from a file
            using (StreamReader file = File.OpenText(ResourcePath.UnitDataPath))
            using (JsonTextReader reader = new JsonTextReader(file))
            {
                JArray a = (JArray)JToken.ReadFrom(reader);
                if (a == null)
                {
                    if (GameEngine.debug) Console.WriteLine("Units Json file did not load properly!!!");
                }
                else
                {
                    JToken token = a.First;
                    while (token != null)
                    {
                        unitData.Add((string)token["name"], getUnit(token));
                        token = token.Next;
                    }
                    if (GameEngine.debug) Console.WriteLine("Done Loading units!!!");
                }
            }

            int i = 0;
            //Loads scalable story data
            using (StreamReader file = File.OpenText(ResourcePath.StoryDataPath))
            using (JsonTextReader reader = new JsonTextReader(file))
            {
                JObject o2 = (JObject)JToken.ReadFrom(reader);
                if (o2 == null)
                {
                    if (GameEngine.debug) Console.WriteLine("Story Data Json file did not load properly!!!");
                }
                else
                {
                    JProperty p = (JProperty)o2.First;
                    while (p != null)
                    {
                        List<StoryData> newData = new List<StoryData>();
                        JArray a = (JArray)p.First;
                        JToken token = a.First;
                        while (token != null)
                        {
                            newData.Add(getStoryData(token, i++, p.Path));
                            token = token.Next;
                        }
                        storyData.Add(p.Path, newData);
                        p = (JProperty)p.Next;
                    }
                    if (GameEngine.debug) Console.WriteLine("Done Loading Story Data!!!");
                }
            }
            i = 0;
            //Loads scalable enemy spawn data
            using (StreamReader file = File.OpenText(ResourcePath.EnemySpawnDataPath))
            using (JsonTextReader reader = new JsonTextReader(file))
            {
                JObject o2 = (JObject)JToken.ReadFrom(reader);
                if (o2 == null)
                {
                    if (GameEngine.debug) Console.WriteLine("Enemy Spawn Json file did not load properly!!!");
                }
                else
                {
                    JProperty p = (JProperty)o2.First;
                    while (p != null)
                    {
                        List<EnemySpawnData> newData = new List<EnemySpawnData>();
                        JArray a = (JArray)p.First;
                        JToken token = a.First;
                        while (token != null)
                        {
                            newData.Add(getEnemySpawnData(token, i++, p.Path));
                            token = token.Next;
                        }
                        enemySpawnData.Add(p.Path, newData);
                        p = (JProperty)p.Next;
                    }
                    if (GameEngine.debug) Console.WriteLine("Done Loading Enemy Spawn Data!!!");
                }

            }

            // read JSON directly from a file
            using (StreamReader file = File.OpenText(ResourcePath.TownDataPath))
            using (JsonTextReader reader = new JsonTextReader(file))
            {
                JObject o2 = (JObject)JToken.ReadFrom(reader);
                if (o2 == null)
                {
                    if (GameEngine.debug) Console.WriteLine("Town Data Json file did not load properly!!!");
                }
                else
                {
                    JProperty p = (JProperty)o2.First;
                    while (p != null)
                    {
                        JToken token = p.First;
                        townData.Add(p.Path, createTown(p.Path, token));
                        p = (JProperty)p.Next;
                    }
                    if (GameEngine.debug) Console.WriteLine("Done Loading Town Data info!!!");
                }
            }

            using (StreamReader file = File.OpenText(ResourcePath.LoadoutDataPath))
            using (JsonTextReader reader = new JsonTextReader(file))
            {
                JObject o2 = (JObject)JToken.ReadFrom(reader);
                if (o2 == null)
                {
                    if (GameEngine.debug) Console.WriteLine("Preset Json file did not load properly!!!");
                }
                else
                {
                    JProperty p = (JProperty)o2.First;
                    while (p != null)
                    {
                        JToken token = p.First;
                        if (token != null)
                        {
                            SaveData newPreset = createSaveData(token);
                            presetData.Add(p.Path, newPreset);
                        }
                        p = (JProperty)p.Next;
                    }
                    if (GameEngine.debug) Console.WriteLine("Done Loading preset info!!!");
                }
            }

            using (StreamReader file = File.OpenText(ResourcePath.SaveFilePath))
            using (JsonTextReader reader = new JsonTextReader(file))
            {
                JArray a = (JArray)JToken.ReadFrom(reader);
                if (a == null)
                {
                    if (GameEngine.debug) Console.WriteLine("Save files not loaded properly, or save list is empty!!!");
                }
                else
                {
                    JToken token = a.First;
                    while (token != null)
                    {
                        SaveData save = createSaveData(token);
                        Party.saves.Add(save);
                        token = token.Next;
                    }
                }
                if (GameEngine.debug) Console.WriteLine("Done Loading save files!!!");
            }

            if (GameEngine.debug) Console.WriteLine("Loading assets complete!\n");
        }

        private static List<Race> getRace(JArray a)
        {
            List<Race> killerTypes = new List<Race>();
            if (a == null) return killerTypes;
            JToken type = a.First;
            while (type != null)
            {
                switch ((string)type)
                {
                    case "HUMAN":   killerTypes.Add(Race.HUMAN);    break;
                    case "BEAST":   killerTypes.Add(Race.BEAST);    break;
                    case "DEMON":   killerTypes.Add(Race.DEMON);    break;
                    case "BIRD":    killerTypes.Add(Race.BIRD);     break;
                    case "PLANT":   killerTypes.Add(Race.PLANT);    break;
                    case "MACHINE": killerTypes.Add(Race.MACHINE);  break;
                    case "STONE":   killerTypes.Add(Race.STONE);    break;
                    case "BUG":     killerTypes.Add(Race.BUG);      break;
                    case "SPIRIT":  killerTypes.Add(Race.SPIRIT);   break;
                    case "UNDEAD":  killerTypes.Add(Race.UNDEAD);   break;
                    case "DRAGON":  killerTypes.Add(Race.DRAGON);   break;
                    case "AQUATIC": killerTypes.Add(Race.AQUATIC);  break;
                    default:        killerTypes.Add(Race.NONE);     break;
                }
                type = type.Next;
            }
            return killerTypes;
        }

        private static AbilityType getAbilityType(string condition)
        {
            switch (condition)
            {
                case "ITEM_ABILITY":    return AbilityType.ITEM_ABILITY;
                case "COMBO_ABILITY":   return AbilityType.COMBO_ABILITY;
                case "ESPER_ABILITY":   return AbilityType.ESPER_ABILITY;
                case "LIMIT_BURST":     return AbilityType.LIMIT_BURST;
                case "BLACK_MAGIC":     return AbilityType.BLACK_MAGIC;
                case "WHITE_MAGIC":     return AbilityType.WHITE_MAGIC;
                case "GREEN_MAGIC":     return AbilityType.GREEN_MAGIC;
                case "ALL_MAGIC":       return AbilityType.ALL_MAGIC;
            }
            //Console.WriteLine("AbilityType does not have correct input!");
            return AbilityType.NORMAL_ABILITY;
        }

        private static TargetType getTargetType(string condition)
        {
            switch (condition)
            {
                case "SELF":            return TargetType.SELF;
                case "SINGLE_ALLY":     return TargetType.SINGLE_ALLY;
                case "OTHER_ALLY":      return TargetType.OTHER_ALLY;
                case "ALL_ALLIES":      return TargetType.ALL_ALLIES;
                case "OTHER_ALLIES":    return TargetType.OTHER_ALLIES;
                case "SINGLE_ENEMY":    return TargetType.SINGLE_ENEMY;
                case "RANDOM_ENEMY":    return TargetType.RANDOM_ENEMY;
                case "ALL_ENEMIES":     return TargetType.ALL_ENEMIES;
            }
            //Console.WriteLine("TargetType does not have correct input!");
            return TargetType.NONE;
        }

        private static AttackType getAttackType(string condition)
        {
            switch (condition)
            {
                case "PHYSICAL_ATTACK": return AttackType.PHYSICAL_ATTACK;
                case "MAGICAL_ATTACK":  return AttackType.MAGICAL_ATTACK;
                case "HYBRID_ATTACK":   return AttackType.HYBRID_ATTACK;
                case "FIXED_ATTACK":    return AttackType.FIXED_ATTACK;
            }
            //Console.WriteLine("AttackType does not have correct input!");
            return AttackType.NONE;
        }

        private static DamageType getDamageType(string condition)
        {
            switch (condition)
            {
                case "PHYSICAL_DAMAGE": return DamageType.PHYSICAL_DAMAGE;
                case "MAGICAL_DAMAGE":  return DamageType.MAGICAL_DAMAGE;
                case "HYBRID_DAMAGE":   return DamageType.HYBRID_DAMAGE;
                case "FIXED_DAMAGE":    return DamageType.FIXED_DAMAGE;
                case "EVOKE_DAMAGE":    return DamageType.EVOKE_DAMAGE;
                case "HP_DAMAGE":       return DamageType.HP_DAMAGE;
            }
            //Console.WriteLine("DamageType does not have correct input!");
            return DamageType.NONE;
        }

        private static DrainType getDrainType(string condition)
        {
            switch (condition)
            {
                case "HP_DRAIN": return DrainType.HP_DRAIN;
                case "MP_DRAIN": return DrainType.MP_DRAIN;
            }
            //Console.WriteLine("AttackType does not have correct input!");
            return DrainType.NONE;
        }

        private static DefensiveType getDefensiveType(string condition)
        {
            switch (condition)
            {
                case "HP_RESTORE":              return DefensiveType.HP_RESTORE;
                case "MP_RESTORE":              return DefensiveType.MP_RESTORE;
                case "LB_RESTORE":              return DefensiveType.LB_RESTORE;
                case "EP_RESTORE":              return DefensiveType.EP_RESTORE;
                case "HP_RESTORE_PERCENT":      return DefensiveType.HP_RESTORE_PERCENT;
                case "MP_RESTORE_PERCENT":      return DefensiveType.MP_RESTORE_PERCENT;
                case "LB_RESTORE_PERCENT":      return DefensiveType.LB_RESTORE_PERCENT;
                case "BARRIER":                 return DefensiveType.BARRIER;
                case "RAISE":                   return DefensiveType.RAISE;
                case "ENTRUST":                 return DefensiveType.ENTRUST;
                case "REMOVE_DEBUFF":           return DefensiveType.REMOVE_DEBUFF;
                case "REMOVE_STATUS":           return DefensiveType.REMOVE_STATUS;
                case "REMOVE_ALL_STATUS":       return DefensiveType.REMOVE_ALL_STATUS;
                case "REMOVE_PARAMETER":        return DefensiveType.REMOVE_PARAMETER;
                case "REMOVE_ALL_PARAMETERS":   return DefensiveType.REMOVE_ALL_PARAMETERS;
                case "REMOVE_IMPERIL":          return DefensiveType.REMOVE_IMPERIL;
                case "REMOVE_ALL_IMPERILS":     return DefensiveType.REMOVE_ALL_IMPERILS;
            }
            //Console.WriteLine("DefensiveType does not have correct input!");
            return DefensiveType.NONE;
        }

        private static BuffType getBuffType(string condition)
        {
            switch (condition)
            {
                case "GAIN_ABILITY":        return BuffType.GAIN_ABILITY;
                case "TRIGGER_ABILITY":     return BuffType.TRIGGER_ABILITY;
                case "DAMAGE_MODIFIER":     return BuffType.DAMAGE_MODIFIER;
                case "ELEMENTAL_BUFF":      return BuffType.ELEMENTAL_BUFF;
                case "ELEMENTAL_IMBUE":     return BuffType.ELEMENTAL_IMBUE;
                case "MITIGATION_BUFF":     return BuffType.MITIGATION_BUFF;
                case "COUNTER_PHYSICAL":    return BuffType.COUNTER_PHYSICAL;
                case "COUNTER_MAGICAL":     return BuffType.COUNTER_MAGICAL;
                case "COUNTER_ALL":         return BuffType.COUNTER_ALL;
                case "PARAMETER_BUFF":      return BuffType.PARAMETER_BUFF;
                case "PARAMETER_RESIST":    return BuffType.PARAMETER_RESIST;
                case "PHYSICAL_KILLERS":    return BuffType.PHYSICAL_KILLERS;
                case "MAGICAL_KILLERS":     return BuffType.MAGICAL_KILLERS;
                case "ALL_KILLERS":         return BuffType.ALL_KILLERS;
                case "STATUS_BUFF":         return BuffType.STATUS_BUFF;
                case "AI_MANIPULATION":     return BuffType.AI_MANIPULATION;
                case "UNLOCKS":             return BuffType.UNLOCKS;
                case "RERAISE":             return BuffType.RERAISE;
                case "JUMP":                return BuffType.JUMP;
            }
            //Console.WriteLine("BuffType does not have correct input!");
            return BuffType.NONE;
        }
        
        private static DebuffType getDebuffType(string condition)
        {
            switch (condition)
            {
                case "ELEMENTAL_DEBUFF":    return DebuffType.ELEMENTAL_DEBUFF;
                case "PARAMETER_DEBUFF":    return DebuffType.PARAMETER_DEBUFF;
                case "STATUS_DEBUFF":       return DebuffType.STATUS_DEBUFF;
                case "REMOVE_BUFF":         return DebuffType.REMOVE_STATUS;
                case "REMOVE_STATUS":       return DebuffType.REMOVE_STATUS;
                case "SCAN_STATS":          return DebuffType.SCAN_STATS;
                case "DAMAGE_PER_TURN":     return DebuffType.DAMAGE_PER_TURN;
                case "REMOVE_FROM_FIGHT":   return DebuffType.REMOVE_FROM_FIGHT;
            }
            //Console.WriteLine("DebuffType does not have correct input!");
            return DebuffType.NONE;
        }

        private static List<ParameterType> getParameterTypes(JArray a)
        {
            List<ParameterType> parameterTypes = new List<ParameterType>();
            if (a == null) return parameterTypes;
            JToken type = a.First;
            while (type != null)
            {
                switch ((string)type)
                {
                    case "ATK_PARAMETER":       parameterTypes.Add(ParameterType.ATK_PARAMETER);    break;
                    case "DEF_PARAMETER":       parameterTypes.Add(ParameterType.DEF_PARAMETER);    break;
                    case "MAG_PARAMETER":       parameterTypes.Add(ParameterType.MAG_PARAMETER);    break;
                    case "SPR_PARAMETER":       parameterTypes.Add(ParameterType.SPR_PARAMETER);    break;
                    case "SET HP":              parameterTypes.Add(ParameterType.SET_HP);           break;
                    case "LB_FILL_RATE":        parameterTypes.Add(ParameterType.LB_FILL_RATE);     break;
                    case "LB_DAMAGE":           parameterTypes.Add(ParameterType.LB_DAMAGE);        break;
                    case "LB_REGEN":            parameterTypes.Add(ParameterType.LB_REGEN);         break;
                    case "LB_REGEN_PERCENT":    parameterTypes.Add(ParameterType.LB_REGEN_PERCENT); break;
                    case "HP_REGEN":            parameterTypes.Add(ParameterType.HP_REGEN);         break;
                    case "HP_REGEN_PERCENT":    parameterTypes.Add(ParameterType.HP_REGEN_PERCENT); break;
                    case "MP_REGEN":            parameterTypes.Add(ParameterType.MP_REGEN);         break;
                    case "MP_REGEN_PERCENT":    parameterTypes.Add(ParameterType.MP_REGEN_PERCENT); break;
                    default:                    parameterTypes.Add(ParameterType.NONE);             break;
                }
                type = type.Next;
            }
            return parameterTypes;
        }

        private static List<StatusType> getStatusTypes(JArray a)
        {
            List<StatusType> statusTypes = new List<StatusType>();
            if (a == null) return statusTypes;
            JToken type = a.First;
            while (type != null)
            {
                switch ((string)type)
                {
                    case "BERSERK":     statusTypes.Add(StatusType.BERSERK);    break;
                    case "BLIND":       statusTypes.Add(StatusType.BLIND);      break;
                    case "CHARM":       statusTypes.Add(StatusType.CHARM);      break;
                    case "CONFUSE":     statusTypes.Add(StatusType.CONFUSE);    break;
                    case "DEATH":       statusTypes.Add(StatusType.DEATH);      break;
                    case "DISEASE":     statusTypes.Add(StatusType.DISEASE);    break;
                    case "PARALYSE":    statusTypes.Add(StatusType.PARALYSE);   break;
                    case "PETRIFY":     statusTypes.Add(StatusType.PETRIFY);    break;
                    case "POISON":      statusTypes.Add(StatusType.POISON);     break;
                    case "SILENCE":     statusTypes.Add(StatusType.SILENCE);    break;
                    case "SLEEP":       statusTypes.Add(StatusType.SLEEP);      break;
                    case "STOP":        statusTypes.Add(StatusType.STOP);       break;
                    default:            statusTypes.Add(StatusType.NONE);       break;
                }
                type = type.Next;
            }
            return statusTypes;
        }

        private static List<ScalingType> getScalingTypes(JArray a)
        {
            List<ScalingType> scalingTypes = new List<ScalingType>();
            if (a == null) return scalingTypes;
            JToken type = a.First;
            while (type != null)
            {
                switch((string)type){
                    case "SCALE_BY_ATK": scalingTypes.Add(ScalingType.SCALE_BY_ATK); break;
                    case "SCALE_BY_MAG": scalingTypes.Add(ScalingType.SCALE_BY_MAG); break;
                    case "SCALE_BY_DEF": scalingTypes.Add(ScalingType.SCALE_BY_DEF); break;
                    case "SCALE_BY_SPR": scalingTypes.Add(ScalingType.SCALE_BY_SPR); break;
                }
                type = type.Next;
            }
            return scalingTypes;
        }

        private static List<ElementalType> getElementalTypes(JArray a)
        {
            List<ElementalType> elementalTypes = new List<ElementalType>();
            if (a == null) return elementalTypes;
            JToken type = a.First;
            while (type != null)
            {
                switch ((string)type)
                {
                    case "FIRE":        elementalTypes.Add(ElementalType.FIRE);         break;
                    case "ICE":         elementalTypes.Add(ElementalType.ICE);          break;
                    case "THUNDER":     elementalTypes.Add(ElementalType.THUNDER);      break;
                    case "WATER":       elementalTypes.Add(ElementalType.WATER);        break;
                    case "WIND":        elementalTypes.Add(ElementalType.WIND);         break;
                    case "EARTH":       elementalTypes.Add(ElementalType.EARTH);        break;
                    case "LIGHT":       elementalTypes.Add(ElementalType.LIGHT);        break;
                    case "DARK":        elementalTypes.Add(ElementalType.DARK);         break;
                }
                type = type.Next;
            }
            return elementalTypes;
        }

        private static List<MitigationType> getMitigationTypes(JArray a)
        {
            List<MitigationType> mitigationTypes = new List<MitigationType>();
            if (a == null) return mitigationTypes;
            JToken type = a.First;
            while (type != null)
            {
                switch ((string)type)
                {
                    case "COVER_MITIGATION":    mitigationTypes.Add(MitigationType.COVER_MITIGATION);       break;
                    case "DEFEND_MITIGATION":   mitigationTypes.Add(MitigationType.DEFEND_MITIGATION);      break;
                    case "GENERAL_MITIGATION":  mitigationTypes.Add(MitigationType.GENERAL_MITIGATION);     break;
                    case "MAGICAL_MITIGATION":  mitigationTypes.Add(MitigationType.MAGICAL_MITIGATION);     break;
                    case "PHYSICAL_MITIGATION": mitigationTypes.Add(MitigationType.PHYSICAL_MITIGATION);    break;
                    default:                    mitigationTypes.Add(MitigationType.NONE);                   break;
                }
                type = type.Next;
            }
            
            //Console.WriteLine("MitigationType does not have correct input!");
            return mitigationTypes;
        }

        private static List<AIManipulationType> getAIManipulationTypes(JArray a)
        {
            List<AIManipulationType> aiManipulationTypes = new List<AIManipulationType>();
            if (a == null) return aiManipulationTypes;
            JToken type = a.First;
            while (type != null)
            {
                switch ((string)type)
                {
                    case "ALL_COVER_CHANCES":       aiManipulationTypes.Add(AIManipulationType.ALL_COVER_CHANCES);      break;
                    case "PHYSICAL_COVER_CHANCES":  aiManipulationTypes.Add(AIManipulationType.PHYSICAL_COVER_CHANCES); break;
                    case "MAGICAL_COVER_CHANCES":   aiManipulationTypes.Add(AIManipulationType.MAGICAL_COVER_CHANCES);  break;
                    case "TARGETING_CHANCES":       aiManipulationTypes.Add(AIManipulationType.TARGETING_CHANCES);      break;
                    default:                        aiManipulationTypes.Add(AIManipulationType.NONE);                   break;
                }
                type = type.Next;
            }
            //Console.WriteLine("AIManipulationType does not have correct input!");
            return aiManipulationTypes;
        }

        private static void setNameConditionAndIntValue(string name, string statNameString, string statValueString, string statConditionString, JToken token, List<string> intStatsName,
            List<string> intStatsCondition, List<int> intStatsValue)
        {
            JArray statNames = (JArray)token[statNameString];
            JArray statValues = (JArray)token[statValueString];
            JArray statConditions = (JArray)token[statConditionString];

            if (statNames != null && statConditions != null && statValues != null)
            {
                if (GameEngine.debug && (statNames.Count != statValues.Count || statNames.Count != statConditions.Count))
                    Console.WriteLine(name + " - All int stat details don't have matching value counts!");
                JToken statName = statNames.First;
                JToken statValue = statValues.First;
                JToken statCondition = statConditions.First;

                while (statName != null && statValue != null && statCondition != null)
                {
                    intStatsName.Add((string)statName);
                    intStatsCondition.Add((string)statCondition);
                    intStatsValue.Add((int)statValue);
                    statName = statName.Next;
                    statValue = statValue.Next;
                    statCondition = statCondition.Next;
                }
            } 
        }

        private static void setNameConditionAndDoubleValue(string name, string statNameString, string statValueString, string statConditionString, JToken token, List<string> doubleStatsName,
            List<string> doubleStatsCondition, List<double> doubleStatsValue)
        {
            JArray statNames = (JArray)token[statNameString];
            JArray statValues = (JArray)token[statValueString];
            JArray statConditions = (JArray)token[statConditionString];

            if (statNames != null && statConditions != null && statValues != null)
            {
                if (GameEngine.debug && (statNames.Count != statValues.Count || statNames.Count != statConditions.Count))
                    Console.WriteLine(name + " - All int stat details don't have matching value counts!");
                JToken statName = statNames.First;
                JToken statValue = statValues.First;
                JToken statCondition = statConditions.First;

                while (statName != null && statValue != null && statCondition != null)
                {
                    doubleStatsName.Add((string)statName);
                    doubleStatsCondition.Add((string)statCondition);
                    doubleStatsValue.Add((double)statValue);
                    statName = statName.Next;
                    statValue = statValue.Next;
                    statCondition = statCondition.Next;
                }
            }
        }

        private static void setNameAndIntValue(string name, string statNameString, string statValueString, JToken token, List<string> intStatsName, List<int> intStatsValue)
        {
            JArray statNames = (JArray)token[statNameString];
            JArray statValues = (JArray)token[statValueString];

            if (statNames != null && statValues != null)
            {
                if (GameEngine.debug && statNames.Count != statValues.Count)
                    Console.WriteLine(name + " - All int stat details don't have matching value counts!");
                JToken statName = statNames.First;
                JToken statValue = statValues.First;

                while (statName != null && statValue != null)
                {
                    intStatsName.Add((string)statName);
                    intStatsValue.Add((int)statValue);
                    statName = statName.Next;
                    statValue = statValue.Next;
                }
            } 
        }

        private static void setNameAndDoubleValue(string name, string statNameString, string statValueString, JToken token, List<string> doubleStatsName, List<double> doubleStatsValue)
        {
            JArray statNames = (JArray)token[statNameString];
            JArray statValues = (JArray)token[statValueString];

            if (statNames != null && statValues != null)
            {
                if (GameEngine.debug && statNames.Count != statValues.Count)
                    Console.WriteLine(name + " - All int stat details don't have matching value counts!");
                JToken statName = statNames.First;
                JToken statValue = statValues.First;

                while (statName != null && statValue != null)
                {
                    doubleStatsName.Add((string)statName);
                    doubleStatsValue.Add((double)statValue);
                    statName = statName.Next;
                    statValue = statValue.Next;
                }
            } 
        }

        private static void setNameAndBoolValue(string name, string statNameString, string statValueString, JToken token, List<string> boolStatsName, List<bool> boolStatsValue)
        {
            JArray statNames = (JArray)token[statNameString];
            JArray statValues = (JArray)token[statValueString];

            if (statNames != null && statValues != null)
            {
                if (GameEngine.debug && statNames.Count != statValues.Count)
                    Console.WriteLine(name + " - All int stat details don't have matching value counts!");
                JToken statName = statNames.First;
                JToken statValue = statValues.First;

                while (statName != null && statValue != null)
                {
                    boolStatsName.Add((string)statName);
                    boolStatsValue.Add((bool)statValue);
                    statName = statName.Next;
                    statValue = statValue.Next;
                }
            }
        }

        private static void setIntDictionary(string name, string statNameString, string statValueString, JToken token, Dictionary<string, int> intStats)
        {
            JArray statNames = (JArray)token[statNameString];
            JArray statValues = (JArray)token[statValueString];

            if (statNames != null && statValues != null)
            {
                if (GameEngine.debug && statNames.Count != statValues.Count)
                    Console.WriteLine(name + " - All int stat details don't have matching value counts!");
                JToken statName = statNames.First;
                JToken statValue = statValues.First;

                while (statName != null && statValue != null)
                {
                    intStats.Add((string)statName, (int)statValue);
                    statName = statName.Next;
                    statValue = statValue.Next;
                }
            }
        }

        private static void setDoubleDictionary(string name, string statNameString, string statValueString, JToken token, Dictionary<string, double> doubleStats)
        {
            JArray statNames = (JArray)token[statNameString];
            JArray statValues = (JArray)token[statValueString];

            if (statNames != null && statValues != null)
            {
                if (GameEngine.debug && statNames.Count != statValues.Count)
                    Console.WriteLine(name + " - All int stat details don't have matching value counts!");
                JToken statName = statNames.First;
                JToken statValue = statValues.First;

                while (statName != null && statValue != null)
                {
                    doubleStats.Add((string)statName, (double)statValue);
                    statName = statName.Next;
                    statValue = statValue.Next;
                }
            }
        }

        private static void setBoolDictionary(string name, string statNameString, string statValueString, JToken token, Dictionary<string, bool> boolStats)
        {
            JArray statNames = (JArray)token[statNameString];
            JArray statValues = (JArray)token[statValueString];

            if (statNames != null && statValues != null)
            {
                if (GameEngine.debug && statNames.Count != statValues.Count)
                    Console.WriteLine(name + " - All int stat details don't have matching value counts!");
                JToken statName = statNames.First;
                JToken statValue = statValues.First;

                while (statName != null && statValue != null)
                {
                    boolStats.Add((string)statName, (bool)statValue);
                    statName = statName.Next;
                    statValue = statValue.Next;
                }
            }
        }

        private static void setStringDictionary(string name, string statNameString, string statValueString, JToken token, Dictionary<string, string> stringStats)
        {
            JArray statNames = (JArray)token[statNameString];
            JArray statValues = (JArray)token[statValueString];

            if (statNames != null && statValues != null)
            {
                if (GameEngine.debug && statNames.Count != statValues.Count)
                    Console.WriteLine(name + " - All int stat details don't have matching value counts!");
                JToken statName = statNames.First;
                JToken statValue = statValues.First;

                while (statName != null && statValue != null)
                {
                    stringStats.Add((string)statName, (string)statValue);
                    statName = statName.Next;
                    statValue = statValue.Next;
                }
            }
        }

        private static void setPassives(bool equip, string name, JToken token, List<Passive> passives, List<int> levelRequired)
        {
            JArray a = (JArray)token["passives"];
            JArray b = (JArray)token["levelRequiredPassives"];
            if (a != null && (equip || b != null))
            {
                if (GameEngine.debug && !equip && a.Count != b.Count)
                     Console.WriteLine(name + " - Level required list count does not match passive count!");
                JToken type = a.First;
                JToken type2 = null;
                if (!equip) type2 = b.First;
                while (type != null && (equip || type2 != null))
                {
                    if (passivesData.ContainsKey((string)type))
                    {
                        passives.Add(passivesData[(string)type]);
                        if(!equip)levelRequired.Add((int)type2);
                    }
                    else if (GameEngine.debug) Console.WriteLine((string)type + " did not load properly!");
                    type = type.Next;
                    if (!equip) type2 = type2.Next;
                }
            }
        }

        private static void setAbilities(bool equip, string name, JToken token, List<Ability> abilities, List<int> levelRequired)
        {
            JArray a = (JArray)token["abilities"];
            JArray b = (JArray)token["levelRequiredAbilities"];
            if (a != null && (equip || b != null))
            {
                if (GameEngine.debug && !equip && a.Count != b.Count)
                    Console.WriteLine(name + " - Level required list count does not match ability count!");
                JToken type = a.First;
                JToken type2 = null;
                if (!equip) type2 = b.First;
                while (type != null && (equip || type2 != null))
                {
                    if (abilitiesData.ContainsKey((string)type))
                    {
                        abilities.Add(abilitiesData[(string)type]);
                        if (!equip) levelRequired.Add((int)type2);
                    }
                    else if (GameEngine.debug) Console.WriteLine((string)type + " did not load properly!");
                    type = type.Next;
                    if (!equip) type2 = type2.Next;
                }
            }
        }

        private static Ability createAbility(JToken token)
        {
            /*if(token == null)
            {
                Console.WriteLine("ERROR: token cannot be read properly!");
                return null;
            }*/

            List<AbilityComponent> components = new List<AbilityComponent>();

            string name = (string)token["name"];
            string displayName = "";
            if (token["displayName"] == null) displayName = name;
            else displayName = (string)token["displayName"];

            string description = "";
            if (token["description"] != null) description = (string)token["description"];
            string condition = "";
            if (token["condition"] != null) condition = (string)token["condition"];
            string requirement = "";
            if (token["requirement"] != null) requirement = (string)token["requirement"];

            bool onFirstTurn = false;
            if (token["onFirstTurn"] != null) onFirstTurn = (bool)token["onFirstTurn"];
            int cooldown = 0;
            if (token["cooldown"] != null) cooldown = (int)token["cooldown"];
            int cost = 0;
            if (token["cost"] != null) cost = (int)token["cost"];
            int repeat = 0;
            if (token["repeat"] != null) repeat = (int)token["repeat"];
            int repeatRand = 0;
            if (token["repeatRand"] != null) repeatRand = (int)token["repeatRand"];

            AbilityType abilityType = getAbilityType((string)token["abilityType"]);
            CostType costType = CostType.NONE;
            if (token["costType"] != null)
            {
                switch ((string)token["costType"])
                {
                    case "MP": costType = CostType.MP; break;
                    case "LB": costType = CostType.LB; break;
                    case "EP": costType = CostType.EP; break;
                }
            }
            int id = 0;
            JArray a = (JArray)token["components"];
            if(a != null)
            {
                JToken componentToken = a.First;
                while (componentToken != null)
                {
                    AbilityComponent newComp = createComponent(componentToken, name, " " + id.ToString());
                    if(newComp != null)
                    {
                        components.Add(newComp);
                        id++;
                    }
                    componentToken = componentToken.Next;
                }
            }
            Ability newAbility = new Ability(name, displayName, abilityType, description, condition, requirement, cooldown, onFirstTurn, repeat, repeatRand, cost, costType, components);
            return newAbility;
        }

        private static AbilityComponent createComponent(JToken token, string name, string id)
        {
            /*if(token == null)
            {
                if (GameEngine.debug) Console.WriteLine("Token cannot be read properly!");
                return null;
            }*/

            AbilityComponent component = null;
            double effect = 0.0;
            double scale = 0.0;
            double limitScale = 0.0;
            double ignore = 0.0;
            int sacrifice = 0;
            int duration = 0;
            int charge = 0;
            int castTime = 0;
            bool dispelable = true;

            if (token["castTime"] != null) castTime = (int)token["castTime"];
            if (token["sacrifice"] != null) sacrifice = (int)token["sacrifice"];
            if (token["effect"] != null) effect = (double)token["effect"];
            if (token["scale"] != null) scale = (double)token["scale"];
            if (token["limitScale"] != null) limitScale = (double)token["limitScale"];

            TargetType targetType = getTargetType((string)token["targetType"]);
            List<ScalingType> scalingTypes = getScalingTypes((JArray)token["scalingTypes"]);
            List<ElementalType> elementalTypes = getElementalTypes((JArray)token["elementalTypes"]);

            //Gets Frame Data
            JArray fData = (JArray)token["frameData"];
            List<int> frameData = new List<int>();
            if (fData != null)
            {
                JToken frameToken = fData.First;
                while (frameToken != null)
                {
                    frameData.Add((int)frameToken);
                    frameToken = frameToken.Next;
                }
            }

            List<string> abilities = new List<string>();
            List<ParameterType> parameterTypes;
            List<StatusType> statusTypes;
            AttackType attackType;
            DamageType damageType;
            //Gets Action Type
            ActionType actionType;
            switch ((string)token["actionType"])
            {
                case "OFFENSIVE_NEW": break;
                case "DEFENSIVE_NEW": break;
                case "BUFFING_NEW": break;
                case "DEBUFFING_NEW": break;
                case "OFFENSIVE":
                    actionType = ActionType.OFFENSIVE;
                    attackType = getAttackType((string)token["attackType"]);
                    damageType = getDamageType((string)token["damageType"]);
                    DrainType drainType = getDrainType((string)token["drainType"]);

                    if (token["ignore"] != null) ignore = (double)token["ignore"];
                    bool duplicate = false;
                    if (token["duplicate"] != null) duplicate = (bool)token["duplicate"];
                    bool jump = false;
                    if (token["jump"] != null) jump = (bool)token["jump"];

                    //Gets Weight
                    JArray wData = (JArray)token["weight"];
                    List<double> weight = new List<double>();
                    if (wData == null) weight.Add(1.0);
                    else
                    {
                        JToken weightToken = wData.First;
                        while (weightToken != null)
                        {
                            weight.Add((double)weightToken);
                            weightToken = weightToken.Next;
                        }
                    }
                    if (GameEngine.debug && weight.Count != frameData.Count)
                        Console.WriteLine("Weight and framedata sizes are uneven!");

                    component = new OffensiveComponent(name + id, duplicate, jump, castTime, sacrifice, effect, scale, limitScale, ignore, actionType, targetType, frameData, scalingTypes, attackType,
                        damageType, drainType, weight, elementalTypes);
                    break;
                case "DEFENSIVE":
                    actionType = ActionType.DEFENSIVE;
                    DefensiveType defensiveType = getDefensiveType((string)token["defensiveType"]);
                    parameterTypes = getParameterTypes((JArray)token["parameterTypes"]);
                    statusTypes = getStatusTypes((JArray)token["statusTypes"]);

                    component = new DefensiveComponent(name + id, castTime, sacrifice, effect, scale, limitScale, actionType, targetType, frameData, defensiveType, parameterTypes, statusTypes, scalingTypes,
                        elementalTypes);
                    break;
                case "BUFFING":
                    actionType = ActionType.BUFFING;
                    bool multiCover = false;
                    if (token["multiCover"] != null) multiCover = (bool)token["multiCover"];
                    if (token["dispelable"] != null) dispelable = (bool)token["dispelable"];
                    if (token["duration"] != null) duration = (int)token["duration"];
                    if (token["charge"] != null) charge = (int)token["charge"];

                    List<string> unlocks = new List<string>();
                    JArray uData = (JArray)token["unlocks"];
                    if (uData != null)
                    {
                        JToken uToken = uData.First;
                        while (uToken != null)
                        {
                            unlocks.Add((string)uToken);
                            uToken = uToken.Next;
                        }
                    }
                    uData = (JArray)token["abilitiesList"];
                    if (uData != null)
                    {
                        JToken uToken = uData.First;
                        while (uToken != null)
                        {
                            abilities.Add((string)uToken);
                            uToken = uToken.Next;
                        }
                    }

                    BuffType buffType = getBuffType((string)token["buffType"]);
                    parameterTypes = getParameterTypes((JArray)token["parameterTypes"]);
                    statusTypes = getStatusTypes((JArray)token["statusTypes"]);
                    List<MitigationType> mitigationTypes = getMitigationTypes((JArray)token["mitigationTypes"]);
                    List<AIManipulationType> aiManipulationTypes = getAIManipulationTypes((JArray)token["aiManipulationTypes"]);
                    List<Race> physicalKillers = getRace((JArray)token["killerTypes"]);
                    List<Race> magicalKillers = getRace((JArray)token["killerTypes"]);

                    string finalName = name + id;
                    if (token["buffName"] != null) finalName = (string)token["buffName"] + id;

                    component = new BuffComponent(finalName, castTime, sacrifice, effect, scale, limitScale, actionType, targetType, frameData, dispelable, multiCover, duration, charge, unlocks, abilities,
                        buffType, physicalKillers, magicalKillers, mitigationTypes, parameterTypes, statusTypes, aiManipulationTypes, elementalTypes, scalingTypes);
                    break;
                case "DEBUFFING":
                    actionType = ActionType.DEBUFFING;

                    bool hex = false;
                    if (token["hex"] != null) hex = (bool)token["hex"];
                    if (token["dispelable"] != null) dispelable = (bool)token["dispelable"];
                    if (token["duration"] != null) duration = (int)token["duration"];
                    if (token["ignore"] != null) ignore = (double)token["ignore"];

                    DebuffType debuffType = getDebuffType((string)token["debuffType"]);
                    attackType = getAttackType((string)token["attackType"]);
                    damageType = getDamageType((string)token["damageType"]);
                    parameterTypes = getParameterTypes((JArray)token["parameterTypes"]);
                    statusTypes = getStatusTypes((JArray)token["statusTypes"]);

                    component = new DebuffComponent(name + id, castTime, sacrifice, effect, scale, limitScale, ignore, actionType, targetType, frameData, dispelable, hex, duration, debuffType, attackType,
                        damageType, parameterTypes, statusTypes, elementalTypes, scalingTypes);
                    break;
                case "COMBO":
                    actionType = ActionType.COMBO;
                    AbilityType comboCondition = getAbilityType((string)token["condition"]);
                    List<String> abilitiesList = new List<String>();
                    JArray cData = (JArray)token["abilitiesList"];
                    if (cData != null)
                    {
                        JToken cToken = cData.First;
                        while (cToken != null)
                        {
                            abilitiesList.Add((string)cToken);
                            cToken = cToken.Next;
                        }
                    }
                    component = new ComboComponent(name + id, effect, scale, limitScale, actionType, targetType, frameData, comboCondition, abilitiesList);
                    break;
                case "RANDOM":
                    actionType = ActionType.RANDOM;
                    List<int> odds = new List<int>();
                    JArray rData = (JArray)token["odds"];
                    JArray rData2 = (JArray)token["abilities"];
                    if (rData != null && rData2 != null)
                    {
                        if (GameEngine.debug && rData.Count != rData2.Count)
                            Console.WriteLine("Random Component abilities and random list are uneven!");
                        JToken uToken = rData.First;
                        JToken uToken2 = rData2.First;
                        while (uToken != null && uToken2 != null)
                        {
                            odds.Add((int)uToken);
                            abilities.Add((string)uToken2);
                            uToken = uToken.Next;
                            uToken2 = uToken2.Next;
                        }
                    }
                    component = new RandomComponent(name + id, effect, scale, limitScale, actionType, targetType, frameData, odds, abilities);
                    break;
                default: if(GameEngine.debug) Console.WriteLine("ERROR: ActionType does not have correct input!"); break;
            }
            return component;
        }

        private static Passive createPassive(JToken token)
        {
            /*if(token == null)
            {
                Console.WriteLine("ERROR: token cannot be read properly!");
                return null;
            }*/

            List<string> doubleStatsName = new List<string>();
            List<string> doubleStatsCondition = new List<string>();
            List<double> doubleStatsValue = new List<double>();
            List<string> intStatsName = new List<string>();
            List<string> intStatsCondition = new List<string>();
            List<int> intStatsValue = new List<int>();

            string name = (string)token["name"];
            string displayName = "";
            if (token["displayName"] == null) displayName = name;
            else displayName = (string)token["displayName"];
            string description = "";
            if (token["description"] != null) description = (string)token["description"];
            string condition = "";
            if (token["conditions"] != null && token["conditions"].First != null) condition = (string)((JArray)token["conditions"]).First;
            string requirement = "";
            if (token["requirements"] != null && token["requirements"].First != null) requirement = (string)((JArray)token["requirements"]).First;

            setNameConditionAndIntValue(name, "intStatNames", "intStatValues", "intStatCondition", token, intStatsName, intStatsCondition, intStatsValue);
            setNameConditionAndDoubleValue(name, "doubleStatNames", "doubleStatValues", "doubleStatCondition", token, doubleStatsName, doubleStatsCondition, doubleStatsValue);
            
            Passive newPassive = new Passive(name, displayName, description, condition, requirement, doubleStatsName, doubleStatsCondition, doubleStatsValue, intStatsName, intStatsCondition, intStatsValue);
            return newPassive;
        }

        private static ItemData createItem(JToken token)
        {
            string name = (string)token["name"];
            string displayName = "";
            if (token["displayName"] == null) displayName = name;
            else displayName = (string)token["displayName"];
            string description = "";
            if (token["description"] != null) description = (string)token["description"];
            int gilValue = -1;
            if (token["gilValue"] != null) gilValue = (int)token["gilValue"];
            int gilCost = -1;
            if (token["gilCost"] != null) gilCost = (int)token["gilCost"];
            Ability ability = null;
            if (token["ability"] != null) ability = abilitiesData[(string)token["ability"]];

            ItemData newItem = new ItemData(name, displayName, description, gilValue, gilCost, ability);
            return newItem;
        }

        private static EquipmentData createEquipment(JToken token)
        {
            Dictionary<string, double> doubleStats = new Dictionary<string, double>();
            Dictionary<string, int> intStats = new Dictionary<string, int>();
            Dictionary<string, bool> boolStats = new Dictionary<string, bool>();
            List<Ability> abilities = new List<Ability>();
            List<Passive> passives = new List<Passive>();

            string name = (string)token["name"];
            string displayName = "";
            if (token["displayName"] == null) displayName = name;
            else displayName = (string)token["displayName"];
            string description = "";
            if (token["description"] != null) description = (string)token["description"];
            string condition = "";
            if (token["condition"] != null) condition = (string)token["condition"];
            string requirement = "";
            if (token["requirement"] != null) requirement = (string)token["requirement"];
            int gilValue = -1;
            if (token["gilValue"] != null) gilValue = (int)token["gilValue"];
            int gilCost = -1;
            if (token["gilCost"] != null) gilCost = (int)token["gilCost"];

            List<ElementalType> elementalTypes = getElementalTypes((JArray)token["elementalTypes"]);
            EquipmentType equipType = EquipmentType.NONE;
            switch ((string)token["type"])
            {
                case "SHORTSWORD": equipType = EquipmentType.SHORTSWORD; break;
                case "SWORD": equipType = EquipmentType.SWORD; break;
                case "GREATSWORD": equipType = EquipmentType.GREATSWORD; break;
                case "KATANA": equipType = EquipmentType.KATANA; break;
                case "FIST": equipType = EquipmentType.FIST; break;
                case "HAMMER": equipType = EquipmentType.HAMMER; break;
                case "AXE": equipType = EquipmentType.AXE; break;
                case "MACE": equipType = EquipmentType.MACE; break;
                case "WHIP": equipType = EquipmentType.WHIP; break;
                case "GUN": equipType = EquipmentType.GUN; break;
                case "SPEAR": equipType = EquipmentType.SPEAR; break;
                case "BOW": equipType = EquipmentType.BOW; break;
                case "THROWING": equipType = EquipmentType.THROWING; break;
                case "STAFF": equipType = EquipmentType.STAFF; break;
                case "ROD": equipType = EquipmentType.ROD; break;
                case "HARP": equipType = EquipmentType.HARP; break;
                case "LSHIELD": equipType = EquipmentType.LSHIELD; break;
                case "HSHIELD": equipType = EquipmentType.HSHIELD; break;
                case "HAT": equipType = EquipmentType.HAT; break;
                case "HELMET": equipType = EquipmentType.HELMET; break;
                case "ROBES": equipType = EquipmentType.ROBES; break;
                case "CLOTHES": equipType = EquipmentType.CLOTHES; break;
                case "LIGHT_ARMOR": equipType = EquipmentType.LIGHT_ARMOR; break;
                case "HEAVY_ARMOR": equipType = EquipmentType.HEAVY_ARMOR; break;
                case "ACCESSORY": equipType = EquipmentType.ACCESSORY; break;
            }

            setIntDictionary(name, "intStatNames", "intStatValues", token, intStats);
            setDoubleDictionary(name, "doubleStatNames", "doubleStatValues", token, doubleStats);
            setBoolDictionary(name, "boolStatNames", "boolStatValues", token, boolStats);
            setAbilities(true, name, token, abilities, null);
            setPassives(true, name, token, passives, null);

            EquipmentData newEquipData = new EquipmentData(name, displayName, description, gilValue, gilCost, elementalTypes, equipType, passives, abilities, intStats, doubleStats, boolStats);
            return newEquipData;
        }

        private static MateriaData createMateria(JToken token)
        {
            string name = (string)token["name"];
            string displayName = "";
            if (token["displayName"] == null) displayName = name;
            else displayName = (string)token["displayName"];
            bool unstackable = false;
            if (token["unstackable"] != null) unstackable = (bool)token["unstackable"];
            int gilValue = -1;
            if (token["gilValue"] != null) gilValue = (int)token["gilValue"];
            int gilCost = -1;
            if (token["gilCost"] != null) gilCost = (int)token["gilCost"];
            Ability ability = null;
            if (token["ability"] != null) ability = abilitiesData[(string)token["ability"]];
            Passive passive = null;
            if (token["passive"] != null) passive = passivesData[(string)token["passive"]];

            MateriaData newMateria = new MateriaData(name, displayName, unstackable, gilValue, gilCost, passive, ability);
            return newMateria;
        }

        private static EsperData createEsper(JToken token)
        {
            Dictionary<string, double> doubleStats = new Dictionary<string, double>();
            Dictionary<string, int> intStats = new Dictionary<string, int>();
            List<Ability> abilities = new List<Ability>();
            List<Passive> passives = new List<Passive>();

            string name = (string)token["name"];
            string displayName = "";
            if (token["displayName"] == null) displayName = name;
            else displayName = (string)token["displayName"];
            string description = "";
            if (token["description"] != null) description = (string)token["description"];

            setIntDictionary(name, "intStatNames", "intStatValues", token, intStats);
            setDoubleDictionary(name, "doubleStatNames", "doubleStatValues", token, doubleStats);
            setAbilities(true, name, token, abilities, null);
            setPassives(true, name, token, passives, null);

            EsperData newEsper = new EsperData(name, displayName, description, passives, abilities, intStats, doubleStats);
            return newEsper;
        }

        private static UnitData getUnit(JToken token)
        {
            List<Ability> abilities = new List<Ability>();
            List<int> levelRequired = new List<int>();
            List<Passive> passives = new List<Passive>();
            List<int> levelRequiredPassives = new List<int>();
            Dictionary<string, int> intStats = new Dictionary<string, int>();
            Dictionary<string, double> doubleStats = new Dictionary<string, double>();

            string name = (string)token["name"];
            string displayName = "";
            if (token["displayName"] == null) displayName = name;
            else displayName = (string)token["displayName"];
            int baseLevel = 1;
            if (token["baseLevel"] != null) baseLevel = (int)token["baseLevel"];
            int lbCost = 1;
            if (token["lbCost"] != null) lbCost = (int)token["lbCost"];
            int lbPerAttack = 0;
            if (token["lbPerAttack"] != null) lbPerAttack = (int)token["lbPerAttack"];

            List<Race> raceTypes = getRace((JArray)token["race"]);

            setIntDictionary(name, "intStatNames", "intStatValues", token, intStats);
            setDoubleDictionary(name, "doubleStatNames", "doubleStatValues", token, doubleStats);
            setAbilities(false, name, token, abilities, levelRequired);
            setPassives(false, name, token, passives, levelRequiredPassives);

            UnitData newUnitData = new UnitData(name, displayName, baseLevel, lbCost, lbPerAttack, intStats, doubleStats, abilities, levelRequired, passives, levelRequiredPassives, raceTypes);
            return newUnitData;
        }

        private static void setStringList(List<string> list, JArray array)
        {
            if(array != null)
            {
                JToken token = array.First;
                while (token != null)
                {
                    list.Add((string)token);
                    token = token.Next;
                }
            }
        }

        public static LoadoutData getLoadoutData(JToken token)
        {
            List<string> weaponPassivesA = new List<string>();
            List<string> weaponPassivesB = new List<string>();
            List<string> armor = new List<string>();
            List<string> materia = new List<string>();

            int id = (int)token["id"];
            int level = (int)token["level"];
            int lb = (int)token["lb"];
            double xp = (double)token["xp"];
            string name = (string)token["name"];
            string esperName = "";
            if (token["esperName"] != null) esperName = (string)token["esperName"];
            int esperLevel = -1;
            if (token["esperLevel"] != null) esperLevel = (int)token["esperLevel"];

            string weaponSlotA = "";
            if (token["weaponSlotA"] != null) weaponSlotA = (string)token["weaponSlotA"];
            setStringList(weaponPassivesA, (JArray)token["weaponPassivesA"]);
            string weaponSlotB = "";
            if (token["weaponSlotB"] != null) weaponSlotB = (string)token["weaponSlotB"];
            setStringList(weaponPassivesB, (JArray)token["weaponPassivesB"]);

            setStringList(armor, (JArray)token["armor"]);
            setStringList(materia, (JArray)token["materia"]);

            LoadoutData newData = new LoadoutData(id, level, lb, xp, name, esperName, esperLevel, weaponSlotA, weaponPassivesA, weaponSlotB, weaponPassivesB, armor, materia);
            return newData;
        }

        private static TownData createTown(string name, JToken token)
        {
            List<string> shopItems = new List<string>();
            Dictionary<string, string> talkData = new Dictionary<string, string>();

            string intro = "";
            if (token["intro"] != null) intro = (string)token["intro"];
            setStringList(shopItems, (JArray)token["shopItems"]);
            setStringDictionary(name, "talkTitle", "talkData", token, talkData);

            TownData newTown = new TownData(name, intro, shopItems, talkData);
            return newTown;
        }

        private static StoryData getStoryData(JToken token, int i, string path)
        {
            string description = (string)token["description"];
            string title = "";
            if (token["title"] != null) title = (string)token["title"];
            string teleport = "";
            if (token["teleport"] != null) title = (string)token["teleport"];
            string endOfStory = "";
            if (token["endOfStory"] != null) endOfStory = (string)token["endOfStory"];
            string battleName = "";
            if (token["battleName"] != null) battleName = (string)token["battleName"];
            string unlockName = "";
            if (token["unlockName"] != null) unlockName = (string)token["unlockName"];
            bool makeChoice = false;
            if (token["makeChoice"] != null) makeChoice = (bool)token["makeChoice"];
            bool savePoint = false;
            if (token["savePoint"] != null) savePoint = (bool)token["savePoint"];

            Dictionary<string, string> choices = new Dictionary<string, string>();
            Dictionary<string, int> extraData = new Dictionary<string, int>();

            setStringDictionary(path + " in " + i.ToString(), "choiceNames", "choiceValues", token, choices);
            setIntDictionary(path + " in " + i.ToString(), "extraNames", "extraValues", token, extraData);

            StoryData newData = new StoryData(title, description, teleport, endOfStory, battleName, unlockName, makeChoice, savePoint, choices, extraData);
            return newData;
        }

        private static EnemySpawnData getEnemySpawnData(JToken token, int i, string path)
        {
			string announcement = "";
            if (token["announcement"] != null) announcement = (string)token["announcement"];
            string special = "";
            if (token["special"] != null) special = (string)token["special"];
            string endOfBattle = "";
            if (token["endOfBattle"] != null) endOfBattle = (string)token["endOfBattle"];
            string storyName = "";
            if (token["storyName"] != null) storyName = (string)token["storyName"];
            bool bossBattle = false;
            if (token["bossBattle"] != null) bossBattle = (bool)token["bossBattle"];
            bool onEnemyTurnFirst = false;
            if (token["onEnemyTurnFirst"] != null) onEnemyTurnFirst = (bool)token["onEnemyTurnFirst"];

            List<string> enemies = new List<string>();
            List<string> enemyTypes = new List<string>();
            List<int> enemyActions = new List<int>();
            List<int> enemyGils = new List<int>();
            List<double> enemyExps = new List<double>();

            JArray statNames = (JArray)token["enemies"];
            JArray statTypes = (JArray)token["enemyTypes"];
            JArray statValues = (JArray)token["enemyActions"];
            JArray statXps = (JArray)token["enemyXp"];
            JArray statGils = (JArray)token["enemyGil"];

            if (statNames != null && statTypes != null && statValues != null && statXps != null && statGils != null)
            {
                if (GameEngine.debug && (statNames.Count != statTypes.Count || statNames.Count != statValues.Count || statNames.Count != statXps.Count || statNames.Count != statGils.Count))
                    Console.WriteLine(path + " in " + i.ToString() + " - All enemy spawn details don't have matching value counts!");
                
                JToken statName = statNames.First;
                JToken statType = statTypes.First;
                JToken statValue = statValues.First;
                JToken statXp = statXps.First;
                JToken statGil = statGils.First;
                
                while (statName != null && statType != null && statValue != null && statXp != null && statGil != null)
                {
                    enemies.Add((string)statName);
                    enemyTypes.Add((string)statType);
                    enemyActions.Add((int)statValue);
                    enemyExps.Add((double)statXp);
                    enemyGils.Add((int)statGil);
                    statName = statName.Next;
                    statType = statType.Next;
                    statValue = statValue.Next;
                    statXp = statXp.Next;
                    statGil = statGil.Next;
                }
            }

            EnemySpawnData newData = new EnemySpawnData(bossBattle, onEnemyTurnFirst, announcement, special, endOfBattle, storyName, enemies, enemyTypes, enemyActions, enemyExps, enemyGils);
            return newData;
        }

        private static void createStringAndIntList(JProperty p, List<string> stringList, List<int> intList)
        {
            JArray a = (JArray)p.First;
            p = (JProperty)p.Next;
            JArray a2 = (JArray)p.First;
            JToken token = a.First;
            JToken token2 = a2.First;
            if (GameEngine.debug && a.Count != a2.Count)
                Console.WriteLine("Item token counts don't match properly!");
            while (token != null && token2 != null)
            {
                stringList.Add((string)token);
                intList.Add((int)token2);
                token = token.Next;
                token2 = token2.Next;
            }
        }

        private static void createIntList(JProperty p, List<int> intList)
        {
            JArray a = (JArray)p.First;
            JToken token = a.First;
            while (token != null)
            {
                intList.Add((int)token);
                token = token.Next;
            }
        }

        private static void createStringList(JProperty p, List<string> stringList)
        {
            JArray a = (JArray)p.First;
            JToken token = a.First;
            while (token != null)
            {
                stringList.Add((string)token);
                token = token.Next;
            }
        }

        private static void createLoadoutList(JProperty p, List<LoadoutData> loadoutList)
        {
            JArray a = (JArray)p.First;
            JToken token = a.First;
            while (token != null)
            {
                loadoutList.Add(getLoadoutData(token));
                token = token.Next;
            }
        }

        private static SaveData createSaveData(JToken main)
        {
            List<LoadoutData> visionData = new List<LoadoutData>();
            List<LoadoutData> partyData = new List<LoadoutData>();
            List<string> itemsData = new List<string>();
            List<int> itemsQtyData = new List<int>();
            List<string> equipsData = new List<string>();
            List<int> equipsQtyData = new List<int>();
            List<string> equipsDataUnsellable = new List<string>();
            List<int> equipsQtyDataUnsellable = new List<int>();
            List<string> materiasData = new List<string>();
            List<int> materiasQtyData = new List<int>();
            List<string> materiasDataUnsellable = new List<string>();
            List<int> materiasQtyDataUnsellable = new List<int>();
            List<string> esperNamesData = new List<string>();
            List<int> esperLevelsData = new List<int>();
            List<string> unlockedContent = new List<string>();

            JProperty p = (JProperty)main.First;
            JToken token;

            //Visions Loadout data
            createLoadoutList(p, visionData);
            //Units Loadout data
            p = (JProperty)p.Next;
            createLoadoutList(p, partyData);
            //Items loadout data
            p = (JProperty)p.Next;
            createStringAndIntList(p, itemsData, itemsQtyData);
            //Equipment loadout data
            p = (JProperty)p.Next;
            p = (JProperty)p.Next;
            createStringAndIntList(p, equipsData, equipsQtyData);
            //Equipment Unsellable loadout data
            p = (JProperty)p.Next;
            p = (JProperty)p.Next;
            createStringAndIntList(p, equipsDataUnsellable, equipsQtyDataUnsellable);
            //Materia loadout data
            p = (JProperty)p.Next;
            p = (JProperty)p.Next;
            createStringAndIntList(p, materiasData, materiasQtyData);
            //Materia Unsellable loadout data
            p = (JProperty)p.Next;
            p = (JProperty)p.Next;
            createStringAndIntList(p, materiasDataUnsellable, materiasQtyDataUnsellable);
            //Esper list
            p = (JProperty)p.Next;
            p = (JProperty)p.Next;
            createStringAndIntList(p, esperNamesData, esperLevelsData);
            //Unlocked list
            p = (JProperty)p.Next;
            p = (JProperty)p.Next;
            createStringList(p, unlockedContent);

            token = p.Next;
            int gil = (int)token;
            token = token.Next;
            string assetName = (string)token;
            token = token.Next;
            string state = (string)token;
            token = token.Next;
            string selectedVision = (string)token;

            SaveData newPreset = new SaveData(visionData, partyData, itemsData, itemsQtyData, equipsData, equipsQtyData, equipsDataUnsellable, equipsQtyDataUnsellable, materiasData, materiasQtyData,
                materiasDataUnsellable, materiasQtyDataUnsellable, esperNamesData, esperLevelsData, unlockedContent, gil, assetName, state, selectedVision);
            return newPreset;
        }
    }
}
