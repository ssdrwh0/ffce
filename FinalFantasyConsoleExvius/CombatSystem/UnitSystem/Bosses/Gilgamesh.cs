﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    public class Gilgamesh : Enemy
    {
        private List<int> triggeredAbilities;
        private bool renewShield;
        private int elementalHitsTaken;

        public Gilgamesh(string name, int lbLevel, int id, int actions, UnitData unitData)
            : base(name, lbLevel, id, actions, unitData)
        {
            triggeredAbilities = new List<int>();
        }

        public override void resetStats(bool startOfMission)
        {
            base.resetStats(startOfMission);
            renewShield = true;
            elementalHitsTaken = 0;
        }

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);
            if (unitData.Abilities.Count < 1) return false;

            actions = 40;
            int i = 0;
            string error = "";
            double threshold = HP / MaxHP;

            triggeredAbilities.Clear();
            if (CombatMode.enemyTurn == 1)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Barrier"], characters, allies);
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Wall"], characters, allies);
                renewShield = false;
                return true;
            }
            if (threshold < 0.8 && thresholds < 1)
            {
                prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Bushido - Freedom I"], characters, allies);
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["The reason was reversed"], characters, allies);
                thresholds++;
                renewShield = true;
                return true;
            }
            if (threshold < 0.6 && thresholds < 2)
            {
                prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Bushido - Freedom II"], characters, allies);
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["The reason was reversed"], characters, allies);
                thresholds++;
                renewShield = true;
                return true;
            }
            if (threshold < 0.4 && thresholds < 3)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Bushido - Freedom III"], characters, allies);
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["The reason was reversed"], characters, allies);
                thresholds++;
                renewShield = true;
                return true;
            }
            if (threshold < 0.2 && thresholds < 4)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Bushido - Freedom III"], characters, allies);
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["The reason was reversed"], characters, allies);
                thresholds++;
                renewShield = true;
                return true;
            }
            triggeredAbilities.Add(0);
            //triggeredAbilities.Add(1);
            //triggeredAbilities.Add(2);
            triggeredAbilities.Add(3);
            if (elementalItemHitters.Count > 0 && ++elementalHitsTaken >= 3)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["BREAK"], characters, allies);
                elementalHitsTaken = 0;
                return true;
            }
            if (renewShield)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Barrier"], characters, allies);
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Wall"], characters, allies);
                renewShield = false;
            }
            if (CombatMode.enemyTurn % 3 == 1)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["I got a new weapon"], characters, allies);
                prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Genji's Blade"], characters, allies);
                if (elementalItemHitters.Count < 1) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Genji's Helm"], characters, allies);
            }
            if (CombatMode.enemyTurn % 6 == 2 && CombatMode.enemyTurn > 6)
            {
                prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Genji's Armor"], characters, allies);
                prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Genji's Shield"], characters, allies);
            }
            if (CombatMode.enemyTurn % 3 == 0)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["I got a new weapon"], characters, allies);
                prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Genji's Bow"], characters, allies);
            }
            if (threshold > 0.8)
            {
                //Seal
                if (fireHitters.Count < 1) triggeredAbilities.Add(4); //4 "Muramasa"
                if (waterHitters.Count < 1) triggeredAbilities.Add(5); //5 "Masamune"
                if (windHitters.Count < 1) triggeredAbilities.Add(6);  //6 "Tornado Strike"
                if (earthHitters.Count < 1) triggeredAbilities.Add(7); //7 "Zantetsuken Gilgamesh"
                if (lightHitters.Count < 1) triggeredAbilities.Add(8); //8 "Excalibur"
            }
            else if (threshold > 0.6)
            {
                //Trigger
                if (fireHitters.Count > 0) triggeredAbilities.Add(12); //12 "True Muramasa"
                if (waterHitters.Count > 0) triggeredAbilities.Add(13); //13 "True Masamune"
                if (windHitters.Count > 0) triggeredAbilities.Add(14);  //14 "True Tornado Strike"
                if (earthHitters.Count > 0) triggeredAbilities.Add(15); //15 "True Zantetsuken Gilgamesh"
                if (lightHitters.Count > 0) triggeredAbilities.Add(16); //16 "Excalibur II"
            }
            else if (threshold > 0.4)
            {
                //Seal
                if (fireHitters.Count < 1) triggeredAbilities.Add(4); //4 "Muramasa"
                if (waterHitters.Count < 1) triggeredAbilities.Add(5); //5 "Masamune"
                if (windHitters.Count < 1) triggeredAbilities.Add(6);  //6 "Tornado Strike"
                if (earthHitters.Count < 1) triggeredAbilities.Add(7); //7 "Zantetsuken Gilgamesh"
                if (lightHitters.Count < 1) triggeredAbilities.Add(8); //8 "Excalibur"
                if (iceHitters.Count < 1) triggeredAbilities.Add(9); //9 "Ice Strike"
                if (thunderHitters.Count < 1) triggeredAbilities.Add(10); //10 "Shock Arts"
                if (darkHitters.Count < 1) triggeredAbilities.Add(11); //11 "Apocalypse"
            }
            else if (threshold > 0.2)
            {
                //Trigger
                if (fireHitters.Count > 0) triggeredAbilities.Add(12); //12 "True Muramasa"
                if (waterHitters.Count > 0) triggeredAbilities.Add(13); //13 "True Masamune"
                if (windHitters.Count > 0) triggeredAbilities.Add(14);  //14 "True Tornado Strike"
                if (earthHitters.Count > 0) triggeredAbilities.Add(15); //15 "True Zantetsuken Gilgamesh"
                if (lightHitters.Count > 0) triggeredAbilities.Add(16); //16 "Excalibur II"
                if (iceHitters.Count > 0) triggeredAbilities.Add(9); //9 "Ice Strike"
                if (thunderHitters.Count > 0) triggeredAbilities.Add(10); //10 "Shock Arts"
                if (darkHitters.Count > 0) triggeredAbilities.Add(11); //11 "Apocalypse"
            }
            else
            {
                //Seal
                if (fireHitters.Count < 1) triggeredAbilities.Add(12); //12 "True Muramasa"
                if (waterHitters.Count < 1) triggeredAbilities.Add(13); //13 "True Masamune"
                if (windHitters.Count < 1) triggeredAbilities.Add(14);  //14 "True Tornado Strike"
                if (earthHitters.Count < 1) triggeredAbilities.Add(15); //15 "True Zantetsuken Gilgamesh"
                if (lightHitters.Count < 1) triggeredAbilities.Add(16); //16 "Excalibur II"
                if (iceHitters.Count < 1) triggeredAbilities.Add(9); //9 "Ice Strike"
                if (thunderHitters.Count < 1) triggeredAbilities.Add(10); //10 "Shock Arts"
                if (darkHitters.Count < 1) triggeredAbilities.Add(11); //11 "Apocalypse"
            }
            while (i < actions)
            {
                int randomAbility = triggeredAbilities[AssetLoader.random.Next(triggeredAbilities.Count)];
                switch (randomAbility)
                {
                    case 1: prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Tsubame Gaeshi"], characters, allies); break;
                    case 2: prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Excalipher"], characters, allies); break;
                    case 3: prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["X Slash"], characters, allies); break;
                    case 4: prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Muramasa"], characters, allies); break;
                    case 5: prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Masamune"], characters, allies); break;
                    case 6: prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Whirlwind Blade"], characters, allies); break;
                    case 7: prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Zantetsuken Gilgamesh"], characters, allies); break;
                    case 8: prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Excalibur"], characters, allies); break;
                    case 9: prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Ice Blade"], characters, allies); break;
                    case 10: prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Electrocute"], characters, allies); break;
                    case 11: prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Apocalypse"], characters, allies); break;
                    case 12: prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["True Muramasa"], characters, allies); break;
                    case 13: prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["True Masamune"], characters, allies); break;
                    case 14: prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["True Whirlwind Blade"], characters, allies); break;
                    case 15: prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["True Zantetsuken Gilgamesh"], characters, allies); break;
                    case 16: prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Excalibur II"], characters, allies); break;
                    default: prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies); break;
                }
            }
            return true;
        }
    }
}
