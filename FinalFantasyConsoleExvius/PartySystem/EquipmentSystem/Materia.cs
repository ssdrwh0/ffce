﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    public class Materia
    {
        private MateriaData data;
        private bool used;

        public string Name { get { return data.Name; } }
        public string DisplayName { get { return data.DisplayName; } }
        public bool Unstackable { get { return data.Unstackable; } }
        public bool Used { get { return used; } }
        public Passive PassiveEffect { get { return data.PassiveEffect; } }
        public Ability AbilityEffect { get { return data.AbilityEffect; } }
        public Materia(MateriaData data, bool used)
        {
            this.data = data;
            this.used = used;
        }
    }

    public class MateriaData
    {
        private bool unstackable;
        private int gilValue;
        private int gilCost;
        private string name;
        private string displayName;
        private Passive passive;
        private Ability ability;

        public bool Unstackable { get { return unstackable; } }
        public int GilValue { get { return gilValue; } }
        public int GilCost { get { return gilCost; } }
        public string Name { get { return name; } }
        public string DisplayName { get { return displayName; } }
        public Passive PassiveEffect { get { return passive; } }
        public Ability AbilityEffect { get { return ability; } }
        public MateriaData(string name, string displayName, bool unstackable, int gilValue, int gilCost, Passive passive, Ability ability)
        {
            this.name = name;
            this.displayName = displayName;
            this.unstackable = unstackable;
            this.gilValue = gilValue;
            this.gilCost = gilCost;
            this.passive = passive;
            this.ability = ability;
        }
    }
}
