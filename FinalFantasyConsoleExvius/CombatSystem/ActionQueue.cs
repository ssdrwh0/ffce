﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    public enum QueueType{
        ATTACK,
        PROTECT,
        BUFF,
        DEBUFF
    };

    public class AbilityCommand
    {
        public ActionType type;
        public AbilityType abilityType;
        public AttackType attackType;
        public bool multiCast;
        public bool dualWield;
        public string abilityName;
        public int id;
        public int target;

        public AbilityCommand()
        {

        }
    }

    public class ActionQueue
    {
        protected QueueType type;

        public AbilityType abilityType;
        public string abilityName;
        public int id;
        public int target;
        public int delay;
        public int checkpoint;
        public int currentFrame;
        public int currentCastDelay;
        //public int globalFrame;
        public double limitBurst;
        public List<int> frameData;
        public bool isPlayer;
        public Unit self;

        public QueueType Type { get { return type; } }

        public ActionQueue()
        {

        }
    }

    public class AttackQueue : ActionQueue
    {
        public int lbCrystal;
        public bool dualWieldPlus;
        public int accuracy;
        public List<double> calculatedDamageAllies;
        public List<double> calculatedDamage;
        public double drainScale;
        public List<double> weight;
        public List<ElementalType> elements;
        public AttackType attackType;
        public TargetType targetType;
        public DrainType drainType;

        public AttackQueue() : base()
        {
            type = QueueType.ATTACK;
            calculatedDamage = new List<double>();
            calculatedDamageAllies = new List<double>();
        }
    }

    public class ProtectQueue : ActionQueue
    {
        public double healEffect;
        public double limitScale;
        public TargetType targetType;
        public DefensiveType defensiveType;
        public List<ParameterType> paramTypes;
        public List<StatusType> statusTypes;
        public List<ElementalType> elementalTypes;

        public ProtectQueue() : base()
        {
            type = QueueType.PROTECT;
        }
    }

    public class BuffQueue : ActionQueue
    {
        public double regenEffect;
        public BuffComponent buff;
        public BuffQueue() : base()
        {
            type = QueueType.BUFF;
        }
    }

    public class DebuffQueue : ActionQueue
    {
        public bool hex;
        public double atkDamage;
        public double magDamage;
        public double weaponMod;
        public double skillMod;
        public double levelCorrection;
        public double finalMod;
        public double ignore;
        public List<ElementalType> elements;
        public List<double> physicalKillers;
        public List<double> magicalKillers;
        public AttackType attackType;
        public DamageType damageType;
        public DebuffComponent debuff;
        public DebuffQueue() : base()
        {
            type = QueueType.DEBUFF;
        }
    }
}
