﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    public enum EquipmentType
    {
        SHORTSWORD,
        SWORD,
        GREATSWORD,
        KATANA,
        FIST,
        HAMMER,
        AXE,
        MACE,
        WHIP,
        GUN,
        BOW,
        SPEAR,
        THROWING,
        STAFF,
        ROD,
        HARP,
        LSHIELD,
        HSHIELD,
        HAT,
        HELMET,
        ROBES,
        CLOTHES,
        LIGHT_ARMOR,
        HEAVY_ARMOR,
        ACCESSORY,
        NONE
    }

    public class Equipment
    {
        protected EquipmentData equipData;
        protected bool used;
        public EquipmentData EquipData { get { return equipData; } }
        public bool Used { get { return used; } }
        public Equipment(EquipmentData equipData, bool used)
        {
            this.equipData = equipData;
            this.used = used;
        }
    }

    public class Weapon : Equipment
    {
        private List<Passive> passives;
        public IList<Passive> EquipPassives { get { return passives.AsReadOnly(); } }
        public Weapon(EquipmentData equipData, List<Passive> passives, bool used): base(equipData, used)
        {
            this.passives = passives;
        }
    }

    public class EquipmentData
    {
        #region Attributes
        //Equipment name
        protected string name;
        private string displayName;
        private string description;
        private int gilValue;
        private int gilCost;
        private EquipmentType equipType;
        private List<ElementalType> elementalTypes;
        private List<Passive> passives;
        private List<Ability> abilities;
        private Dictionary<string, double> doubleStats;
        private Dictionary<string, int> intStats;
        private Dictionary<string, bool> boolStats;
        #endregion

        #region Get Attributes
        //Unit name
        public string Name { get { return name; } }
        public string DisplayName { get { return displayName; } }
        public string Description { get { return description; } }
        public int GilValue { get { return gilValue; } }
        public int GilCost { get { return gilCost; } }
        public EquipmentType EquipType { get { return equipType; } }
        public IList<Passive> EquipPassives { get { return passives.AsReadOnly(); } }
        public IList<Ability> EquipAbilities { get { return abilities.AsReadOnly(); } }
        public IList<ElementalType> ElementalTypes { get { return elementalTypes.AsReadOnly(); } }

        public bool containsIntStat(string name)
        {
            foreach (string target in intStats.Keys)
                if (target.Equals(name))
                    return true;
            return false;
        }
        public bool containsDoubleStat(string name)
        {
            foreach (string target in doubleStats.Keys)
                if (target.Equals(name))
                    return true;
            return false;
        }
        public bool containsBoolStat(string name)
        {
            foreach (string target in boolStats.Keys)
                if (target.Equals(name))
                    return true;
            return false;
        }

        //Base Unit Attributes
        public double getDoubleStat(string name)
        {
            if (doubleStats.ContainsKey(name))
                return doubleStats[name];
            return 0.0;
        }
        public int getIntStat(string name)
        {
            if (intStats.ContainsKey(name))
                return intStats[name];
            return 0;
        }
        public bool getBoolStat(string name)
        {
            if (boolStats.ContainsKey(name))
                return boolStats[name];
            return false;
        }
        #endregion

        public EquipmentData(string name, string displayName, string description, int gilValue, int gilCost, List<ElementalType> elementalTypes, EquipmentType equipType, List<Passive> passives,
            List<Ability> abilities, Dictionary<string, int> intStats, Dictionary<string, double> doubleStats, Dictionary<string, bool> boolStats)
        {
            this.name = name;
            this.displayName = displayName;
            this.gilValue = gilValue;
            this.gilCost = gilCost;
            this.description = description;
            this.passives = passives;
            this.abilities = abilities;
            this.equipType = equipType;
            this.elementalTypes = elementalTypes;
            this.intStats = intStats;
            this.doubleStats = doubleStats;
            this.boolStats = boolStats;
        }
    }
}
