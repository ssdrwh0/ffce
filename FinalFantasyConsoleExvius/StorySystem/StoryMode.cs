﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    public class StoryMode
    {
        public static StoryMode storyMode;

        public static string battleName;
        public static string oldBattleName;
        public static Dictionary<string, int> extraScores = new Dictionary<string,int>();

        private List<StoryData> dataCollection;
        private StoryData data;
	    private int id;

        public StoryMode()
        {
		    
	    }

        public void beginScene(List<StoryData> collection, string startingKey)
        {
            oldBattleName = startingKey;
            battleName = startingKey;
            dataCollection = collection;
            id = findId(startingKey);
	    }

	    public EngineState run()
        {
            data = dataCollection[id];
            if (!data.EndOfStory.Equals("")) return getNewState(data.EndOfStory);
            Console.WriteLine(data.Description);
		    if(data.MakeChoice)
            {
                int maxSize = data.getChoiceSize();
                Console.WriteLine("");
			    for(int i = 0; i < maxSize; i++) Console.WriteLine((i+1).ToString() + " - " + data.getChoiceValue(i));
		        int target = -1;
                bool flag = true;
                while (flag)
                {
                    if (int.TryParse(Console.ReadLine(), out target))
                    {
                        if (target >= 1 && target <= maxSize) flag = false;
                        else Console.WriteLine("Invalid choice!");
                    }
                    else Console.WriteLine("Please type in an integer between 1 and " + maxSize.ToString() + "!");
                }
                int choice = target - 1;
				id = findId(data.getChoiceValue(choice));
				for(int i = 0; i < data.getExtraDataSize(); i++){
					string key = data.getExtraDataKey(i);
					int origin = 0;
					if(extraScores.ContainsKey(key)) origin = extraScores[key];
					extraScores[key] = data.getExtraDataValue(i) + origin;
				}
            }
            else
            {
                string input = Console.ReadLine().ToLower();
                do
                {
                    if (data.Teleport.Equals("")) id++;
                    else id = findId(data.Teleport);
                    data = dataCollection[id];
                }
                while (input.Equals("skip") && !(data.MakeChoice || !data.EndOfStory.Equals("")));
            }
            battleName = "";
		    return EngineState.STORY_STATE;
	    }

        private EngineState getNewState(string newState)
        {
            battleName = data.BattleName;
            if(!data.UnlockName.Equals("")) Party.unlock(AssetLoader.presetData[data.UnlockName]);
            if(data.SavePoint)
            {
                Console.WriteLine("Do you wish to save progress?");
                bool flag = true;
                while(flag)
                {
                    string input = Console.ReadLine().ToLower();
                    if(input.Equals("yes") | input.Equals("y"))
                    {
                        EngineState state;
                        switch(data.EndOfStory)
                        {
                            case "COMBAT_STATE": state = EngineState.COMBAT_STATE; break;
                            default: state = EngineState.STORY_STATE; break;
                        }
                        int target = -1;
                        if(Party.saves.Count > 0)
                        {
                            bool flag2 = true;
                            while (flag2)
                            {
                                Console.WriteLine("Use an existing save between 0 and " + (Party.saves.Count - 1).ToString() + ", or type in " + Party.saves.Count.ToString() +
                                    " to create a new save!");
                                if (int.TryParse(Console.ReadLine(), out target))
                                {
                                    if (target >= 0 && target <= Party.saves.Count) flag2 = false;
                                    else Console.WriteLine("Invalid target!");
                                }
                                else Console.WriteLine("Please type in an integer between 0 and " + Party.saves.Count.ToString() + "!");
                            }
                        }
                        Party.save(target, battleName, state);
                        Console.WriteLine("Progress has been saved!");
                        Console.ReadLine();
                        flag = false;
                    }
                    else if (input.Equals("no") | input.Equals("n")) flag = false;
                    else Console.WriteLine("Answer yes or no!");
                }
                
            }
            switch (newState)
            {
                case "MAIN_MENU": return EngineState.MAIN_MENU;
                case "STORY_STATE": return EngineState.STORY_STATE;
                case "TOWN_STATE": return EngineState.TOWN_STATE;
                case "DUNGEON_STATE": return EngineState.DUNGEON_STATE;
                case "COMBAT_STATE": return EngineState.COMBAT_STATE;
                default:
                    if(GameEngine.debug)
                    {
                        Console.WriteLine("ERROR: Mode setting not found!!!\n");
                        Console.ReadLine();
                    }
                    return EngineState.COMBAT_STATE;
            }
        }
	
	    private int findId(string key){
            id = 0;
            foreach (StoryData data in dataCollection)
            {
                if (data.Title.Equals(key)) return id;
                id++;
            }
		    return -1;
	    }
    }
}
