﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    public class Item
    {
        private ItemData data;

        public string Name { get { return data.Name; } }
        public string DisplayName { get { return data.DisplayName; } }
        public string Description { get { return data.Description; } }
        public int GilValue { get { return data.GilValue; } }
        public int GilCost { get { return data.GilCost; } }
        public Ability AbilityEffect { get { return data.AbilityEffect; } }
        public Item(ItemData data)
        {
            this.data = data;
        }
    }

    public class ItemData
    {
        private string name;
        private string displayName;
        private string description;
        private int gilValue;
        private int gilCost;
        private Ability ability;

        public string Name { get { return name; } }
        public string DisplayName { get { return displayName; } }
        public string Description { get { return description; } }
        public Ability AbilityEffect { get { return ability; } }
        public int GilValue { get { return gilValue; } }
        public int GilCost { get { return gilCost; } }

        public ItemData(string name, string displayName, string description, int gilValue, int gilCost, Ability ability)
        {
            this.name = name;
            this.displayName = displayName;
            this.description = description;
            this.gilValue = gilValue;
            this.gilCost = gilCost;
            this.ability = ability;
        }
    }
}
