﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    #region enum types
    public enum AbilityType
    {
        NORMAL_ABILITY,
        ITEM_ABILITY,
        COMBO_ABILITY,
        ESPER_ABILITY,
        LIMIT_BURST,
        BLACK_MAGIC,
        WHITE_MAGIC,
        GREEN_MAGIC,
        ALL_MAGIC
    }
    public enum ActionType
    {
        OFFENSIVE,
        DEFENSIVE,
        BUFFING,
        DEBUFFING,
        COMBO,
        RANDOM
    }
    public enum TargetType
    {
        NONE,
        SELF,
        SINGLE_ALLY,
        OTHER_ALLY,
        ALL_ALLIES,
        OTHER_ALLIES,
        SINGLE_ENEMY,
        RANDOM_ENEMY,
        ALL_ENEMIES
    }
    public enum AttackType
    {
        NONE,
        PHYSICAL_ATTACK,
        MAGICAL_ATTACK,
        HYBRID_ATTACK,
        FIXED_ATTACK
    }
    public enum DrainType
    {
        NONE,
        HP_DRAIN,
        MP_DRAIN
    }
    public enum DamageType
    {
        NONE,
        PHYSICAL_DAMAGE,
        EVOKE_DAMAGE,
        MAGICAL_DAMAGE,
        HYBRID_DAMAGE,
        FIXED_DAMAGE,
        HP_DAMAGE
    }
    public enum ScalingType
    {
        NONE,
        SCALE_BY_ATK,
        SCALE_BY_MAG,
        SCALE_BY_DEF,
        SCALE_BY_SPR
    }
    public enum DefensiveType
    {
        NONE,
        ENTRUST,
        HP_RESTORE,
        MP_RESTORE,
        LB_RESTORE,
        EP_RESTORE,
        HP_RESTORE_PERCENT,
        MP_RESTORE_PERCENT,
        LB_RESTORE_PERCENT,
        RAISE,
        BARRIER,
        REMOVE_ALL_IMPERILS,
        REMOVE_IMPERIL,
        REMOVE_ALL_PARAMETERS,
        REMOVE_PARAMETER,
        REMOVE_DEBUFF,
        REMOVE_ALL_STATUS,
        REMOVE_STATUS
    }
    public enum ElementalType
    {
        FIRE,
        ICE,
        THUNDER,
        WATER,
        WIND,
        EARTH,
        LIGHT,
        DARK
    }
    public enum DebuffType
    {
        NONE,
        REMOVE_FROM_FIGHT,
        DAMAGE_PER_TURN,
        REMOVE_BUFF,
        REMOVE_STATUS,
        ELEMENTAL_DEBUFF,
        PARAMETER_DEBUFF,
        STATUS_DEBUFF,
        SCAN_STATS
    }
    public enum BuffType
    {
        NONE,
        JUMP,
        FALLING,
        GAIN_ABILITY,
        TRIGGER_ABILITY,
        DAMAGE_MODIFIER,
        ELEMENTAL_IMBUE,
        ELEMENTAL_BUFF,
        PARAMETER_BUFF,
        PARAMETER_RESIST,
        PHYSICAL_KILLERS,
        MAGICAL_KILLERS,
        ALL_KILLERS,
        STATUS_BUFF,
        MITIGATION_BUFF,
        AI_MANIPULATION,
        UNLOCKS,
        RERAISE,
        COUNTER_PHYSICAL,
        COUNTER_MAGICAL,
        COUNTER_ALL
    }
    public enum MitigationType
    {
        NONE,
        DEFEND_MITIGATION,
        GENERAL_MITIGATION,
        COVER_MITIGATION,
        PHYSICAL_MITIGATION,
        MAGICAL_MITIGATION
    }
    public enum ParameterType
    {
        NONE,
        ATK_PARAMETER,
        MAG_PARAMETER,
        DEF_PARAMETER,
        SPR_PARAMETER,
        SET_HP,
        LB_DAMAGE,
        LB_FILL_RATE,
        HP_REGEN,
        HP_REGEN_PERCENT,
        MP_REGEN,
        MP_REGEN_PERCENT,
        LB_REGEN,
        LB_REGEN_PERCENT
    }
    public enum StatusType
    {
        NONE,
        POISON,
        BLIND,
        SILENCE,
        PARALYSE,
        SLEEP,
        CONFUSE,
        PETRIFY,
        DISEASE,
        CHARM,
        STOP,
        BERSERK,
        DEATH
    }
    public enum AIManipulationType
    {
        NONE,
        PHYSICAL_COVER_CHANCES,
        MAGICAL_COVER_CHANCES,
        ALL_COVER_CHANCES,
        TARGETING_CHANCES
    }
    public enum CostType
    {
        NONE,
        MP,
        LB,
        EP
    }
    #endregion

    public class Ability
    {
        //Unit name
        private string name;
        private string displayName;
        private string description;
        private string condition;
        private string requirement;
        private int cooldown;
        private bool onFirstTurn;
        private int cost;
        private int repeat;
        private int repeatRand;
        private CostType costType;
        private AbilityType abilityType;
        private List<AbilityComponent> abilityComponents;

        public string Name { get { return name; } }
        public string DisplayName { get { return displayName; } }
        public string Description { get { return description; } }
        public string Condition { get { return condition; } }
        public string Requirement { get { return requirement; } }
        public int Cooldown { get { return cooldown; } }
        public bool OnFirstTurn { get { return onFirstTurn; } }
        public int Repeat { get { return repeat; } }
        public int RepeatRand { get { return repeatRand; } }
        public int Cost { get { return cost; } }
        public CostType CostType { get { return costType; } }
        public AbilityType AbilityType { get { return abilityType; } }
        public IList<AbilityComponent> AbilityComponents { get { return abilityComponents.AsReadOnly(); } }

        public Ability(string name, string displayName, AbilityType abilityType, string description, string condition, string requirement, int cooldown, bool onFirstTurn, int repeat, int repeatRand, int cost,
            CostType costType, List<AbilityComponent> abilityComponents)
        {
            this.name = name;
            this.displayName = displayName;
            this.abilityType = abilityType;
            this.description = description;
            this.condition = condition;
            this.requirement = requirement;
            this.cooldown = cooldown;
            this.onFirstTurn = onFirstTurn;
            this.repeat = repeat;
            this.repeatRand = repeatRand;
            this.cost = cost;
            this.costType = costType;
            this.abilityComponents = abilityComponents;
        }
    }
}
