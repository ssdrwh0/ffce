﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius.CombatSystem.AbilitySystem
{
    public class Moveset
    {
        #region attributes
        private string name;
        #endregion

        #region get attributes
        public string Name { get { return name; } }
        #endregion

        public Moveset(string name)
        {
            this.name = name;
        }
    }
}
