﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    public enum EngineState
    {
        MAIN_MENU,
        TOWN_STATE,
        STORY_STATE,
        COMBAT_STATE,
        DUNGEON_STATE,
        FINISHED_STATE
    }

    public enum GameMode
    {
        PRACTICE,
        MAIN_STORY,
        BOSS_TRIAL
    }

    public class GameEngine
    {
        private const string titleScreen = "______ _             _  ______          _                          \r\n" +
                                           "|  ___(_)           | | |  ___|        | |                         \r\n" +
                                           "| |_   _ _ __   __ _| | | |_ __ _ _ __ | |_ __ _ ___ _   _         \r\n" +
                                           "|  _| | | '_ \\ / _` | | |  _/ _` | '_ \\| __/ _` / __| | | |        \r\n" +
                                           "| |   | | | | | (_| | | | || (_| | | | | || (_| \\__ \\ |_| |        \r\n" +
                                           "\\_|   |_|_| |_|\\__,_|_| \\_| \\__,_|_| |_|\\__\\__,_|___/\\__, |        \r\n" +
                                           "                                                      __/ |        \r\n" +
                                           "                                                     |___/         \r\n" +
                                           " _____                       _        _____            _           \r\n" +
                                           "/  __ \\                     | |      |  ___|          (_)          \r\n" +
                                           "| /  \\/ ___  _ __  ___  ___ | | ___  | |____  ____   ___ _   _ ___ \r\n" +
                                           "| |    / _ \\| '_ \\/ __|/ _ \\| |/ _ \\ |  __\\ \\/ /\\ \\ / / | | | / __|\r\n" +
                                           "| \\__/\\ (_) | | | \\__ \\ (_) | |  __/ | |___>  <  \\ V /| | |_| \\__ \\\r\n" +
                                           " \\____/\\___/|_| |_|___/\\___/|_|\\___| \\____/_/\\_\\  \\_/ |_|\\__,_|___/\r\n";

        public static bool debug = true;// = true;
        private bool displayMenu;
        private bool displayTitle;

        private EngineState state;
        private EngineState current;

        public GameEngine()
        {
            try
            {/**/
                AssetLoader.init();

                state = EngineState.MAIN_MENU;
                current = EngineState.MAIN_MENU;

                CombatMode.combatMode = new CombatMode();
                StoryMode.storyMode = new StoryMode();
                TownMode.townMode = new TownMode();
                DungeonMode.dungeonMode = new DungeonMode();

                displayTitle = true;
                displayMenu = true;
            }
            catch(Exception ex)
            {
                Console.WriteLine("Message: " + ex.Message + "\n\n" + ex.StackTrace);// + "\n\nInner: " + ex.InnerException + "\n\nSource: " + ex.Source);
                Console.ReadLine();
                System.Environment.Exit(0);
            }/**/
        }

        public void run()
        {
            try
            {/**/
                while (state != EngineState.FINISHED_STATE)
                {
                    if (displayTitle)
                    {
                        Console.WriteLine(titleScreen);
                        Console.ReadLine();
                        displayTitle = false;
                    }
                    switch (state)
                    {
                        case EngineState.MAIN_MENU:
                            if(displayMenu)
                            {
                                displayMenu = false;
                                Console.WriteLine("Type one of the following options, and then press the enter key:\n\n\"new game\"  - Begins a new game\n\"load game\" - Loads from a savepoint" +
                                    "\n\"practice\"  - Fight against a training dummy\n\"trial\"     - Fight against difficult bosses\n\"exit\"      - Quit the game");
                            }
                            string input = Console.ReadLine().ToLower();
                            displayMenu = true;
                            switch (input)
                            {
                                case "new game": loadPreset("NewGame"); break;
                                case "load game": loadGame(); break;
                                case "practice": practiceMode(); break;
                                case "trial": trialMode(); break;
                                case "exit": current = EngineState.FINISHED_STATE; break;
                                default:
                                    Console.WriteLine("\nERROR: Please type in a valid command!\n");
                                    displayMenu = false;
                                    break;
                            }
                            break;
                        case EngineState.COMBAT_STATE:
                            current = CombatMode.combatMode.run();
                            if (current == EngineState.MAIN_MENU) displayTitle = true;
                            else if (current == EngineState.STORY_STATE)
                            {
                                StoryMode.storyMode.beginScene(AssetLoader.storyData[CombatMode.storyName], "Start");
                            }
                            break;
                        case EngineState.STORY_STATE:
                            current = StoryMode.storyMode.run();
                            if (current == EngineState.MAIN_MENU) displayTitle = true;
                            else if(current == EngineState.COMBAT_STATE)
                            {
                                CombatMode.combatMode.resetBattles(GameMode.MAIN_STORY, StoryMode.battleName);
                            }
                            else if (current == EngineState.TOWN_STATE)
                            {
                                TownMode.townMode.resetTown(GameMode.MAIN_STORY, StoryMode.battleName);
                            }
                            else if (current == EngineState.STORY_STATE && !StoryMode.battleName.Equals(""))
                            {
                                StoryMode.storyMode.beginScene(AssetLoader.storyData[StoryMode.battleName], "Start");
                            }
                            break;
                        case EngineState.TOWN_STATE:
                            current = TownMode.townMode.run();
                            if (current == EngineState.MAIN_MENU) displayTitle = true;
                            else if (current == EngineState.STORY_STATE)
                            {
                                StoryMode.storyMode.beginScene(AssetLoader.storyData[CombatMode.storyName], "Start");
                            }
                            break;
                        case EngineState.DUNGEON_STATE:
                            Console.WriteLine("Dungeon Mode... Coming soon...\n");
                            Console.ReadLine();
                            displayTitle = true;
                            current = EngineState.MAIN_MENU;
                            break;
                    }
                    state = current;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Message: " + ex.Message + "\n\n" + ex.StackTrace);// + "\n\nInner: " + ex.InnerException + "\n\nSource: " + ex.Source);
                Console.ReadLine();
                System.Environment.Exit(0);
            }/**/
        }

        private void loadPreset(string dataName)
        {
            string assetName = "";
            EngineState state = EngineState.STORY_STATE;
            Party.loadPreset(dataName, out assetName, out state);
            switch (state)
            {
                case EngineState.STORY_STATE: StoryMode.storyMode.beginScene(AssetLoader.storyData[assetName], "Start"); break;
            }
            current = state;
        }

        private void loadGame()
        {
            if(Party.saves.Count > 0)
            {
                int target = 0;
                bool flag = true;
                Console.WriteLine("Use an existing save between 0 and " + (Party.saves.Count - 1).ToString() + "! Or type in \"back\" to return to main menu");
                while (flag)
                {
                    string input = Console.ReadLine();
                    if (input.ToLower().Equals("back")) return;
                    else if (int.TryParse(input, out target))
                    {
                        if (target >= 0 && target < Party.saves.Count) flag = false;
                        else Console.WriteLine("Invalid target!");
                    }
                    else Console.WriteLine("Please type in an integer between 0 and " + (Party.saves.Count - 1).ToString() + "!");
                }
                string assetName = "";
                EngineState state = EngineState.STORY_STATE;
                Party.loadCurrentData(target, out assetName, out state);
                switch(state)
                {
                    case EngineState.TOWN_STATE: TownMode.townMode.resetTown(GameMode.MAIN_STORY, assetName); break;
                    default: StoryMode.storyMode.beginScene(AssetLoader.storyData[assetName], "Start"); break;
                }
                current = state;
            }
            else
            {
                Console.WriteLine("There are no save files present in this game!");
                Console.ReadLine();
            }
        }

        private void trialMode()
        {
            bool flag = true;
            int target = 0, MaxTeams = 11;

            Console.WriteLine("\nType one of the following options, and then press the enter key:\n" +
                "\n#01  - Trial of the Hard Working Father" +
                "\n#02  - Trial of the Legendary Stag" +
                "\n#03  - Trial of the Ancient Hellbringer" +
                "\n#04  - Trial of the Beast Parade" +
                "\n#05  - Trial of the Impervious Crimeboss" +
                "\n#06  - Trial of the Legendary Stag 2" +
                "\n#07  - Trial of Lapis' Horrors" +
                "\n#08  - Trial of Malboro's Infestation" +
                "\n#09  - Trial of the Fairy King" +
                "\n#10 - Trial of the Vindictive Spirit" +
                "\n#11 - Trial of Gilgamesh"
                + "\n\nor type in \"back\" to return to the main menu");

            while (flag)
            {
                string input = Console.ReadLine();
                if (input.ToLower().Equals("back")) return;
                else if (int.TryParse(input, out target))
                {
                    if (target > 0 && target <= MaxTeams) flag = false;
                    else Console.WriteLine("Invalid target!");
                }
                else Console.WriteLine("Please type in an integer between 1 and " + (MaxTeams).ToString() + "!");
            }
            string gameName = "";
            switch (target)
            {
                case 1: gameName = "FamilyMan"; break;
                case 2: gameName = "LegendaryStag"; break;
                case 3: gameName = "AncientHellbringer"; break;
                case 4: gameName = "BeastParade"; break;
                case 5: gameName = "Tombstone"; break;
                case 6: gameName = "LegendaryStag2"; break;
                case 7: gameName = "ChaoticDarknessReborn"; break;
                case 8: gameName = "ScornOfMalboroGLEX"; break;
                case 9: gameName = "Oberon"; break;
                case 10: gameName = "Hasiko"; break;
                case 11: gameName = "Gilgamesh"; break;
            }
            loadPreset(gameName);
            CombatMode.combatMode.resetBattles(GameMode.BOSS_TRIAL, gameName);
        }

        private void practiceMode()
        {
            bool flag = true;
            int target = 0;

            string[] teams = new string[]{
                "\n#1 - 7* Hyoh",
                "\n#2 - 7* Trance Terra",
                "\n#3 - 7* Circe",
                "\n#4 - 7* Malphasie",
                "\n#5 - 7* Akstar",
                "\n#6 - 7* Fryevia, 7* Aurora Fryevia",
                "\n#7 - 7* Kurasame",
                "\n#8 - 7* Queen",
                "\n#9 - 7* Loren",
                "\n#10 - 7* CG Lightning",
                "\n#11 - 7* Nero",
                "\n#12 - 7* Emperor of Aldore",
                "\n#13 - 7* Ang, 7* Auron",
                "\n#14 - 7* Beryl, 7* Daisy",
                "\n#15 - 7* Lucky Gambler Setzer",
                "\n#16 - 7* CG Bartz",
                "\n#17 - 6* Rain & friends",
                "\n#18 - 7* Sworn Six of Paladia",
                "\n#19 - 7* Pyro Glacial Lasswell & friends",
                "\n#20 - 7* Pyro Glacial Lasswell",
                "\n#21 - 7* Veritas Group (Dark, Fire, Wind)",
                "\n#22 - 7* Veritas Group (Light, Water)",
                "\n#23 - 7* Cid+",
                "\n#24 - 7* Christmas Units",
                "\n#25 - 7* Sora",
                "\n#26 - 7* Cloud (KH)",
                "\n#27 - 7* Christmas Units part 2",
                "\n#28 - 7* Kirito",
                "\n#29 - 7* Sephiroth",
                "\n#30 - 7* Sylvanas",
                "\n#31 - 7* Barbariccia",
                "\n#32 - 7* Lila",
                "\n#33 - 7* Bai Hu & Zhu Que",
                "\n#34 - 7* Jecht",
                "\n#35 - 7* Qin",
                "\n#36 - 7* Sophia",
                "\n#37 - 7* Raegen, 7* Citra, 7* Ignacio",
                "\n#38 - 7* Sieghard, 7* Folka, 7* Cid",
                "\n#39 - 7* Lightning",
                "\n#40 - 7* 2B",
                "\n#41 - 7* A2",
                "\n#42 - 7* Beatrix"
            };

            string displayScreen = "Type one of the following options, and then press the enter key:\n";
            foreach (string team in teams) displayScreen += team;
            displayScreen += "\n\nor type in \"back\" to return to the main menu";
            Console.WriteLine(displayScreen);

            while (flag)
            {
                string input = Console.ReadLine();
                if (input.ToLower().Equals("back"))  return;
                else if (int.TryParse(input, out target))
                {
                    if (target > 0 && target <= teams.Length) flag = false;
                    else Console.WriteLine("Invalid target!");
                }
                else Console.WriteLine("Please type in an integer between 1 and " + teams.Length.ToString() + "!");
            }
            loadPreset("TrainingDummy" + target.ToString());
            CombatMode.combatMode.resetBattles(GameMode.PRACTICE, "Practice");
        }

        static void Main(string[] args)
        {
            GameEngine engine = new GameEngine();
            engine.run();
        }
    }
}
