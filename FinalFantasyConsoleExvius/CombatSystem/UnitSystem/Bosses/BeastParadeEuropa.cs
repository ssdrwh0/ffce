﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    public class BeastParadeMantis : Enemy
    {

        public BeastParadeMantis(string name, int lbLevel, int id, int actions, UnitData unitData) : base(name, lbLevel, id, actions, unitData)
        {
            
        }

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);
            if (unitData.Abilities.Count < 1) return false;
            string error = "";
            int i = 0;
            prepareAbility(i++, false, false, getLowestHPTarget(characters), out error, AssetLoader.abilitiesData["Paranoia Sickle"], characters, allies);
            prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Sickle"], characters, allies);
            prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Sickle"], characters, allies);

            while (i < actions) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
            return true;
        }
    }
    public class BeastParadeBemoldar : Enemy
    {
        private Unit ally;

        public override void trackAllies(List<Unit> enemies)
        {
            ally = enemies[0];
        }

        public BeastParadeBemoldar(string name, int lbLevel, int id, int actions, UnitData unitData) : base(name, lbLevel, id, actions, unitData)
        {

        }

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);
            if (unitData.Abilities.Count < 1) return false;

            string error = "";
            int i = 0;

            if (autoAttackHitters.Count > 0 || ally.HitByAuto)
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["1000000 Volts"], characters, allies);

            if (magicalHitters.Count > 0 || ally.HitByMagic)
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["1000000 Volts"], characters, allies);

            prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["10000 Volts"], characters, allies);
            prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Charge Bemoldar"], characters, allies);

            while (i < actions) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
            return true;
        }
    }
    public class BeastParadeOchu : Enemy
    {

        public BeastParadeOchu(string name, int lbLevel, int id, int actions, UnitData unitData) : base(name, lbLevel, id, actions, unitData)
        {

        }

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);
            if (unitData.Abilities.Count < 1) return false;

            string error = "";
            int i = 0;
            bool triggered = false;
            if (HP <= MaxHP * 0.7 && thresholds < 1)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["True Ochu Dance"], characters, allies);
                thresholds++;
                triggered = true;
            }
            if (HP <= MaxHP * 0.5 && thresholds < 2)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["True Ochu Dance"], characters, allies);
                thresholds++;
                triggered = true;
            }
            if (HP <= MaxHP * 0.3 && thresholds < 3)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["True Ochu Dance"], characters, allies);
                thresholds++;
                triggered = true;
            }/**/
            /*if ((HP <= MaxHP * 0.7 && thresholds < 1) || (HP <= MaxHP * 0.5 && thresholds < 2) || (HP <= MaxHP * 0.3 && thresholds < 3))
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["True Ochu Dance"], characters, allies);
                thresholds++;
                triggered = true;
            }/**/

            if (!triggered) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Osmose Bind"], characters, allies);
            prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Soil Slap"], characters, allies);
            prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Soil Slap"], characters, allies);
            prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Soil Slap"], characters, allies);

            while (i < actions) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
            return true;
        }
    }
    public class BeastParadeMicrochu : Enemy
    {

        public BeastParadeMicrochu(string name, int lbLevel, int id, int actions, UnitData unitData) : base(name, lbLevel, id, actions, unitData)
        {

        }

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);
            if (unitData.Abilities.Count < 1) return false;

            string error = "";
            int i = 0;
            switch(name)
            {
                case "Microchu A": prepareAbility(i++, false, false, getLowestHPTarget(characters), out error, AssetLoader.abilitiesData["Guardian Ivy"], characters, allies); break;
                case "Microchu B": prepareAbility(i++, false, false, getLowestHPTarget(characters), out error, AssetLoader.abilitiesData["Snatching Ivy"], characters, allies); break;
                case "Microchu C": prepareAbility(i++, false, false, getLowestHPTarget(characters), out error, AssetLoader.abilitiesData["Fertile Sprout"], characters, allies); break;
            }

            while (i < actions) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
            return true;
        }
    }

    public class BeastParadeRoper : Enemy
    {
        private Enemy yellowJelly;
        private Enemy greenSlime;
        private Enemy redMarshmallow;

        public BeastParadeRoper(string name, int lbLevel, int id, int actions, UnitData unitData) : base(name, lbLevel, id, actions, unitData)
        {

        }

        public override void trackAllies(List<Unit> enemies)
        {
            this.yellowJelly = enemies[1] as Enemy;
            this.greenSlime = enemies[2] as Enemy;
            this.redMarshmallow = enemies[3] as Enemy;
        }

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);
            if (unitData.Abilities.Count < 1) return false;

            string error = "";
            int i = 0;

            if (yellowJelly.CurrentState != UnitState.UNIT_ALIVE)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Possession: Yellow Jelly"], characters, allies);
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Sinuous Eel"], characters, allies);
            }
            if (greenSlime.CurrentState != UnitState.UNIT_ALIVE)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Possession: Green Slime"], characters, allies);
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Sinuous Eel"], characters, allies);
            }
            if (redMarshmallow.CurrentState != UnitState.UNIT_ALIVE)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Possession: Red Marshmallow"], characters, allies);
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Sinuous Eel"], characters, allies);
            }

            if (magicalHitters.Count > 0) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Sweep"], characters, allies);

            prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Gaze"], characters, allies);
            while (i < actions) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
            return true;
        }
    }

    public class BeastParadeHellrider : Enemy
    {
        public BeastParadeHellrider(string name, int lbLevel, int id, int actions, UnitData unitData) : base(name, lbLevel, id, actions, unitData)
        {

        }

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);
            if (unitData.Abilities.Count < 1) return false;

            int i = 0;
            string error = "";
            prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Poison Mist"], characters, allies);

            while (i < actions)
            {
                int roll = AssetLoader.random.Next(100);
                if (roll < 10) prepareAbility(i++, false, false, getLowestHPTarget(characters), out error, AssetLoader.abilitiesData["Silver Lance"], characters, allies);
                else if (roll < 60) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Silver Lance"], characters, allies);
                else prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
            }
            return true;
        }
    }

    public class BeastParadeFlamesblade : Enemy
    {
        public BeastParadeFlamesblade(string name, int lbLevel, int id, int actions, UnitData unitData) : base(name, lbLevel, id, actions, unitData)
        {

        }

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);
            if (unitData.Abilities.Count < 1) return false;

            string error = "";
            int i = 0;
            if (HP <= MaxHP * 0.8 && thresholds < 1)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Fury"], characters, allies);
                thresholds++;
            }
            if (HP <= MaxHP * 0.6 && thresholds < 2)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Fury"], characters, allies);
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Iai Strike"], characters, allies);
                thresholds++;
            }
            if (HP <= MaxHP * 0.4 && thresholds < 3)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Fury"], characters, allies);
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Iai Strike"], characters, allies);
                thresholds++;
            }
            if (HP <= MaxHP * 0.2 && thresholds < 4)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Iai Strike"], characters, allies);
                thresholds++;
            }
            prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Fira"], characters, allies);
            if (CombatMode.enemyTurn % 2 == 0)
            {
                prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Fingersnap"], characters, allies);
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Fira"], characters, allies);
            }

            while (i < actions)
            {
                int roll = AssetLoader.random.Next(100);
                if (roll < 80) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Mirror of Equity"], characters, allies);
                else prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
            }
            return true;
        }
    }

    public class BeastParadeArchiteuth : Enemy
    {
        public BeastParadeArchiteuth(string name, int lbLevel, int id, int actions, UnitData unitData) : base(name, lbLevel, id, actions, unitData)
        {

        }

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);
            if (unitData.Abilities.Count < 1) return false;

            string error = "";
            int i = 0;
            if (CombatMode.enemyTurn == 1) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Freeze Beast Parade"], characters, allies);
            if (HP <= MaxHP * 0.75 && thresholds < 1)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Freeze Beast Parade"], characters, allies);
                thresholds++;
            }
            if (HP <= MaxHP * 0.5 && thresholds < 2)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Freeze Beast Parade"], characters, allies);
                thresholds++;
            }
            if (HP <= MaxHP * 0.25 && thresholds < 3)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Freeze Beast Parade"], characters, allies);
                thresholds++;
            }
            prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Fira"], characters, allies);
            if (CombatMode.enemyTurn % 2 == 0)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Blizzara"], characters, allies);
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Blizzara"], characters, allies);
            }
            if (CombatMode.enemyTurn % 3 == 0) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Freeze"], characters, allies);

            while (i < actions) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
            return true;
        }
    }

    public class BeastParadeArchiteuthTentacle : Enemy
    {
        private Enemy body;

        public BeastParadeArchiteuthTentacle(string name, int lbLevel, int id, int actions, UnitData unitData) : base(name, lbLevel, id, actions, unitData)
        {

        }

        public override void trackAllies(List<Unit> enemies)
        {
            this.body = enemies[0] as Enemy;
        }

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);
            if (unitData.Abilities.Count < 1) return false;

            string error = "";
            if (body.CurrentState != UnitState.UNIT_ALIVE) prepareAbility(0, false, false, 0, out error, AssetLoader.abilitiesData["Kill self"], characters, allies);
            else
            {
                int i = 0;
                prepareAbility(i++, false, false, getTarget(characters), out error, unitData.Abilities[1], characters, allies);
                if (CombatMode.enemyTurn % 2 == 0 && unitData.Abilities.Count > 2)
                    prepareAbility(i++, false, false, getTarget(characters), out error, unitData.Abilities[2], characters, allies);

                while (i < actions) prepareAbility(i++, false, false, getTarget(characters), out error, unitData.Abilities[0], characters, allies); //AssetLoader.abilitiesData["Attack"]
            }
            return true;
        }
    }

    public class KrakenTentacle : Enemy
    {
        private Enemy body;

        public KrakenTentacle(string name, int lbLevel, int id, int actions, UnitData unitData) : base(name, lbLevel, id, actions, unitData)
        {

        }

        public override void trackAllies(List<Unit> enemies)
        {
            this.body = enemies[0] as Enemy;
        }

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);
            if (unitData.Abilities.Count < 1) return false;

            string error = "";
            if (body.CurrentState != UnitState.UNIT_ALIVE) prepareAbility(0, false, false, 0, out error, AssetLoader.abilitiesData["Kill self"], characters, allies);
            else
            {
                for (int i = 0; i < actions; i++)
                {
                    Ability ability = unitData.Abilities[AssetLoader.random.Next(unitData.Abilities.Count)];
                    prepareAbility(i, false, false, getTarget(characters), out error, ability, characters, allies);
                }
            }
            return true;
        }
    }

    public class BeastParadeTerrorKnight : Enemy
    {
        public BeastParadeTerrorKnight(string name, int lbLevel, int id, int actions, UnitData unitData) : base(name, lbLevel, id, actions, unitData)
        {

        }

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);
            if (unitData.Abilities.Count < 1) return false;

            string error = "";
            if(unitState == UnitState.UNIT_DYING)
            {
                /*int undying = getIntStat("undying");
                unitState = UnitState.UNIT_ALIVE;
                switch(undying){
                    case 1:
                        prepareAbility(0, false, 0, out error, AssetLoader.abilitiesData["XXXXXXXX"], characters, allies);
                        break;
                    case 2:
                        prepareAbility(0, false, 0, out error, AssetLoader.abilitiesData["XXXXXXXX"], characters, allies);
                        break;
                }
                setIntStat("undying", --undying);*/
                unitState = UnitState.UNIT_DEAD;
                prepareAbility(0, false, false, 0, out error, AssetLoader.abilitiesData["Call of Death"], characters, allies);
            }
            else
            {
                int i = 0;
                if (HP <= MaxHP * 0.8 && thresholds < 1)
                {
                    prepareAbility(i++, false, false, getHighestAtkTarget(characters), out error, AssetLoader.abilitiesData["Inescapable Death"], characters, allies);
                    thresholds++;
                }
                if (HP <= MaxHP * 0.5 && thresholds < 2)
                {
                    prepareAbility(i++, false, false, getHighestHPTarget(characters), out error, AssetLoader.abilitiesData["Inescapable Death"], characters, allies);
                    thresholds++;
                }
                if (HP <= MaxHP * 0.2 && thresholds < 3)
                {
                    prepareAbility(i++, false, false, getLowestHPTarget(characters), out error, AssetLoader.abilitiesData["Inescapable Death"], characters, allies);
                    thresholds++;
                }
                bool triggered = false;
                if (CombatMode.enemyTurn % 6 == 0)
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Dispelling Roar"], characters, allies);
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Dispelling Roar"], characters, allies);
                    triggered = true;
                }
                if (CombatMode.enemyTurn % 2 == 0 && !triggered) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Dispelling Roar"], characters, allies);
                if (CombatMode.enemyTurn % 3 == 0) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Inescapable Death"], characters, allies);
                if (CombatMode.enemyTurn % 2 == 0) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Sweep Terror Knight"], characters, allies);
                prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Onrush"], characters, allies);
                prepareAbility(i++, false, false, getHighestSprTarget(characters), out error, AssetLoader.abilitiesData["Onrush"], characters, allies);

                while (i < actions)
                {
                    int roll = AssetLoader.random.Next(1000);
                    if (roll < 300) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Aerora"], characters, allies);
                    else if (roll < 510) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Blizzara"], characters, allies);
                    else if (roll < 657) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Stonra"], characters, allies);
                    else prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
                }
            }
            
            return true;
        }
    }
    public class BeastParadeChimera : Enemy
    {

        public BeastParadeChimera(string name, int lbLevel, int id, int actions, UnitData unitData) : base(name, lbLevel, id, actions, unitData)
        {

        }

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);
            if (unitData.Abilities.Count < 1) return false;

            string error = "";
            int i = 0;
            if (HP <= MaxHP * 0.5 && thresholds < 1)
            {
                prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Frost Blast"], characters, allies);
                thresholds++;
            }
            if (HP > MaxHP * 0.5)
            {
                prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Frost Blast"], characters, allies);
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Aqua Breath"], characters, allies);
            }
            else if (HP < MaxHP * 0.5)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Aqua Breath"], characters, allies);
                prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Frost Blast"], characters, allies);
                prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Frost Blast"], characters, allies);
            }

            while (i < actions)
            {
                int roll = AssetLoader.random.Next(100);
                if (roll < 60) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Romp"], characters, allies);
                else prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
            }
            return true;
        }
    }
    public class BeastParadeAntlion : Enemy
    {
        private Unit ally;
        private bool burrowed;
        public BeastParadeAntlion(string name, int lbLevel, int id, int actions, UnitData unitData) : base(name, lbLevel, id, actions, unitData)
        {
            burrowed = false;
        }

        public override void trackAllies(List<Unit> enemies)
        {
            this.ally = enemies[0];
        }

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);
            if (unitData.Abilities.Count < 1) return false;

            string error = "";
            int i = 0;
            if(burrowed)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Sandstorm Beast Parade"], characters, allies);
                burrowed = false;
            }
            else
            {
                if (CombatMode.enemyTurn % 3 == 0 && !burrowed)
                {
                    prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["It has burrowed."], characters, allies);
                    burrowed = true;
                }
                else
                {
                    if (autoAttackHitters.Count > 0 || ally.HitByAuto)
                        prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Pincers"], characters, allies);

                    while (i < actions)
                    {
                        int roll = AssetLoader.random.Next(100);
                        if (roll < 60) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Bile"], characters, allies);
                        else prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
                    }
                }
            }
            return true;
        }
    }

    public class BeastParadeWyvern : Enemy
    {
        private bool hurt;
        public BeastParadeWyvern(string name, int lbLevel, int id, int actions, UnitData unitData) : base(name, lbLevel, id, actions, unitData)
        {

        }

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);
            if (unitData.Abilities.Count < 1) return false;

            string error = "";
            int i = 0;
            if(hurt)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Hellflame Dive"], characters, allies);
                hurt = false;
            }
            else
            {
                if (HP <= MaxHP * 0.5 && thresholds < 1 && !hurt)
                {
                    prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Fully covered in flames."], characters, allies);
                    thresholds++;
                    hurt = true;
                }
                else
                {
                    if (CombatMode.enemyTurn % 2 == 0) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Needle Breath"], characters, allies);

                    while (i < actions)
                    {
                        int roll = AssetLoader.random.Next(100);
                        if (roll < 60) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Bite"], characters, allies);
                        else prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
                    }
                }
            }
            
            return true;
        }
    }

    public class BeastParadeWanderingOne : Enemy
    {
        private Unit ally;
        private int allyThreshold;

        public BeastParadeWanderingOne(string name, int lbLevel, int id, int actions, UnitData unitData) : base(name, lbLevel, id, actions, unitData)
        {
            allyThreshold = 0;
        }

        public override void trackAllies(List<Unit> enemies)
        {
            this.ally = enemies[0];
        }

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);
            if (unitData.Abilities.Count < 1) return false;

            int i = 0;
            string error = "";
            if (HP <= MaxHP * 0.6 && thresholds < 1)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Protectga"], characters, allies);
                thresholds++;
            }
            if (HP <= MaxHP * 0.5 && thresholds < 2)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Fire Break"], characters, allies);
                thresholds++;
            }
            if (HP <= MaxHP * 0.3 && thresholds < 3)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Shellga"], characters, allies);
                thresholds++;
            }
            if (ally.HP <= ally.MaxHP * 0.5 && allyThreshold < 1)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Fire Break"], characters, allies);
                allyThreshold++;
            }

            prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Fira"], characters, allies);
            bool triggered = false;
            while (i < actions)
            {
                int roll = AssetLoader.random.Next(100);
                if (roll < 10 && !triggered)
                {
                    prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Flash"], characters, allies);
                    triggered = true;
                }
                else if (roll < 60) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Fire"], characters, allies);
                else prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
            }

            return true;
        }
    }

    public class BeastParadeEuropa : Enemy
    {
        private Enemy searcherA;
        private Enemy searcherB;
        private int shootingCountdown;
        private int gunElement;
        public bool shootingMode;

        public BeastParadeEuropa(string name, int lbLevel, int id, int actions, UnitData unitData) : base(name, lbLevel, id, actions, unitData)
        {
            shootingCountdown = 0;
            shootingMode = false;
            gunElement = 0;
        }

        public override void trackAllies(List<Unit> enemies)
        {
            this.searcherA = enemies[1] as Enemy;
            this.searcherB = enemies[2] as Enemy;
        }

        private bool injuredAllies()
        {
            return HP <= MaxHP * 0.5 && unitState != UnitState.UNIT_ALIVE && searcherA.HP <= searcherA.MaxHP * 0.5 && searcherA.CurrentState != UnitState.UNIT_ALIVE &&
                searcherB.HP <= searcherB.MaxHP * 0.5 && searcherB.CurrentState != UnitState.UNIT_ALIVE;
        }

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);
            if (unitData.Abilities.Count < 1) return false;

            /*actions = 8;
            if (HP <= MaxHP * 0.5) actions += 3;*/
            int i = 0;
            int roll;
            string error = "";
            
            if(CombatMode.enemyTurn == 1)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["System Reinforce"], characters, allies);
                shootingMode = true;
                shootingCountdown = 3;
            }
            else
            {
                if(shootingMode)
                {
                    switch(gunElement++)
                    {
                        case 1: prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Rapid Shot - Light"], characters, allies); break;
                        case 2: prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Rapid Shot - Water"], characters, allies); break;
                        case 3: prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Rapid Shot - Ice"], characters, allies); break;
                        case 4: prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Rapid Shot - Dark"], characters, allies); break;
                        default: prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Rapid Shot - Wind"], characters, allies); break;
                    }
                    if (gunElement > 4) gunElement = 0;
                    if(CombatMode.enemyTurn % 3 == 0)
                    {
                        prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Twin Gatling Gun"], characters, allies);
                        if (injuredAllies()) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Twin Gatling Gun"], characters, allies);
                    }
                    if (injuredAllies())
                    {
                        while (i < actions) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
                        if (CombatMode.enemyTurn % 4 == 0) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Recovery System On"], characters, allies);
                        if (CombatMode.enemyTurn % 2 == 0) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["System Reinforce"], characters, allies);
                    }
                    else
                    {
                        while (i < actions - 2) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
                        if (CombatMode.enemyTurn % 4 == 0) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Recovery System On"], characters, allies);
                        if (CombatMode.enemyTurn % 3 == 0) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["System Reinforce"], characters, allies);
                    }
                    if(--shootingCountdown < 1)
                    {
                        prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Change: Impact Mode"], characters, allies);
                        shootingMode = false;
                        shootingCountdown = 2;
                    }
                }
                else
                {
                    roll = AssetLoader.random.Next(2);
                    if (roll > 0) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Pile Banger"], characters, allies);
                    else prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Super Blast"], characters, allies);
                    if (injuredAllies())
                    {
                        while (i < actions - 1) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
                        prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Crush All"], characters, allies);
                        prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Crush All"], characters, allies);
                        if (CombatMode.enemyTurn % 2 == 0) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["System Reinforce"], characters, allies);
                    }
                    else
                    {
                        while (i < actions - 2) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
                        prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Crush All"], characters, allies);
                        if (CombatMode.enemyTurn % 3 == 0) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["System Reinforce"], characters, allies);
                    }

                    if(--shootingCountdown < 1)
                    {
                        prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Change: Shooting Mode"], characters, allies);
                        shootingMode = true;
                        shootingCountdown = 3;
                    }
                }
            }
            return true;
        }
    }

    public class BeastParadeSearcher : Enemy
    {
        private BeastParadeEuropa europa;

        public BeastParadeSearcher(string name, int lbLevel, int id, int actions, UnitData unitData) : base(name, lbLevel, id, actions, unitData)
        {

        }

        public override void trackAllies(List<Unit> enemies)
        {
            this.europa = enemies[0] as BeastParadeEuropa;
        }

        public override void resetStats(bool startOfMission)
        {
            base.resetStats(startOfMission);
            setDoubleStat("hp", 0.0);
            unitState = UnitState.UNIT_DEAD;
        }

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);
            if (unitData.Abilities.Count < 1) return false;

            int i = 0;
            string error = "";

            if (CombatMode.enemyTurn == 1) return true;

            if (europa.shootingMode)
            {
                if (name.Equals("Searcher A"))
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Chain Laser"], characters, allies);
                    bool triggered = false;
                    if (CombatMode.enemyTurn % 3 == 0)
                    {
                        prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Melting Eye - Light/Dark"], characters, allies);
                        triggered = true;
                    }
                    if (CombatMode.enemyTurn % 4 == 0 && !triggered) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Melting Eye - Ice/Water"], characters, allies);
                    while (i < actions) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
                }
                else
                {
                    if (CombatMode.enemyTurn % 2 == 0) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Stop"], characters, allies);
                    if (CombatMode.enemyTurn % 4 == 0) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Elemental Shield"], characters, allies);
                    while (i < actions) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
                    if (CombatMode.enemyTurn % 2 == 0) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Attack Assist"], characters, allies);
                }
            }
            else
            {
                if (name.Equals("Searcher A"))
                {
                    if (CombatMode.enemyTurn % 2 == 0) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["System Attack - ATK/MAG"], characters, allies);
                    if (CombatMode.enemyTurn % 3 == 0) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["System Attack - DEF/SPR"], characters, allies);
                    while (i < actions) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
                }
                else
                {
                    if (CombatMode.enemyTurn % 2 == 0) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Enthralled Eye"], characters, allies);
                    bool triggered = false;
                    if (CombatMode.enemyTurn % 3 == 0)
                    {
                        prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Material Field"], characters, allies);
                        triggered = true;
                    }
                    if (CombatMode.enemyTurn % 4 == 0 && !triggered) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Magic Field"], characters, allies);
                    while (i < actions) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
                }
            }

            return true;
        }
    }
}
