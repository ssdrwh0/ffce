﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    public class Enemy : Unit
    {
        protected int storyPhase;
        //TO DO: Add Enemy Patterns Data
        protected bool recovering;
        protected int actions;
        protected int thresholds;

        public Enemy(string name, int lbLevel, int id, int actions, UnitData unitData) : base(name, lbLevel, id, unitData, null)
        {
            this.actions = actions;
            isPlayer = false;
            recovering = false;
            thresholds = 0;
            storyPhase = 0;
        }

        protected virtual void displayDialogue(string name)
        {
            StoryMode.storyMode.beginScene(AssetLoader.storyData[name], "Start");
            EngineState state = EngineState.STORY_STATE;
            while (state == EngineState.STORY_STATE)
                state = StoryMode.storyMode.run();
        }

        private List<Unit> getAvailableTargets(List<Unit> victims)
        {
            int remainingTargets = 0;
            List<Unit> available = new List<Unit>();
            foreach (Unit victim in victims)
                if (victim.CurrentState == UnitState.UNIT_ALIVE && !victim.Petrified)
                    remainingTargets++;
            foreach (Unit victim in victims)
            {
                int roll = AssetLoader.random.Next(100);
                int result = 100 + (int)(victim.getDoubleStat("targetChance") + victim.getTotalDoubleStat("targetChance", "") * 100);
                if (victim.CurrentState == UnitState.UNIT_ALIVE && !victim.Petrified && (roll < result || remainingTargets < 2))
                    available.Add(victim);
            }
            return available;
        }

        private int finaliseTarget(int target, List<Unit> victims)
        {
            Unit victim = victims[target];
            int provoker = (int)((victim.getDoubleStat("targetChance") + victim.getTotalDoubleStat("targetChance", "")) * 100);
            if (provoker >= 100) return target;
            int i = 0;
            foreach (Unit otherVictim in victims)
            {
                if (i != target && otherVictim.CurrentState == UnitState.UNIT_ALIVE && !otherVictim.Petrified)
                {
                    int roll = AssetLoader.random.Next(100);
                    provoker = (int)((otherVictim.getDoubleStat("targetChance") + otherVictim.getTotalDoubleStat("targetChance", "")) * 100);
                    if (roll < provoker) return i;
                }
                i++;
            }
            return target;
        }

        protected int getHighestAtkTarget(List<Unit> victims)
        {
            int target = 0;
            List<Unit> available = getAvailableTargets(victims);
            double highest = victims[target].Atk;
            int i = 0;
            foreach (Unit victim in victims)
            {
                double current = victim.Atk;
                if (current > highest)
                {
                    target = i;
                    highest = current;
                }
                i++;
            }
            return finaliseTarget(target, victims);
        }

        protected int getHighestMagTarget(List<Unit> victims)
        {
            int target = 0;
            List<Unit> available = getAvailableTargets(victims);
            double highest = victims[target].Mag;
            int i = 0;
            foreach (Unit victim in victims)
            {
                double current = victim.Mag;
                if (current > highest)
                {
                    target = i;
                    highest = current;
                }
                i++;
            }
            return finaliseTarget(target, victims);
        }

        protected int getHighestSprTarget(List<Unit> victims)
        {
            int target = 0;
            List<Unit> available = getAvailableTargets(victims);
            double highest = victims[target].Spr;
            int i = 0;
            foreach (Unit victim in victims)
            {
                double current = victim.Spr;
                if (current > highest)
                {
                    target = i;
                    highest = current;
                }
                i++;
            }
            return finaliseTarget(target, victims);
        }

        protected int getHighestHPTarget(List<Unit> victims)
        {
            int target = 0;
            List<Unit> available = getAvailableTargets(victims);
            double highest = victims[target].getDoubleStat("hp");
            int i = 0;
            foreach (Unit victim in victims)
            {
                double current = victim.getDoubleStat("hp");
                if (current > highest)
                {
                    target = i;
                    highest = current;
                }
                i++;
            }
            return finaliseTarget(target, victims);
        }

        protected int getLowestHPTarget(List<Unit> victims)
        {
            int target = 0;
            List<Unit> available = getAvailableTargets(victims);
            double lowest = victims[target].getDoubleStat("hp");
            int i = 0;
            foreach (Unit victim in victims)
            {
                double current = victim.getDoubleStat("hp");
                if (current < lowest)
                {
                    target = i;
                    lowest = current;
                }
                i++;
            }
            return finaliseTarget(target, victims);
        }

        protected int getTarget(List<Unit> victims)
        {
            int target = 0;
            List<Unit> available = getAvailableTargets(victims);
            if (available.Count > 1) target = AssetLoader.random.Next(available.Count);
            if (available.Count < 1) return -1;
            int i = 0;
            foreach (Unit victim in victims)
            {
                if (available[target] == victim)
                {
                    target = i;
                    break;
                }
                i++;
            }
            return finaliseTarget(target, victims);
        }

        public bool isRecovering() { return recovering; }

        public virtual void trackAllies(List<Unit> enemies)
        {

        }

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            string error = "";
            setBoolStat("actionTaken", true);
            if (unitData.Abilities.Count < 1)
            {
                Console.WriteLine("ERROR: This enemy needs abilities!!!");
                return false;
            }
            for (int i = 0; i < actions; i++)
            {
                Ability ability = unitData.Abilities[AssetLoader.random.Next(unitData.Abilities.Count)];
                prepareAbility(i, false, false, getTarget(characters), out error, ability, characters, allies);
            }
            return true;
        }
    }
}
