﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    public enum Race
    {
        NONE,
        BEAST,
        PLANT,
        DEMON,
        HUMAN,
        BIRD,
        MACHINE,
        STONE,
        SPIRIT,
        BUG,
        UNDEAD,
        DRAGON,
        AQUATIC
    }

    public class UnitData
    {
        public const int MAX_UNIT_LEVEL = 120;
        public const int MAX_LIMIT_BURST = 30;
        public const int MAX_UNIT_LEVEL_6_STARS = 100;
        public const int MAX_LIMIT_BURST_6_STARS = 25;

        #region Attributes
        //Unit name
        protected string displayName;
        protected string baseName;
        //Unit level
        protected int baseLevel;
        //LB Cost
        protected double lbCost;
        //Limit Crystals per attack
        protected int lbPerAttack;

        //Base Unit Attributes
        protected Dictionary<string, double> doubleStats;
        protected Dictionary<string, int> intStats;

        protected List<Race> raceTypes;
        protected List<Ability> abilities;
        protected List<int> levelRequired;
        protected List<Passive> passives;
        protected List<int> levelRequiredPassives;
        #endregion

        #region Get Attributes
        //Unit name
        public string DisplayName { get { return displayName; } }
        //Unit name
        public string BaseName { get { return baseName; } }
        //Base level
        public int BaseLevel { get { return baseLevel; } }
        //LB level
        public double LbCost { get { return lbCost; } }
        //LB level
        public int LBPerAttack { get { return lbPerAttack; } }

        //Base Unit Attributes
        public double getDoubleStat(string name)
        {
            if (doubleStats.ContainsKey(name))
                return doubleStats[name];
            return 0.0;
        }
        public int getIntStat(string name)
        {
            if (intStats.ContainsKey(name))
                return intStats[name];
            return 0;
        }

        public IList<Race> RaceTypes { get { return raceTypes.AsReadOnly(); } }
        public IList<Ability> Abilities { get { return abilities.AsReadOnly(); } }
        public IList<int> LevelRequired { get { return levelRequired.AsReadOnly(); } }
        public IList<Passive> Passives { get { return passives.AsReadOnly(); } }
        public IList<int> LevelRequiredPassives { get { return levelRequiredPassives.AsReadOnly(); } }
        #endregion

        public UnitData(string baseName, string displayName, int baseLevel, double lbCost, int lbPerAttack, Dictionary<string, int> intStats, Dictionary<string, double> doubleStats, List<Ability> abilities,
            List<int> levelRequired, List<Passive> passives, List<int> levelRequiredPassives, List<Race> raceTypes)
        {
            #region Attributes Declared
            this.baseName = baseName;
            this.displayName = displayName;
            this.baseLevel = baseLevel;
            this.lbCost = lbCost;
            this.lbPerAttack = lbPerAttack;
            if (baseLevel > MAX_UNIT_LEVEL) baseLevel = MAX_UNIT_LEVEL;
            this.doubleStats = doubleStats;
            this.intStats = intStats;
            this.abilities = abilities;
            this.levelRequired = levelRequired;
            this.passives = passives;
            this.levelRequiredPassives = levelRequiredPassives;
            this.raceTypes = raceTypes;
            #endregion
        }

        public int getIntStatAdvanced(string name, string type, Unit unit)
        {
            int boost = 0;
            int i = 0;
            foreach (Passive passive in passives)
                if(unit.UnitLevel >= levelRequiredPassives[i++] && passive.checkCondition(unit))
                    boost += passive.getIntStat(name, type, unit);
            return boost;
        }

        public double getDoubleStatAdvanced(string name, string type, Unit unit)
        {
            double boost = 0.0;
            int i = 0;
            foreach (Passive passive in passives)
                if (unit.UnitLevel >= levelRequiredPassives[i++] && passive.checkCondition(unit))
                    boost += passive.getDoubleStat(name, type, unit);
            return boost;
        }

        public double getHpThreshold(string passiveName, Unit unit)
        {
            foreach (Passive passive in passives)
                if (passive.Name.Equals(passiveName))
                    return passive.getSimpleDoubleStat("direThreshold");
            return 0.0;
        }

        private bool checkPassive(string condition, string requirement, Unit unit)
        {
            foreach (Passive passive in passives)
                if (passive.Name.Equals(requirement) && passive.checkCondition(unit))
                    return true;
            return false;
        }

        private bool checkEquipment(string requirement, Unit unit)
        {
            foreach (Equipment equip in unit.weaponSet)
                if (equip.EquipData.DisplayName.Equals(requirement))
                    return true;
            foreach (Equipment equip in unit.equipmentSet)
                if (equip.EquipData.DisplayName.Equals(requirement))
                    return true;
            foreach (Materia materia in unit.materiaSet)
                if (materia.DisplayName.Equals(requirement))
                    return true;
            return false;
        }

        public bool fulfilsPassiveCondition(int i, string condition, string requirement, Unit unit)
        {
            switch (condition)
            {
                case "passiveCondition": return checkPassive(condition, requirement, unit) && unit.UnitLevel >= levelRequiredPassives[i];
                case "notPassiveCondition": return !checkPassive(condition, requirement, unit) && unit.UnitLevel >= levelRequiredPassives[i];
                case "equipCondition": return checkEquipment(requirement, unit) && unit.UnitLevel >= levelRequiredPassives[i];
                case "notEquipCondition": return !checkEquipment(requirement, unit) && unit.UnitLevel >= levelRequiredPassives[i];
                default: return unit.UnitLevel >= levelRequiredPassives[i];
            }
        }
    }
}
