﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    public class Tombstone : Enemy
    {
        public int commonTarget;

        private bool gotDamaged;
        private bool instantSummon;

        public Tombstone(string name, int lbLevel, int id, int actions, UnitData unitData) : base(name, lbLevel, id, actions, unitData)
        {
            gotDamaged = false;
            instantSummon = true;
            commonTarget = -1;
        }

        public override void setUnitState(UnitState state)
        {
            unitState = state;
            if (unitState == UnitState.UNIT_DEAD)
                displayDialogue("tombstoneDefeat");
        }

        /*public override void resetStats(bool startOfMission)
        {
            base.resetStats(startOfMission);
            setDoubleStat("hp", HP * 0.45);
            gotDamaged = true;
        }*/

        public override double takeDamage(AttackQueue queue, int id)
        {
            chainFrame = 0;
            if (unitState == UnitState.UNIT_DEAD || unitState == UnitState.UNIT_REMOVED || unitState == UnitState.UNIT_AIRBORNE || unitState == UnitState.UNIT_HIDING) return 0.0;
            if (Petrified)
            {
                Console.WriteLine(Name + " cannot be damaged once petrified!");
                return 0.0;
            }
            if (!(queue.abilityType == AbilityType.LIMIT_BURST || queue.abilityType == AbilityType.ESPER_ABILITY))
            {
                Console.WriteLine(Name + " is currently immune to damage!");
                return 0.0;
            }

            if (!gotDamaged) gotDamaged = true;

            switch (queue.abilityType)
            {
                case AbilityType.ESPER_ABILITY: esperHitters.Add(queue.id - 1); break;
                case AbilityType.LIMIT_BURST: limitHitters.Add(queue.id - 1); break;
            }
            switch (queue.attackType)
            {
                case AttackType.HYBRID_ATTACK: physicalHitters.Add(queue.id - 1); break;
                case AttackType.MAGICAL_ATTACK: magicalHitters.Add(queue.id - 1); break;
                case AttackType.PHYSICAL_ATTACK:
                    if (queue.abilityName.Equals("Attack"))
                        autoAttackHitters.Add(queue.id - 1);
                    physicalHitters.Add(queue.id - 1);
                    break;
            }

            /*if(checkDamageResists(queue.attackType)) return 0.0;

            string elementMessage = "";
            double elementalMod = calculateElementalMod(queue.elements, out elementMessage);
            if (elementalMod <= 1e-4)
            {
                Console.WriteLine(Name + elementMessage);
                return 0.0;
            }*/

            string output = "";
            if (queue.drainType != DrainType.MP_DRAIN)
            {
                if (whoIsChaining == queue.id)
                {
                    output += "Streak Ended...";
                    chainBonus = 1;
                    totalChainCount = 1;
                }
                else if (++totalChainCount > 1)
                {
                    output += chainCalculations(queue);
                    if (!queue.dualWieldPlus && chainBonus > 4.0)
                        chainBonus = 4.0;
                }
                CombatMode.toSpark = true;
            }

            //if (GameEngine.debug) Console.WriteLine("Frame Count - " + queue.currentFrame.ToString() + "; Weight - " + queue.weight[queue.currentFrame].ToString());
            double finalDamage = queue.calculatedDamage[id] * chainBonus * queue.weight[queue.currentFrame];
            if (queue.self.IsPlayer == isPlayer) finalDamage = queue.calculatedDamageAllies[id] * chainBonus * queue.weight[queue.currentFrame];

            switch (queue.drainType)
            {
                case DrainType.HP_DRAIN:
                    double drainHP = setHp(finalDamage, true, out output) * queue.drainScale;
                    double enemyHp = queue.self.getDoubleStat("hp");
                    enemyHp += drainHP;
                    if (enemyHp > queue.self.MaxHP) enemyHp = queue.self.MaxHP;
                    Console.WriteLine(queue.self.Name + " has been healed by " + Math.Floor(drainHP).ToString() + " HP!");
                    queue.self.setDoubleStat("hp", enemyHp);
                    break;
                case DrainType.MP_DRAIN:
                    double mp = getDoubleStat("mp");
                    double drainMP = finalDamage;
                    if (finalDamage > mp)
                    {
                        drainMP = mp;
                        mp = 0;
                    }
                    else mp -= drainMP;
                    Console.WriteLine(Name + " has been drained by " + Math.Floor(drainMP).ToString() + " MP!");
                    setDoubleStat("mp", mp);

                    drainMP *= queue.drainScale;
                    double enemyMp = queue.self.getDoubleStat("mp");
                    enemyMp += drainMP;
                    if (enemyMp > queue.self.MaxMP) enemyMp = queue.self.MaxMP;
                    Console.WriteLine(queue.self.Name + " has been healed for " + Math.Floor(drainMP).ToString() + " MP!");
                    queue.self.setDoubleStat("mp", enemyMp);
                    break;
                default: setHp(finalDamage, false, out output); break;
            }

            previousElements = queue.elements;
            whoIsChaining = queue.id;

            return 0.0;
        }

        protected override void takeDamageOverTime(DebuffQueue queue)
        {
            if (!(queue.abilityType == AbilityType.LIMIT_BURST || queue.abilityType == AbilityType.ESPER_ABILITY))
            {
                Console.WriteLine(Name + " is currently immune to damage!");
                return;
            }
            takeDamageOverTime(queue);
        }

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);
            if (unitData.Abilities.Count < 1) return false;

            commonTarget = getTarget(characters);

            actions = 8;
            if (HP <= MaxHP * 0.5) actions += 3;
            int i = 0;
            int roll;
            string error = "";
            if (CombatMode.enemyTurn == 1) displayDialogue("tombstoneIntro");
            bool triggeredSummon = false;
            if (gotDamaged && instantSummon)
            {
                displayDialogue("tombstoneDamaged");
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Get in here boys!"], characters, allies);
                instantSummon = false;
                triggeredSummon = true;
            }
            if (HP <= MaxHP * 0.5)
            {
                if (gotDamaged && CombatMode.enemyTurn % 3 == 0 && !triggeredSummon)
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Get in here boys!"], characters, allies);
                actions++;

                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Time to get serious!"], characters, allies);
                actions++;
                while (i < actions)
                {
                    roll = AssetLoader.random.Next(100);
                    if (roll < 15) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Three Punch Combo"], characters, allies);
                    if (roll < 40) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Uppercut Fist"], characters, allies);
                    else prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
                }

                if (CombatMode.enemyTurn % 3 == 2) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Thunderclap"], characters, allies);
                if (CombatMode.enemyTurn % 3 == 1) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Throw Car"], characters, allies);
                if (CombatMode.enemyTurn % 3 == 0) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Roundhouse Kick"], characters, allies);
                if (CombatMode.enemyTurn % 6 == 1) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Roundhouse Kick"], characters, allies);
                if (CombatMode.enemyTurn % 9 == 2) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Roundhouse Kick"], characters, allies);
                if (CombatMode.enemyTurn % 12 == 0)
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Roundhouse Kick"], characters, allies);
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Roundhouse Kick"], characters, allies);
                }

                roll = AssetLoader.random.Next(100);
                if (roll < 20) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Choke Hold"], characters, allies);
                if (CombatMode.enemyTurn % 3 == 0) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Final Blow"], characters, allies);
            }
            else
            {
                if (gotDamaged && CombatMode.enemyTurn % 4 == 0 && !triggeredSummon)
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Get in here boys!"], characters, allies);
                actions++;
                while (i < actions)
                {
                    roll = AssetLoader.random.Next(100);
                    if (roll < 15) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Three Punch Combo"], characters, allies);
                    if (roll < 40) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Uppercut Fist"], characters, allies);
                    else prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
                }
                if (CombatMode.enemyTurn % 3 == 1) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Throw Car"], characters, allies);
                if (CombatMode.enemyTurn % 3 == 0) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Roundhouse Kick"], characters, allies);
                roll = AssetLoader.random.Next(100);
                if (roll < 10) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Choke Hold"], characters, allies);
            }
            
            return true;
        }
    }

    public class TombstonesLackey : Enemy
    {

        private Tombstone tombstone;
        private int windupTime;

        public TombstonesLackey(string name, int lbLevel, int id, int actions, UnitData unitData) : base(name, lbLevel, id, actions, unitData)
        {
            windupTime = 0;
        }

        public override void trackAllies(List<Unit> enemies)
        {
            tombstone = enemies[0] as Tombstone;
        }

        public override void resetStats(bool startOfMission)
        {
            base.resetStats(startOfMission);
            setDoubleStat("hp", 0.0);
            unitState = UnitState.UNIT_DEAD;
        }

        protected override double unitIsDead()
        {
            double value = base.unitIsDead();
            windupTime = 0;
            return value;
        }

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);
            if (unitData.Abilities.Count < 1) return false;

            actions = 8;
            int i = 0;
            string error = "";
            if(++windupTime > 2)
            {
                prepareAbility(i++, false, false, tombstone.commonTarget, out error, AssetLoader.abilitiesData["Co-ordinated Assault Barrage"], characters, allies);
                windupTime = 0;
            }
            else prepareAbility(i++, false, false, tombstone.commonTarget, out error, AssetLoader.abilitiesData["Assault Rifle Fire"], characters, allies);
            while (i < actions) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);

            return true;
        }
    }
}
