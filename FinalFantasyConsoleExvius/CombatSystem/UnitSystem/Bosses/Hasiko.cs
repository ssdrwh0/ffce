﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    public class Hasiko : Enemy
    {

        public Hasiko(string name, int lbLevel, int id, int actions, UnitData unitData)
            : base(name, lbLevel, id, actions, unitData)
        {

        }

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);
            if (unitData.Abilities.Count < 1) return false;

            actions = 8;
            int i = 0;
            string error = "";
            double threshold = HP / MaxHP;

            if(CombatMode.enemyTurn == 1)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Spirit Burn"], characters, allies);
                prepareAbility(i++, false, false, getHighestSprTarget(characters), out error, AssetLoader.abilitiesData["Cursed Mind"], characters, allies);
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Intimidating Gaze"], characters, allies);
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Sinister Gaze"], characters, allies);
            }
            else
            {
                if (threshold <= 0.7 && thresholds < 1)
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Sinister Gaze"], characters, allies);
                    thresholds++;
                    actions++;
                }
                if (threshold <= 0.5 && thresholds < 2)
                {
                    prepareAbility(i++, false, false, getHighestSprTarget(characters), out error, AssetLoader.abilitiesData["Cursed Mind"], characters, allies);
                    thresholds++;
                    actions++;
                }
                if (threshold <= 0.3 && thresholds < 3)
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Maniacal Slash"], characters, allies);
                    thresholds++;
                    actions++;
                }
                if (CombatMode.enemyTurn % 6 == 2)
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Growl"], characters, allies);
                    thresholds++;
                    actions++;
                }
                if (CombatMode.enemyTurn % 6 == 5)
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Wail"], characters, allies);
                    thresholds++;
                    actions++;
                }
                while (i < actions)
                {
                    if(threshold > 0.69)
                    {
                        int roll = AssetLoader.random.Next(1000);
                        if (roll < 150) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Spirit Burn"], characters, allies);
                        else if (roll < 320) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Intimidating Gaze"], characters, allies);
                        else if (roll < 524) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Sinister Gaze"], characters, allies);
                        else if (roll < 714) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Deadly Thrust"], characters, allies);
                        else prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
                    }
                    else if (threshold > 0.49)
                    {
                        int roll = AssetLoader.random.Next(1000);
                        if (roll < 300) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Sinister Gaze"], characters, allies);
                        else if (roll < 405) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Spirit Burn"], characters, allies);
                        else if (roll < 524) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Maniacal Slash"], characters, allies);
                        else if (roll < 572) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Intimidating Gaze"], characters, allies);
                        else if (roll < 700) prepareAbility(i++, false, false, getHighestMagTarget(characters), out error, AssetLoader.abilitiesData["Deadly Thrust"], characters, allies);
                        else prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
                    }
                    else if (threshold > 0.29)
                    {
                        int roll = AssetLoader.random.Next(1000);
                        if (roll < 100) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Cursed Mind"], characters, allies);
                        else if (roll < 235) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Spirit Burn"], characters, allies);
                        else if (roll < 388) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Maniacal Slash"], characters, allies);
                        else if (roll < 510) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Sinister Gaze"], characters, allies);
                        else if (roll < 657) prepareAbility(i++, false, false, getHighestMagTarget(characters), out error, AssetLoader.abilitiesData["Deadly Thrust"], characters, allies);
                        else prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
                    }
                    else
                    {
                        int roll = AssetLoader.random.Next(1000);
                        if (roll < 100) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Cursed Mind"], characters, allies);
                        else if (roll < 235) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Intimidating Gaze"], characters, allies);
                        else if (roll < 388) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Spirit Burn"], characters, allies);
                        else if (roll < 510) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Maniacal Slash"], characters, allies);
                        else prepareAbility(i++, false, false, getHighestMagTarget(characters), out error, AssetLoader.abilitiesData["Deadly Thrust"], characters, allies);
                    }
                }
            }
            
            return true;
        }
    }

    public class AwakenedHasiko : Enemy
    {
        private List<HasikosSpirit> spirits;
        private bool deadSpiritsA, deadSpiritsB;
        private int ressurectTimer;

        public AwakenedHasiko(string name, int lbLevel, int id, int actions, UnitData unitData)
            : base(name, lbLevel, id, actions, unitData)
        {
            spirits = new List<HasikosSpirit>();
        }

        public override void resetStats(bool startOfMission)
        {
            base.resetStats(startOfMission);
            deadSpiritsA = deadSpiritsB = false;
            ressurectTimer = 3;
        }

        public override void trackAllies(List<Unit> enemies)
        {
            for (int i = 1; i < enemies.Count; i++) spirits.Add(enemies[i] as HasikosSpirit);
        }

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);
            if (unitData.Abilities.Count < 1) return false;

            actions = 8;
            int i = 0;
            string error = "";
            double threshold = HP / MaxHP;

            if(CombatMode.enemyTurn == 1)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Spirit Burn"], characters, allies);
                prepareAbility(i++, false, false, getHighestSprTarget(characters), out error, AssetLoader.abilitiesData["Deadly Cursed Mind"], characters, allies);
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Deadly Intimidating Gaze"], characters, allies);
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Deadly Sinister Gaze"], characters, allies);
            }
            else
            {
                if (threshold <= 0.7 && thresholds < 1)
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Deadly Sinister Gaze"], characters, allies);
                    thresholds++;
                    actions++;
                }
                if (threshold <= 0.5 && thresholds < 2)
                {
                    prepareAbility(i++, false, false, getHighestSprTarget(characters), out error, AssetLoader.abilitiesData["Deadly Cursed Mind"], characters, allies);
                    thresholds++;
                    actions++;
                }
                if (threshold <= 0.3 && thresholds < 3)
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Maniacal Slash"], characters, allies);
                    thresholds++;
                    actions++;
                }
                if (spirits[0].CurrentState == UnitState.UNIT_DEAD && !deadSpiritsA)
                {
                    deadSpiritsA = true;
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["I will destroy you!"], characters, allies);
                    thresholds++;
                    actions++;
                }
                if (spirits[1].CurrentState == UnitState.UNIT_DEAD && !deadSpiritsB)
                {
                    deadSpiritsB = true;
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["I will destroy you!"], characters, allies);
                    thresholds++;
                    actions++;
                }
                if (deadSpiritsA && deadSpiritsB && --ressurectTimer < 1)
                {
                    deadSpiritsA = deadSpiritsB = false;
                    ressurectTimer = 3;
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Claim your vengeance!"], characters, allies);
                    foreach (HasikosSpirit spirit in spirits) spirit.countdown = 15;
                }
                if (CombatMode.enemyTurn % 6 == 2)
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Growl"], characters, allies);
                    thresholds++;
                    actions++;
                }
                if (CombatMode.enemyTurn % 6 == 5)
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Wail"], characters, allies);
                    thresholds++;
                    actions++;
                }
                while (i < actions)
                {
                    if(threshold > 0.69)
                    {
                        int roll = AssetLoader.random.Next(1000);
                        if (roll < 150) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Spirit Burn"], characters, allies);
                        else if (roll < 320) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Deadly Intimidating Gaze"], characters, allies);
                        else if (roll < 524) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Deadly Sinister Gaze"], characters, allies);
                        else if (roll < 714) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Deadly Thrust"], characters, allies);
                        else prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
                    }
                    else if (threshold > 0.49)
                    {
                        int roll = AssetLoader.random.Next(1000);
                        if (roll < 300) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Deadly Sinister Gaze"], characters, allies);
                        else if (roll < 405) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Spirit Burn"], characters, allies);
                        else if (roll < 524) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Maniacal Slash"], characters, allies);
                        else if (roll < 572) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Deadly Intimidating Gaze"], characters, allies);
                        else if (roll < 700) prepareAbility(i++, false, false, getHighestMagTarget(characters), out error, AssetLoader.abilitiesData["Deadly Thrust"], characters, allies);
                        else prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
                    }
                    else if (threshold > 0.29)
                    {
                        int roll = AssetLoader.random.Next(1000);
                        if (roll < 100) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Deadly Cursed Mind"], characters, allies);
                        else if (roll < 235) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Spirit Burn"], characters, allies);
                        else if (roll < 388) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Maniacal Slash"], characters, allies);
                        else if (roll < 510) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Deadly Sinister Gaze"], characters, allies);
                        else if (roll < 657) prepareAbility(i++, false, false, getHighestMagTarget(characters), out error, AssetLoader.abilitiesData["Deadly Thrust"], characters, allies);
                        else prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
                    }
                    else
                    {
                        int roll = AssetLoader.random.Next(1000);
                        if (roll < 100) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Deadly Cursed Mind"], characters, allies);
                        else if (roll < 235) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Deadly Intimidating Gaze"], characters, allies);
                        else if (roll < 388) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Spirit Burn"], characters, allies);
                        else if (roll < 510) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Maniacal Slash"], characters, allies);
                        else prepareAbility(i++, false, false, getHighestMagTarget(characters), out error, AssetLoader.abilitiesData["Deadly Thrust"], characters, allies);
                    }
                }
            }
            
            return true;
        }
    }

    public class HasikosSpirit : Enemy
    {
        public int countdown;
        public HasikosSpirit(string name, int lbLevel, int id, int actions, UnitData unitData) : base(name, lbLevel, id, actions, unitData)
        {
            
        }

        public override void resetStats(bool startOfMission)
        {
            base.resetStats(startOfMission);
            countdown = 15;
        }

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);
            if (unitData.Abilities.Count < 1) return false;

            actions = 4;
            int i = 0;
            string error = "";
            double threshold = HP / MaxHP;

            countdown--;
            if (CombatMode.enemyTurn > 1)
            {
                if(countdown < 1)
                {
                    countdown = 15;
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Malefic Sacrifice"], characters, allies);
                }
                else
                {
                    while (i < actions)
                    {
                        int roll = AssetLoader.random.Next(5);
                        if (roll < 2) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Cursed Flash"], characters, allies);
                        else prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
                    }
                }
            }

            return true;
        }
    }
}
