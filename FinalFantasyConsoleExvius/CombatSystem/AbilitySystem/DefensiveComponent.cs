﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    public class DefensiveComponent : AbilityComponent
    {
        private DefensiveType defensiveType;
        private List<ParameterType> parameterTypes;
        private List<StatusType> statusTypes;

        public DefensiveType DefensiveType { get { return defensiveType; } }
        public IList<ParameterType> ParameterTypes { get { return parameterTypes.AsReadOnly(); } }
        public IList<StatusType> StatusTypes { get { return statusTypes.AsReadOnly(); } }

        public DefensiveComponent(string name, int castTime, int sacrifice, double effect, double scale, double limitScale, ActionType actionType, TargetType targetType, List<int> frameData,
            DefensiveType defensiveType, List<ParameterType> parameterTypes, List<StatusType> statusTypes, List<ScalingType> scalingTypes, List<ElementalType> elementalTypes)
            : base(name, castTime, sacrifice, effect, scale, limitScale, actionType, frameData, targetType, scalingTypes, elementalTypes)
        {
            this.defensiveType = defensiveType;
            this.parameterTypes = parameterTypes;
            this.statusTypes = statusTypes;
        }
    }
}
