﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    public class EnemySpawnData
    {
        private bool bossBattle;
        private bool onEnemyTurnFirst;
        private string special;
        private string announcement;
        private string storyName;
        private string endOfBattle;
        private List<string> enemiesList;
        private List<string> enemyTypesList;
        private List<int> enemyActionsList;
        private List<double> enemyExpList;
        private List<int> enemyGilList;

        public bool BossBattle { get { return bossBattle; } }
        public bool OnEnemyTurnFirst { get { return onEnemyTurnFirst; } }
        public string Special { get { return special; } }
        public string StoryName { get { return storyName; } }
        public string Announcement { get { return announcement; } }
        public string EndOfBattle { get { return endOfBattle; } }
        public IList<string> EnemiesList { get { return enemiesList.AsReadOnly(); } }
        public IList<string> EnemyTypesList { get { return enemyTypesList.AsReadOnly(); } }
        public IList<int> EnemyActionsList { get { return enemyActionsList.AsReadOnly(); } }
        public IList<double> EnemyExpList { get { return enemyExpList.AsReadOnly(); } }
        public IList<int> EnemyGilList { get { return enemyGilList.AsReadOnly(); } }

        public EnemySpawnData(bool bossBattle, bool onEnemyTurnFirst, string announcement, string special, string endOfBattle, string storyName, List<string> enemiesList, List<string> enemyTypesList,
            List<int> enemyActionsList, List<double> enemyExpList, List<int> enemyGilList)
        {
            this.bossBattle = bossBattle;
            this.onEnemyTurnFirst = onEnemyTurnFirst;
            this.special = special;
            this.announcement = announcement;
            this.endOfBattle = endOfBattle;
            this.storyName = storyName;
            this.enemiesList = enemiesList;
            this.enemyTypesList = enemyTypesList;
            this.enemyActionsList = enemyActionsList;
            this.enemyExpList = enemyExpList;
            this.enemyGilList = enemyGilList;
        }
    }
}
