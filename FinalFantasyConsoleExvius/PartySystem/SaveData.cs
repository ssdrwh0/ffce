﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    public class SaveData
    {
        public List<LoadoutData> visionData;
        public List<LoadoutData> partyData;
        public List<string> itemsData;
        public List<int> itemsQtyData;
        public List<string> equipmentData;
        public List<int> equipmentQtyData;
        public List<string> equipmentDataUnsellable;
        public List<int> equipmentQtyDataUnsellable;
        public List<string> materiaData;
        public List<int> materiaQtyData;
        public List<string> materiaDataUnsellable;
        public List<int> materiaQtyDataUnsellable;
        public List<string> esperNamesData;
        public List<int> esperLevelsData;
        public List<string> unlockedContent;
        public int gil;
        public string assetName;
        public string state;
        public string selectedVision;

        public SaveData(List<LoadoutData> visionData, List<LoadoutData> partyData, List<string> itemsData, List<int> itemsQtyData, List<string> equipmentData, List<int> equipmentQtyData,
            List<string> equipmentDataUnsellable, List<int> equipmentQtyDataUnsellable, List<string> materiaData, List<int> materiaQtyData, List<string> materiaDataUnsellable,
            List<int> materiaQtyDataUnsellable, List<string> esperNamesData, List<int> esperLevelsData, List<string> unlockedContent, int gil, string assetName, string state,
            string selectedVision)
        {
            this.visionData = visionData;
            this.partyData = partyData;
            this.itemsData = itemsData;
            this.itemsQtyData = itemsQtyData;
            this.equipmentData = equipmentData;
            this.equipmentQtyData = equipmentQtyData;
            this.equipmentDataUnsellable = equipmentDataUnsellable;
            this.equipmentQtyDataUnsellable = equipmentQtyDataUnsellable;
            this.materiaData = materiaData;
            this.materiaQtyData = materiaQtyData;
            this.materiaDataUnsellable = materiaDataUnsellable;
            this.materiaQtyDataUnsellable = materiaQtyDataUnsellable;
            this.esperNamesData = esperNamesData;
            this.esperLevelsData = esperLevelsData;
            this.unlockedContent = unlockedContent;
            this.gil = gil;
            this.assetName = assetName;
            this.state = state;
            this.selectedVision = selectedVision;
        }
    }
}
