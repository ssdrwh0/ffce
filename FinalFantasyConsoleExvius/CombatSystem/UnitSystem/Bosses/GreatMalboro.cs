﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    public class GreatMalboro : Enemy
    {
        private List<MiniMalboro> children;

        public GreatMalboro(string name, int lbLevel, int id, int actions, UnitData unitData)
            : base(name, lbLevel, id, actions, unitData)
        {
            children = new List<MiniMalboro>();
        }

        public override void trackAllies(List<Unit> enemies)
        {
            for (int i = 1; i < enemies.Count; i++ )
                children.Add(enemies[i] as MiniMalboro);
        }

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);
            if (unitData.Abilities.Count < 1) return false;

            actions = 8;
            int i = 0;
            string error = "";
            double threshold = HP / MaxHP;

            int resCount = 0;
            if(children[0].CurrentState == UnitState.UNIT_DEAD)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Proliferation (A)"], characters, allies);
                children[0].raiseUnit(children[0].MaxHP, out error);
                children[0].setBoolStat("actionTaken", false);
                actions++;
                resCount++;
            }
            if (children[1].CurrentState == UnitState.UNIT_DEAD)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Proliferation (B)"], characters, allies);
                children[1].raiseUnit(children[0].MaxHP, out error);
                children[1].setBoolStat("actionTaken", false);
                actions++;
                resCount++;
            }
            if (resCount >= 2)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Skin Softened"], characters, allies);
                actions++;
            }
            if (threshold <= 0.5 && thresholds < 1)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Gone Mad"], characters, allies);
                actions++;
                thresholds++;
            }
            if (threshold <= 0.1 && thresholds < 2)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Great Malboro feels weak"], characters, allies);
                actions++;
                thresholds++;
            }

            if (threshold <= 0.5)
            {
                if (CombatMode.enemyTurn % 3 == 0)
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Bad Breath"], characters, allies);
                    actions++;
                    if (resCount < 2)
                    {
                        prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Devour"], characters, allies);
                        actions++;
                        prepareAbility(i++, false, false, getHighestAtkTarget(characters), out error, AssetLoader.abilitiesData["Berserk Touch"], characters, allies);
                        actions++;
                    }
                    
                }
                if (CombatMode.enemyTurn % 4 == 0 && resCount < 2)
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Tentacles Rampage"], characters, allies);
                    actions++;
                }
            }
            else
            {
                if (CombatMode.enemyTurn % 4 == 0)
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Bad Breath"], characters, allies);
                    actions++;
                    if (resCount < 2)
                    {
                        prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Devour"], characters, allies);
                        actions++;
                    }
                }
            }

            prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Tentacles Blow"], characters, allies);
            actions++;
            prepareAbility(i++, false, false, getHighestSprTarget(characters), out error, AssetLoader.abilitiesData["Paralyzing Touch"], characters, allies);
            actions++;
            while (i < actions) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);

            return true;
        }
    }

    public class MiniMalboro : Enemy
    {
        private GreatMalboro father;

        public MiniMalboro(string name, int lbLevel, int id, int actions, UnitData unitData)
            : base(name, lbLevel, id, actions, unitData)
        {

        }

        public override void trackAllies(List<Unit> enemies)
        {
            father = enemies[0] as GreatMalboro;
        }

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);
            if (unitData.Abilities.Count < 1) return false;

            actions = 8;
            int i = 0;
            string error = "";

            if(unitState == UnitState.UNIT_DEAD)
            {
                prepareAbility(0, false, false, 0, out error, AssetLoader.abilitiesData["Proliferation"], characters, allies);
                father.raiseUnit(father.MaxHP, out error);
            }
            else
            {
                if (CombatMode.enemyTurn % 4 == 0)
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Sclerosis"], characters, allies);
                    actions++;
                }
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Acidic Spew"], characters, allies);
                actions++;
                while (i < actions) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
            }

            return true;
        }
    }
}
