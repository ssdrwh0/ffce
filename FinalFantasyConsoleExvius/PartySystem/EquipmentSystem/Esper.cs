﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    public class EsperData
    {
        #region attributes

        //Base Unit Attributes
        private string name;
        private string displayName;
        private string description;
        private List<Passive> passives;
        private List<Ability> abilities;
        private Dictionary<string, double> doubleStats;
        private Dictionary<string, int> intStats;

        public string Name { get { return name; } }
        public string DisplayName { get { return displayName; } }
        public string Description { get { return description; } }

        //Access Modifiers
        public bool containsIntStat(string name)
        {
            foreach (string target in intStats.Keys)
                if (target.Equals(name))
                    return true;
            return false;
        }
        public bool containsDoubleStat(string name)
        {
            foreach (string target in doubleStats.Keys)
                if (target.Equals(name))
                    return true;
            return false;
        }

        public double getDoubleStat(string name)
        {
            if (doubleStats.ContainsKey(name))
                return doubleStats[name];
            return 0.0;
        }
        public int getIntStat(string name)
        {
            if (intStats.ContainsKey(name))
                return intStats[name];
            return 0;
        }
        public IList<Ability> Abilities { get { return abilities.AsReadOnly(); } }
        public IList<Passive> EquipPassives { get { return passives.AsReadOnly(); } }
        #endregion

        public EsperData(string name, string displayName, string description, List<Passive> passives, List<Ability> abilities,
            Dictionary<string, int> intStats, Dictionary<string, double> doubleStats)
        {
            this.name = name;
            this.displayName = displayName;
            this.description = description;
            this.passives = passives;
            this.abilities = abilities;
            this.intStats = intStats;
            this.doubleStats = doubleStats;
        }
    }

    public class Esper
    {
        #region attributes
        //Base Unit Attributes
        private EsperData data;
        private int esperLevel;
        private bool used;

        public string Name { get { return data.Name; } }
        public string DisplayName { get { return data.DisplayName; } }
        public string Description { get { return data.Description; } }

        public bool containsIntStat(string name) { return data.containsIntStat(name); }
        public bool containsDoubleStat(string name) { return data.containsDoubleStat(name); }
        public bool Used { get { return used; } }

        public double getDoubleStat(string name) { return data.getDoubleStat(name); }
        public int getIntStat(string name) { return data.getIntStat(name); }

        public int EsperLevel { get { return esperLevel; } }

        //Base Unit Attributes

        public IList<Ability> Abilities { get { return data.Abilities; } }
        public IList<Passive> EquipPassives { get { return data.EquipPassives; } }
        #endregion

        public Esper(EsperData data, int esperLevel, bool used)
        {
            this.data = data;
            this.used = used;
            this.esperLevel = esperLevel;
        }
    }
}
