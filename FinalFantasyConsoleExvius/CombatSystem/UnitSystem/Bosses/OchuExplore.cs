﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    public class Ochu : Enemy
    {

        public Ochu(string name, int lbLevel, int id, int actions, UnitData unitData) : base(name, lbLevel, id, actions, unitData)
        {

        }

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);
            if (unitData.Abilities.Count < 1) return false;

            string error = "";
            int i = 0;
            bool triggered = false;
            if (HP <= MaxHP * 0.5 && thresholds < 1)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Ochu Dance"], characters, allies);
                thresholds++;
                triggered = true;
            }

            while (i < actions)
            {
                int roll = AssetLoader.random.Next(100);
                if (roll < 20 && !triggered) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Osmose Bind"], characters, allies);
                else if (roll < 55) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Soil Slap"], characters, allies);
                else prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
            }
            return true;
        }
    }

    public class OchuExplore : Enemy
    {

        public OchuExplore(string name, int lbLevel, int id, int actions, UnitData unitData) : base(name, lbLevel, id, actions, unitData)
        {

        }

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);
            if (unitData.Abilities.Count < 1) return false;

            string error = "";
            int i = 0;
            bool triggered = false;
            if (HP <= MaxHP * 0.5 && thresholds < 1)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Ochu Dance"], characters, allies);
                thresholds++;
                triggered = true;
            }
            if (CombatMode.enemyTurn % 3 == 0 && !triggered)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Ochu Dance"], characters, allies);
                triggered = true;
            }

            if (!triggered) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Osmose Bind"], characters, allies);

            while (i < actions)
            {
                int roll = AssetLoader.random.Next(100);
                if(roll < 35) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Soil Slap"], characters, allies);
                else prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
            }
            return true;
        }
    }
}
