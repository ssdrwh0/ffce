﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    public class LoadoutData
    {
        public int id;
        public int level;
        public int lb;
        public double xp;
        public string name;
        public string esperName;
        public int esperLevel;
        public string weaponSlotA;
        public List<string> weaponPassivesA;
        public string weaponSlotB;
        public List<string> weaponPassivesB;
        public List<string> armor;
        public List<string> materia;

        public LoadoutData(int id, int level, int lb, double xp, string name, string esperName, int esperLevel, string weaponSlotA, List<string> weaponPassivesA, string weaponSlotB, 
            List<string> weaponPassivesB, List<string> armor, List<string> materia)
        {
            this.id = id;
            this.level = level;
            this.lb = lb;
            this.xp = xp;
            this.name = name;
            this.esperName = esperName;
            this.esperLevel = esperLevel;
            this.weaponSlotA = weaponSlotA;
            this.weaponPassivesA = weaponPassivesA;
            this.weaponSlotB = weaponSlotB;
            this.weaponPassivesB = weaponPassivesB;
            this.armor = armor;
            this.materia = materia;
        }
    }
}
