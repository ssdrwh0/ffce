﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    public enum CombatState
    {
        START_OF_TURN,
        SELECTING_ABILITIES,
        EXECUTING_ABILITIES,
        END_OF_TURN
    }

    public enum CoverMode
    {
        NO_COVER,
        SINGLE_COVER,
        MULTI_COVER
    }

    public class CombatMode
    {
        public static CombatMode combatMode;
        public static Unit defender;

        public static bool toSpark;
        //public static int frameRate;
        public static int gilEarned;
        public static int currentBattle;
        public static int maxBattles;
        public static int playerTurn;
        public static int enemyTurn;
        public static int Physical_Hits;
        public static int Magical_Hits;
        public static double expGained;

        public static string gameName;
        public static string storyName;
        public static GameMode gameMode;

        public static List<Unit> enemies = new List<Unit>();
        public static List<ActionQueue> activeQueue = new List<ActionQueue>();

        private bool displayStats;
        private bool onPlayerTurn;
        private double totalDamage;

        private static CoverMode coverMode;

        private CombatState previousState;
        public static CombatState combatState;
        private List<EnemySpawnData> dataList;
        private EnemySpawnData data;

        public CombatMode()
        {
            
        }

        public void switchState(CombatState state) { combatState = state; }

        public void resetBattles(GameMode gameMode, string gameName)
        {
            CombatMode.gameMode = gameMode;
            CombatMode.gameName = gameName;
            storyName = gameName;
            dataList = AssetLoader.enemySpawnData[gameName];
            expGained = 0;
            gilEarned = 0;
            currentBattle = 0;
            Party.ep = 0;
            Party.epRefund = 0;
            toSpark = false;

            Party.currentParty = new List<Unit>(Party.players);
            if (Party.selectedVision != null) Party.currentParty.Add(Party.selectedVision);

            foreach (Unit ally in Party.currentParty) ally.resetStats(true);
            maxBattles = dataList.Count;
            loadBattle();

            if (GameEngine.debug) Console.WriteLine("Enemies loaded!\n");
        }

        private void loadBattle()
        {
            foreach (Unit ally in Party.currentParty) ally.resetStats(false);
            enemies.Clear();

            displayStats = true;
            onPlayerTurn = false;
            enemyTurn = 0;
            playerTurn = 0;
            defender = null;
            coverMode = CoverMode.NO_COVER;
            previousState = combatState = CombatState.START_OF_TURN;

            data = dataList[currentBattle];

            string battleLine = "\n=== BATTLE " + (currentBattle + 1).ToString() + " OF " + maxBattles.ToString() + " ===";
            if (currentBattle < 1) Console.WriteLine(data.Announcement + battleLine);
            else Console.WriteLine(battleLine);
            if (data.BossBattle) Console.WriteLine("\n=== BOSS BATTLE!!! ===");
            Console.ReadLine();
            if (data.OnEnemyTurnFirst) onPlayerTurn = true;

            int i = 0;
            foreach (string enemy in data.EnemiesList)
            {
                if(data.EnemyTypesList[i].Equals("")) enemies.Add(new Enemy("", 1, i + 1, data.EnemyActionsList[i], AssetLoader.unitData[enemy]));
                else
                {
                    Type t = Type.GetType("FinalFantasyConsoleExvius." + data.EnemyTypesList[i]);
                    enemies.Add(Activator.CreateInstance(t, "", 1, i + 1, data.EnemyActionsList[i], AssetLoader.unitData[enemy]) as Enemy);  
                }
                expGained += data.EnemyExpList[i];
                gilEarned += data.EnemyGilList[i];
                i++;
            }

            foreach (Enemy enemy in enemies){ enemy.trackAllies(enemies); }

            if (GameEngine.debug) Console.WriteLine("Enemies loaded!\n");
        }

        private bool checkAllFrames()
        {
            int flag = 0;
            foreach (Unit player in Party.currentParty)
                if(player.castCount < player.abilityCommands.Count)
                    flag--;
            foreach (Unit enemy in enemies)
                if (enemy.castCount < enemy.abilityCommands.Count)
                    flag--;
            for (int i = 0; i < activeQueue.Count; i++)
                if (activeQueue[i].currentFrame >= activeQueue[i].frameData.Count)
                    flag++;
            return flag >= activeQueue.Count;
        }

        private void checkStats()
        {
            int i = 0;
            Console.WriteLine("\n-- Player Units --\n");
            Console.WriteLine("... ESPER ORBS: " + Party.ep.ToString() + " ...");
            foreach (Unit character in Party.currentParty)
            {
                character.displayStats(i);
                i++;
            }
            i = 0;
            Console.WriteLine("\n-- Enemy Units --\n");
            foreach (Unit enemy in enemies)
            {
                if (!(enemy.CurrentState == UnitState.UNIT_AIRBORNE || enemy.CurrentState == UnitState.UNIT_HIDING ||
                    enemy.CurrentState == UnitState.UNIT_DEAD || enemy.CurrentState == UnitState.UNIT_REMOVED))
                    enemy.displayStats(i);
                i++;
            }
            Console.WriteLine("");
        }

        private bool checkRemainingUnits(out bool flag)
        {
            flag = false;
            int enemyDeathCount = 0;
            int deathCount = 0;
            foreach (Unit player in Party.currentParty)
            {
                if (player.abilityCommands.Count > 0)
                {
                    player.abilityCommands.Clear();
                    player.castFrame = player.castCount = 0;
                }
                if (player.CurrentState == UnitState.UNIT_DYING && player.getIntStat("undying") == 0)
                    player.setUnitState(UnitState.UNIT_DEAD);
                if (player.CurrentState == UnitState.UNIT_DEAD || player.CurrentState == UnitState.UNIT_REMOVED || player.Petrified)
                    deathCount++;
            }
            foreach (Unit enemy in enemies)
            {
                if (enemy.abilityCommands.Count > 0)
                {
                    enemy.abilityCommands.Clear();
                    enemy.castFrame = enemy.castCount = 0;
                }
                if (enemy.CurrentState == UnitState.UNIT_DYING && enemy.getIntStat("undying") == 0)
                    enemy.setUnitState(UnitState.UNIT_DEAD);
                if (enemy.CurrentState == UnitState.UNIT_DEAD || enemy.CurrentState == UnitState.UNIT_REMOVED || enemy.Petrified)
                    enemyDeathCount++;
            }

            if (deathCount >= Party.currentParty.Count) return true;
            else if (enemyDeathCount >= enemies.Count)
            {
                flag = true;
                return true;
            }
            return false;
        }

        public EngineState run()
        {
            switch (combatState)
            {
                case CombatState.START_OF_TURN: return startOfTurnPhase();
                case CombatState.SELECTING_ABILITIES: return selectingAbilities();
                case CombatState.EXECUTING_ABILITIES: return executingAbilities();
                case CombatState.END_OF_TURN: return endOfTurnPhase();
            }
            return EngineState.COMBAT_STATE;
        }

        private int getTarget(List<Unit> victims)
        {
            int currentTarget = 0, i = 0, remainingTargets = 0;
            List<Unit> available = new List<Unit>();
            foreach (Unit victim in victims)
                if (victim.CurrentState == UnitState.UNIT_ALIVE && !victim.Petrified)
                    remainingTargets++;
            foreach (Unit victim in victims)
            {
                int roll = AssetLoader.random.Next(100);
                int result = 100 + (int)(victim.getDoubleStat("targetChance") + victim.getTotalDoubleStat("targetChance", "") * 100);
                if (victim.CurrentState == UnitState.UNIT_ALIVE && !victim.Petrified && (roll < result || remainingTargets < 2))
                    available.Add(victim);
            }
            if (available.Count > 1) currentTarget = AssetLoader.random.Next(available.Count);
            if (available.Count < 1) return -1;
            i = 0;
            foreach (Unit victim in victims)
            {
                if (available[currentTarget] == victim)
                {
                    currentTarget = i;
                    break;
                }
                i++;
            }
            int provoker = (int)((victims[currentTarget].getDoubleStat("targetChance") + victims[currentTarget].getTotalDoubleStat("targetChance", "")) * 100);
            if (provoker >= 100) return currentTarget;
            i = 0;
            foreach (Unit victim in victims)
            {
                if (i != currentTarget && victim.CurrentState == UnitState.UNIT_ALIVE && !victim.Petrified)
                {
                    int roll = AssetLoader.random.Next(100);
                    provoker = (int)((victim.getDoubleStat("targetChance") + victim.getTotalDoubleStat("targetChance", "")) * 100);
                    if (roll < provoker) return i;
                }
                i++;
            }
            return currentTarget;
        }

        private EngineState startOfTurnPhase()
        {
            onPlayerTurn = !onPlayerTurn;

            bool flag = false;
            if (checkRemainingUnits(out flag)) return endOfGameState(flag);

            if (onPlayerTurn)
            {
                playerTurn++;
                Console.WriteLine("\n=== PLAYER TURN " + playerTurn.ToString() + " ===\n");
                foreach (Unit player in Party.currentParty)
                {
                    player.startOfTurnPhase(playerTurn == 1, enemies, Party.currentParty);
                    //Target Dummy mechanics
                    if (gameMode == GameMode.PRACTICE) player.setDoubleStat("lb", player.LBCost);
                }
                foreach (Unit enemy in enemies)
                {
                    enemy.physicalHitters.Clear();
                    enemy.magicalHitters.Clear();
                }
                if (gameMode == GameMode.PRACTICE) Party.ep = Party.MAX_ESPER_POINTS;
            }
            else
            {
                enemyTurn++;
                Console.WriteLine("\n=== ENEMY TURN " + enemyTurn.ToString() + " ===\n");
                foreach (Unit enemy in enemies) enemy.startOfTurnPhase(enemyTurn == 1, Party.currentParty, enemies);
                foreach (Unit player in Party.currentParty)
                {
                    player.physicalHitters.Clear();
                    player.magicalHitters.Clear();
                }
            }

            defender = null;
            coverMode = CoverMode.NO_COVER;
            previousState = CombatState.START_OF_TURN;
            combatState = checkActionsTaken() ? CombatState.EXECUTING_ABILITIES : CombatState.SELECTING_ABILITIES;
            return EngineState.COMBAT_STATE;
        }

        private bool checkActionsTaken()
        {
            foreach (Unit player in Party.currentParty)
                if (player.abilityCommands.Count > 0)
                    return true;
            foreach (Unit enemy in enemies)
                if (enemy.abilityCommands.Count > 0)
                    return true;
            return false;//inactiveQueue.Count > 0 || activeQueue.Count > 0;
        }

        private EngineState endOfGameMenu()
        {
            Console.WriteLine("\n\"menu\" - Return to menu\n\"exit\" - Quit the game!");
            string input = Console.ReadLine().ToLower();
            if (input.Equals("exit")) return EngineState.FINISHED_STATE;
            else if (input.Equals("menu")) return EngineState.MAIN_MENU;
            else Console.WriteLine("Invalid command!");
            return EngineState.COMBAT_STATE;
        }

        private EngineState endOfGameState(bool winner)
        {
            if (winner)
            {
                if (++currentBattle < maxBattles)
                {
                    loadBattle();
                    return EngineState.COMBAT_STATE;
                }
                else
                {
                    Console.WriteLine("\n=== YOUR PARTY HAS WON! ===\n");
                    int count = Party.players.Count;
                    if (Party.selectedVision != null) count--;
                    switch(data.EndOfBattle)
                    {
                        case "TOWN_STATE":
                            foreach (Character player in Party.currentParty)
                                if (player != Party.selectedVision)
                                    player.gainXp(expGained / count);
                            Party.gil += gilEarned;
                            Console.ReadLine();
                            return EngineState.TOWN_STATE;
                        case "DUNGEON_STATE":
                            //Put XP and Gil count in there in the future
                            Console.ReadLine();
                            return EngineState.DUNGEON_STATE;
                        case "STORY_MODE":
                            Console.WriteLine("\n=== MISSION COMPLETE! ===\n");
                            foreach (Character player in Party.currentParty)
                                if (player != Party.selectedVision)
                                    player.gainXp(expGained / count);
                            Party.gil += gilEarned;
                            Console.ReadLine();
                            storyName = data.StoryName;
                            return EngineState.STORY_STATE;
                        default:
                            return endOfGameMenu();
                    }
                }
            }
            else
            {
                Console.WriteLine("\n=== Game over... :'( ===");
                return endOfGameMenu();
            }
        }

        private EngineState endOfTurnPhase()
        {
            Console.WriteLine("Turn Completed!\n");

            bool flag = false;
            Party.deathCount = 0;
            if (checkRemainingUnits(out flag))
                return endOfGameState(flag);

            if(onPlayerTurn)
            {
                foreach (Unit player in Party.currentParty) player.endOfTurnPhase();
                foreach (Unit enemy in enemies)
                {
                    enemy.resetChain();
                    enemy.endOfTurnCounter(Party.currentParty, enemies);
                }
                //displayStats = true;
            }
            else
            {
                foreach (Unit enemy in enemies) enemy.endOfTurnPhase();
                foreach (Unit player in Party.currentParty)
                {
                    player.resetChain();
                    player.endOfTurnCounter(enemies, Party.currentParty);
                }
            }

            defender = null;
            coverMode = CoverMode.NO_COVER;
            previousState = CombatState.END_OF_TURN;
            combatState = checkActionsTaken() ? CombatState.EXECUTING_ABILITIES : CombatState.START_OF_TURN;
            return EngineState.COMBAT_STATE;
        }

        private EngineState selectingAbilities()
        {
            if(displayStats)
            {
                checkStats();
                displayStats = false;
            }
            bool flag;
            if(onPlayerTurn)
            {
                Console.WriteLine("\nType one of the following options, and then press the enter key:\n\n\"start\" - Begin current turn\n\"menu\"  - Return to main menu\n\"exit\"  - Quit the game!");
                string input = Console.ReadLine().ToLower();
                if (input.Equals("exit")) return EngineState.FINISHED_STATE;
                else if (input.Equals("menu")) return EngineState.MAIN_MENU;
                else if (input.Equals("start"))
                {
                    foreach (Unit character in Party.currentParty)
                    {
                        if ((character.CurrentState == UnitState.UNIT_ALIVE || character.CurrentState == UnitState.UNIT_FALLING) && character.canPerformAction())
                        {
                            if (character.CurrentState == UnitState.UNIT_FALLING)
                                Console.WriteLine("\n=== " + character.Name + " can now take action! ===\n\nType one of the following options, and then press the enter key:\n" +
                                    "\n\"attack\"         - fall down and attack" +
                                    "\n\"skip\"           - take action later during your turn" +
                                    "\n\"change-delay\"   - adjust when a character will trigger their action");
                            else
                                Console.WriteLine("\n=== " + character.Name + " can now take action! ===\n\nType one of the following options, and then press the enter key:\n" +
                                    "\nAbility Name     - type in any ability to use" +
                                    "\n\"attack\"         - deal basic physical damage to an enemy unit" +
                                    "\n\"defend\"         - reduce damage taken by half for this turn" +
                                    "\n\"item\"           - use an item from your inventory" +
                                    "\n\"skip\"           - take action later during your turn" +
                                    "\n\"change-delay\"   - adjust when a character will trigger their action" +
                                    "\n\"display-stats\"  - check enemy and player stats" +
                                    "\n\"list-abilities\" - display available abilities to use" +
                                    "\n\"cooldowns\"      - display abilities with cooldowns counting down");
                            flag = true;
                            while (flag)
                            {
                                input = Console.ReadLine().ToLower();
                                if (input.Equals("display-stats"))
                                {
                                    checkStats();
                                    Console.WriteLine("\nSelect another option for this character!");
                                }
                                else flag = character.takeAction(input, enemies, Party.currentParty);
                            }
                        }
                    }
                    flag = true;
                    while(flag)
                    {
                        Console.WriteLine("\nType one of the following options, and then press the enter key:\n\n\"activate\" - Activate all abilities\n\"cancel\"   - Redo selection for all characters");
                        input = Console.ReadLine().ToLower();
                        if(input.Equals("activate"))
                        {
                            Console.WriteLine("Abilities activated!");
                            displayStats = true;
                            foreach (Unit character in Party.currentParty)
                            {
                                if (character.getBoolStat("readyState")) character.setBoolStat("actionTaken", true);
                                character.castFrame = character.getIntStat("timedDelay");
                                if (CombatMode.gameMode != GameMode.PRACTICE) character.castFrame += character.getIntStat("hitResult");
                                character.castCount = 0;
                            }
                            flag = false;
                            previousState = CombatState.SELECTING_ABILITIES;
                            combatState = CombatState.EXECUTING_ABILITIES;
                        }
                        else if (input.Equals("cancel"))
                        {
                            Console.WriteLine("Abilities cancelled!");
                            foreach (Unit character in Party.currentParty)
                                if (character.getBoolStat("readyState"))
                                {
                                    character.setBoolStat("readyState", false);
                                    double mp = character.getDoubleStat("mp") + character.mpRefund;
                                    if (mp >= character.MaxMP) mp = character.MaxMP;
                                    character.setDoubleStat("mp", mp);
                                    character.setDoubleStat("lb", character.getDoubleStat("lb") + character.lbRefund);
                                    character.mpRefund = 0;
                                    character.lbRefund = 0;
                                    character.abilityCommands.Clear();
                                    //character.setIntStat(character.abilityRefund, 0);
                                }
                            Party.ep += Party.epRefund;
                            flag = false;
                        }
                        else Console.WriteLine("Invalid command!");
                    }
                }
                else Console.WriteLine("Invalid command!");
            }
            else
            {
                Console.WriteLine("Press enter to witness enemy turn combat");
                Console.ReadLine();
                foreach (Unit enemy in enemies)
                    if ((enemy.CurrentState == UnitState.UNIT_ALIVE || enemy.CurrentState == UnitState.UNIT_DYING) && enemy.canPerformAction())
                        enemy.takeAction("", Party.currentParty, enemies);
                displayStats = true;
                previousState = CombatState.SELECTING_ABILITIES;
                combatState = CombatState.EXECUTING_ABILITIES;
            }
            return EngineState.COMBAT_STATE;
        }

        private void checkForCover(Unit victim, List<Unit> coverCandidates, AttackType attackType, TargetType targetType, int hits)
        {
            if (coverMode == CoverMode.MULTI_COVER) return;
            else if (coverMode == CoverMode.SINGLE_COVER && (victim == null || (victim.CurrentState != UnitState.UNIT_ALIVE || victim.Petrified) || victim.guardian != null)) return;
            foreach (Unit candidate in coverCandidates)
            {
                if (candidate.CurrentState != UnitState.UNIT_ALIVE || candidate.Petrified || candidate.Confused || candidate.Sleeping || candidate.Berserked || candidate.Stopped || candidate.Paralysed ||
                    candidate.Charmed || (coverMode == CoverMode.SINGLE_COVER && candidate.hasMultiCover())) continue;
                if (coverMode != CoverMode.SINGLE_COVER && defender == null && candidate.hasMultiCover())
                {
                    if (findCandidate(victim, candidate, CoverMode.MULTI_COVER, attackType, hits))
                        break;
                }
                else if (coverMode != CoverMode.MULTI_COVER && !candidate.hasMultiCover())
                {
                    if (candidate.guardTarget == victim && findCandidate(victim, candidate, CoverMode.SINGLE_COVER, attackType, hits))
                        break;
                }
            }
        }

        private bool findCandidate(Unit target, Unit candidate, CoverMode mode, AttackType attackType, int hits)
        {
            if (target == candidate)
            {
                if (mode == CoverMode.SINGLE_COVER) return false;
                if (mode == CoverMode.MULTI_COVER) hits--;
            }
            if (coverRoll("allCover", hits, candidate.AllCover))
            {
                setCandidate(target, candidate, mode, AIManipulationType.ALL_COVER_CHANCES, " will now take damage for all allies!", " will now take damage for ");
                return true;
            }
            else
            {
                switch (attackType)
                {
                    case AttackType.PHYSICAL_ATTACK:
                        if (coverRoll("physicalCover", hits, candidate.PhysicalCover))
                        {
                            setCandidate(target, candidate, mode, AIManipulationType.PHYSICAL_COVER_CHANCES, " will now take physical damage for all allies!", " will now take physical damage for ");
                            return true;
                        }
                        break;
                    case AttackType.HYBRID_ATTACK:
                        if (coverRoll("physicalCover", hits, candidate.PhysicalCover))
                        {
                            setCandidate(target, candidate, mode, AIManipulationType.PHYSICAL_COVER_CHANCES, " will now take physical damage for all allies!", " will now take physical damage for ");
                            return true;
                        }
                        else if (coverRoll("magicalCover", hits, candidate.MagicalCover))
                        {
                            setCandidate(target, candidate, mode, AIManipulationType.PHYSICAL_COVER_CHANCES, " will now take magical damage for all allies!", " will now take magical damage for ");
                            return true;
                        }
                        break;
                    case AttackType.MAGICAL_ATTACK:
                        if (coverRoll("magicalCover", hits, candidate.MagicalCover))
                        {
                            setCandidate(target, candidate, mode, AIManipulationType.MAGICAL_COVER_CHANCES, " will now take magical damage for all allies!", " will now take magical damage for ");
                            return true;
                        }
                        break;
                }
            }
            return false;
        }

        private bool coverRoll(string coverType, int hits, double chance)
        {
            if (chance < 1e-4) return false;
            int intChance = (int)(chance * 100);
            for (int i = 0; i < hits; i++)
            {
                int roll = AssetLoader.random.Next(100);
                //if (GameEngine.debug) Console.WriteLine("Cover Result: " + roll.ToString() + " and " + intChance.ToString());
                if (roll < intChance) return true;
            }
            return false;
        }

        private void setCandidate(Unit target, Unit candidate, CoverMode mode, AIManipulationType type, string allMessage, string singleMessage)
        {
            if(mode == CoverMode.MULTI_COVER)
            {
                defender = candidate;
                Console.WriteLine(candidate.Name + allMessage);
            }
            else
            {
                target.guardian = candidate;
                Console.WriteLine(candidate.Name + singleMessage + target.Name + "!");
            }
            if(coverMode == CoverMode.NO_COVER) coverMode = mode;
            candidate.currentCover = type;
        }

        private EngineState executingAbilities()
        {
            System.Threading.Thread.Sleep(1000/60);
            //frameRate++;
            toSpark = false;
            if (onPlayerTurn)
            {
                foreach (Unit enemy in enemies)
                    enemy.updateChainFrames();
            }
            else
            {
                foreach (Unit player in Party.currentParty)
                    player.updateChainFrames();
            }
            foreach (Unit player in Party.currentParty) player.castingAbility(enemies, Party.currentParty);
            foreach (Unit enemy in enemies) enemy.castingAbility(Party.currentParty, enemies);
            //if(GameEngine.debug) Console.WriteLine("Time: " + frameRate.Tostring());
            foreach (ActionQueue queue in activeQueue)
            {
                if (queue.currentFrame < queue.frameData.Count && ++queue.currentCastDelay >= queue.checkpoint + queue.frameData[queue.currentFrame] + queue.delay)
                {
                    /*if(GameEngine.debug)
                        Console.WriteLine("Frame Count - " + queue.currentFrame.ToString() + "; Total Frames - " + frameRate.ToString() +
                            "; Checkpoint - " + (queue.checkpoint + queue.frameData[queue.currentFrame]).ToString() +
                            "; Current - " + (queue.checkpoint + queue.frameData[queue.currentFrame] + queue.delay).ToString());*/
                    switch (queue.Type)
                    {
                        case QueueType.ATTACK:
                            if (queue.isPlayer) executeAttackQueue(queue as AttackQueue, enemies, Party.currentParty);
                            else executeAttackQueue(queue as AttackQueue, Party.currentParty, enemies);
                            break;
                        case QueueType.PROTECT:
                            if (queue.isPlayer) executeProtectQueue(queue as ProtectQueue, Party.currentParty);
                            else executeProtectQueue(queue as ProtectQueue, enemies);
                            break;
                        case QueueType.BUFF:
                            if (queue.isPlayer) executeBuffQueue(queue as BuffQueue, Party.currentParty, enemies);
                            else executeBuffQueue(queue as BuffQueue, enemies, Party.currentParty);
                            break;
                        case QueueType.DEBUFF:
                            if (queue.isPlayer) executeDebuffQueue(queue as DebuffQueue, enemies, Party.currentParty);
                            else executeDebuffQueue(queue as DebuffQueue, Party.currentParty, enemies);
                            break;
                    }
                    queue.checkpoint += queue.frameData[queue.currentFrame];
                    queue.currentFrame++;
                }
            }
            if (checkAllFrames())
            {
                if (enemies[0].Name.Equals("Training Dummy")) Console.WriteLine("Total Damage: " + Math.Round(totalDamage).ToString());
                //This makes sure everything is cleared evry time the active queue is used.
                activeQueue.Clear();
                //frameRate = 0;
                totalDamage = 0.0;
                Physical_Hits = 0;
                Magical_Hits = 0;

                //After combat timeline is completed, chain counts and timers should reset
                foreach (Unit player in Party.currentParty) player.resetChain();
                foreach (Unit enemy in enemies) enemy.resetChain();

                bool flag = false;
                if (checkRemainingUnits(out flag)) return endOfGameState(flag);

                if (checkRemainingActions()) combatState = CombatState.SELECTING_ABILITIES;
                else
                {
                    switch(previousState)
                    {
                        case CombatState.START_OF_TURN: combatState = CombatState.SELECTING_ABILITIES; break;
                        case CombatState.END_OF_TURN: combatState = CombatState.START_OF_TURN; break;
                        default: combatState = CombatState.END_OF_TURN; break;
                    }
                }
            }
            return EngineState.COMBAT_STATE;
        }

        private bool checkRemainingActions()
        {
            if (onPlayerTurn)
            {
                foreach (Unit player in Party.currentParty)
                    if (player.CurrentState == UnitState.UNIT_ALIVE && player.canPerformAction())
                        return true;
            }
            else
            {
                foreach (Unit enemy in enemies)
                    if (enemy.CurrentState == UnitState.UNIT_ALIVE && enemy.canPerformAction())
                        return true;
            }
            return false;    
        }

        private void rollForLBAndEP(int lbCrystal, List<Unit> available)
        {
            int winner;
            int roll = AssetLoader.random.Next(100);
            if (onPlayerTurn && roll < 10 && Party.ep < Party.MAX_ESPER_POINTS) Party.ep++;
            
            for (int i = 0; i < lbCrystal; i++)
            {
                roll = AssetLoader.random.Next(100);
                if (roll < 35)
                {
                    winner = AssetLoader.random.Next(available.Count);
                    available[winner].gainLB(1.0);
                }
            }
        }

        private List<string> unlockAbilities(List<string> abilities, List<BuffComponent> buffs)
        {
            if (abilities.Count > 0)
            {
                switch(abilities[0])
                {
                    case "Random Ability":
                        List<string> availableAbilities = new List<string>();
                        foreach(string name in abilities)
                        {
                            if (name.Equals("Random Ability")) continue;
                            bool flag = true;
                            foreach(BuffComponent buff in buffs)
                                if (buff.Unlocks.Count > 0 && buff.Unlocks[0].Equals(name))
                                {
                                    flag = false;
                                    break;
                                }
                            if(flag) availableAbilities.Add(name);
                        }
                        List<string> newAbilities = new List<string>();
                        if (availableAbilities.Count > 0)
                            newAbilities.Add(availableAbilities[AssetLoader.random.Next(availableAbilities.Count)]);
                        return newAbilities;
                }
            }
            return abilities;
        }

        private void attackingTarget(List<Unit> available, AttackQueue attacker, Unit victim, bool allies, bool evade, int id, bool firstFrame)
        {
            int evadeRoll = AssetLoader.random.Next(100);
            bool evadeCheck = false;
            switch(attacker.attackType)
            {
                case AttackType.PHYSICAL_ATTACK:
                    if (evadeRoll < (int)victim.PhysicalEvasion)
                    {
                        if(firstFrame) victim.physicalHitters.Add(id);
                        evadeCheck = true;
                    }
                    break;
                case AttackType.HYBRID_ATTACK:
                    if (evadeRoll < (int)victim.PhysicalEvasion)
                    {
                        if (firstFrame) victim.physicalHitters.Add(id);
                        evadeCheck = true;
                    }
                    break;
                case AttackType.MAGICAL_ATTACK:
                    if (evadeRoll < (int)victim.MagicalEvasion)
                    {
                        if (firstFrame) victim.magicalHitters.Add(id);
                        evadeCheck = true;
                    }
                    break;
            }
            if (evade && victim.CurrentState == UnitState.UNIT_ALIVE && !(victim.Petrified || victim.Sleeping || victim.Stopped || victim.Charmed) && evadeCheck)
                Console.WriteLine(victim.Name + " has evaded an attack!");
            else
            {
                double currentDamage = victim.takeDamage(attacker, id);
                if (!allies && currentDamage > 1e-4)
                {
                    totalDamage += currentDamage;
                    rollForLBAndEP(attacker.lbCrystal, available);
                }
            }
        }

        private void executeAttackQueue(AttackQueue attacker, List<Unit> victims, List<Unit> allies)
        {
            //Add confusion/berserk mechanics
            List<Unit> available = new List<Unit>();
            foreach (Unit ally in allies)
                if (ally.CurrentState == UnitState.UNIT_ALIVE)
                    available.Add(ally);
            bool evade = true;
            int roll = AssetLoader.random.Next(100);

            if (attacker.attackType != AttackType.MAGICAL_ATTACK)
            {
                if (attacker.self.Blinded && roll >= 10)
                {
                    Console.WriteLine(attacker.self.Name + " failed to land a hit!");
                    return;
                }
                roll = AssetLoader.random.Next(100);
                if (roll < attacker.accuracy) evade = false;
            }

            int hits = 1;
            if (attacker.targetType == TargetType.ALL_ENEMIES && victims.Count > 0) hits = victims.Count - 1;

            switch (attacker.targetType)
            {
                case TargetType.ALL_ALLIES: foreach (Unit ally in allies) attackingTarget(available, attacker, ally, true, evade, ally.ChainId - 1, attacker.currentFrame < 1); break;
                case TargetType.SINGLE_ALLY: attackingTarget(available, attacker, allies[attacker.target], true, evade, attacker.target, attacker.currentFrame < 1); break;
                case TargetType.SELF: attackingTarget(available, attacker, attacker.self, true, evade, attacker.self.ChainId - 1, attacker.currentFrame < 1); break;
                case TargetType.ALL_ENEMIES:
                    if (victims.Count > 1) hits = victims.Count - 1;
                    checkForCover(null, victims, attacker.attackType, attacker.targetType, hits);
                    if (defender != null && defender.takingCover(attacker.attackType))
                        for (int i = 0; i < victims.Count; i++)
                            attackingTarget(available, attacker, defender, false, evade, defender.ChainId - 1, attacker.currentFrame < 1);
                    else
                    {
                        foreach (Unit victim in victims)
                        {
                            checkForCover(victim, victims, attacker.attackType, attacker.targetType, hits);
                            if (victim.guardian != null && victim.guardian.takingCover(attacker.attackType))
                                attackingTarget(available, attacker, victim.guardian, false, evade, victim.guardian.ChainId - 1, attacker.currentFrame < 1);
                            else attackingTarget(available, attacker, victim, false, evade, victim.ChainId - 1, attacker.currentFrame < 1);
                        }
                    }  
                    break;
                case TargetType.RANDOM_ENEMY:
                    attacker.target = getTarget(victims);
                    checkForCover(victims[attacker.target], victims, attacker.attackType, attacker.targetType, hits);
                    if (attacker.target < 0)
                    {
                        if (GameEngine.debug) Console.WriteLine("Target can't be found!");
                        break;
                    }
                    else if (defender != null && defender.takingCover(attacker.attackType))
                        attackingTarget(available, attacker, defender, false, evade, defender.ChainId - 1, attacker.currentFrame < 1);
                    else if (victims[attacker.target].guardian != null && victims[attacker.target].guardian.takingCover(attacker.attackType))
                        attackingTarget(available, attacker, victims[attacker.target].guardian, false, evade, victims[attacker.target].guardian.ChainId - 1, attacker.currentFrame < 1);
                    else attackingTarget(available, attacker, victims[attacker.target], false, evade, attacker.target, attacker.currentFrame < 1);
                    break;
                case TargetType.SINGLE_ENEMY:
                    if (attacker.target < 0)
                    {
                        if(GameEngine.debug) Console.WriteLine("Target can't be found!");
                        break;
                    }
                    if (victims[attacker.target].CurrentState == UnitState.UNIT_DEAD || victims[attacker.target].CurrentState == UnitState.UNIT_REMOVED || victims[attacker.target].Petrified)
                        attacker.target = getTarget(victims);
                    checkForCover(victims[attacker.target], victims, attacker.attackType, attacker.targetType, hits);
                    if (attacker.target < 0)
                    {
                        if (GameEngine.debug) Console.WriteLine("Target can't be found!");
                        break;
                    }
                    else if (defender != null && defender.takingCover(attacker.attackType))
                        attackingTarget(available, attacker, defender, false, evade, defender.ChainId - 1, attacker.currentFrame < 1);
                    else if (victims[attacker.target].guardian != null && victims[attacker.target].guardian.takingCover(attacker.attackType))
                        attackingTarget(available, attacker, victims[attacker.target].guardian, false, evade, victims[attacker.target].guardian.ChainId - 1, attacker.currentFrame < 1);
                    else attackingTarget(available, attacker, victims[attacker.target], false, evade, attacker.target, attacker.currentFrame < 1);
                    break;
            }
        }

        private void executeProtectQueue(ProtectQueue protector, List<Unit> allies)
        {
            switch (protector.targetType)
            {
                case TargetType.OTHER_ALLIES:
                    foreach (Unit ally in allies)
                    {
                        if (ally == protector.self) continue;
                        ally.restoreResource(protector);
                    }
                    break;
                case TargetType.ALL_ALLIES: foreach (Unit ally in allies) ally.restoreResource(protector); break;
                case TargetType.OTHER_ALLY: allies[protector.target].restoreResource(protector); break;
                case TargetType.SINGLE_ALLY: allies[protector.target].restoreResource(protector); break;
                case TargetType.SELF: protector.self.restoreResource(protector); break;
            }
        }

        private void newCover(Unit target, Unit self, BuffComponent buff, double regenEffect)
        {
            if (target == self) return;
            double doubleBuff = buff.Effect;
            double doubleScale = buff.Scale;
            double doubleLimitScale = buff.LimitScale;
            if (buff.Sacrifice > 0)
            {
                int sacrificeCount = Party.deathCount;
                if (sacrificeCount > buff.Sacrifice) sacrificeCount = buff.Sacrifice;
                doubleBuff *= sacrificeCount;
                doubleScale *= sacrificeCount;
                doubleLimitScale *= sacrificeCount;
                regenEffect *= sacrificeCount;
            }

            if (regenEffect <= 1e-4) regenEffect = doubleBuff;

            BuffComponent newBuff = new BuffComponent(buff.Name, 0, 0, regenEffect, doubleScale, doubleLimitScale, buff.ActionType, TargetType.SELF, null, buff.Dispelable, buff.MultiCover, buff.Duration,
                buff.Charge, new List<string>(), new List<string>(), buff.BuffType, new List<Race>(), new List<Race>(), new List<MitigationType>(buff.MitigationTypes), new List<ParameterType>(),
                new List<StatusType>(), new List<AIManipulationType>(buff.AIManipulationTypes), new List<ElementalType>(), new List<ScalingType>());

            newBuff.limitBurst = buff.limitBurst;
            self.guardTarget = target;
            for (int i = 0; i < self.buffs.Count; i++)
            {
                bool flag = false;
                foreach (AIManipulationType type in self.buffs[i].AIManipulationTypes)
                    if (type == AIManipulationType.ALL_COVER_CHANCES || type == AIManipulationType.MAGICAL_COVER_CHANCES || type == AIManipulationType.PHYSICAL_COVER_CHANCES)
                    {
                        flag = true;
                        break;
                    }
                if (flag)
                {
                    self.removeBuff(self.buffs[i]);
                    self.buffs.RemoveAt(i);
                    break;
                }
            }
            self.checkBuff(newBuff, regenEffect);
            self.buffs.Add(newBuff);
        }

        private void addNewBuff(Unit target, Unit self, BuffComponent buff, double regenEffect)
        {
            if (target == self) return;
            double doubleBuff = buff.Effect;
            double doubleScale = buff.Scale;
            double doubleLimitScale = buff.LimitScale;
            if (buff.Sacrifice > 0)
            {
                int sacrificeCount = Party.deathCount;
                if (sacrificeCount > buff.Sacrifice) sacrificeCount = buff.Sacrifice;
                doubleBuff *= sacrificeCount;
                doubleScale *= sacrificeCount;
                doubleLimitScale *= sacrificeCount;
                regenEffect *= sacrificeCount;
            }

            if (regenEffect <= 1e-4) regenEffect = doubleBuff;

            BuffComponent newBuff = new BuffComponent(buff.Name, 0, 0, regenEffect, doubleScale, doubleLimitScale, buff.ActionType, buff.TargetType, null, buff.Dispelable, buff.MultiCover, buff.Duration,
                buff.Charge, unlockAbilities(new List<string>(buff.Unlocks), target.buffs), new List<string>(buff.AbilitiesList), buff.BuffType, new List<Race>(buff.PhysicalKillers),
                new List<Race>(buff.MagicalKillers), new List<MitigationType>(buff.MitigationTypes), new List<ParameterType>(buff.ParameterTypes), new List<StatusType>(buff.StatusTypes),
                new List<AIManipulationType>(buff.AIManipulationTypes),  new List<ElementalType>(buff.ElementalTypes), new List<ScalingType>(buff.ScalingTypes));

            newBuff.limitBurst = buff.limitBurst;
            target.checkBuff(newBuff, regenEffect);
            target.checkDamageModifierBuffs();
            if ((buff.Unlocks.Count < 1 || newBuff.Unlocks.Count > 0))
            {
                for (int i = 0; i < target.buffs.Count; i++)
                    if(target.buffs[i].Name.Equals(buff.Name))
                    {
                        target.buffs.RemoveAt(i);
                        break;
                    }
                target.buffs.Add(newBuff);
            }
        }

        private void executeBuffQueue(BuffQueue buffer, List<Unit> allies, List<Unit> enemies)
        {
            BuffComponent buff = buffer.buff;
            IList<AIManipulationType> aiTypes = buff.AIManipulationTypes;
            bool flag = false;
            foreach (AIManipulationType type in aiTypes)
                if (type == AIManipulationType.ALL_COVER_CHANCES || type == AIManipulationType.MAGICAL_COVER_CHANCES || type == AIManipulationType.PHYSICAL_COVER_CHANCES)
                {
                    flag = true;
                    break;
                }
            switch (buff.TargetType)
            {
                case TargetType.ALL_ENEMIES:
                    foreach (Unit enemy in enemies)
                    {
                        if (flag) newCover(null, buffer.self, buff, buffer.regenEffect);
                        else addNewBuff(enemy, buffer.self, buff, buffer.regenEffect);
                    }
                    break;
                case TargetType.ALL_ALLIES:
                    foreach (Unit ally in allies)
                    {
                        if (flag) newCover(null, ally, buff, buffer.regenEffect);
                        else addNewBuff(ally, null, buff, buffer.regenEffect);     
                    }
                    break;
                case TargetType.OTHER_ALLIES:
                    if (flag) newCover(null, buffer.self, buff, buffer.regenEffect);
                    else
                    {
                        foreach (Unit ally in allies)
                            addNewBuff(ally, buffer.self, buff, buffer.regenEffect);  
                    }
                    break;
                case TargetType.OTHER_ALLY:
                    if (flag) newCover(allies[buffer.target], buffer.self, buff, buffer.regenEffect);
                    else addNewBuff(allies[buffer.target], buffer.self, buff, buffer.regenEffect);
                    break;
                case TargetType.SINGLE_ALLY:
                    if (flag) newCover(null, allies[buffer.target], buff, buffer.regenEffect);
                    else addNewBuff(allies[buffer.target], null, buff, buffer.regenEffect);
                    break;
                case TargetType.SINGLE_ENEMY:
                    if (flag) newCover(buffer.self, enemies[buffer.target], buff, buffer.regenEffect);
                    else addNewBuff(enemies[buffer.target], buffer.self, buff, buffer.regenEffect);
                    break;
                case TargetType.SELF:
                    if (flag) newCover(null, buffer.self, buff, buffer.regenEffect);
                    else addNewBuff(buffer.self, null, buff, buffer.regenEffect);
                    break;
            }
            Console.WriteLine("Applied buff component from " + buffer.abilityName + "!");
        }

        private bool addNewDebuff(Unit target, Unit self, DebuffComponent debuff, DebuffQueue queue)
        {
            if (target == self) return false;
            double doubleDebuff = debuff.Effect;
            double doubleScale =  debuff.Scale;
            double doubleLimitScale = debuff.LimitScale;
            if (debuff.Sacrifice > 0)
            {
                int sacrificeCount = Party.deathCount;
                if (sacrificeCount > debuff.Sacrifice) sacrificeCount = debuff.Sacrifice;
                doubleDebuff *= sacrificeCount;
                doubleScale *= sacrificeCount;
                doubleLimitScale *= sacrificeCount;
            }
            DebuffComponent newDebuff = new DebuffComponent(debuff.Name, 0, 0, doubleDebuff, doubleScale, doubleLimitScale, debuff.Ignore, debuff.ActionType, debuff.TargetType, null, debuff.Dispelable,
                debuff.Hex, debuff.Duration, debuff.DebuffType, debuff.AttackType, debuff.DamageType, new List<ParameterType>(debuff.ParameterTypes), new List<StatusType>(debuff.StatusTypes),
                new List<ElementalType>(debuff.ElementalTypes), new List<ScalingType>(debuff.ScalingTypes));
            newDebuff.limitBurst = debuff.limitBurst;
            newDebuff.queue = queue;
            if (target.checkDebuff(newDebuff, false) && !(newDebuff.DebuffType == DebuffType.REMOVE_STATUS || newDebuff.DebuffType == DebuffType.REMOVE_BUFF))
            {
                for (int i = 0; i < target.debuffs.Count; i++)
                    if (target.debuffs[i].Name.Equals(debuff.Name))
                    {
                        target.debuffs.RemoveAt(i);
                        break;
                    }
                target.debuffs.Add(newDebuff);
                return true;
            }
            return false;
        }

        private void executeDebuffQueue(DebuffQueue debuffer, List<Unit> victims, List<Unit> allies)
        {
            DebuffComponent debuff = debuffer.debuff;
            bool flag = false;
            switch (debuff.TargetType)
            {
                case TargetType.OTHER_ALLIES:
                    foreach (Unit ally in allies)
                        if(addNewDebuff(ally, debuffer.self, debuff, debuffer) && !flag)
                            flag = true;
                    break;
                case TargetType.ALL_ALLIES:
                    foreach (Unit ally in allies)
                        if (addNewDebuff(ally, null, debuff, debuffer) && !flag)
                            flag = true;
                    break;
                case TargetType.OTHER_ALLY: flag = addNewDebuff(allies[debuffer.target], debuffer.self, debuff, debuffer); break;
                case TargetType.SINGLE_ALLY: flag = addNewDebuff(allies[debuffer.target], null, debuff, debuffer); break;
                case TargetType.SELF: flag = addNewDebuff(debuffer.self, null, debuff, debuffer); break;
                case TargetType.ALL_ENEMIES:
                    foreach (Unit victim in victims)
                        if (addNewDebuff(victim, null, debuff, debuffer) && !flag)
                            flag = true;
                    break;
                case TargetType.SINGLE_ENEMY:
                    if (debuffer.target < 0)
                    {
                        Console.WriteLine("Target can't be found!");
                        break;
                    }
                    if (victims[debuffer.target].CurrentState != UnitState.UNIT_ALIVE || victims[debuffer.target].Petrified)
                        debuffer.target = getTarget(victims);
                    if (debuffer.target < 0)
                    {
                        Console.WriteLine("Target can't be found!");
                        break;
                    }
                    flag = addNewDebuff(victims[debuffer.target], null, debuff, debuffer);
                    break;
            }
            if(debuff.DebuffType != DebuffType.SCAN_STATS)
            {
                if (flag || debuff.DebuffType == DebuffType.REMOVE_STATUS || debuff.DebuffType == DebuffType.REMOVE_BUFF)
                    Console.WriteLine("Applied debuff component from " + debuffer.abilityName + "!");
                else Console.WriteLine("Failed to apply debuff component from " + debuffer.abilityName + "!");
            }
        }
    }
}
