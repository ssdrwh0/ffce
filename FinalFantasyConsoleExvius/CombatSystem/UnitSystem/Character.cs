﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    public class Vision : Character
    {
        public Vision(string name, int lbLevel, int id, UnitData unitData, Esper esper)
            : base(name, lbLevel, id, unitData, esper)
        {

        }
    }

    public class Character : Unit
    {
        public Character(string name, int lbLevel, int id, UnitData unitData, Esper esper)
            : base(name, lbLevel, id, unitData, esper)
        {
            isPlayer = true;
        }

        private bool useItem(List<Unit> enemies, List<Unit> allies)
        {
            Console.WriteLine("Item Name    - type in any available item to use \n\"list-items\" - display a list of available items to use\n\"cancel\"     - choose another option");
            bool notselected = true;
            bool flag = true;
            string error = "";
            while (flag)
            {
                string input = Console.ReadLine().ToLower();
                if (input.Equals("list-items")) Party.listAvailableItems();
                else if (input.Equals("cancel"))
                {
                    Console.WriteLine("item selection cancelled! Select another option!");
                    flag = false;
                }
                else if (Party.hasItem(input))
                {
                    prepareAbility(0, false, false, 0, out error, Party.getItemAbility(input), enemies, allies);
                    setBoolStat("readyState", true);
                    notselected = false;
                    flag = false;
                }
            }
            return notselected;
        }

        public override bool takeAction(string input, List<Unit> enemies, List<Unit> allies)
        {
            string error = "";
            string output = "";
            if(unitState == UnitState.UNIT_FALLING)
            {
                switch (input)
                {
                    case "attack":
                        string attack = "";
                        foreach (BuffComponent buff in buffs)
                            if (buff.BuffType == BuffType.FALLING)
                            {
                                attack = buff.AbilitiesList[0];
                                break;
                            }
                        prepareAbility(0, false, false, 0, out error, AssetLoader.abilitiesData[attack], enemies, allies);
                        setBoolStat("readyState", true);
                        return false;
                    case "skip":
                        Console.WriteLine(Name + " will wait until other allies have performed their action");
                        return false;
                    case "change-delay":
                        selectTimedDelay();
                        Console.WriteLine("\nSelect another option for this character!");
                        return true;
                }
            }
            else
            {
                switch (input)
                {
                    case "attack":
                        prepareAbility(0, false, false, 0, out error, AssetLoader.abilitiesData["Attack"], enemies, allies);
                        setBoolStat("readyState", true);
                        return false;
                    case "defend":
                        prepareAbility(0, false, false, 0, out error, AssetLoader.abilitiesData["Defend"], enemies, allies);
                        setBoolStat("readyState", true);
                        return false;
                    case "item": return useItem(enemies, allies);
                    case "skip":
                        Console.WriteLine(Name + " will wait until other allies have performed their action");
                        return false;
                    case "change-delay":
                        selectTimedDelay();
                        Console.WriteLine("\nSelect another option for this character!");
                        return true;
                    case "list-abilities":
                        listAbilities(false, false, AbilityType.NORMAL_ABILITY, new List<string>());
                        Console.WriteLine("\nSelect another option for this character!");
                        return true;
                    case "cooldowns":
                        listAbilities(true, false, AbilityType.NORMAL_ABILITY, new List<string>());
                        Console.WriteLine("\nSelect another option for this character!");
                        return true;
                    default:
                        if (checkAbility(input, AbilityType.NORMAL_ABILITY, out output, allies, new List<string>(), out error) > 1)
                        {
                            if (!prepareAbility(0, false, false, 0, out error, AssetLoader.abilitiesData[output], enemies, allies))
                            {
                                Console.WriteLine(error += " Try again!");
                                return true;
                            }
                            else
                            {
                                setBoolStat("readyState", true);
                                return false;
                            }
                        }
                        else
                        {
                            Console.WriteLine(error += " Try again!");
                            return true;
                        }
                }
            }
            return true;
        }

        public void gainXp(double newXp)
        {
            int prevLevel = unitLevel;
            double xp = getDoubleStat("currentXP") + newXp;
            double leftFormula = Math.Pow((double)unitLevel / 99, 2.5);
            double rightFormula = Math.Pow((double)(unitLevel - 1) / 99, 2.5);
            double threshold = 3000000 * (leftFormula - rightFormula);
            if (unitLevel > 79) threshold += 760 * (unitLevel - 79);
            while(xp > threshold)
            {
                xp -= threshold;
                unitLevel++;
                leftFormula = Math.Pow((double)unitLevel / 99, 2.5);
                rightFormula = Math.Pow((double)(unitLevel - 1) / 99, 2.5);
                threshold = 3000000 * (leftFormula - rightFormula);
            }
            if(unitLevel > prevLevel)
                Console.WriteLine(Name + " has leveled up to " + unitLevel.ToString() + "!");
            setDoubleStat("currentXP", xp);
        }

        public virtual void loadCurrentStats(double currentXp, int currentLevel, int currentLb)
        {
            setDoubleStat("currentXP", currentXp);
            unitLevel = currentLevel;
            lbLevel = currentLb;
        }
    }
}
