﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    public class RandomComponent : AbilityComponent
    {
        private List<int> odds;
        private List<string> abilities;

        public IList<int> Odds { get { return odds.AsReadOnly(); } }
        public IList<string> Abilities { get { return abilities.AsReadOnly(); } }

        public RandomComponent(string name, double effect, double scale, double limitScale, ActionType actionType, TargetType targetType, List<int> frameData, List<int> odds, List<string> abilities)
            : base(name, 0, 0, effect, scale, limitScale, actionType, frameData, targetType, new List<ScalingType>(), new List<ElementalType>())
        {
            this.odds = odds;
            this.abilities = abilities;
        }
    }
}
