﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    public class ComboComponent : AbilityComponent
    {
        private AbilityType condition;
        private List<string> list;

        public AbilityType Condition { get { return condition; } }
        public IList<string> List { get { return list.AsReadOnly(); } }


        public int MaxCast { get { return (int) effect; } }

        public ComboComponent(string name, double effect, double scale, double limitScale, ActionType actionType, TargetType targetType, List<int> frameData, AbilityType condition, List<string> list)
            : base(name, 0, 0, effect, scale, limitScale, actionType, frameData, targetType, new List<ScalingType>(), new List<ElementalType>())
        {
            this.condition = condition;
            this.list = list;
        }
    }
}
