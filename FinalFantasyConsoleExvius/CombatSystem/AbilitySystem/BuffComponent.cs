﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    public class BuffComponent : AbilityComponent
    {
        private bool dispelable;
        private bool multiCover;
        private int duration;
        private int charge;
        private BuffType buffType;
        private List<string> unlocks;
        private List<string> abilitiesList;
        private List<AIManipulationType> aiManipulationTypes;
        private List<MitigationType> mitigationTypes;
        private List<ParameterType> parameterTypes;
        private List<StatusType> statusTypes;
        private List<Race> physicalKillers;
        private List<Race> magicalKillers;

        public bool Dispelable { get { return dispelable; } }
        public bool MultiCover { get { return multiCover; } }
        public int Duration { get { return duration; } }
        public int Charge { get { return charge; } }
        public BuffType BuffType { get { return buffType; } }
        public IList<string> Unlocks { get { return unlocks.AsReadOnly(); } }
        public IList<string> AbilitiesList { get { return abilitiesList.AsReadOnly(); } }
        public IList<Race> PhysicalKillers { get { return physicalKillers.AsReadOnly(); } }
        public IList<Race> MagicalKillers { get { return magicalKillers.AsReadOnly(); } }
        public IList<MitigationType> MitigationTypes { get { return mitigationTypes.AsReadOnly(); } }
        public IList<ParameterType> ParameterTypes { get { return parameterTypes.AsReadOnly(); } }
        public IList<StatusType> StatusTypes { get { return statusTypes.AsReadOnly(); } }
        public IList<AIManipulationType> AIManipulationTypes { get { return aiManipulationTypes.AsReadOnly(); } }

        public BuffComponent(string name, int castTime, int sacrifice, double effect, double scale, double limitScale, ActionType actionType, TargetType targetType, List<int> frameData, bool dispelable,
            bool multiCover, int duration, int charge, List<string> unlocks, List<string> abilitiesList, BuffType buffType, List<Race> physicalKillers, List<Race> magicalKillers,
            List<MitigationType> mitigationTypes, List<ParameterType> parameterTypes, List<StatusType> statusTypes, List<AIManipulationType> aiManipulationTypes, List<ElementalType> elementalTypes,
            List<ScalingType> scalingTypes)
            : base(name, castTime, sacrifice, effect, scale, limitScale, actionType, frameData, targetType, scalingTypes, elementalTypes)
        {
            this.dispelable = dispelable;
            this.multiCover = multiCover;
            this.duration = duration;
            this.charge = charge;
            this.buffType = buffType;
            this.unlocks = unlocks;
            this.abilitiesList = abilitiesList;
            this.physicalKillers = physicalKillers;
            this.magicalKillers = magicalKillers;
            this.mitigationTypes = mitigationTypes;
            this.parameterTypes = parameterTypes;
            this.statusTypes = statusTypes;
            this.aiManipulationTypes = aiManipulationTypes;
        }

        public void discharge() { if (charge > 0) charge--; }

        public bool timePassed()
        {
            if (duration < 0) return false;
            if (duration > 0) duration--;
            return (duration < 1 && charge < 1);
        }
    }
}
