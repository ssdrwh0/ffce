﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    public class HeartOfDarkness : Enemy
    {
        /*- Surrender to Chaos
          - Enveloping Darkness
          - Total Oblivion
          - Growing Madness
          - Freeze Time
          - Its body reformed!
          - Its arm has regenerated!*/

        private ChaoticDarkness body;
        private List<ChaoticArm> arms;

        public HeartOfDarkness(string name, int lbLevel, int id, int actions, UnitData unitData)
            : base(name, lbLevel, id, actions, unitData)
        {
            arms = new List<ChaoticArm>();
        }

        public override void resetStats(bool startOfMission)
        {
            base.resetStats(startOfMission);
            unitState = UnitState.UNIT_HIDING;
        }

        public override void trackAllies(List<Unit> enemies)
        {
            body = enemies[1] as ChaoticDarkness;
            for (int i = 2; i < enemies.Count; i++)
                arms.Add(enemies[i] as ChaoticArm);
        }

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);
            if (unitData.Abilities.Count < 1) return false;

            actions = 8;
            int i = 0;
            string error = "";
            double threshold = HP / MaxHP;

            if (--body.ressurectTimer < 1)
            {
                unitState = UnitState.UNIT_HIDING;
                prepareAbility(0, false, false, 0, out error, AssetLoader.abilitiesData["Its body reformed!"], characters, allies);
                body.setDoubleStat("hp", body.MaxHP);
                body.setUnitState(UnitState.UNIT_ALIVE);
                body.ressurectTimer = 3;
            }
            else
            {
                int armCount = 0;
                foreach(ChaoticArm arm in arms)
                {
                    if ((armCount++ < 2 || threshold <= 0.5) && (arm.CurrentState == UnitState.UNIT_DEAD && --arm.ressurectTimer < 1))
                    {
                        //actions++;
                        prepareAbility(0, false, false, 0, out error, AssetLoader.abilitiesData["Its arm has regenerated!"], characters, allies);
                        arm.raiseUnit(arm.MaxHP, out error);
                        arm.ressurectTimer = 3;
                        if (threshold <= 0.5) arm.ressurectTimer = 2;
                    }
                }
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Enveloping Darkness"], characters, allies);
                if (threshold <= 0.5)
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Growing Madness"], characters, allies);
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Freeze Time"], characters, allies);
                    if (CombatMode.enemyTurn % 2 == 0) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Absolute Oblivion"], characters, allies);
                }
                if (CombatMode.enemyTurn % 3 == 0) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Absolute Oblivion"], characters, allies);
                if (threshold <= 0.25) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Surrender to Chaos"], characters, allies);
            }

            return true;
        }
    }

    public class ChaoticDarkness : Enemy
    {
        /*- Total Oblivion
          - Enveloping Darkness
          - Empower Darkness
          - Empower Darkness+
          - Its heart is exposed!
          - Its arm has regenerated!*/

        private HeartOfDarkness heart;
        private List<ChaoticArm> arms;

        public int ressurectTimer;

        public ChaoticDarkness(string name, int lbLevel, int id, int actions, UnitData unitData)
            : base(name, lbLevel, id, actions, unitData)
        {
            arms = new List<ChaoticArm>();
        }

        public override void trackAllies(List<Unit> enemies)
        {
            heart = enemies[0] as HeartOfDarkness;
            for (int i = 2; i < enemies.Count; i++)
                arms.Add(enemies[i] as ChaoticArm);
        }

        public override void resetStats(bool startOfMission)
        {
            base.resetStats(startOfMission);
            ressurectTimer = 3;
        }

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);
            if (unitData.Abilities.Count < 1) return false;

            actions = 8;
            int i = 0;
            string error = "";
            double threshold = heart.HP / heart.MaxHP;

            if (heart.CurrentState == UnitState.UNIT_DEAD) prepareAbility(0, false, false, 0, out error, AssetLoader.abilitiesData["Kill self"], characters, allies);
            else if (unitState == UnitState.UNIT_DYING)
            {
                unitState = UnitState.UNIT_DEAD;
                prepareAbility(0, false, false, 0, out error, AssetLoader.abilitiesData["Its heart is exposed!"], characters, allies);
                heart.setUnitState(UnitState.UNIT_ALIVE);
                heart.setBoolStat("actionTaken", true);
            }
            else
            {
                int armCount = 0;
                foreach (ChaoticArm arm in arms)
                {
                    if ((armCount++ < 2 || threshold <= 0.5) && (arm.CurrentState == UnitState.UNIT_DEAD && --arm.ressurectTimer < 1))
                    {
                        //actions++;
                        prepareAbility(0, false, false, 0, out error, AssetLoader.abilitiesData["Its arm has regenerated!"], characters, allies);
                        arm.raiseUnit(arm.MaxHP, out error);
                        arm.ressurectTimer = 3;
                        if (threshold <= 0.5) arm.ressurectTimer = 2;
                    }
                }
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Enveloping Darkness"], characters, allies);
                if (threshold <= 0.5 && CombatMode.enemyTurn % 2 == 0) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Absolute Oblivion"], characters, allies);
                if (CombatMode.enemyTurn % 3 == 0) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Absolute Oblivion"], characters, allies);
                if (threshold <= 0.5)
                    if (CombatMode.enemyTurn % 2 == 1)
                        prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Empower Darkness+"], characters, allies);
                else
                    if (CombatMode.enemyTurn % 3 == 2)
                        prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Empower Darkness"], characters, allies);
            }

            return true;
        }
    }

    public class ChaoticArm : Enemy
    {
        /*- Crush All
          - Enveloping Darkness
          - Shattering Void
          - Shattering Void+
          - Punch of a thousand souls*/

        private HeartOfDarkness heart;
        private ChaoticDarkness body;

        public int ressurectTimer;

        public ChaoticArm(string name, int lbLevel, int id, int actions, UnitData unitData)
            : base(name, lbLevel, id, actions, unitData)
        {
            
        }

        public override void resetStats(bool startOfMission)
        {
            base.resetStats(startOfMission);
            ressurectTimer = 3;
            if (DataName.Equals("Chaotic Arm C") || DataName.Equals("Chaotic Arm D"))
            {
                setDoubleStat("hp", 0.0);
                unitState = UnitState.UNIT_DEAD;
                ressurectTimer = 2;
            }
        }

        public override void trackAllies(List<Unit> enemies)
        {
            heart = enemies[0] as HeartOfDarkness;
            body = enemies[1] as ChaoticDarkness;
        }

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);
            if (unitData.Abilities.Count < 1) return false;

            actions = 8;
            int i = 0;
            string error = "";
            double threshold = heart.HP / heart.MaxHP;

            if (heart.CurrentState == UnitState.UNIT_DEAD) prepareAbility(0, false, false, 0, out error, AssetLoader.abilitiesData["Kill self"], characters, allies);
            else
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Enveloping Darkness"], characters, allies);
                if (threshold <= 0.5)
                {
                    if (CombatMode.enemyTurn % 2 == 1) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Shattering Void+"], characters, allies);
                    while (i < actions)
                    {
                        int roll = AssetLoader.random.Next(1000);
                        if (roll < 300) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Crush All"], characters, allies);
                        else if (roll < 510) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Punch of a thousand souls"], characters, allies);
                        else prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
                    }
                }
                else
                {
                    if (CombatMode.enemyTurn % 3 == 1) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Shattering Void"], characters, allies);
                    while (i < actions)
                    {
                        int roll = AssetLoader.random.Next(1000);
                        if (roll < 150) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Crush All"], characters, allies);
                        else if (roll < 265) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Punch of a thousand souls"], characters, allies);
                        else prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
                    }
                }
            }

            return true;
        }
    }
}
