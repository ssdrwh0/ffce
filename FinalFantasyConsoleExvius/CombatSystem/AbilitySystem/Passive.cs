﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    public class Passive
    {
        //Unit name
        private string name;
        private string displayName;
        private string description;
        private string condition;
        private string requirement;
        private List<string> doubleStatsName;
        private List<string> doubleStatsCondition;
        private List<double> doubleStatsValue;
        private List<string> intStatsName;
        private List<string> intStatsCondition;
        private List<int> intStatsValue;

        public string Name { get { return name; } }
        public string DisplayName { get { return displayName; } }
        public string Description { get { return description; } }
        public string Condition { get { return condition; } }
        public string Requirement { get { return requirement; } }

        //Base Unit Attributes
        public double getDoubleStat(string name, string type, Unit unit)
        {
            double value = 0;
            int i = 0;
            if (checkCondition(unit))
            {
                foreach (string target in doubleStatsName)
                {
                    if (target.Equals(name))
                    {
                        string localCondition = doubleStatsCondition[i];
                        string localRequirement = type;
                        if (name.Equals("damageModifier"))
                        {
                            if (unit.checkAbilityName(localRequirement) && localCondition.Equals(localRequirement))
                                value += doubleStatsValue[i];
                        }
                        else
                        {
                            if (localCondition.Equals("unitCondition")) localRequirement = this.requirement;
                            if (checkExternalCondition(localCondition, localRequirement, unit))
                                value += doubleStatsValue[i];
                        }
                    }
                    i++;
                }
            }
            return value;
        }

        public double getSimpleDoubleStat(string name)
        {
            int i = 0;
            foreach (string target in doubleStatsName)
            {
                if (target.Equals(name)) return doubleStatsValue[i];
                i++;
            }
            return 0.0;
        }

        public string getDoubleCondition(string name)
        {
            int i = 0;
            foreach (string target in doubleStatsName)
            {
                if (target.Equals(name)) return doubleStatsCondition[i];
                i++;
            }
            return "";
        }

        public int getIntStat(string name, string type, Unit unit)
        {
            int value = 0, i = 0;
            if (checkCondition(unit))
            {
                foreach (string target in intStatsName)
                {
                    if (target.Equals(name))
                    {
                        string currentCondition = intStatsCondition[i];
                        if (name.Equals("startOfTurn") || name.Equals("startOfBattle") || name.Equals("guarding"))
                            currentCondition = "";
                        if (checkExternalCondition(currentCondition, type, unit))
                            value += intStatsValue[i];
                    }
                    i++;
                }
            }
            return value;
        }
        public int getSimpleIntStat(string name)
        {
            int i = 0;
            foreach (string target in intStatsName)
            {
                if (target.Equals(name)) return intStatsValue[i];
                i++;
            }
            return 0;
        }

        public string getIntCondition(string name)
        {
            int i = 0;
            foreach (string target in intStatsName)
            {
                if (target.Equals(name)) return intStatsCondition[i];
                i++;
            }
            return "";
        }

        private bool checkElementType(ElementalType target, List<Weapon> weaponSet)
        {
            foreach (Weapon weapon in weaponSet)
                foreach (ElementalType element in weapon.EquipData.ElementalTypes)
                    if (element == target)
                        return true;
            return false;
        }

        private bool checkEquipType(EquipmentType target, List<Equipment> equipSet)
        {
            foreach (Equipment equip in equipSet)
                if (equip.EquipData.EquipType == target)
                    return true;
            return false;
        }

        private bool checkWeaponType(EquipmentType target, List<Weapon> weaponSet)
        {
            foreach (Weapon weapon in weaponSet)
                if (weapon.EquipData.EquipType == target)
                    return true;
            return false;
        }

        private bool checkExternalCondition(string externalCondition, string externalRequirment, Unit unit)
        {
            switch (externalCondition)
            {
                case "": return true;
                case "jump": return true;
                case "unitCondition":
                    switch (requirement)
                    {
                        case "rainCondition": return unit.Name.Equals("Rain") || unit.Name.Equals("Awakened Rain");
                        case "lasswellCondition": return unit.Name.Equals("Lasswell") || unit.Name.Equals("Pyro Glacial Lasswell");
                        case "finaCondition": return unit.Name.Equals("Fina") || unit.Name.Equals("Lotus Mage Fina");
                        case "lidCondition": return unit.Name.Equals("Lid") || unit.Name.Equals("Heavenly Technician Lid");
                        case "nicholCondition": return unit.Name.Equals("Nichol") || unit.Name.Equals("Maritime Strategist Nichol");
                        case "jakeCondition": return unit.Name.Equals("Jake") || unit.Name.Equals("Nameless Gunner Jake");
                        case "sakuraCondition": return unit.Name.Equals("Sakura") || unit.Name.Equals("Blossom Sage Sakura");
                        default: return unit.Name.Equals(requirement);
                    }
                case "shieldBoost": return (checkWeaponType(EquipmentType.LSHIELD, unit.weaponSet) || checkWeaponType(EquipmentType.HSHIELD, unit.weaponSet)) && externalRequirment == "shieldBoost";
                case "trueDualWield":
                    return unit.weaponSet.Count == 2 && !(checkWeaponType(EquipmentType.LSHIELD, unit.weaponSet) || checkWeaponType(EquipmentType.HSHIELD, unit.weaponSet)) && externalRequirment == "tdwBoost";
                case "trueDoubleHand":
                    return unit.weaponSet.Count == 1 && !(checkWeaponType(EquipmentType.LSHIELD, unit.weaponSet) || checkWeaponType(EquipmentType.HSHIELD, unit.weaponSet)) && externalRequirment == "tdhBoost";
                case "doubleHand":
                    return unit.weaponSet.Count == 1 && !(checkWeaponType(EquipmentType.LSHIELD, unit.weaponSet) || checkWeaponType(EquipmentType.HSHIELD, unit.weaponSet))
                        && !unit.weaponSet[0].EquipData.getBoolStat("two-handed") && externalRequirment == "tdhBoost";
                case "trueUnarmed": return unit.weaponSet.Count == 0 && externalRequirment == "trueUnarmed";
                case "unarmed": return unit.weaponSet.Count == 0;
                case "equipFire": return checkElementType(ElementalType.FIRE, unit.weaponSet);
                case "equipIce": return checkElementType(ElementalType.ICE, unit.weaponSet);
                case "equipThunder": return checkElementType(ElementalType.THUNDER, unit.weaponSet);
                case "equipWater": return checkElementType(ElementalType.WATER, unit.weaponSet);
                case "equipWind": return checkElementType(ElementalType.WIND, unit.weaponSet);
                case "equipEarth": return checkElementType(ElementalType.EARTH, unit.weaponSet);
                case "equipLight": return checkElementType(ElementalType.LIGHT, unit.weaponSet);
                case "equipDark": return checkElementType(ElementalType.DARK, unit.weaponSet);
                case "equipDagger": return checkWeaponType(EquipmentType.SHORTSWORD, unit.weaponSet);
                case "equipSword": return checkWeaponType(EquipmentType.SWORD, unit.weaponSet);
                case "equipGSword": return checkWeaponType(EquipmentType.GREATSWORD, unit.weaponSet);
                case "equipKatana": return checkWeaponType(EquipmentType.KATANA, unit.weaponSet);
                case "equipFist": return checkWeaponType(EquipmentType.FIST, unit.weaponSet);
                case "equipHammer": return checkWeaponType(EquipmentType.HAMMER, unit.weaponSet);
                case "equipAxe": return checkWeaponType(EquipmentType.AXE, unit.weaponSet);
                case "equipMace": return checkWeaponType(EquipmentType.MACE, unit.weaponSet);
                case "equipWhip": return checkWeaponType(EquipmentType.WHIP, unit.weaponSet);
                case "equipGun": return checkWeaponType(EquipmentType.GUN, unit.weaponSet);
                case "equipBow": return checkWeaponType(EquipmentType.BOW, unit.weaponSet);
                case "equipSpear": return checkWeaponType(EquipmentType.SPEAR, unit.weaponSet);
                case "equipThrowing": return checkWeaponType(EquipmentType.THROWING, unit.weaponSet);
                case "equipStaff": return checkWeaponType(EquipmentType.STAFF, unit.weaponSet);
                case "equipRod": return checkWeaponType(EquipmentType.ROD, unit.weaponSet);
                case "equipHarp": return checkWeaponType(EquipmentType.HARP, unit.weaponSet);
                case "equipLShield": return checkWeaponType(EquipmentType.LSHIELD, unit.weaponSet);
                case "equipHShield": return checkWeaponType(EquipmentType.HSHIELD, unit.weaponSet);
                case "equipHat": return checkEquipType(EquipmentType.HAT, unit.equipmentSet);
                case "equipHelm": return checkEquipType(EquipmentType.HELMET, unit.equipmentSet);
                case "equipRobes": return checkEquipType(EquipmentType.ROBES, unit.equipmentSet);
                case "equipClothes": return checkEquipType(EquipmentType.CLOTHES, unit.equipmentSet);
                case "equipLArmor": return checkEquipType(EquipmentType.LIGHT_ARMOR, unit.equipmentSet);
                case "equipHArmor": return checkEquipType(EquipmentType.HEAVY_ARMOR, unit.equipmentSet);
                case "equipWeapon":
                    foreach(Weapon weapon in unit.weaponSet)
                        if(weapon.EquipData.Name.Equals(externalRequirment))
                            return true;
                    return false;
                case "damageModifier": return unit.checkAbilityName(externalRequirment);
                case "dire": return unit.checkHPThreshold(name);
                case "elemental": return true;
                case "HUMAN": return externalCondition.Equals(externalRequirment);
                case "BEAST": return externalCondition.Equals(externalRequirment);
                case "DEMON": return externalCondition.Equals(externalRequirment);
                case "BIRD": return externalCondition.Equals(externalRequirment);
                case "PLANT": return externalCondition.Equals(externalRequirment);
                case "MACHINE": return externalCondition.Equals(externalRequirment);
                case "STONE": return externalCondition.Equals(externalRequirment);
                case "BUG": return externalCondition.Equals(externalRequirment);
                case "SPIRIT": return externalCondition.Equals(externalRequirment);
                case "UNDEAD": return externalCondition.Equals(externalRequirment);
                case "DRAGON": return externalCondition.Equals(externalRequirment);
                case "AQUATIC": return externalCondition.Equals(externalRequirment);
                default: Console.WriteLine("ERROR: Condition does not exist!"); break;
            }
            return false;
        }

        public bool checkCondition(Unit unit)
        {
            switch(condition)
            {
                case "": return true;
                case "hpCondition": return unit.checkHPThreshold(name);
                case "equipCondition": return unit.checkEquipment(requirement);
                case "notEquipCondition": return !unit.checkEquipment(requirement);
                case "unitAliveCondition":
                    foreach (Unit target in Party.currentParty)
                        if (target.Name.Equals(requirement) && target.CurrentState != UnitState.UNIT_DEAD && target.CurrentState != UnitState.UNIT_REMOVED)
                            return true;
                    return false;
                case "unitCondition":
                    switch (requirement)
                    {
                        case "rainCondition": return unit.Name.Equals("Rain") || unit.Name.Equals("Awakened Rain");
                        case "lasswellCondition": return unit.Name.Equals("Lasswell") || unit.Name.Equals("Pyro Glacial Lasswell");
                        case "finaCondition": return unit.Name.Equals("Fina") || unit.Name.Equals("Lotus Mage Fina");
                        case "lidCondition": return unit.Name.Equals("Lid") || unit.Name.Equals("Heavenly Technician Lid");
                        case "nicholCondition": return unit.Name.Equals("Nichol") || unit.Name.Equals("Maritime Strategist Nichol");
                        case "jakeCondition": return unit.Name.Equals("Jake") || unit.Name.Equals("Nameless Gunner Jake");
                        case "sakuraCondition": return unit.Name.Equals("Sakura") || unit.Name.Equals("Blossom Sage Sakura");
                        default: return unit.Name.Equals(requirement);
                    }
                default: Console.WriteLine("ERROR: Condition does not exist!"); break;
            }
            return false;
        }

        public Passive(string name, string displayName, string description, string condition, string requirement, List<string> doubleStatsName, List<string> doubleStatsCondition,
            List<double> doubleStatsValue, List<string> intStatsName, List<string> intStatsCondition, List<int> intStatsValue)
        {
            this.name = name;
            this.displayName = displayName;
            this.description = description;
            this.condition = condition;
            this.requirement = requirement;
            this.doubleStatsName = doubleStatsName;
            this.doubleStatsCondition = doubleStatsCondition;
            this.doubleStatsValue = doubleStatsValue;
            this.intStatsName = intStatsName;
            this.intStatsCondition = intStatsCondition;
            this.intStatsValue = intStatsValue;
        }
    }
}
