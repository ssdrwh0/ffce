﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    public abstract class AbilityComponent
    {
        public double limitBurst;

        protected int castTime;
        protected int sacrifice;

        protected double effect;
        protected double scale;
        protected double limitScale;

        protected string name;

        protected List<int> frameData;
        protected ActionType actionType;
        protected TargetType targetType;
        protected List<ScalingType> scalingTypes;
        protected List<ElementalType> elementalTypes;

        public int CastTime { get { return castTime; } }
        public int Sacrifice { get { return sacrifice; } }
        public double Effect { get { return effect; } }
        public double Scale { get { return scale; } }
        public double LimitScale { get { return limitScale; } }
        public string Name { get { return name; } }
        public IList<int> FrameData { get { return frameData.AsReadOnly(); } }
        public ActionType ActionType { get { return actionType; } }
        public TargetType TargetType { get { return targetType; } }
        public IList<ScalingType> ScalingTypes { get { return scalingTypes.AsReadOnly(); } }
        public IList<ElementalType> ElementalTypes { get { return elementalTypes.AsReadOnly(); } }

        public AbilityComponent(string name, int castTime, int sacrifice, double effect, double scale, double limitScale, ActionType actionType, List<int> frameData, TargetType targetType,
            List<ScalingType> scalingTypes, List<ElementalType> elementalTypes)
        {
            this.name = name;
            this.castTime = castTime;
            this.sacrifice = sacrifice;
            this.effect = effect;
            this.scale = scale;
            this.limitScale = limitScale;
            this.actionType = actionType;
            this.frameData = frameData;
            this.targetType = targetType;
            this.scalingTypes = scalingTypes;
            this.elementalTypes = elementalTypes;
        }
    }
}
