﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    public enum TownState {
        TOWN_MENU,
        TOWN_SHOP,
        TOWN_TALK,
        TOWN_PARTY,
        TOWN_SAVE,
        TOWN_MOVE
    };

    public class TownMode
    {
        public static TownMode townMode;
        public static GameMode gameMode;
        public static string gameName;

        private TownData data;
        private TownState townState;
        private bool displayMenu;

        public TownMode()
        {
            
        }

        public void resetTown(GameMode gameMode, string gameName)
        {
            TownMode.gameMode = gameMode;
            TownMode.gameName = gameName;
            townState = TownState.TOWN_MENU;
            displayMenu = true;

            data = AssetLoader.townData[gameName];
        }

        public EngineState run()
        {
            switch (townState)
            {
                case TownState.TOWN_MENU: return townMenuFunction();
                case TownState.TOWN_SHOP: return townShopFunction();
                case TownState.TOWN_TALK: return townTalkFunction();
                case TownState.TOWN_PARTY: return townPartyFunction();
                case TownState.TOWN_SAVE: return townSaveFunction();
                case TownState.TOWN_MOVE: return townMoveFunction();
            }
            return EngineState.TOWN_STATE;
        }

        private EngineState townMenuFunction()
        {
            if (displayMenu)
            {
                displayMenu = false;
                Console.WriteLine("=== " + data.Name + " ===\n\nType one of the following options, and then press the enter key:\n\n\"shop\"  - Buy and sell items\n\"talk\"  - Talk to people from this town" +
                    "\n\"party\" - Customise your party\n\"save\"  - Save your current game progress\n\"move\"  - Move party to other places\n\"menu\"  - Return to main menu");
            }
            string input = Console.ReadLine().ToLower();
            displayMenu = true;
            switch (input)
            {
                case "shop": townState = TownState.TOWN_SHOP; break;
                case "talk": townState = TownState.TOWN_TALK; break;
                case "party": townState = TownState.TOWN_PARTY; break;
                case "save": townState = TownState.TOWN_SAVE; break;
                case "move": townState = TownState.TOWN_MOVE; break;
                case "menu": return EngineState.MAIN_MENU;
                default:
                    Console.WriteLine("\nERROR: Please type in a valid command!\n");
                    displayMenu = false;
                    break;
            }
            return EngineState.TOWN_STATE;
        }

        private EngineState townShopFunction()
        {
            if (displayMenu)
            {
                displayMenu = false;
                string displayScreen = "Type one of the following options, and then press the enter key:\n";
                int total = data.ShopItems.Count;
                for (int i = 0; i < total; i++)
                {
                    string itemName = data.ShopItems[i];
                    if(AssetLoader.itemsData.Keys.Contains(itemName))
                    {
                        ItemData itemData = AssetLoader.itemsData[itemName];
                        displayScreen += "\n#" + i.ToString() + " - " + itemName + ": " + itemData.GilCost + "g";
                    }
                    else if (AssetLoader.equipData.Keys.Contains(itemName))
                    {
                        EquipmentData equipData = AssetLoader.equipData[itemName];
                        displayScreen += "\n#" + i.ToString() + " - " + itemName + ": " + equipData.GilCost + "g";
                    }
                    else if (AssetLoader.materiaData.Keys.Contains(itemName))
                    {
                        MateriaData materiaData = AssetLoader.materiaData[itemName];
                        displayScreen += "\n#" + i.ToString() + " - " + itemName + ": " + materiaData.GilCost + "g";
                    }
                }
                displayScreen += "\n\nor type in \"back\" to return to the main menu\n\nShop function: coming soon!!!";
                Console.WriteLine(displayScreen);
            }
            string input = Console.ReadLine().ToLower();
            displayMenu = true;
            townState = TownState.TOWN_MENU;
            /*switch (input)
            {
                case "shop": townState = TownState.TOWN_MENU; break;
                case "talk": townState = TownState.TOWN_TALK; break;
                case "party": townState = TownState.TOWN_PARTY; break;
                case "save": townState = TownState.TOWN_SAVE; break;
                case "move": townState = TownState.TOWN_MOVE; break;
                case "menu": return EngineState.MAIN_MENU;
                default:
                    Console.WriteLine("\nERROR: Please type in a valid command!\n");
                    displayMenu = false;
                    break;
            }*/
            return EngineState.TOWN_STATE;
        }

        private EngineState townTalkFunction()
        {
            bool flag = true;
            int target = 0;

            string displayScreen = "Type one of the following options, and then press the enter key:\n";
            int total = data.getTalkDataSize();
            for (int i = 0; i < total; i++) displayScreen += "\n#" + i.ToString() + " - " + data.getTalkDataKey(i);
            displayScreen += "\n\nor type in \"back\" to return to the main menu";
            Console.WriteLine(displayScreen);

            while (flag)
            {
                string input = Console.ReadLine();
                if (input.ToLower().Equals("back")) townState = TownState.TOWN_MENU;
                else if (int.TryParse(input, out target))
                {
                    if (target >= 0 && target < total) flag = false;
                    else Console.WriteLine("Invalid target!");
                }
                else Console.WriteLine("Please type in an integer between 0 and " + total.ToString() + "!");
            }
            StoryMode.storyMode.beginScene(AssetLoader.storyData[data.getTalkDataValue(target)], "Start");
            EngineState state = EngineState.STORY_STATE;
            while (state == EngineState.STORY_STATE) state = StoryMode.storyMode.run();
            return EngineState.TOWN_STATE;
        }

        private EngineState townPartyFunction()
        {
            if (displayMenu)
            {
                displayMenu = false;
                Console.WriteLine("Party function: coming soon!!!");
            }
            string input = Console.ReadLine().ToLower();
            displayMenu = true;
            townState = TownState.TOWN_MENU;
            /*switch (input)
            {
                case "shop": townState = TownState.TOWN_MENU; break;
                case "talk": townState = TownState.TOWN_TALK; break;
                case "party": townState = TownState.TOWN_PARTY; break;
                case "save": townState = TownState.TOWN_SAVE; break;
                case "move": townState = TownState.TOWN_MOVE; break;
                case "menu": return EngineState.MAIN_MENU;
                default:
                    Console.WriteLine("\nERROR: Please type in a valid command!\n");
                    displayMenu = false;
                    break;
            }*/
            return EngineState.TOWN_STATE;
        }

        private EngineState townSaveFunction()
        {
            Console.WriteLine("Do you wish to save progress?");
            bool flag = true;
            while (flag)
            {
                string input = Console.ReadLine().ToLower();
                if (input.Equals("yes") | input.Equals("y"))
                {
                    EngineState state = EngineState.TOWN_STATE;
                    int target = -1;
                    if (Party.saves.Count > 0)
                    {
                        bool flag2 = true;
                        while (flag2)
                        {
                            Console.WriteLine("Use an existing save between 0 and " + (Party.saves.Count - 1).ToString() + ", or type in " + Party.saves.Count.ToString() +
                                " to create a new save!");
                            if (int.TryParse(Console.ReadLine(), out target))
                            {
                                if (target >= 0 && target <= Party.saves.Count) flag2 = false;
                                else Console.WriteLine("Invalid target!");
                            }
                            else Console.WriteLine("Please type in an integer between 0 and " + Party.saves.Count.ToString() + "!");
                        }
                    }
                    Party.save(target, TownMode.gameName, state);
                    Console.WriteLine("Progress has been saved!");
                    Console.ReadLine();
                    flag = false;
                }
                else if (input.Equals("no") | input.Equals("n")) flag = false;
                else Console.WriteLine("Answer yes or no!");
            }
            townState = TownState.TOWN_MENU;
            return EngineState.TOWN_STATE;
        }

        private EngineState townMoveFunction()
        {
            if (displayMenu)
            {
                displayMenu = false;
                Console.WriteLine("Move function: coming soon!!!");
            }
            string input = Console.ReadLine().ToLower();
            displayMenu = true;
            townState = TownState.TOWN_MENU;
            /*switch (input)
            {
                case "shop": townState = TownState.TOWN_MENU; break;
                case "talk": townState = TownState.TOWN_TALK; break;
                case "party": townState = TownState.TOWN_PARTY; break;
                case "save": townState = TownState.TOWN_SAVE; break;
                case "move": townState = TownState.TOWN_MOVE; break;
                case "menu": return EngineState.MAIN_MENU;
                default:
                    Console.WriteLine("\nERROR: Please type in a valid command!\n");
                    displayMenu = false;
                    break;
            }*/
            return EngineState.TOWN_STATE;
        }
    }
}
