﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    public class Oberon : Enemy
    {

        public Oberon(string name, int lbLevel, int id, int actions, UnitData unitData) : base(name, lbLevel, id, actions, unitData)
        {
            
        }

        public override void setUnitState(UnitState state)
        {
            unitState = state;
            if (unitState == UnitState.UNIT_DEAD)
                displayDialogue("oberonDefeat");
        }

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);
            if (unitData.Abilities.Count < 1) return false;

            actions = 8;
            if (HP <= MaxHP * 0.5) actions += 3;
            int i = 0;
            string error = "";

            if (CombatMode.enemyTurn == 1)
            {
                displayDialogue("oberonIntro");
            }
            else
            {
                while (i < actions) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
            }
            return true;
        }
    }
}
