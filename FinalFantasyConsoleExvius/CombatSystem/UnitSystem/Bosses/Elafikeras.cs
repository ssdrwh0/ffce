﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    public class Elafikeras : Enemy
    {

        public Elafikeras(string name, int lbLevel, int id, int actions, UnitData unitData) : base(name, lbLevel, id, actions, unitData)
        {
            
        }

        /*public override void resetStats()
        {
            base.resetStats();
            recovering = false;
            thresholds = 0;
        }*/

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);
            if (unitData.Abilities.Count < 1) return false;

            actions = 8;
            int i = 0;
            string error = "";

            if (CombatMode.enemyTurn == 1) prepareAbility(0, false, false, 0, out error, AssetLoader.abilitiesData["Sparkling Horn"], characters, allies);
            else if (CombatMode.enemyTurn >= 8)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Supersonic Thanatos"], characters, allies);
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["The giant horn drains all!"], characters, allies);
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["The giant horn drains all!"], characters, allies);
            }
            else
            {
                if (HP <= MaxHP * 0.75 && thresholds < 1)
                {
                    thresholds++;
                    if (CombatMode.Magical_Hits > 0) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Multi Horn"], characters, allies);
                    else prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Demon's Stone"], characters, allies);
                }
                if (HP <= MaxHP * 0.5 && thresholds < 2)
                {
                    thresholds++;
                    if (CombatMode.Magical_Hits > 0) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Multi Horn"], characters, allies);
                    else prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Demon's Stone"], characters, allies);
                }
                if (HP <= MaxHP * 0.3 && thresholds < 3)
                {
                    thresholds++;
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Menacing Stampede"], characters, allies);
                }
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Charge"], characters, allies);
            }
            return true;
        }
    }

    public class ElafikerasPlus : Enemy
    {

        public ElafikerasPlus(string name, int lbLevel, int id, int actions, UnitData unitData)
            : base(name, lbLevel, id, actions, unitData)
        {

        }

        /*public override void resetStats()
        {
            base.resetStats();
            recovering = false;
            thresholds = 0;
        }*/

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);
            if (unitData.Abilities.Count < 1) return false;

            actions = 8;
            int i = 0;
            string error = "";

            if (CombatMode.enemyTurn == 1) prepareAbility(0, false, false, 0, out error, AssetLoader.abilitiesData["Sparkling Horn+"], characters, allies);
            else if (CombatMode.enemyTurn >= 10)
            {
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Supersonic Thanatos"], characters, allies);
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["The giant horn drains all!"], characters, allies);
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["The giant horn drains all!"], characters, allies);
            }
            else
            {
                if (HP <= MaxHP * 0.75 && thresholds < 1)
                {
                    thresholds++;
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Multi Horn"], characters, allies);
                }
                if (HP <= MaxHP * 0.5 && thresholds < 2)
                {
                    thresholds++;
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Multi Horn"], characters, allies);
                }
                if (HP <= MaxHP * 0.3 && thresholds < 3)
                {
                    thresholds++;
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Multi Horn"], characters, allies);
                }
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Charge"], characters, allies);
            }
            return true;
        }
    }
}
