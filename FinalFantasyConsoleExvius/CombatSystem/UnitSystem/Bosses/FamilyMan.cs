﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    public class FamilyMan : Enemy
    {
        private bool bothDead;
        private bool daughterDead;
        private bool sonDead;
        private Enemy son;
        private Enemy daughter;

        public FamilyMan(string name, int lbLevel, int id, int actions, UnitData unitData) : base(name, lbLevel, id, actions, unitData)
        {
            bothDead = false;
            daughterDead = false;
            sonDead = false;
        }

        public override void trackAllies(List<Unit> enemies)
        {
            this.son = enemies[1] as Enemy;
            this.daughter = enemies[2] as Enemy;
        }

        /*protected override void displayDialogue(string name)
        {
            
        }
        
        public override void resetStats()
        {
            base.resetStats();
            recovering = false;
            thresholds = 0;
        }*/

        public override void setUnitState(UnitState state)
        {
            unitState = state;
            if (unitState == UnitState.UNIT_DEAD)
                displayDialogue("familyManDefeat");
        }

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);
            if (unitData.Abilities.Count < 1) return false;

            string error = "";
            int roll;
            actions = 12;
            int i = 0;

            if ((son == null || son.CurrentState == UnitState.UNIT_DEAD) && (daughter == null || daughter.CurrentState == UnitState.UNIT_DEAD) && !bothDead)
            {
                bothDead = true;
                displayDialogue("familyManBothDead");
            }
            else if ((son == null || son.CurrentState == UnitState.UNIT_DEAD) && !sonDead && !bothDead)
            {
                sonDead = true;
                displayDialogue("familyManBoyDead");
            }
            else if ((daughter == null || daughter.CurrentState == UnitState.UNIT_DEAD) && !daughterDead && !bothDead)
            {
                daughterDead = true;
                displayDialogue("familyManGirlDead");
            }
            if (CombatMode.enemyTurn == 1)
            {
                displayDialogue("familyManIntro");
                prepareAbility(0, false, false, 0, out error, AssetLoader.abilitiesData["Overprotective Nature"], characters, allies);
            }
            else if ((son == null || son.CurrentState == UnitState.UNIT_DEAD) && (daughter == null || daughter.CurrentState == UnitState.UNIT_DEAD))
            {
                actions += 6;
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["I'LL KILL YOU ALL!!!"], characters, allies);
                if (CombatMode.enemyTurn % 2 == 0) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Furious Inferno"], characters, allies);
                else prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Demon's Revenge"], characters, allies);
                if (HP <= MaxHP * 0.8 && thresholds < 1)
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Meltdown"], characters, allies);
                    thresholds++;
                }
                if (HP <= MaxHP * 0.6 && thresholds < 2)
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Meltdown"], characters, allies);
                    thresholds++;
                }
                if (HP <= MaxHP * 0.4 && thresholds < 3)
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Meltdown"], characters, allies);
                    thresholds++;
                }
                if (HP <= MaxHP * 0.2 && thresholds < 4)
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Meltdown"], characters, allies);
                    thresholds++;
                }
                while (i < actions)
                {
                    roll = AssetLoader.random.Next(100);
                    if (roll < 30) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Enraged Strike"], characters, allies);
                    else prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
                }
            }
            else if (son == null || son.CurrentState == UnitState.UNIT_DEAD)
            {
                if (recovering)
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Family man recovered from his blackout!"], characters, allies);
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["How dare you kill my boy?!"], characters, allies);
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Overprotective Nature"], characters, allies);
                    if (CombatMode.enemyTurn % 3 == 0) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Fiery Aura"], characters, allies);
                    while (i < actions)
                    {
                        roll = AssetLoader.random.Next(100);
                        if (roll < 30) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Furious Strike"], characters, allies);
                        else prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
                    }
                    recovering = false;
                }
                else if ((limitHitters.Count > 0 || esperHitters.Count > 0) && !recovering)
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Family man got knocked out!"], characters, allies);
                    recovering = true;
                }
                else
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["How dare you kill my boy?!"], characters, allies);
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Overprotective Nature"], characters, allies);
                    if (CombatMode.enemyTurn % 3 == 0) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Fiery Aura"], characters, allies);
                    while (i < actions)
                    {
                        roll = AssetLoader.random.Next(100);
                        if (roll < 30) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Furious Strike"], characters, allies);
                        else prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
                    }
                }
            }
            else if (daughter == null || daughter.CurrentState == UnitState.UNIT_DEAD)
            {
                if (recovering)
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Family man recovered from his blackout!"], characters, allies);
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["She's not ready to meet her mother again!!"], characters, allies);
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Overprotective Nature"], characters, allies);
                    if (CombatMode.enemyTurn % 3 == 0) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Witch's Revenge"], characters, allies);
                    while (i < actions)
                    {
                        roll = AssetLoader.random.Next(100);
                        if (roll < 30) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Furious Strike"], characters, allies);
                        else prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
                    }
                    recovering = false;
                }
                else if ((limitHitters.Count > 0 || esperHitters.Count > 0) && !recovering)
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Family man got knocked out!"], characters, allies);
                    recovering = true;
                }
                else
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["She's not ready to meet her mother again!!"], characters, allies);
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Overprotective Nature"], characters, allies);
                    if (CombatMode.enemyTurn % 3 == 0) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Witch's Revenge"], characters, allies);
                    while (i < actions)
                    {
                        roll = AssetLoader.random.Next(100);
                        if (roll < 30) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Furious Strike"], characters, allies);
                        else prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
                    }
                }
            }
            else
            {
                if (recovering)
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Family man recovered from his blackout!"], characters, allies);
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Overprotective Nature"], characters, allies);
                    while (i < actions)
                    {
                        prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
                    }
                    recovering = false;
                }
                else if ((limitHitters.Count > 0 || esperHitters.Count > 0) && !recovering)
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Family man got knocked out!"], characters, allies);
                    recovering = true;
                }
                else
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Overprotective Nature"], characters, allies);
                    while (i < actions)
                    {
                        roll = AssetLoader.random.Next(100);
                        if (roll < 35) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Fearful Strike"], characters, allies);
                        else prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
                    }
                }
            }
            return true;
        }
    }

    public class SpoiltBoy : Enemy
    {
        private Enemy father;
        private Enemy sister;

        public SpoiltBoy(string name, int lbLevel, int id, int actions, UnitData unitData) : base(name, lbLevel, id, actions, unitData)
        {
            
        }

        public override void trackAllies(List<Unit> enemies)
        {
            this.father = enemies[0] as Enemy;
            this.sister = enemies[2] as Enemy;
        }

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);

            if (unitData.Abilities.Count < 1) return false;

            int roll;
            string error = "";
            actions = 12;
            int i = 0;

            if (CombatMode.enemyTurn == 1) prepareAbility(0, false, false, 0, out error, AssetLoader.abilitiesData["Throw Manure"], characters, allies);
            else if (sister.CurrentState == UnitState.UNIT_DEAD)
            {
                if (HP <= MaxHP * 0.75 && thresholds < 1)
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Throw Dead Malboro"], characters, allies);
                    thresholds++;
                }
                if (HP <= MaxHP * 0.5 && thresholds < 2)
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Throw Dead Malboro"], characters, allies);
                    thresholds++;
                }
                if (HP <= MaxHP * 0.25 && thresholds < 3)
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Throw Dead Malboro"], characters, allies);
                    thresholds++;
                }
                if (father.isRecovering()) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Pyromaniac"], characters, allies);
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Throw Manure"], characters, allies);
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Throw Sand"], characters, allies);
                while (i < actions)
                {
                    roll = AssetLoader.random.Next(100);
                    if (roll < 25) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Throw Rock"], characters, allies);
                    else prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
                }
            }
            else
            {
                roll = AssetLoader.random.Next(2);
                if (CombatMode.enemyTurn % 4 == 0) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Candy Barrage+"], characters, allies);
                else if (CombatMode.enemyTurn % 2 == 0) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Candy Barrage"], characters, allies);
                if (HP <= MaxHP * 0.75 && thresholds < 1)
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Throw Dead Malboro"], characters, allies);
                    thresholds++;
                }
                if (HP <= MaxHP * 0.5 && thresholds < 2)
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Throw Dead Malboro"], characters, allies);
                    thresholds++;
                }
                if (HP <= MaxHP * 0.25 && thresholds < 3)
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Throw Dead Malboro"], characters, allies);
                    thresholds++;
                }
                if (roll < 1) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Throw Manure"], characters, allies);
                else prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Throw Sand"], characters, allies);
                while (i < actions)
                {
                    roll = AssetLoader.random.Next(100);
                    if (roll < 15) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Throw Rock"], characters, allies);
                    else prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
                }
            }
            return true;
        }
    }

    public class SpitefulGirl : Enemy
    {
        private Enemy father;
        private Enemy brother;

        public SpitefulGirl(string name, int lbLevel, int id, int actions, UnitData unitData) : base(name, lbLevel, id, actions, unitData)
        {

        }

        public override void trackAllies(List<Unit> enemies)
        {
            this.father = enemies[0] as Enemy;
            this.brother = enemies[1] as Enemy;
        }

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);

            string error = "";
            int roll;
            actions = 12;
            int i = 0;

            if (unitData.Abilities.Count < 1) return false;

            if (CombatMode.enemyTurn == 1) prepareAbility(0, false, false, 0, out error, AssetLoader.abilitiesData["Spiteful Stare"], characters, allies);
            else if (brother.CurrentState == UnitState.UNIT_DEAD)
            {
                if (HP <= MaxHP * 0.75 && thresholds < 1)
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["I didn't do anything wrong!"], characters, allies);
                    thresholds++;
                }
                if (HP <= MaxHP * 0.5 && thresholds < 2)
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["I didn't do anything wrong!"], characters, allies);
                    thresholds++;
                }
                if (HP <= MaxHP * 0.25 && thresholds < 3)
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["I didn't do anything wrong!"], characters, allies);
                    thresholds++;
                }
                if (CombatMode.enemyTurn % 2 == 0) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Fearful Gaze"], characters, allies);
                if (CombatMode.enemyTurn % 3 == 0) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Witch's Gaze"], characters, allies);
                while (i < actions)
                    prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
            }
            else
            {
                roll = AssetLoader.random.Next(2);
                if (CombatMode.enemyTurn % 4 == 0) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Candy Barrage+"], characters, allies);
                else if (CombatMode.enemyTurn % 2 == 0) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Candy Barrage"], characters, allies);
                if (HP <= MaxHP * 0.75 && thresholds < 1)
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["I didn't do anything wrong!"], characters, allies);
                    thresholds++;
                }
                if (HP <= MaxHP * 0.5 && thresholds < 2)
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["I didn't do anything wrong!"], characters, allies);
                    thresholds++;
                }
                if (HP <= MaxHP * 0.25 && thresholds < 3)
                {
                    prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["I didn't do anything wrong!"], characters, allies);
                    thresholds++;
                }
                if (CombatMode.enemyTurn % 3 == 0) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Spiteful Stare"], characters, allies);
                if (CombatMode.enemyTurn % 4 == 0) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Fearful Gaze"], characters, allies);
                while (i < actions)
                    prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
            }
            return true;
        }
    }

    public class WeakMan : Enemy
    {

        public WeakMan(string name, int lbLevel, int id, int actions, UnitData unitData) : base(name, lbLevel, id, actions, unitData)
        {

        }

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);

            if (unitData.Abilities.Count < 1) return false;

            actions = 11;
            string error = "";
            int i = 0;

            prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Inescapable Death"], characters, allies);
            return true;
        }
    }
}
