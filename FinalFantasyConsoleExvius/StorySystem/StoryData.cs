﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    public class StoryData
    {
        private string title;
	    private string description;
        private string teleport;
        private string endOfStory;
        private string battleName;
        private string unlockName;
        private bool makeChoice;
        private bool savePoint;
	    private Dictionary<string, string> choices;
	    private Dictionary<string, int> extraData;

        public StoryData(string title, string description, string teleport, string endOfStory, string battleName, string unlockName, bool makeChoice, bool savePoint, Dictionary<string, string> choices,
            Dictionary<string, int> extraData)
        {
		    this.title = title;
		    this.description = description;
            this.teleport = teleport;
            this.endOfStory = endOfStory;
            this.battleName = battleName;
            this.unlockName = unlockName;
            this.makeChoice = makeChoice;
            this.savePoint = savePoint;
            this.choices = choices;
            this.extraData = extraData;
	    }

        public string Title { get { return title; }  }
        public string Description { get { return description; } }
        public string Teleport { get { return teleport; } }
        public string EndOfStory { get { return endOfStory; } }
        public string BattleName { get { return battleName; } }
        public string UnlockName { get { return unlockName; } }
        public bool MakeChoice { get { return makeChoice; } }
        public bool SavePoint { get { return savePoint; } }
	
	    public int getChoiceSize(){ return choices.Count; }
        public string getChoiceKey(int i) { return choices.Keys.ToList()[i]; }
        public string getChoiceValue(int i) { return choices.Values.ToList()[i]; }
        public string getChoiceValue(string key) { return choices[key]; }

        public int getExtraDataSize() { return extraData.Count; }
        public string getExtraDataKey(int i) { return extraData.Keys.ToList()[i]; }
        public int getExtraDataValue(int i) { return extraData.Values.ToList()[i]; }
        public int getExtraDataValue(string key) { return extraData[key]; }
    }
}
