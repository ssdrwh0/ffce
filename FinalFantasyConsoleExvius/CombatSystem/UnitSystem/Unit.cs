﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    public enum UnitState
    {
        UNIT_ALIVE,
        UNIT_FALLING,
        UNIT_AIRBORNE,
        UNIT_HIDING,
        UNIT_DYING,
        UNIT_DEAD,
        UNIT_REMOVED
    }

    public abstract class Unit
    {
        #region Attributes
        public static List<string> triggerList = new List<string>();

        public List<DebuffComponent> debuffs;
        public List<BuffComponent> buffs;
        public List<Weapon> weaponSet;
        public List<Equipment> equipmentSet;
        public List<ElementalType> imbueList;
        public List<Materia> materiaSet;
        public List<AbilityCommand> abilityCommands;
        public Unit guardTarget;
        public Unit guardian;
        public AIManipulationType currentCover;
        public double mpRefund;
        public double lbRefund;
        public string abilityRefund;
        public string itemRefund;
        public int castFrame;
        public int castCount;

        private bool coverTriggered;
        private double coverBonus;

        //Unit name
        protected string name;
        //Unit Level
        protected int unitLevel;
        //Limit Burst Level
        protected int lbLevel;
        //Id
        protected int chainId;

        protected UnitState unitState;
        protected bool isPlayer;

        //Unit Attributes
        protected Dictionary<string, double> doubleStats;
        protected Dictionary<string, int> intStats;
        protected Dictionary<string, bool> boolStats;
        protected List<string> listOfStacks;
        protected List<string> listOfCooldowns;
        protected List<string> listOfCounters;
        protected List<int> fireHitters;
        protected List<int> iceHitters;
        protected List<int> thunderHitters;
        protected List<int> waterHitters;
        protected List<int> windHitters;
        protected List<int> earthHitters;
        protected List<int> lightHitters;
        protected List<int> darkHitters;
        protected List<int> elementalItemHitters;
        protected List<int> esperHitters;
        protected List<int> limitHitters;
        protected List<int> autoAttackHitters;
        public List<int> physicalHitters;
        public List<int> magicalHitters;

        protected UnitData unitData;
        protected Esper esper;

        protected int whoIsChaining;
        protected int chainFrame;
        protected int totalChainCount;
        protected double chainBonus;
        protected List<ElementalType> previousElements;
        #endregion

        #region Get Attributes

        //Unit name
        public string Name { get { return name; } }
        //Unit name
        public string DataName { get { return unitData.BaseName; } }
        //Unit Level
        public int UnitLevel { get { return unitLevel; } }
        //Limit Burst Level
        public int LBLevel { get { return lbLevel; } }
        //Chain id
        public int ChainId { get { return chainId; } }
        //IsPlayer
        public bool IsPlayer { get { return isPlayer; } }
        //Base Unit Attributes
        public double getDoubleStat(string name)
        {
            if (doubleStats.ContainsKey(name))
                return doubleStats[name];
            return 0.0;
        }
        public int getIntStat(string name)
        {
            if (intStats.ContainsKey(name))
                return intStats[name];
            return 0;
        }
        public bool getBoolStat(string name)
        {
            if (boolStats.ContainsKey(name))
                return boolStats[name];
            return false;
        }
        public void setDoubleStat(string name, double value)
        {
            if (doubleStats.ContainsKey(name))
                doubleStats[name] = value;
            else
                doubleStats.Add(name, value);
        }
        public void setIntStat(string name, int value)
        {
            if (intStats.ContainsKey(name))
                intStats[name] = value;
            else
                intStats.Add(name, value);
        }
        public void setBoolStat(string name, bool value)
        {
            if (boolStats.ContainsKey(name))
                boolStats[name] = value;
            else
                boolStats.Add(name, value);
        }
        public int getTotalIntStat(string name, string type)
        {
            int total = unitData.getIntStatAdvanced(name, type, this);
            foreach (Weapon weapon in weaponSet)
            {
                total += weapon.EquipData.getIntStat(name);
                foreach (Passive passive in weapon.EquipPassives)
                    total += passive.getIntStat(name, type, this);
                foreach (Passive passive in weapon.EquipData.EquipPassives)
                    total += passive.getIntStat(name, type, this);
            }
            foreach (Equipment equip in equipmentSet)
            {
                total += equip.EquipData.getIntStat(name);
                foreach (Passive passive in equip.EquipData.EquipPassives)
                    total += passive.getIntStat(name, type, this);
            }
            foreach (Materia materia in materiaSet)
                if (materia.PassiveEffect != null)
                    total += materia.PassiveEffect.getIntStat(name, type, this);
            if (esper != null)
                total += esper.getIntStat(name);
            switch (type)
            {
                case "statusResist":
                    if (total > 100) total = 100;
                    if (total < -100) total = -100;
                    break;
                case "tdhBoost": if (total > 100) total = 100; break;
            }
            return total;
        }
        public double getDamageModifierBuff(string name, double limit, double scale)
        {
            if (scale < 1e-4) return 0.0;
            double boost = getDoubleStat(name);
            if (boost + scale <= limit) setDoubleStat(name, boost + scale);
            if (!listOfStacks.Contains(name)) listOfStacks.Add(name);
            return boost;
        }
        public double getTotalDoubleStat(string name, string type)
        {
            double boost = unitData.getDoubleStatAdvanced(name, type, this);
            foreach (Weapon weapon in weaponSet)
            {
                foreach (Passive passive in weapon.EquipPassives)
                    boost += passive.getDoubleStat(name, type, this);
                foreach (Passive passive in weapon.EquipData.EquipPassives)
                    boost += passive.getDoubleStat(name, type, this);
                if (type.Equals("elemental")) boost += weapon.EquipData.getDoubleStat(name);
            }
            foreach (Equipment equip in equipmentSet)
            {
                foreach (Passive passive in equip.EquipData.EquipPassives)
                    boost += passive.getDoubleStat(name, type, this);
                if (type.Equals("elemental")) boost += equip.EquipData.getDoubleStat(name);
            }
            foreach (Materia materia in materiaSet)
                if (materia.PassiveEffect != null)
                    boost += materia.PassiveEffect.getDoubleStat(name, type, this);
            if (esper != null)
            {
                foreach (Passive passive in esper.EquipPassives)
                    boost += passive.getDoubleStat(name, type, this);
                if(type.Equals("elemental")) boost += esper.getDoubleStat(name);
            }
            switch (type)
            {
                case "jump": if (boost > 4.0) boost = 4.0; break;
                case "boost": if (boost > 4.0) boost = 4.0; break;
                case "dire": if (boost > 3.0) boost = 3.0; break;
                case "shieldBoost": if (boost > 3.0) boost = 3.0; break;
                case "trueUnarmed": if (boost > 3.0) boost = 3.0; break;
                case "tdhBoost": if (boost > 3.0) boost = 3.0; break;
                case "tdwBoost":
                    double limit = 1.0;
                    if(unitData.getIntStatAdvanced("dualWieldPlus", "", this) > 0) limit += 1.0;
                    if (boost > limit) boost = limit;
                    break;
                case "evoMag": if (boost > 3.0) boost = 3.0; break;
                case "evasion": if (boost > 1.0) boost = 1.0; break;
                case "lbRecover": if (boost > 12.0) boost = 12.0; break;
            }
            if (boost < 1e-4) boost = 0.0;
            return boost;
        }
        public double getBaseEquipStat(string name)
        {
            double flat = 0.0;
            foreach (Weapon weapon in weaponSet)
                flat += weapon.EquipData.getDoubleStat(name);
            foreach (Equipment equip in equipmentSet)
                flat += equip.EquipData.getDoubleStat(name);
            return flat;
        }
        public double getEsperStat(string baseName, string growthName, double divider)
        {
            double flat = 0.0;
            if(esper != null)
                flat = (esper.getDoubleStat(baseName) + esper.getDoubleStat(growthName) * esper.EsperLevel) / divider;
            return flat;
        }
        
        public string getEsperName()
        {
            if (esper != null) return esper.Name;
            return "";
        }

        public int getEsperLevel()
        {
            if (esper != null) return esper.EsperLevel;
            return 0;
        }

        public string getWeaponSlotA()
        {
            if (weaponSet.Count > 0) return weaponSet[0].EquipData.Name;
            return "";
        }

        public List<string> getWeaponPassivesA()
        {
            List<string> results = new List<string>();
            if(weaponSet.Count > 0)
                foreach (Passive passive in weaponSet[0].EquipPassives)
                    results.Add(passive.Name);
            return results;
        }

        public string getWeaponSlotB()
        {
            if (weaponSet.Count > 1) return weaponSet[1].EquipData.Name;
            return "";
        }

        public List<string> getWeaponPassivesB()
        {
            List<string> results = new List<string>();
            if (weaponSet.Count > 1)
                foreach (Passive passive in weaponSet[1].EquipPassives)
                    results.Add(passive.Name);
            return results;
        }

        public List<string> getEquipmentNames()
        {
            List<string> results = new List<string>();
            foreach (Equipment equip in equipmentSet)
                results.Add(equip.EquipData.Name);
            return results;
        }

        public List<string> getMateriaNames()
        {
            List<string> results = new List<string>();
            foreach (Materia materia in materiaSet)
                results.Add(materia.Name);
            return results;
        }

        //Unit Resources
        public double HP { get { return getDoubleStat("hp"); } }
        public double MaxHP { get { return getDoubleStat("maxHp"); } } //Health Points
        public double MP { get { return getDoubleStat("mp"); } }
        public double MaxMP { get { return getDoubleStat("maxMp"); } } //Mana Points
        public double LB { get { return getDoubleStat("lb"); } } //Limit Burst Points
        public double LBCost { get { return unitData.LbCost; } } //Limit Burst Cost
        public double Exp { get { return getDoubleStat("currentXP"); } } //Experience

        //Base Unit Attributes
        public double BaseHp { get { return unitData.getDoubleStat("baseHp") + unitData.getDoubleStat("hpGrowth") * (double)(unitLevel - 1); } }
        public double BaseMp { get { return unitData.getDoubleStat("baseMp") + unitData.getDoubleStat("mpGrowth") * (double)(unitLevel - 1); } }
        public double BaseAtk { get { return unitData.getDoubleStat("baseAtk") + unitData.getDoubleStat("atkGrowth") * (double)(unitLevel - 1); } }
        public double BaseMag { get { return unitData.getDoubleStat("baseMag") + unitData.getDoubleStat("magGrowth") * (double)(unitLevel - 1); } }
        public double BaseDef { get { return unitData.getDoubleStat("baseDef") + unitData.getDoubleStat("defGrowth") * (double)(unitLevel - 1); } }
        public double BaseSpr { get { return unitData.getDoubleStat("baseSpr") + unitData.getDoubleStat("sprGrowth") * (double)(unitLevel - 1); } }
        //Base Boosts
        public double HpBoost { get { return 1.0 + getTotalDoubleStat("hpBoost", "boost"); } }
        public double MpBoost { get { return 1.0 + getTotalDoubleStat("mpBoost", "boost"); } }
        public double AtkBoost { get { return 1.0 + getTotalDoubleStat("atkBoost", "boost") + getTotalDoubleStat("atkDire", "dire") + getDoubleStat("baseAtkBonus") - getDoubleStat("baseAtkPenalty"); } }
        public double MagBoost { get { return 1.0 + getTotalDoubleStat("magBoost", "boost") + getTotalDoubleStat("magDire", "dire") + getDoubleStat("baseMagBonus") - getDoubleStat("baseMagPenalty"); } }
        public double DefBoost { get { return 1.0 + getTotalDoubleStat("defBoost", "boost") + getTotalDoubleStat("defDire", "dire") + getDoubleStat("baseDefBonus") - getDoubleStat("baseDefPenalty"); } }
        public double SprBoost { get { return 1.0 + getTotalDoubleStat("sprBoost", "boost") + getTotalDoubleStat("sprDire", "dire") + getDoubleStat("baseSprBonus") - getDoubleStat("baseSprPenalty"); } }
        //Equip Boosts
        public double EquipHp { get { return (1.0 + getTotalDoubleStat("equipHpBoost", "tdhBoost")) * (1.0 + getTotalDoubleStat("equipHpBoost", "tdwBoost")) * (1.0 + getTotalDoubleStat("equipHpBoost", "shieldBoost")); } }
        public double EquipMp { get { return (1.0 + getTotalDoubleStat("equipMpBoost", "tdhBoost")) * (1.0 + getTotalDoubleStat("equipMpBoost", "tdwBoost")) * (1.0 + getTotalDoubleStat("equipMpBoost", "shieldBoost")); } }
        public double EquipAtk { get { return (1.0 + getTotalDoubleStat("equipAtkBoost", "tdhBoost")) * (1.0 + getTotalDoubleStat("equipAtkBoost", "tdwBoost")) * (1.0 + getTotalDoubleStat("equipAtkBoost", "shieldBoost")); } }
        public double EquipMag { get { return (1.0 + getTotalDoubleStat("equipMagBoost", "tdhBoost")) * (1.0 + getTotalDoubleStat("equipMagBoost", "tdwBoost")) * (1.0 + getTotalDoubleStat("equipMagBoost", "shieldBoost")); } }
        public double EquipDef { get { return (1.0 + getTotalDoubleStat("equipDefBoost", "tdhBoost")) * (1.0 + getTotalDoubleStat("equipDefBoost", "tdwBoost")) * (1.0 + getTotalDoubleStat("equipDefBoost", "shieldBoost")); } }
        public double EquipSpr { get { return (1.0 + getTotalDoubleStat("equipSprBoost", "tdhBoost")) * (1.0 + getTotalDoubleStat("equipSprBoost", "tdwBoost")) * (1.0 + getTotalDoubleStat("equipSprBoost", "shieldBoost")); } }
        //Current Attributes
        public double TotalHp { get { return BaseHp * HpBoost + getBaseEquipStat("hp") * EquipHp + EsperHp; } }
        public double TotalMp { get { return BaseMp * MpBoost + getBaseEquipStat("mp") * EquipMp + EsperMp; } }
        public double Atk { get { return BaseAtk * AtkBoost + getBaseEquipStat("atk") * EquipAtk + EsperAtk; } }
        public double Mag { get { return BaseMag * MagBoost + getBaseEquipStat("mag") * EquipMag + EsperMag; } }
        public double Def { get { return BaseDef * DefBoost + getBaseEquipStat("def") * EquipDef + EsperDef; } }
        public double Spr { get { return BaseSpr * SprBoost + getBaseEquipStat("spr") * EquipSpr + EsperSpr; } }
        //Esper Boosts
        public double EsperHp { get { return getEsperStat("baseHp", "hpGrowth", 100) * (1.0 + getTotalDoubleStat("esperBoost", "")); } }
        public double EsperMp { get { return getEsperStat("baseMp", "mpGrowth", 100) * (1.0 + getTotalDoubleStat("esperBoost", "")); } }
        public double EsperAtk { get { return getEsperStat("baseAtk", "atkGrowth", 100) * (1.0 + getTotalDoubleStat("esperBoost", "")); } }
        public double EsperMag { get { return getEsperStat("baseMag", "magGrowth", 100) * (1.0 + getTotalDoubleStat("esperBoost", "")); } }
        public double EsperDef { get { return getEsperStat("baseDef", "defGrowth", 100) * (1.0 + getTotalDoubleStat("esperBoost", "")); } }
        public double EsperSpr { get { return getEsperStat("baseSpr", "sprGrowth", 100) * (1.0 + getTotalDoubleStat("esperBoost", "")); } }
        public double EsperAtk2 { get { return getEsperStat("baseAtk", "atkGrowth", 1); } } // * (1.0 + getTotalDoubleStat("esperBoost", ""));
        public double EsperMag2 { get { return getEsperStat("baseMag", "magGrowth", 1); } } // * (1.0 + getTotalDoubleStat("esperBoost", ""));
        public double EsperDef2 { get { return getEsperStat("baseDef", "defGrowth", 1); } } // * (1.0 + getTotalDoubleStat("esperBoost", ""));
        public double EsperSpr2 { get { return getEsperStat("baseSpr", "sprGrowth", 1); } } // * (1.0 + getTotalDoubleStat("esperBoost", ""));
        //Mitigation Attributes
        public double GeneralMitigation { get { return getDoubleStat("generalMitigation"); } }
        public double PhysicalMitigation { get { return getDoubleStat("physicalMitigation"); } }
        public double MagicalMitigation { get { return getDoubleStat("magicalMitigation"); } }
        public double CoverMitigation
        {
            get
            {
                if (currentCover != AIManipulationType.NONE && currentCover != AIManipulationType.TARGETING_CHANCES)
                {
                    if (!coverTriggered)
                    {
                        coverTriggered = true;
                        coverBonus = (double)AssetLoader.random.Next((int)(getDoubleStat("coverMitigationRange") * 100) + 1) / 100;
                    }
                    return getDoubleStat("coverMitigation") + coverBonus;
                }
                return 0.0;
            }
        }
        public double DefendMitigation { get { return getDoubleStat("defendMitigation"); } }
        //Evasion
        public double PhysicalEvasion { get { return getTotalDoubleStat("pEvasion", "evasion") * 100; } }
        public double MagicalEvasion { get { return getTotalDoubleStat("mEvasion", "evasion") * 100; } }
        //AI Manipulation
        public double TargetChance { get { return getDoubleStat("targetChance"); } }
        public double AllCover { get { return getDoubleStat("allCover"); } }
        public double PhysicalCover { get { return getDoubleStat("physicalCover"); } }
        public double MagicalCover { get { return getDoubleStat("magicalCover"); } }
        //Bonus Attribute From Party Buffs
        public double BaseAtkBonus { get { return getDoubleStat("baseAtkBonus"); } }
        public double BaseMagBonus { get { return getDoubleStat("baseMagBonus"); } }
        public double BaseDefBonus { get { return getDoubleStat("baseDefBonus"); } }
        public double BaseSprBonus { get { return getDoubleStat("baseSprBonus"); } }

        //Parameter penalties From Party Debuffs
        public double BaseAtkPenalty { get { return getDoubleStat("baseAtkPenalty"); } }
        public double BaseMagPenalty { get { return getDoubleStat("baseMagPenalty"); } }
        public double BaseDefPenalty { get { return getDoubleStat("baseDefPenalty"); } }
        public double BaseSprPenalty { get { return getDoubleStat("baseSprPenalty"); } }
        //Base Parameter debuffs resists
        public int BaseAtkBreakResist { get { return unitData.getIntStat("baseAtkBreakResist"); } }
        public int BaseMagBreakResist { get { return unitData.getIntStat("baseMagBreakResist"); } }
        public int BaseDefBreakResist { get { return unitData.getIntStat("baseDefBreakResist"); } }
        public int BaseSprBreakResist { get { return unitData.getIntStat("baseSprBreakResist"); } }
        //Parameter debuffs resists
        public int AtkBreakResist { get { return getIntStat("atkBreakResist") + BaseAtkBreakResist; } }
        public int MagBreakResist { get { return getIntStat("magBreakResist") + BaseMagBreakResist; } }
        public int DefBreakResist { get { return getIntStat("defBreakResist") + BaseDefBreakResist; } }
        public int SprBreakResist { get { return getIntStat("sprBreakResist") + BaseSprBreakResist; } }
        //Base Status Resist
        public int BasePoisonResist { get { return unitData.getIntStat("basePoisonResist") + getTotalIntStat("poisonResist", "statusResist"); } }
        public int BaseSilenceResist { get { return unitData.getIntStat("baseSilenceResist") + getTotalIntStat("silenceResist", "statusResist"); } }
        public int BaseSleepResist { get { return unitData.getIntStat("baseSleepResist") + getTotalIntStat("sleepResist", "statusResist"); } }
        public int BaseBlindResist { get { return unitData.getIntStat("baseBlindResist") + getTotalIntStat("blindResist", "statusResist"); } }
        public int BaseCharmResist { get { return unitData.getIntStat("baseCharmResist") + getTotalIntStat("charmResist", "statusResist"); } }
        public int BaseParalyseResist { get { return unitData.getIntStat("baseParalyseResist") + getTotalIntStat("paralyseResist", "statusResist"); } }
        public int BaseConfuseResist { get { return unitData.getIntStat("baseConfuseResist") + getTotalIntStat("confuseResist", "statusResist"); } }
        public int BaseStopResist { get { return unitData.getIntStat("baseStopResist") + getTotalIntStat("stopResist", "statusResist"); } }
        public int BaseBerserkResist { get { return unitData.getIntStat("baseBerserkResist") + getTotalIntStat("berserkResist", "statusResist"); } }
        public int BaseDiseaseResist { get { return unitData.getIntStat("baseDiseaseResist") + getTotalIntStat("diseaseResist", "statusResist"); } }
        public int BasePetrifyResist { get { return unitData.getIntStat("basePetrifyResist") + getTotalIntStat("petrifyResist", "statusResist"); } }
        public int BaseDeathResist { get { return unitData.getIntStat("baseDeathResist") + getTotalIntStat("deathResist", "statusResist"); } }
        public int BaseGravityResist { get { return unitData.getIntStat("baseGravityResist") + getTotalIntStat("gravityResist", "statusResist"); } }
        //Status Resist
        public int PoisonResist { get { return getIntStat("poisonResist") + BasePoisonResist; } }
        public int SilenceResist { get { return getIntStat("silenceResist") + BaseSilenceResist; } }
        public int SleepResist { get { return getIntStat("sleepResist") + BaseSleepResist; } }
        public int BlindResist { get { return getIntStat("blindResist") + BaseBlindResist; } }
        public int CharmResist { get { return getIntStat("charmResist") + BaseCharmResist; } }
        public int ParalyseResist { get { return getIntStat("paralyseResist") + BaseParalyseResist; } }
        public int ConfuseResist { get { return getIntStat("confuseResist") + BaseConfuseResist; } }
        public int StopResist { get { return getIntStat("stopResist") + BaseStopResist; } }
        public int BerserkResist { get { return getIntStat("berserkResist") + BaseBerserkResist; } }
        public int DiseaseResist { get { return getIntStat("diseaseResist") + BaseDiseaseResist; } }
        public int PetrifyResist { get { return getIntStat("petrifyResist") + BasePetrifyResist; } }
        public int DeathResist { get { return getIntStat("deathResist") + BaseDeathResist; } }
        public int GravityResist { get { return getIntStat("gravityResist") + BaseGravityResist; } }
        //Elemental Imperils
        public double FireImperil { get { return getDoubleStat("fireImperil"); } }
        public double IceImperil { get { return getDoubleStat("iceImperil"); } }
        public double ThunderImperil { get { return getDoubleStat("thunderImperil"); } }
        public double WaterImperil { get { return getDoubleStat("waterImperil"); } }
        public double WindImperil { get { return getDoubleStat("windImperil"); } }
        public double EarthImperil { get { return getDoubleStat("earthImperil"); } }
        public double LightImperil { get { return getDoubleStat("lightImperil"); } }
        public double DarkImperil { get { return getDoubleStat("darkImperil"); } }
        //Elemental Resists
        public double FireResist { get { return getDoubleStat("fireResist") + getTotalDoubleStat("fireResist", "elemental") + unitData.getDoubleStat("baseFireResist"); } }
        public double IceResist { get { return getDoubleStat("iceResist") + getTotalDoubleStat("iceResist", "elemental") + unitData.getDoubleStat("baseIceResist"); } }
        public double ThunderResist { get { return getDoubleStat("thunderResist") + getTotalDoubleStat("thunderResist", "elemental") + unitData.getDoubleStat("baseThunderResist"); } }
        public double WaterResist { get { return getDoubleStat("waterResist") + getTotalDoubleStat("waterResist", "elemental") + unitData.getDoubleStat("baseWaterResist"); } }
        public double WindResist { get { return getDoubleStat("windResist") + getTotalDoubleStat("windResist", "elemental") + unitData.getDoubleStat("baseWindResist"); } }
        public double EarthResist { get { return getDoubleStat("earthResist") + getTotalDoubleStat("earthResist", "elemental") + unitData.getDoubleStat("baseEarthResist"); } }
        public double LightResist { get { return getDoubleStat("lightResist") + getTotalDoubleStat("lightResist", "elemental") + unitData.getDoubleStat("baseLightResist"); } }
        public double DarkResist { get { return getDoubleStat("darkResist") + getTotalDoubleStat("darkResist", "elemental") + unitData.getDoubleStat("baseDarkResist"); } }
        public double ElementlessResist { get { return getDoubleStat("elementlessResist") + getTotalDoubleStat("elementlessResist", "elemental") + unitData.getDoubleStat("baseElementlessResist"); } }
        //Elemental Absorbs
        public int FireAbsorb { get { return getIntStat("fireAbsorb") + getTotalIntStat("fireAbsorb", "elemental") + unitData.getIntStat("fireAbsorb"); } }
        public int IceAbsorb { get { return getIntStat("iceAbsorb") + getTotalIntStat("iceAbsorb", "elemental") + unitData.getIntStat("iceAbsorb"); } }
        public int ThunderAbsorb { get { return getIntStat("thunderAbsorb") + getTotalIntStat("thunderAbsorb", "elemental") + unitData.getIntStat("thunderAbsorb"); } }
        public int WaterAbsorb { get { return getIntStat("waterAbsorb") + getTotalIntStat("waterAbsorb", "elemental") + unitData.getIntStat("waterAbsorb"); } }
        public int WindAbsorb { get { return getIntStat("windAbsorb") + getTotalIntStat("windAbsorb", "elemental") + unitData.getIntStat("windAbsorb"); } }
        public int EarthAbsorb { get { return getIntStat("earthAbsorb") + getTotalIntStat("earthAbsorb", "elemental") + unitData.getIntStat("earthAbsorb"); } }
        public int LightAbsorb { get { return getIntStat("lightAbsorb") + getTotalIntStat("lightAbsorb", "elemental") + unitData.getIntStat("lightAbsorb"); } }
        public int DarkAbsorb { get { return getIntStat("darkAbsorb") + getTotalIntStat("darkAbsorb", "elemental") + unitData.getIntStat("darkAbsorb"); } }
        public int ElementlessAbsorb { get { return getIntStat("elementlessAbsorb") + getTotalIntStat("elementlessAbsorb", "elemental") + unitData.getIntStat("elementlessAbsorb"); } }
        //Current Statuses
        public bool Poisoned { get { return getBoolStat("poisoned"); } }
        public bool Silenced { get { return getBoolStat("silenced"); } }
        public bool Sleeping { get { return getBoolStat("sleeping"); } }
        public bool Blinded { get { return getBoolStat("blinded"); } }
        public bool Charmed { get { return getBoolStat("charmed"); } }
        public bool Paralysed { get { return getBoolStat("paralysed"); } }
        public bool Confused { get { return getBoolStat("confused"); } }
        public bool Stopped { get { return getBoolStat("stopped"); } }
        public bool Berserked { get { return getBoolStat("berserked"); } }
        public bool Diseased { get { return getBoolStat("diseased"); } }
        public bool Petrified { get { return getBoolStat("petrified"); } }
        //Regens
        public double LBFillRate { get { return getDoubleStat("lbFillRate"); } }
        public double LBRegen { get { return getDoubleStat("lbRegen"); } }
        public double HPRegen { get { return getDoubleStat("hpRegen"); } }
        public double MPRegen { get { return getDoubleStat("mpRegen"); } }
        public UnitState CurrentState { get { return unitState; } }
        //Hit counters
        public bool HitByLimitBurst { get { return limitHitters.Count > 0; } }
        public bool HitByEsper { get { return esperHitters.Count > 0; } }
        public bool HitByMagic { get { return magicalHitters.Count > 0; } }
        public bool HitByPhysical { get { return physicalHitters.Count > 0; } }
        public bool HitByAuto { get { return autoAttackHitters.Count > 0; } }

        public bool hasMultiCover()
        {
            foreach(BuffComponent buff in buffs)
                if(buff.MultiCover)
                    return true;
            return false;
        }

        public Unit(string name, int lbLevel, int id, UnitData unitData, Esper esper)
        {
            #region Attributes Declared
            this.unitData = unitData;
            this.name = unitData.DisplayName + name;
            this.unitLevel = unitData.BaseLevel;
            this.lbLevel = lbLevel;
            if (lbLevel > UnitData.MAX_LIMIT_BURST) lbLevel = UnitData.MAX_LIMIT_BURST;
            this.chainId = id;
            doubleStats = new Dictionary<string, double>();
            intStats = new Dictionary<string, int>();
            boolStats = new Dictionary<string, bool>();
            listOfStacks = new List<string>();
            listOfCooldowns = new List<string>();
            listOfCounters = new List<string>();
            esperHitters = new List<int>();
            limitHitters = new List<int>();
            autoAttackHitters = new List<int>();
            physicalHitters = new List<int>();
            magicalHitters = new List<int>();
            fireHitters = new List<int>();
            iceHitters = new List<int>();
            thunderHitters = new List<int>();
            waterHitters = new List<int>();
            windHitters = new List<int>();
            earthHitters = new List<int>();
            lightHitters = new List<int>();
            darkHitters = new List<int>();
            elementalItemHitters = new List<int>(); ;
            abilityCommands = new List<AbilityCommand>();

            buffs = new List<BuffComponent>();
            debuffs = new List<DebuffComponent>();
            weaponSet = new List<Weapon>();
            equipmentSet = new List<Equipment>();
            materiaSet = new List<Materia>();

            imbueList = new List<ElementalType>();
            previousElements = new List<ElementalType>();
            this.esper = esper;
            #endregion

            resetStats(true);
        }
        #endregion

        public void testStat()
        {
            double testBaseHp = unitData.getDoubleStat("baseHp") + unitData.getDoubleStat("hpGrowth") * (double)(unitLevel - 1);
            double hpBoost = 1.0 + getTotalDoubleStat("hpBoost", "boost");
            double tdhHp = 1.0 + getTotalDoubleStat("equipHpBoost", "tdhBoost");
            double equipHpBoost = tdhHp * (1.0 + getTotalDoubleStat("equipHpBoost", "tdwBoost"));
            double esperHpBoost = EsperHp * (1.0 + getTotalDoubleStat("esperBoost", ""));
            double flatEquipHp = getBaseEquipStat("hp");

            double hpA = testBaseHp * hpBoost;
            double hpB = flatEquipHp * equipHpBoost;

            double finalHp = hpA + hpB + esperHpBoost;

            double testBaseMp = unitData.getDoubleStat("baseMp") + unitData.getDoubleStat("mpGrowth") * (double)(unitLevel - 1);
            double mpBoost = 1.0 + getTotalDoubleStat("mpBoost", "boost");
            double tdhMp = 1.0 + getTotalDoubleStat("equipMpBoost", "tdhBoost");
            double equipMpBoost = tdhMp * (1.0 + getTotalDoubleStat("equipMpBoost", "tdwBoost"));
            double esperMpBoost = EsperMp * (1.0 + getTotalDoubleStat("esperBoost", ""));
            double flatEquipMp = getBaseEquipStat("mp");

            double mpA = testBaseMp * mpBoost;
            double mpB = flatEquipMp * equipMpBoost;

            double finalMp = mpA + mpB + esperMpBoost;

            double testBaseAtk = unitData.getDoubleStat("baseAtk") + unitData.getDoubleStat("atkGrowth") * (double)(unitLevel - 1);
            double atkBoost = 1.0 + getTotalDoubleStat("atkBoost", "boost");
            double atkBuff = 1.0 + getDoubleStat("baseAtkBonus") - getDoubleStat("baseAtkPenalty");
            double tdhAtk = 1.0 + getTotalDoubleStat("equipAtkBoost", "tdhBoost");
            double equipAtkBoost = tdhAtk * (1.0 + getTotalDoubleStat("equipAtkBoost", "tdwBoost"));
            double esperAtkBoost = getEsperStat("baseAtk", "atkGrowth", 100) * (1.0 + getTotalDoubleStat("esperBoost", ""));
            double flatEquipAtk = getBaseEquipStat("atk");

            double atkA = testBaseAtk * atkBoost * atkBuff;
            double atkB = flatEquipAtk * equipAtkBoost;
            double finalAtk = atkA + atkB + esperAtkBoost;

            double testBaseMag = unitData.getDoubleStat("baseMag") + unitData.getDoubleStat("magGrowth") * (double)(unitLevel - 1);
            double magBoost = 1.0 + getTotalDoubleStat("magBoost", "boost");
            double magBuff = 1.0 + getDoubleStat("baseMagBonus") - getDoubleStat("baseMagPenalty");
            double tdhMag = 1.0 + getTotalDoubleStat("equipMagBoost", "tdhBoost");
            double equipMagBoost = tdhMag * (1.0 + getTotalDoubleStat("equipMagBoost", "tdwBoost"));
            double esperMagBoost = getEsperStat("baseMag", "magGrowth", 100) * (1.0 + getTotalDoubleStat("esperBoost", ""));
            double flatEquipMag = getBaseEquipStat("mag");
            double magA = testBaseMag * magBoost * magBuff;
            double magB = flatEquipMag * equipMagBoost;
            double finalMag = magA + magB + esperMagBoost;

            double testBaseDef = unitData.getDoubleStat("baseDef") + unitData.getDoubleStat("defGrowth") * (double)(unitLevel - 1);
            double defBoost = 1.0 + getTotalDoubleStat("defBoost", "boost");
            double defBuff = 1.0 + getDoubleStat("baseDefBonus") - getDoubleStat("baseDefPenalty");
            double tdhDef = 1.0 + getTotalDoubleStat("equipDefBoost", "tdhBoost");
            double equipDefBoost = tdhDef * (1.0 + getTotalDoubleStat("equipDefBoost", "tdwBoost"));
            double esperDefBoost = getEsperStat("baseDef", "defGrowth", 100) * (1.0 + getTotalDoubleStat("esperBoost", ""));
            double flatEquipDef = getBaseEquipStat("def");

            double defA = testBaseDef * defBoost * defBuff;
            double defB = flatEquipDef * equipDefBoost;
            double finalDef = defA + defB + esperDefBoost;

            double testBaseSpr = unitData.getDoubleStat("baseSpr") + unitData.getDoubleStat("sprGrowth") * (double)(unitLevel - 1);
            double sprBoost = 1.0 + getTotalDoubleStat("sprBoost", "boost");
            double sprBuff = 1.0 + getDoubleStat("baseSprBonus") - getDoubleStat("baseSprPenalty");
            double tdhSpr = 1.0 + getTotalDoubleStat("equipSprBoost", "tdhBoost");
            double equipSprBoost = tdhSpr * (1.0 + getTotalDoubleStat("equipSprBoost", "tdwBoost"));
            double esperSprBoost = getEsperStat("baseSpr", "sprGrowth", 100) * (1.0 + getTotalDoubleStat("esperBoost", ""));
            double flatEquipSpr = getBaseEquipStat("spr");
            double sprA = testBaseSpr * sprBoost * sprBuff;
            double sprB = flatEquipSpr * equipSprBoost;
            double finalSpr = sprA + sprB + esperSprBoost;
        }

        public virtual void resetStats(bool startOfMission)
        {
            if(startOfMission)
            {
                unitState = UnitState.UNIT_ALIVE;
                setBoolStat("reraise", false);
                setDoubleStat("hp", TotalHp);
                setDoubleStat("mp", TotalMp);
                setDoubleStat("maxHp", TotalHp);
                setDoubleStat("maxMp", TotalMp);
                setDoubleStat("lb", 0);
                setDoubleStat("baseAtkBonus", 0.0);
                setDoubleStat("baseMagBonus", 0.0);
                setDoubleStat("baseDefBonus", 0.0);
                setDoubleStat("baseSprBonus", 0.0);
                setDoubleStat("reraiseHp", 0.0);
            }
            coverTriggered = false;
            coverBonus = 0.0;
            mpRefund = 0.0;
            lbRefund = 0.0;
            itemRefund = "";
            abilityRefund = "";
            setBoolStat("actionTaken", false);
            setBoolStat("readyState", false);
            setIntStat("timedDelay", 0);
            setIntStat("hitResult", 0);
            setIntStat("cheatDeath", 0);
            setIntStat("barrierDuration", 0);
            setIntStat("undying", unitData.getIntStat("undying"));
            setDoubleStat("bp", 0);
            guardTarget = null;
            guardian = null;
            currentCover = AIManipulationType.NONE;
            if (unitState == UnitState.UNIT_REMOVED || unitState == UnitState.UNIT_HIDING || unitState == UnitState.UNIT_AIRBORNE)
                unitState = UnitState.UNIT_ALIVE;

            while (buffs.Count > 0)
            {
                removeBuff(buffs[0]);
                buffs.RemoveAt(0);
            }
            while (debuffs.Count > 0)
            {
                removeDebuff(debuffs[0]);
                debuffs.RemoveAt(0);
            }

            foreach (string counter in listOfCounters) setDoubleStat(counter, 0.0);
            listOfCounters.Clear();

            foreach(string stack in listOfStacks) setDoubleStat(stack, 0.0);
            listOfStacks.Clear();
            
            foreach(string cdAbility in listOfCooldowns) setIntStat(cdAbility, 0);
            listOfCooldowns.Clear();

            esperHitters.Clear();
            limitHitters.Clear();
            autoAttackHitters.Clear();
            physicalHitters.Clear();
            magicalHitters.Clear();
            fireHitters.Clear();
            iceHitters.Clear();
            thunderHitters.Clear();
            waterHitters.Clear();
            windHitters.Clear();
            earthHitters.Clear();
            lightHitters.Clear();
            darkHitters.Clear();
            elementalItemHitters.Clear();
            abilityCommands.Clear();
            castCount = castFrame = 0;

            foreach(Ability ability in unitData.Abilities)
            {
                if(ability.Cooldown > 0)
                {
                    listOfCooldowns.Add(ability.DisplayName);
                    setIntStat(ability.DisplayName, ability.Cooldown);
                    if (ability.OnFirstTurn) setIntStat(ability.DisplayName, 0);
                }
            }

            previousElements.Clear();
            resetChain();
        }

        public void castingAbility(List<Unit> victims, List<Unit> allies)
        {
            if(castCount < abilityCommands.Count && --castFrame < 1)
            {
                //Console.WriteLine("Before - " + castFrame.ToString());
                string error = "";
                AbilityCommand command = abilityCommands[castCount];
                useAbility(castCount++, command.dualWield, command.multiCast, false, command.target, out error, AssetLoader.abilitiesData[command.abilityName], victims, allies);
                //Console.WriteLine("After - " + castFrame.ToString());
            }
        }

        public void updateChainFrames()
        {
            if (totalChainCount > 0 && ++chainFrame > 20)
            {
                Console.WriteLine("Streak Ended for " + Name + "...");
                resetChain();
            }
        }

        public void resetChain()
        {
            chainFrame = 0;
            whoIsChaining = 0;
            totalChainCount = 0;
            chainBonus = 1;
        }

        private string callChain(double addition, string message)
        {
            chainBonus += addition;
            if (chainBonus > 6.0) chainBonus = 6.0;
            //if (chainBonus > 4.0) chainBonus = 4.0;

            return message + (totalChainCount - 1).ToString() + "; ";//+ "; " + bonusMod.ToString());
        }

        public string chainCalculations(AttackQueue queue)
        {
            int bonusElements = 0;
            foreach (ElementalType element in previousElements)
                foreach (ElementalType element2 in queue.elements)
                    if (element == element2)
                    {
                        bonusElements++;
                        break;
                    }
            string message = "Chain +";
            if (bonusElements > 0) message = "Element Chain +";
            if (CombatMode.toSpark) return callChain(0.4 + (0.2 * (double)bonusElements), "Spark " + message); //0.6, 0.3
            return callChain(0.1 + (0.2 * (double)bonusElements), message); //0.1, 0.3
        }

        protected double calculateAtkDamage(IList<ScalingType> scalingTypes, int i)//IList<int> frameData, double effect, 
        {
            double diseased = 1.0;
            if (Diseased) diseased = 0.9;
            double minAtk = 0.0, minDef = 0.0;
            if(weaponSet.Count == 2){
                minAtk = weaponSet[1 - i].EquipData.getDoubleStat("atk");
                minDef = weaponSet[1 - i].EquipData.getDoubleStat("def");
            }
            double baseDamage = 0.0;
            foreach (ScalingType type in scalingTypes)
            {
                switch (type)
                {
                    case ScalingType.SCALE_BY_ATK: baseDamage += (Atk - minAtk) * diseased; break;
                    case ScalingType.SCALE_BY_DEF: baseDamage += (Def - minDef) * diseased; break;
                }
            }
            if (baseDamage < 1.0) baseDamage = 1.0;
            //STAT * STAT / DEF * KILLER * (1 - ELEMENT_RESIST) * SKILL_MOD * WEAPON_MOD * (1 + LEVEL/MAX_LEVEL) * FINAL_VAR / CHAIN HITS or
            //STAT * STAT * SKILL_MOD * WEAPON_MOD * (1 + LEVEL/MAX_LEVEL) * FINAL_VAR / CHAIN HITS
            return baseDamage;// *baseDamage;// *effect * 1 * (1 + (double)unitLevel / (double)UnitData.MAX_UNIT_LEVEL) * 0.8 / frameData.Count;
        }

        protected double calculateMagDamage(IList<ScalingType> scalingTypes)//IList<int> frameData, double effect, 
        {
            double diseased = 1.0;
            if (Diseased) diseased = 0.9;
            double baseDamage = 0.0;
            foreach (ScalingType type in scalingTypes)
            {
                switch (type)
                {
                    case ScalingType.SCALE_BY_MAG: baseDamage += Mag * diseased; break;
                    case ScalingType.SCALE_BY_SPR: baseDamage += Spr * diseased; break;
                }
            }
            if (baseDamage < 1.0) baseDamage = 1.0;
            //STAT * STAT / DEF_OR_SPR * KILLER * (1 - ELEMENT_RESIST) * SKILL_MOD * WEAPON_MOD * (1 + LEVEL/MAX_LEVEL) * FINAL_VAR / CHAIN HITS or
            //STAT * STAT * SKILL_MOD * WEAPON_MOD * (1 + LEVEL/MAX_LEVEL) * FINAL_VAR / CHAIN HITS
            return baseDamage;// *baseDamage;// *effect * 1 * (1 + (double)unitLevel / (double)UnitData.MAX_UNIT_LEVEL) * 0.8 / frameData.Count;
        }

        protected double calculateHealing(IList<ScalingType> scalingTypes, double effect, double scaling, int count, int duration)
        {
            double diseased = 1.0;
            if (Diseased) diseased = 0.9;
            double healing = 0.0;
            double stat;
            foreach (ScalingType type in scalingTypes)
            {
                switch (type)
                {
                    case ScalingType.SCALE_BY_ATK: stat = Atk * 0.1 * diseased; break;
                    case ScalingType.SCALE_BY_MAG: stat = Mag * 0.1 * diseased; break;
                    case ScalingType.SCALE_BY_DEF: stat = Def * 0.5 * diseased; break;
                    case ScalingType.SCALE_BY_SPR: stat = Spr * 0.5 * diseased; break;
                    default: stat = 1.0; break;
                }
                if (stat < 1.0) stat = 1.0;
                double calculation = (stat * (scaling + getDoubleStat(name + "Buff") + getTotalDoubleStat("healModifier", name))) / count;
                if (calculation < 1.0) calculation = 1.0;
                healing += calculation;
            }
            return ((healing * (0.85 + (double)(AssetLoader.random.Next(16)) / 100)) + (effect / count)) / duration;
        }

        protected double calculateFixedDamage(IList<int> frameData, double damage)
        {
            //FIXED DAMAGE * KILLER * (1 - ELEMENT_RESIST) / CHAIN HITS
            return damage / frameData.Count;
        }

        protected double calculateEsperDamage(bool physical)//IList<int> frameData, double effect, 
        {
            if (physical) return Math.Floor((EsperAtk2 + EsperDef2 + EsperMag2 / 2 + EsperSpr2 / 2) / 100 * (1 + getTotalDoubleStat("evoMag", "")));
            return Math.Floor((EsperAtk2 / 2 + EsperDef2 / 2 + EsperMag2 + EsperSpr2) / 100 * (1 + getTotalDoubleStat("evoMag", "")));
        }

        protected void calculateEvokeDamage(IList<ScalingType> scalingTypes, out double atkDamage, out double magDamage)//IList<int> frameData, double effect, 
        {
            atkDamage = magDamage = 0.0;
            double diseased = 1.0;
            if (Diseased) diseased = 0.9;
            foreach (ScalingType type in scalingTypes)
            {
                switch (type)
                {
                    case ScalingType.SCALE_BY_ATK: atkDamage = Atk * diseased * (1.0 + getTotalDoubleStat("evoMag", "")); break;
                    case ScalingType.SCALE_BY_MAG: atkDamage = Mag * diseased * (1.0 + getTotalDoubleStat("evoMag", "")); break;
                    case ScalingType.SCALE_BY_DEF: magDamage = Def * diseased * (1.0 + getTotalDoubleStat("evoMag", "")); break;
                    case ScalingType.SCALE_BY_SPR: magDamage = Spr * diseased * (1.0 + getTotalDoubleStat("evoMag", "")); break;
                }
            }
            if (atkDamage < 1.0) atkDamage = 1.0;
            if (magDamage < 1.0) magDamage = 1.0;
        }

        public virtual bool takeAction(string input, List<Unit> victims, List<Unit> allies)
        {
            return true;
        }

        public void removeBuff(BuffComponent buff)
        {
            switch (buff.BuffType)
            {
                case BuffType.PARAMETER_BUFF:
                    foreach (ParameterType paramType in buff.ParameterTypes)
                    {
                        switch (paramType)
                        {
                            case ParameterType.ATK_PARAMETER:       setDoubleStat("baseAtkBonus", 0.0);         break;
                            case ParameterType.MAG_PARAMETER:       setDoubleStat("baseMagBonus", 0.0);         break;
                            case ParameterType.DEF_PARAMETER:       setDoubleStat("baseDefBonus", 0.0);         break;
                            case ParameterType.SPR_PARAMETER:       setDoubleStat("baseSprBonus", 0.0);         break;
                            case ParameterType.LB_FILL_RATE:        setDoubleStat("lbFillRate", 0.0);           break;
                            case ParameterType.LB_DAMAGE:           setDoubleStat("lbDamage", 0.0);             break;
                            case ParameterType.LB_REGEN:            setDoubleStat("lbRecoverBuff", 0.0);        break;
                            case ParameterType.HP_REGEN:            setDoubleStat("hpRecoverBuff", 0.0);        break;
                            case ParameterType.MP_REGEN:            setDoubleStat("mpRecoverBuff", 0.0);        break;
                            case ParameterType.LB_REGEN_PERCENT:    setDoubleStat("lbRecoverPercentBuff", 0.0); break;
                            case ParameterType.HP_REGEN_PERCENT:    setDoubleStat("hpRecoverPercentBuff", 0.0); break;
                            case ParameterType.MP_REGEN_PERCENT:    setDoubleStat("mpRecoverPercentBuff", 0.0); break;
                        }
                    }
                    break;
                case BuffType.PARAMETER_RESIST:
                    foreach (ParameterType paramResist in buff.ParameterTypes)
                    {
                        switch (paramResist)
                        {
                            case ParameterType.ATK_PARAMETER: setIntStat("atkBreakResist", 0); break;
                            case ParameterType.MAG_PARAMETER: setIntStat("magBreakResist", 0); break;
                            case ParameterType.DEF_PARAMETER: setIntStat("defBreakResist", 0); break;
                            case ParameterType.SPR_PARAMETER: setIntStat("sprBreakResist", 0); break;
                        }
                    }
                    break;
                case BuffType.STATUS_BUFF:
                    foreach (StatusType status in buff.StatusTypes)
                    {
                        switch (status)
                        {
                            case StatusType.POISON: setIntStat("poisonResist", 0); break;
                            case StatusType.SILENCE: setIntStat("silenceResist", 0); break;
                            case StatusType.BLIND: setIntStat("blindResist", 0); break;
                            case StatusType.SLEEP: setIntStat("sleepResist", 0); break;
                            case StatusType.CONFUSE: setIntStat("confuseResist", 0); break;
                            case StatusType.PARALYSE: setIntStat("paralyseResist", 0); break;
                            case StatusType.PETRIFY: setIntStat("petrifyResist", 0); break;
                            case StatusType.DISEASE: setIntStat("diseaseResist", 0); break;
                            case StatusType.DEATH: setIntStat("deathResist", 0); break;
                            case StatusType.CHARM: setIntStat("charmResist", 0); break;
                            case StatusType.STOP: setIntStat("stopResist", 0); break;
                            case StatusType.BERSERK: setIntStat("berserkResist", 0); break;
                        }
                    }
                    break;
                case BuffType.MITIGATION_BUFF:
                    foreach (MitigationType mitigation in buff.MitigationTypes)
                    {
                        switch (mitigation)
                        {
                            case MitigationType.GENERAL_MITIGATION: setDoubleStat("generalMitigation", 0.0); break;
                            case MitigationType.PHYSICAL_MITIGATION: setDoubleStat("physicalMitigation", 0.0); break;
                            case MitigationType.MAGICAL_MITIGATION: setDoubleStat("magicalMitigation", 0.0); break;
                            case MitigationType.COVER_MITIGATION:
                                setDoubleStat("coverMitigation", 0.0);
                                setDoubleStat("coverMitigationRange", 0.0);
                                break;
                            case MitigationType.DEFEND_MITIGATION: setDoubleStat("defendMitigation", 0.0); break;
                        }
                    }
                    break;
                case BuffType.AI_MANIPULATION:
                    foreach (AIManipulationType aiType in buff.AIManipulationTypes)
                    {
                        switch (aiType)
                        {
                            case AIManipulationType.TARGETING_CHANCES: setDoubleStat("targetChance", 0.0); break;
                            case AIManipulationType.ALL_COVER_CHANCES: setDoubleStat("allCover", 0.0); break;
                            case AIManipulationType.PHYSICAL_COVER_CHANCES: setDoubleStat("physicalCover", 0.0); break;
                            case AIManipulationType.MAGICAL_COVER_CHANCES: setDoubleStat("magicalCover", 0.0); break;
                        }
                    }
                    guardTarget = null;
                    break;
                case BuffType.ELEMENTAL_BUFF:
                    foreach (ElementalType element in buff.ElementalTypes)
                    {
                        switch (element)
                        {
                            case ElementalType.FIRE: setDoubleStat("fireResist", 0.0); break;
                            case ElementalType.ICE: setDoubleStat("iceResist", 0.0); break;
                            case ElementalType.THUNDER: setDoubleStat("thunderResist", 0.0); break;
                            case ElementalType.WATER: setDoubleStat("waterResist", 0.0); break;
                            case ElementalType.WIND: setDoubleStat("windResist", 0.0); break;
                            case ElementalType.EARTH: setDoubleStat("earthResist", 0.0); break;
                            case ElementalType.LIGHT: setDoubleStat("lightResist", 0.0); break;
                            case ElementalType.DARK: setDoubleStat("darkResist", 0.0); break;
                        }
                    }
                    break;
                case BuffType.ELEMENTAL_IMBUE:
                    foreach (ElementalType element in buff.ElementalTypes)
                    {
                        foreach (ElementalType test in imbueList)
                            if (test == element)
                            {
                                imbueList.Remove(element);
                                break;
                            }
                    }
                    break;
                case BuffType.RERAISE:
                    setBoolStat("reraise", false);
                    setDoubleStat("reraiseHp", 0.0);
                    break;
                case BuffType.PHYSICAL_KILLERS:
                    foreach (Race raceType in buff.PhysicalKillers)
                    {
                        switch (raceType)
                        {
                            case Race.HUMAN: setDoubleStat("killerPhysicalHUMAN", 0.0); break;
                            case Race.BEAST: setDoubleStat("killerPhysicalBEAST", 0.0); break;
                            case Race.DEMON: setDoubleStat("killerPhysicalDEMON", 0.0); break;
                            case Race.BIRD: setDoubleStat("killerPhysicalBIRD", 0.0); break;
                            case Race.PLANT: setDoubleStat("killerPhysicalPLANT", 0.0); break;
                            case Race.MACHINE: setDoubleStat("killerPhysicalMACHINE", 0.0); break;
                            case Race.STONE: setDoubleStat("killerPhysicalSTONE", 0.0); break;
                            case Race.BUG: setDoubleStat("killerPhysicalBUG", 0.0); break;
                            case Race.SPIRIT: setDoubleStat("killerPhysicalSPIRIT", 0.0); break;
                            case Race.UNDEAD: setDoubleStat("killerPhysicalUNDEAD", 0.0); break;
                            case Race.DRAGON: setDoubleStat("killerPhysicalDRAGON", 0.0); break;
                        }
                    }
                    break;
                case BuffType.MAGICAL_KILLERS:
                    foreach (Race raceType in buff.MagicalKillers)
                    {
                        switch (raceType)
                        {
                            case Race.HUMAN: setDoubleStat("killerMagicalHUMAN", 0.0); break;
                            case Race.BEAST: setDoubleStat("killerMagicalBEAST", 0.0); break;
                            case Race.DEMON: setDoubleStat("killerMagicalDEMON", 0.0); break;
                            case Race.BIRD: setDoubleStat("killerMagicalBIRD", 0.0); break;
                            case Race.PLANT: setDoubleStat("killerMagicalPLANT", 0.0); break;
                            case Race.MACHINE: setDoubleStat("killerMagicalMACHINE", 0.0); break;
                            case Race.STONE: setDoubleStat("killerMagicalSTONE", 0.0); break;
                            case Race.BUG: setDoubleStat("killerMagicalBUG", 0.0); break;
                            case Race.SPIRIT: setDoubleStat("killerMagicalSPIRIT", 0.0); break;
                            case Race.UNDEAD: setDoubleStat("killerMagicalUNDEAD", 0.0); break;
                            case Race.DRAGON: setDoubleStat("killerMagicalDRAGON", 0.0); break;
                        }
                    }
                    break;
                case BuffType.TRIGGER_ABILITY: foreach (string ability in buff.AbilitiesList) triggerList.Add(ability); break;
                case BuffType.DAMAGE_MODIFIER: foreach (string ability in buff.AbilitiesList) setDoubleStat(ability + "Buff", 0.0); break;
                case BuffType.JUMP:
                    if(isPlayer)
                    {
                        setUnitState(UnitState.UNIT_FALLING);
                        BuffComponent newBuff = new BuffComponent(buff.Name, 0, 0, buff.Effect, buff.Scale, buff.LimitScale, buff.ActionType, buff.TargetType, null, false, false, 2, 0, new List<string>(),
                            new List<string>(buff.AbilitiesList), BuffType.FALLING, new List<Race>(), new List<Race>(), new List<MitigationType>(), new List<ParameterType>(), new List<StatusType>(),
                            new List<AIManipulationType>(), new List<ElementalType>(), new List<ScalingType>());
                        buffs.Add(newBuff);
                    }
                    else
                    {
                        foreach (string ability in buff.AbilitiesList)
                            triggerList.Add(ability);
                        setBoolStat("actionTaken", true);
                    }  
                    break;
            }
        }

        private void removeDebuff(DebuffComponent debuff)
        {
            switch (debuff.DebuffType)
            {
                case DebuffType.REMOVE_FROM_FIGHT: unitState = UnitState.UNIT_ALIVE; break;
                case DebuffType.DAMAGE_PER_TURN: takeDamageOverTime(debuff.queue); break;
                case DebuffType.PARAMETER_DEBUFF:
                    foreach (ParameterType paramType in debuff.ParameterTypes)
                    {
                        switch (paramType)
                        {
                            case ParameterType.ATK_PARAMETER:
                                if (getDoubleStat("baseAtkPenalty") > 0.0) setDoubleStat("baseAtkPenalty", 0.0);
                                break;
                            case ParameterType.MAG_PARAMETER:
                                if (getDoubleStat("baseMagPenalty") > 0.0) setDoubleStat("baseMagPenalty", 0.0);
                                break;
                            case ParameterType.DEF_PARAMETER:
                                if (getDoubleStat("baseDefPenalty") > 0.0) setDoubleStat("baseDefPenalty", 0.0);
                                break;
                            case ParameterType.SPR_PARAMETER:
                                if (getDoubleStat("baseSprPenalty") > 0.0) setDoubleStat("baseSprPenalty", 0.0);
                                break;
                        }
                    }
                    break;
                case DebuffType.STATUS_DEBUFF:
                    foreach (StatusType status in debuff.StatusTypes)
                    {
                        switch (status)
                        {
                            case StatusType.POISON:
                                if (getBoolStat("poisoned")) setBoolStat("poisoned", false);
                                break;
                            case StatusType.SILENCE:
                                if (getBoolStat("silenced")) setBoolStat("silenced", false);
                                break;
                            case StatusType.BLIND:
                                if (getBoolStat("blinded")) setBoolStat("blinded", false);
                                break;
                            case StatusType.SLEEP:
                                if (getBoolStat("sleeping")) setBoolStat("sleeping", false);
                                break;
                            case StatusType.CONFUSE:
                                if (getBoolStat("confused")) setBoolStat("confused", false);
                                break;
                            case StatusType.PARALYSE:
                                if (getBoolStat("paralysed")) setBoolStat("paralysed", false);
                                break;
                            case StatusType.PETRIFY:
                                if (getBoolStat("petrified")) setBoolStat("petrified", false);
                                break;
                            case StatusType.DISEASE:
                                if (getBoolStat("diseased")) setBoolStat("diseased", false);
                                break;
                            case StatusType.CHARM:
                                if (getBoolStat("charmed")) setBoolStat("charmed", false);
                                break;
                            case StatusType.STOP:
                                if (getBoolStat("stopped")) setBoolStat("stopped", false);
                                break;
                            case StatusType.BERSERK:
                                if (getBoolStat("berserked")) setBoolStat("berserked", false);
                                break;
                        }
                    }/**/
                    break;
                case DebuffType.ELEMENTAL_DEBUFF:
                    foreach (ElementalType element in debuff.ElementalTypes)
                    {
                        switch (element)
                        {
                            case ElementalType.FIRE: setDoubleStat("fireImperil", 0.0); break;
                            case ElementalType.ICE: setDoubleStat("iceImperil", 0.0); break;
                            case ElementalType.THUNDER: setDoubleStat("thunderImperil", 0.0); break;
                            case ElementalType.WATER: setDoubleStat("waterImperil", 0.0); break;
                            case ElementalType.WIND: setDoubleStat("windImperil", 0.0); break;
                            case ElementalType.EARTH: setDoubleStat("earthImperil", 0.0); break;
                            case ElementalType.LIGHT: setDoubleStat("lightImperil", 0.0); break;
                            case ElementalType.DARK: setDoubleStat("darkImperil", 0.0); break;
                        }
                    }
                    break;
            }
        }

        public void checkBuff(BuffComponent buff, double regenEffect)
        {
            if (unitState == UnitState.UNIT_DEAD || Petrified) return;
            double doubleBuff = buff.Effect;
            if (regenEffect > 1e-4) doubleBuff = regenEffect;
            doubleBuff += buff.limitBurst * buff.LimitScale;
            int intBuff = (int)doubleBuff;
            double doubleScaleBuff = buff.Scale;

            switch (buff.BuffType)
            {
                case BuffType.PARAMETER_BUFF:
                    foreach (ParameterType paramType in buff.ParameterTypes)
                    {
                        switch (paramType)
                        {
                            case ParameterType.ATK_PARAMETER:       if (doubleBuff > getDoubleStat("baseAtkBonus"))         setDoubleStat("baseAtkBonus", doubleBuff);          break;
                            case ParameterType.MAG_PARAMETER:       if (doubleBuff > getDoubleStat("baseMagBonus"))         setDoubleStat("baseMagBonus", doubleBuff);          break;
                            case ParameterType.DEF_PARAMETER:       if (doubleBuff > getDoubleStat("baseDefBonus"))         setDoubleStat("baseDefBonus", doubleBuff);          break;
                            case ParameterType.SPR_PARAMETER:       if (doubleBuff > getDoubleStat("baseSprBonus"))         setDoubleStat("baseSprBonus", doubleBuff);          break;
                            case ParameterType.LB_FILL_RATE:        if (doubleBuff > getDoubleStat("lbFillRate"))           setDoubleStat("lbFillRate", doubleBuff);            break;
                            case ParameterType.LB_DAMAGE:           if (doubleBuff > getDoubleStat("lbDamage"))             setDoubleStat("lbDamage", doubleBuff);              break;
                            case ParameterType.LB_REGEN:            if (doubleBuff > getDoubleStat("lbRecoverBuff"))        setDoubleStat("lbRecoverBuff", doubleBuff);         break;
                            case ParameterType.HP_REGEN:            if (doubleBuff > getDoubleStat("hpRecoverBuff"))        setDoubleStat("hpRecoverBuff", doubleBuff);         break;
                            case ParameterType.MP_REGEN:            if (doubleBuff > getDoubleStat("mpRecoverBuff"))        setDoubleStat("mpRecoverBuff", doubleBuff);         break;
                            case ParameterType.LB_REGEN_PERCENT:    if (doubleBuff > getDoubleStat("lbRecoverPercentBuff")) setDoubleStat("lbRecoverPercentBuff", doubleBuff);  break;
                            case ParameterType.HP_REGEN_PERCENT:    if (doubleBuff > getDoubleStat("hpRecoverPercentBuff")) setDoubleStat("hpRecoverPercentBuff", doubleBuff);  break;
                            case ParameterType.MP_REGEN_PERCENT:    if (doubleBuff > getDoubleStat("mpRecoverPercentBuff")) setDoubleStat("mpRecoverPercentBuff", doubleBuff);  break;
                        }
                    }
                    break;
                case BuffType.PARAMETER_RESIST:
                    foreach (ParameterType paramResist in buff.ParameterTypes)
                    {
                        switch (paramResist)
                        {
                            case ParameterType.ATK_PARAMETER: if (intBuff > getIntStat("atkBreakResist")) setIntStat("atkBreakResist", intBuff); break;
                            case ParameterType.MAG_PARAMETER: if (intBuff > getIntStat("magBreakResist")) setIntStat("magBreakResist", intBuff); break;
                            case ParameterType.DEF_PARAMETER: if (intBuff > getIntStat("defBreakResist")) setIntStat("defBreakResist", intBuff); break;
                            case ParameterType.SPR_PARAMETER: if (intBuff > getIntStat("sprBreakResist")) setIntStat("sprBreakResist", intBuff); break;
                        }
                    }
                    break;
                case BuffType.STATUS_BUFF:
                    foreach (StatusType status in buff.StatusTypes)
                    {
                        switch (status)
                        {
                            case StatusType.POISON: if (intBuff > getIntStat("poisonResist")) setIntStat("poisonResist", intBuff); break;
                            case StatusType.SILENCE: if (intBuff > getIntStat("silenceResist")) setIntStat("silenceResist", intBuff); break;
                            case StatusType.BLIND: if (intBuff > getIntStat("blindResist")) setIntStat("blindResist", intBuff); break;
                            case StatusType.SLEEP: if (intBuff > getIntStat("sleepResist")) setIntStat("sleepResist", intBuff); break;
                            case StatusType.CONFUSE: if (intBuff > getIntStat("confuseResist")) setIntStat("confuseResist", intBuff); break;
                            case StatusType.PARALYSE: if (intBuff > getIntStat("paralyseResist")) setIntStat("paralyseResist", intBuff); break;
                            case StatusType.PETRIFY: if (intBuff > getIntStat("petrifyResist")) setIntStat("petrifyResist", intBuff); break;
                            case StatusType.DISEASE: if (intBuff > getIntStat("diseaseResist")) setIntStat("diseaseResist", intBuff); break;
                            case StatusType.DEATH: if (intBuff > getIntStat("deathResist")) setIntStat("deathResist", intBuff); break;
                            case StatusType.CHARM: if (intBuff > getIntStat("charmResist")) setIntStat("charmResist", intBuff); break;
                            case StatusType.STOP: if (intBuff > getIntStat("stopResist")) setIntStat("stopResist", intBuff); break;
                            case StatusType.BERSERK: if (intBuff > getIntStat("berserkResist")) setIntStat("berserkResist", intBuff); break;
                        }
                    }
                    break;
                case BuffType.MITIGATION_BUFF:
                    foreach (MitigationType mitigation in buff.MitigationTypes)
                    {
                        switch (mitigation)
                        {
                            case MitigationType.GENERAL_MITIGATION: if (doubleBuff > getDoubleStat("generalMitigation")) setDoubleStat("generalMitigation", doubleBuff); break;
                            case MitigationType.PHYSICAL_MITIGATION: if (doubleBuff > getDoubleStat("physicalMitigation")) setDoubleStat("physicalMitigation", doubleBuff); break;
                            case MitigationType.MAGICAL_MITIGATION: if (doubleBuff > getDoubleStat("magicalMitigation")) setDoubleStat("magicalMitigation", doubleBuff); break;
                            case MitigationType.COVER_MITIGATION:
                                if (doubleBuff > getDoubleStat("coverMitigation"))
                                    setDoubleStat("coverMitigation", doubleBuff);
                                if (doubleScaleBuff > getDoubleStat("coverMitigationRange"))
                                    setDoubleStat("coverMitigationRange", doubleScaleBuff);
                                break;
                            case MitigationType.DEFEND_MITIGATION: if (doubleBuff > getDoubleStat("defendMitigation")) setDoubleStat("defendMitigation", doubleBuff); break;
                        }
                    }
                    break;
                case BuffType.AI_MANIPULATION:
                    foreach (AIManipulationType aiType in buff.AIManipulationTypes)
                    {
                        switch (aiType)
                        {
                            case AIManipulationType.TARGETING_CHANCES: if (doubleBuff > getDoubleStat("targetChance")) setDoubleStat("targetChance", doubleBuff); break;
                            case AIManipulationType.ALL_COVER_CHANCES: if (doubleBuff > getDoubleStat("allCover")) setDoubleStat("allCover", doubleBuff); break;
                            case AIManipulationType.PHYSICAL_COVER_CHANCES: if (doubleBuff > getDoubleStat("physicalCover")) setDoubleStat("physicalCover", doubleBuff); break;
                            case AIManipulationType.MAGICAL_COVER_CHANCES: if (doubleBuff > getDoubleStat("magicalCover")) setDoubleStat("magicalCover", doubleBuff); break;
                        }
                    }
                    break;
                case BuffType.ELEMENTAL_BUFF:
                    foreach (ElementalType element in buff.ElementalTypes)
                    {
                        switch (element)
                        {
                            case ElementalType.FIRE: if (doubleBuff > getDoubleStat("fireResist")) setDoubleStat("fireResist", doubleBuff); break;
                            case ElementalType.ICE: if (doubleBuff > getDoubleStat("iceResist")) setDoubleStat("iceResist", doubleBuff); break;
                            case ElementalType.THUNDER: if (doubleBuff > getDoubleStat("thunderResist")) setDoubleStat("thunderResist", doubleBuff); break;
                            case ElementalType.WATER: if (doubleBuff > getDoubleStat("waterResist")) setDoubleStat("waterResist", doubleBuff); break;
                            case ElementalType.WIND: if (doubleBuff > getDoubleStat("windResist")) setDoubleStat("windResist", doubleBuff); break;
                            case ElementalType.EARTH: if (doubleBuff > getDoubleStat("earthResist")) setDoubleStat("earthResist", doubleBuff); break;
                            case ElementalType.LIGHT: if (doubleBuff > getDoubleStat("lightResist")) setDoubleStat("lightResist", doubleBuff); break;
                            case ElementalType.DARK: if (doubleBuff > getDoubleStat("darkResist")) setDoubleStat("darkResist", doubleBuff); break;
                        }
                    }
                    break;
                case BuffType.ELEMENTAL_IMBUE:
                    foreach (ElementalType element in buff.ElementalTypes)
                    {
                        bool flag = true;
                        foreach (ElementalType test in imbueList)
                            if (test == element)
                                flag = false;
                        if (flag) imbueList.Add(element);
                    }
                    break;
                case BuffType.RERAISE:
                    setBoolStat("reraise", true);
                    if (doubleBuff > getDoubleStat("reraiseHp")) setDoubleStat("reraiseHp", doubleBuff);
                    break;
                case BuffType.PHYSICAL_KILLERS:
                    foreach (Race raceType in buff.PhysicalKillers)
                    {
                        switch (raceType)
                        {
                            case Race.HUMAN: if (doubleBuff > getDoubleStat("killerPhysicalHUMAN")) setDoubleStat("killerPhysicalHUMAN", doubleBuff); break;
                            case Race.BEAST: if (doubleBuff > getDoubleStat("killerPhysicalBEAST")) setDoubleStat("killerPhysicalBEAST", doubleBuff); break;
                            case Race.DEMON: if (doubleBuff > getDoubleStat("killerPhysicalDEMON")) setDoubleStat("killerPhysicalDEMON", doubleBuff); break;
                            case Race.BIRD: if (doubleBuff > getDoubleStat("killerPhysicalBIRD")) setDoubleStat("killerPhysicalBIRD", doubleBuff); break;
                            case Race.PLANT: if (doubleBuff > getDoubleStat("killerPhysicalPLANT")) setDoubleStat("killerPhysicalPLANT", doubleBuff); break;
                            case Race.MACHINE: if (doubleBuff > getDoubleStat("killerPhysicalMACHINE")) setDoubleStat("killerPhysicalMACHINE", doubleBuff); break;
                            case Race.STONE: if (doubleBuff > getDoubleStat("killerPhysicalSTONE")) setDoubleStat("killerPhysicalSTONE", doubleBuff); break;
                            case Race.BUG: if (doubleBuff > getDoubleStat("killerPhysicalBUG")) setDoubleStat("killerPhysicalBUG", doubleBuff); break;
                            case Race.SPIRIT: if (doubleBuff > getDoubleStat("killerPhysicalSPIRIT")) setDoubleStat("killerPhysicalSPIRIT", doubleBuff); break;
                            case Race.UNDEAD: if (doubleBuff > getDoubleStat("killerPhysicalUNDEAD")) setDoubleStat("killerPhysicalUNDEAD", doubleBuff); break;
                            case Race.DRAGON: if (doubleBuff > getDoubleStat("killerPhysicalDRAGON")) setDoubleStat("killerPhysicalDRAGON", doubleBuff); break;
                        }
                    }
                    break;
                case BuffType.MAGICAL_KILLERS:
                    foreach (Race raceType in buff.MagicalKillers)
                    {
                        switch (raceType)
                        {
                            case Race.HUMAN: if (doubleBuff > getDoubleStat("killerMagicalHUMAN")) setDoubleStat("killerMagicalHUMAN", doubleBuff); break;
                            case Race.BEAST: if (doubleBuff > getDoubleStat("killerMagicalBEAST")) setDoubleStat("killerMagicalBEAST", doubleBuff); break;
                            case Race.DEMON: if (doubleBuff > getDoubleStat("killerMagicalDEMON")) setDoubleStat("killerMagicalDEMON", doubleBuff); break;
                            case Race.BIRD: if (doubleBuff > getDoubleStat("killerMagicalBIRD")) setDoubleStat("killerMagicalBIRD", doubleBuff); break;
                            case Race.PLANT: if (doubleBuff > getDoubleStat("killerMagicalPLANT")) setDoubleStat("killerMagicalPLANT", doubleBuff); break;
                            case Race.MACHINE: if (doubleBuff > getDoubleStat("killerMagicalMACHINE")) setDoubleStat("killerMagicalMACHINE", doubleBuff); break;
                            case Race.STONE: if (doubleBuff > getDoubleStat("killerMagicalSTONE")) setDoubleStat("killerMagicalSTONE", doubleBuff); break;
                            case Race.BUG: if (doubleBuff > getDoubleStat("killerMagicalBUG")) setDoubleStat("killerMagicalBUG", doubleBuff); break;
                            case Race.SPIRIT: if (doubleBuff > getDoubleStat("killerMagicalSPIRIT")) setDoubleStat("killerMagicalSPIRIT", doubleBuff); break;
                            case Race.UNDEAD: if (doubleBuff > getDoubleStat("killerMagicalUNDEAD")) setDoubleStat("killerMagicalUNDEAD", doubleBuff); break;
                            case Race.DRAGON: if (doubleBuff > getDoubleStat("killerMagicalDRAGON")) setDoubleStat("killerMagicalDRAGON", doubleBuff); break;
                        }
                    }
                    break;
                case BuffType.JUMP:
                    if(unitState != UnitState.UNIT_AIRBORNE) setUnitState(UnitState.UNIT_AIRBORNE);
                    setBoolStat("actionTaken", true);
                    break;
                /*case BuffType.DAMAGE_MODIFIER:
                    foreach(string ability in buff.AbilitiesList)
                        if (doubleBuff > getDoubleStat(ability + "Buff"))
                            setDoubleStat(ability + "Buff", doubleBuff);
                    break;*/
            }
        }

        public void checkDamageModifierBuffs()
        {
            Dictionary<string, double> damageMods = new Dictionary<string, double>();
            foreach (BuffComponent buff in buffs)
            {
                foreach (string ability in buff.AbilitiesList)
                {
                    if (damageMods.ContainsKey(ability)) damageMods[ability] += buff.Effect;
                    else damageMods.Add(ability, buff.Effect);
                }
            }
            foreach (string ability in damageMods.Keys) setDoubleStat(ability + "Buff", damageMods[ability]);
        }

        public bool checkDebuff(DebuffComponent debuff, bool applied)
        {
            if (unitState == UnitState.UNIT_DEAD || Petrified) return false;
            double doubleDebuff = debuff.Effect + debuff.limitBurst * debuff.LimitScale;
            int intDebuff = (int) doubleDebuff;
            int chance = AssetLoader.random.Next(100);

            int buffCount, debuffCount, i;
            switch (debuff.DebuffType)
            {
                case DebuffType.SCAN_STATS: displayExtensiveStats(); return true;
                case DebuffType.REMOVE_FROM_FIGHT: if (unitState == UnitState.UNIT_ALIVE) unitState = UnitState.UNIT_REMOVED; return debuff.Duration > 0;
                case DebuffType.DAMAGE_PER_TURN: if (doubleDebuff >= 1 && applied) takeDamageOverTime(debuff.queue); return true;
                case DebuffType.REMOVE_BUFF:
                    buffCount = 0;
                    debuffCount = 0;
                    i = 0;
                    while (i < buffs.Count)
                    {
                        if (buffs[i].Dispelable)
                        {
                            removeBuff(buffs[i]);
                            buffs.RemoveAt(i);
                            buffCount++;
                        }
                        else i++;
                    }
                    foreach (BuffComponent newBuff in buffs) checkBuff(newBuff, 0);
                    checkDamageModifierBuffs();
                    if (buffCount > 0) Console.WriteLine("Removed " + buffCount.ToString() + " buff component/s from " + Name + "!");
                    return true;
                case DebuffType.REMOVE_STATUS:
                    buffCount = 0;
                    debuffCount = 0;
                    i = 0;
                    while (i < buffs.Count)
                    {
                        if (buffs[i].Dispelable)
                        {
                            removeBuff(buffs[i]);
                            buffs.RemoveAt(i);
                            buffCount++;
                        }
                        else i++;
                    }
                    foreach (BuffComponent newBuff in buffs) checkBuff(newBuff, 0);
                    checkDamageModifierBuffs();
                    if (buffCount > 0) Console.WriteLine("Removed " + buffCount.ToString() + " buff component/s from " + Name + "!");
                    i = 0;
                    while (i < debuffs.Count)
                    {
                        if (debuffs[i].Dispelable)
                        {
                            removeDebuff(debuffs[i]);
                            debuffs.RemoveAt(i);
                            debuffCount++;
                        }
                        else i++;
                    }
                    foreach (DebuffComponent newDebuff in debuffs) checkDebuff(newDebuff, true);
                    if (debuffCount > 0) Console.WriteLine("Removed " + debuffCount.ToString() + " debuff component/s from " + Name + "!");
                    return true;
                case DebuffType.PARAMETER_DEBUFF:
                    for (i = 0; i < debuff.ParameterTypes.Count; i++)
                    {
                        switch (debuff.ParameterTypes[i])
                        {
                            case ParameterType.ATK_PARAMETER:
                                if (chance >= AtkBreakResist || applied)
                                {
                                    if(doubleDebuff > getDoubleStat("baseAtkPenalty")) setDoubleStat("baseAtkPenalty", doubleDebuff);
                                    if(!applied) Console.WriteLine(Name + " is now ATK broken!");
                                }
                                else
                                {
                                    debuff.removeParameter(ParameterType.ATK_PARAMETER);
                                    i--;
                                }
                                break;
                            case ParameterType.MAG_PARAMETER:
                                if (chance >= MagBreakResist || applied)
                                {
                                    if (doubleDebuff > getDoubleStat("baseMagPenalty")) setDoubleStat("baseMagPenalty", doubleDebuff);
                                    if (!applied) Console.WriteLine(Name + " is now MAG broken!");
                                }
                                else
                                {
                                    debuff.removeParameter(ParameterType.MAG_PARAMETER);
                                    i--;
                                }
                                break;
                            case ParameterType.DEF_PARAMETER:
                                if (chance >= DefBreakResist || applied)
                                {
                                    if (doubleDebuff > getDoubleStat("baseDefPenalty")) setDoubleStat("baseDefPenalty", doubleDebuff);
                                    if (!applied) Console.WriteLine(Name + " is now DEF broken!");
                                }
                                else
                                {
                                    debuff.removeParameter(ParameterType.DEF_PARAMETER);
                                    i--;
                                }
                                break;
                            case ParameterType.SPR_PARAMETER:
                                if (chance >= SprBreakResist || applied)
                                {
                                    if(doubleDebuff > getDoubleStat("baseSprPenalty")) setDoubleStat("baseSprPenalty", doubleDebuff);
                                    if (!applied) Console.WriteLine(Name + " is now SPR broken!");
                                }
                                else 
                                {
                                    debuff.removeParameter(ParameterType.SPR_PARAMETER);
                                    i--;
                                }
                                break;
                            case ParameterType.SET_HP:
                                if (chance >= GravityResist && !applied)
                                {
                                    double hp = getDoubleStat("hp");
                                    Console.WriteLine(Name + " has their HP reduced by " + Math.Floor(hp * doubleDebuff).ToString() + "!");
                                    hp *= 1.0 - doubleDebuff;
                                    if(hp <= 1) hp = 1;
                                    setDoubleStat("hp", hp);
                                    return true;
                                }
                                return false;
                        }
                    }
                    return debuff.ParameterTypes.Count > 0;
                case DebuffType.STATUS_DEBUFF:
                    if(applied || chance >= intDebuff) return false;
                    for (i = 0; i < debuff.StatusTypes.Count; i++)
                    {
                        chance = AssetLoader.random.Next(100);
                        int extra = 0;
                        if (intDebuff > 100) extra = intDebuff - 100;
                        switch (debuff.StatusTypes[i])
                        {
                            case StatusType.DEATH:
                                if (chance >= DeathResist - extra) setDoubleStat("hp", unitIsDead());
                                else
                                {
                                    debuff.removeStatus(StatusType.DEATH);
                                    i--;
                                }
                                break;
                            case StatusType.POISON:
                                if (!getBoolStat("poisoned") && chance >= PoisonResist - extra)
                                {
                                    setBoolStat("poisoned", true);
                                    Console.WriteLine(Name + " is now poisoned!");
                                }
                                else
                                {
                                    debuff.removeStatus(StatusType.POISON);
                                    i--;
                                }
                                break;
                            case StatusType.SILENCE:
                                if (!getBoolStat("silenced") && chance >= SilenceResist - extra)
                                {
                                    setBoolStat("silenced", true);
                                    Console.WriteLine(Name + " is now silenced!");
                                }
                                else
                                {
                                    debuff.removeStatus(StatusType.SILENCE);
                                    i--;
                                }
                                break;
                            case StatusType.BLIND:
                                if (!getBoolStat("blinded") && chance >= BlindResist - extra)
                                {
                                    setBoolStat("blinded", true);
                                    Console.WriteLine(Name + " is now blind!");
                                }
                                else
                                {
                                    debuff.removeStatus(StatusType.BLIND);
                                    i--;
                                }
                                break;
                            case StatusType.SLEEP:
                                if (!getBoolStat("sleeping") && chance >= SleepResist - extra)
                                {
                                    setBoolStat("sleeping", true);
                                    Console.WriteLine(Name + " is now sleeping!");
                                }
                                else
                                {
                                    debuff.removeStatus(StatusType.SLEEP);
                                    i--;
                                }
                                break;
                            case StatusType.CONFUSE:
                                if (!getBoolStat("confused") && chance >= ConfuseResist - extra)
                                {
                                    setBoolStat("confused", true);
                                    Console.WriteLine(Name + " is now confused!");
                                }
                                else
                                {
                                    debuff.removeStatus(StatusType.CONFUSE);
                                    i--;
                                }
                                break;
                            case StatusType.PARALYSE:
                                if (!getBoolStat("paralysed") && chance >= ParalyseResist - extra)
                                {
                                    setBoolStat("paralysed", true);
                                    Console.WriteLine(Name + " is now paralysed!");
                                }
                                else
                                {
                                    debuff.removeStatus(StatusType.PARALYSE);
                                    i--;
                                }
                                break;
                            case StatusType.PETRIFY:
                                if (!getBoolStat("petrified") && chance >= PetrifyResist - extra)
                                {
                                    setBoolStat("petrified", true);
                                    Console.WriteLine(Name + " is now petrified!");
                                }
                                else
                                {
                                    debuff.removeStatus(StatusType.PETRIFY);
                                    i--;
                                }
                                break;
                            case StatusType.DISEASE:
                                if (!getBoolStat("diseased") && chance >= DiseaseResist - extra)
                                {
                                    setBoolStat("diseased", true);
                                    Console.WriteLine(Name + " is now diseased!");
                                }
                                else
                                {
                                    debuff.removeStatus(StatusType.DISEASE);
                                    i--;
                                }
                                break;
                            case StatusType.CHARM:
                                if (!getBoolStat("charmed") && chance >= CharmResist - extra)
                                {
                                    setBoolStat("charmed", true);
                                    Console.WriteLine(Name + " is now charmed!");
                                }
                                else
                                {
                                    debuff.removeStatus(StatusType.CHARM);
                                    i--;
                                }
                                break;
                            case StatusType.STOP:
                                if (!getBoolStat("stopped") && chance >= StopResist - extra)
                                {
                                    setBoolStat("stopped", true);
                                    Console.WriteLine(Name + " is now stopped!");
                                }
                                else
                                {
                                    debuff.removeStatus(StatusType.STOP);
                                    i--;
                                }
                                break;
                            case StatusType.BERSERK:
                                if (!getBoolStat("berserked") && chance >= BerserkResist - extra)
                                {
                                    setBoolStat("berserked", true);
                                    Console.WriteLine(Name + " is now berserked!");
                                }
                                else
                                {
                                    debuff.removeStatus(StatusType.BERSERK);
                                    i--;
                                }
                                break;
                        }
                    }
                    return debuff.StatusTypes.Count > 0 && debuff.Duration > 0;
                case DebuffType.ELEMENTAL_DEBUFF:
                    foreach (ElementalType element in debuff.ElementalTypes)
                    {
                        switch (element)
                        {
                            case ElementalType.FIRE: if (doubleDebuff > getDoubleStat("fireImperil")) setDoubleStat("fireImperil", doubleDebuff); break;
                            case ElementalType.ICE: if (doubleDebuff > getDoubleStat("iceImperil")) setDoubleStat("iceImperil", doubleDebuff); break;
                            case ElementalType.THUNDER: if (doubleDebuff > getDoubleStat("thunderImperil")) setDoubleStat("thunderImperil", doubleDebuff); break;
                            case ElementalType.WATER: if (doubleDebuff > getDoubleStat("waterImperil")) setDoubleStat("waterImperil", doubleDebuff); break;
                            case ElementalType.WIND: if (doubleDebuff > getDoubleStat("windImperil")) setDoubleStat("windImperil", doubleDebuff); break;
                            case ElementalType.EARTH: if (doubleDebuff > getDoubleStat("earthImperil")) setDoubleStat("earthImperil", doubleDebuff); break;
                            case ElementalType.LIGHT: if (doubleDebuff > getDoubleStat("lightImperil")) setDoubleStat("lightImperil", doubleDebuff); break;
                            case ElementalType.DARK: if (doubleDebuff > getDoubleStat("darkImperil")) setDoubleStat("darkImperil", doubleDebuff); break;
                        }
                    }
                    return true;
            }
            return false;
        }

        public void startOfTurnPhase(bool startOfBattle, List<Unit> victims, List<Unit> allies)
        {
            triggerList.Clear();
            abilityCommands.Clear();
            castCount = castFrame = 0;
            setBoolStat("actionTaken", false);
            setBoolStat("readyState", false);
            setIntStat("timedDelay", 0);
            guardian = null;
            currentCover = AIManipulationType.NONE;
            coverTriggered = false;
            coverBonus = 0.0;
            int roll = AssetLoader.random.Next(100);
            if (roll < 25 + (getTotalIntStat("accuracy", "tdhBoost") / 2)) setIntStat("hitResult", 0);
            else setIntStat("hitResult", AssetLoader.random.Next(5) + 1);

            if (unitState == UnitState.UNIT_DEAD) return;

            foreach (string name in listOfCooldowns)
            {
                int cooldown = getIntStat(name);
                if (cooldown > 0 && --cooldown < 1) Console.WriteLine(name + " can now be used!");
                setIntStat(name, cooldown);
            }

            int buffCount = 0, i = 0;
            foreach (string name in listOfCounters) setIntStat(name, 0);

            while (i < buffs.Count)
            {
                if (buffs[i].timePassed())
                {
                    removeBuff(buffs[i]);
                    buffs.RemoveAt(i);
                    buffCount++;
                }
                else i++;
            }
            foreach (BuffComponent buff in buffs)
                checkBuff(buff, 0);
            checkDamageModifierBuffs();
            if (buffCount > 0) Console.WriteLine("Removed " + buffCount.ToString() + " buff component/s from " + Name + "!");
            
            string output = "";
            double selfDamage = getTotalDoubleStat("selfDamage", "");
            if (selfDamage > 1e-4 && unitData.getIntStatAdvanced("selfDamageImmunity", "", this) < 1) setHp(selfDamage, false, out output);
            int bpDuration = getIntStat("barrierDuration");
            if(bpDuration > 0 && --bpDuration < 1)
            {
                Console.WriteLine(Name + " is no longer protected by a barrier!");
                setDoubleStat("bp", 0.0);
            }
            setIntStat("barrierDuration", bpDuration);

            if (Petrified || Sleeping || Stopped || Paralysed || Charmed) return;

            if (startOfBattle) triggerList.AddRange(findExternalAbility("startOfBattle"));
            triggerList.AddRange(findExternalAbility("startOfTurn"));

            string error = "";
            i = 0;
            foreach (string abilityName in triggerList)
                prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData[abilityName], victims, allies);

            if (Confused)
            {
                roll = AssetLoader.random.Next(2);
                if (roll < 1) confusionAttack(i, "Attack Ally", allies, victims, allies);
                else confusionAttack(i, "Attack", victims, victims, allies);
            }
            else if (Berserked) confusionAttack(i, "Attack", victims, victims, allies);
        }

        private void confusionAttack(int i, string attack, List<Unit> selected, List<Unit> victims, List<Unit> allies)
        {
            string error = "";
            List<Unit> available = new List<Unit>();
            foreach (Unit prey in selected)
                if (prey.CurrentState == UnitState.UNIT_ALIVE && !prey.Petrified)
                    available.Add(prey);
            
            if(available.Count > 0)
            {
                int j = 0;
                int roll = AssetLoader.random.Next(available.Count);
                foreach (Unit prey in selected)
                {
                    if (prey == available[roll])
                    {
                        roll = j;
                        break;
                    }
                    else j++;
                }
                prepareAbility(i, false, true, roll, out error, AssetLoader.abilitiesData[attack], victims, allies);
            }
            else
            {
                if(GameEngine.debug) Console.WriteLine("Error with finding attacker!!!");
            }

            setBoolStat("actionTaken", true);
        }

        private List<string> findExternalAbility(string condition)
        {
            List<string> list = new List<string>();
            foreach (Passive passive in unitData.Passives)
                if (AssetLoader.random.Next(100) < passive.getIntStat(condition, "", this))
                    list.Add(passive.getIntCondition(condition));

            foreach (Weapon weapon in weaponSet)
            {
                foreach (Passive passive in weapon.EquipPassives)
                    if (AssetLoader.random.Next(100) < passive.getIntStat(condition, "", this))
                        list.Add(passive.getIntCondition(condition));
                foreach (Passive passive in weapon.EquipData.EquipPassives)
                    if (AssetLoader.random.Next(100) < passive.getIntStat(condition, "", this))
                        list.Add(passive.getIntCondition(condition));
            }

            foreach (Equipment equip in equipmentSet)
                foreach (Passive passive in equip.EquipData.EquipPassives)
                    if (AssetLoader.random.Next(100) < passive.getIntStat(condition, "", this))
                        list.Add(passive.getIntCondition(condition));

            foreach (Materia materia in materiaSet)
                if (materia.PassiveEffect != null && materia.PassiveEffect.getIntStat(condition, "", this) == 1)
                    list.Add(materia.PassiveEffect.getIntCondition(condition));

            if (esper != null)
                foreach (Passive passive in esper.EquipPassives)
                    if (AssetLoader.random.Next(100) < passive.getIntStat(condition, "", this))
                        list.Add(passive.getIntCondition(condition));
            return list;
        }

        public void endOfTurnPhase()
        {
            guardian = null;
            currentCover = AIManipulationType.NONE;
            coverTriggered = false;
            coverBonus = 0.0;
            abilityCommands.Clear();
            castCount = castFrame = 0;
            if (unitState == UnitState.UNIT_DEAD || Petrified || Confused || Sleeping || Berserked || Stopped || Paralysed || Charmed) return;
            double hp = getDoubleStat("hp");
            double mp = getDoubleStat("mp");
            double lb = getDoubleStat("lb");

            hp += MaxHP * (getDoubleStat("hpRecoverPercentBuff") + getTotalDoubleStat("hpRecover", "") + getDoubleStat("hpRecoverBuff"));
            mp += MaxMP * (getDoubleStat("mpRecoverPercentBuff") + getTotalDoubleStat("mpRecover", "") + getDoubleStat("mpRecoverBuff"));
            lb += unitData.LbCost * getDoubleStat("lbRecoverPercentBuff") + getDoubleStat("lbRecoverBuff") + getTotalDoubleStat("lbRecover", "");

            if (hp >= MaxHP) hp = MaxHP;
            if (mp >= MaxMP) mp = MaxMP;
            if (lb >= unitData.LbCost) lb = unitData.LbCost;

            if (getBoolStat("poisoned")) hp -= MaxHP / 10;
            if(hp <= 0.0) hp = unitIsDead();

            setDoubleStat("hp", hp);
            setDoubleStat("mp", mp);
            setDoubleStat("lb", lb);
            mpRefund = lbRefund = 0;
            itemRefund = "";
            abilityRefund = "";

            int debuffCount = 0, i = 0;
            while (i < debuffs.Count)
            {
                if (debuffs[i].timePassed())
                {
                    removeDebuff(debuffs[i]);
                    debuffs.RemoveAt(i);
                    debuffCount++;
                }
                else i++;
            }
            foreach (DebuffComponent debuff in debuffs) checkDebuff(debuff, true);
            if (debuffCount > 0) Console.WriteLine("Removed " + debuffCount.ToString() + " debuff component/s from " + Name + "!");

            esperHitters.Clear();
            limitHitters.Clear();
            physicalHitters.Clear();
            magicalHitters.Clear();
            //if (GameEngine.debug) Console.WriteLine(Name + "; Physical - " + physicalHitters.Count.ToString() + " ; Magical - " + magicalHitters.Count.ToString());
            fireHitters.Clear();
            iceHitters.Clear();
            thunderHitters.Clear();
            waterHitters.Clear();
            windHitters.Clear();
            earthHitters.Clear();
            lightHitters.Clear();
            darkHitters.Clear();
            elementalItemHitters.Clear();
            if (unitState == UnitState.UNIT_FALLING) setUnitState(UnitState.UNIT_ALIVE);
        }

        public void endOfTurnCounter(List<Unit> victims, List<Unit> allies)
        {
            string error = "";
            if (unitState != UnitState.UNIT_ALIVE || Petrified || Confused || Sleeping || Berserked || Stopped || Paralysed || Charmed) return;
            List<string> list = new List<string>();
            List<int> targets = new List<int>();
            int i, length;
            double bonusCounter = 1.0 + getTotalDoubleStat("increaseCounter", "");
            //if (GameEngine.debug) Console.WriteLine(Name + "; Physical - " + physicalHitters.Count.ToString() + " ; Magical - " + magicalHitters.Count.ToString());
            for (i = 0; i < physicalHitters.Count; i++)
            {
                list.AddRange(counterAttack("counterPhysical", bonusCounter, out length));
                for(int j = 0; j < length; j++) targets.Add(physicalHitters[i]);
            }
            for (i = 0; i < magicalHitters.Count; i++)
            {
                list.AddRange(counterAttack("counterMagical", bonusCounter, out length));
                for(int j = 0; j < length; j++) targets.Add(magicalHitters[i]);
            }
            i = 0;
            foreach (string abilityName in list)
            {
                prepareAbility(i, false, true, targets[i], out error, AssetLoader.abilitiesData[abilityName], victims, allies);
                i++;
            }
        }

        private List<string> counterAttack(string counterType, double bonusCounter, out int length)
        {
            int i = 0;
            List<string> list = new List<string>();
            foreach (Passive passive in unitData.Passives)
                if (passive.checkCondition(this) && checkCounterLimit((int)(passive.getSimpleDoubleStat(counterType) * 100 * bonusCounter), passive.getSimpleIntStat("counterTriggers"),
                    passive.getIntCondition("counterTriggers")) && unitLevel >= unitData.LevelRequiredPassives[i++])
                    list.Add(passive.getDoubleCondition(counterType));

            foreach (Weapon weapon in weaponSet)
            {
                foreach (Passive passive in weapon.EquipPassives)
                    if (passive.checkCondition(this) && checkCounterLimit((int)(passive.getSimpleDoubleStat(counterType) * 100 * bonusCounter), passive.getSimpleIntStat("counterTriggers"),
                        passive.getIntCondition("counterTriggers")))
                        list.Add(passive.getDoubleCondition(counterType));
                foreach (Passive passive in weapon.EquipData.EquipPassives)
                    if (passive.checkCondition(this) && checkCounterLimit((int)(passive.getSimpleDoubleStat(counterType) * 100 * bonusCounter), passive.getSimpleIntStat("counterTriggers"),
                        passive.getIntCondition("counterTriggers")))
                        list.Add(passive.getDoubleCondition(counterType));
            }

            foreach (Equipment equip in equipmentSet)
                foreach (Passive passive in equip.EquipData.EquipPassives)
                    if (passive.checkCondition(this) && checkCounterLimit((int)(passive.getSimpleDoubleStat(counterType) * 100 * bonusCounter), passive.getSimpleIntStat("counterTriggers"),
                        passive.getIntCondition("counterTriggers")))
                        list.Add(passive.getDoubleCondition(counterType));

            foreach (Materia materia in materiaSet)
                if (materia.PassiveEffect != null && materia.PassiveEffect.checkCondition(this) && checkCounterLimit((int)(materia.PassiveEffect.getSimpleDoubleStat(counterType) * 100 * bonusCounter),
                    materia.PassiveEffect.getSimpleIntStat("counterTriggers"), materia.PassiveEffect.getIntCondition("counterTriggers")))
                    list.Add(materia.PassiveEffect.getDoubleCondition(counterType));

            if (esper != null)
                foreach (Passive passive in esper.EquipPassives)
                    if (passive.checkCondition(this) && checkCounterLimit((int)(passive.getSimpleDoubleStat(counterType) * 100 * bonusCounter), passive.getSimpleIntStat("counterTriggers"),
                        passive.getIntCondition("counterTriggers")))
                        list.Add(passive.getDoubleCondition(counterType));

            foreach (BuffComponent buff in buffs)
            {
                int counter = 0;
                if((counterType.Equals("counterPhysical") && buff.BuffType == BuffType.COUNTER_PHYSICAL) || (counterType.Equals("counterMagical") && buff.BuffType == BuffType.COUNTER_MAGICAL)
                    || buff.BuffType == BuffType.COUNTER_ALL)
                    counter = (int)(buff.Effect * 100 * bonusCounter);
                if (buff.Unlocks.Count > 0 && checkCounterLimit(counter, buff.Charge, buff.Unlocks[0]))
                    list.Add(buff.Unlocks[0]);
            }

            length = list.Count;
            return list;
        }

        public bool checkCounterLimit(int counterGoal, int counterLimit, string counterName)
        {
            int roll = AssetLoader.random.Next(100);
            if (roll >= counterGoal) return false;
            if (counterLimit == -1) return true;
            else
            {
                int countersUsed = getIntStat(counterName);
                if (countersUsed < counterLimit)
                {
                    if (!listOfCounters.Contains(counterName)) listOfCounters.Add(counterName);
                    setIntStat(counterName, countersUsed + 1);
                    return true;
                }
            }
            return false;
        }

        public bool takingCover(AttackType type)
        {
            if (unitState != UnitState.UNIT_ALIVE) return false;
            if (Petrified) return false;
            switch (currentCover)
            {
                case AIManipulationType.ALL_COVER_CHANCES: return true;
                case AIManipulationType.PHYSICAL_COVER_CHANCES: return type == AttackType.PHYSICAL_ATTACK || type == AttackType.HYBRID_ATTACK;
                case AIManipulationType.MAGICAL_COVER_CHANCES: return type == AttackType.MAGICAL_ATTACK;
            }
            return false;
        }

        protected virtual void getKillerMods(out double physicalKiller, out double magicalKiller, List<double> physicalList, List<double> magicalList)
        {
            int physicalKillerCount = 0, magicalKillerCount = 0;
            physicalKiller = 0.0;
            magicalKiller = 0.0;

            foreach (Race current in unitData.RaceTypes)
            {
                int i = 0;
                foreach (Race killType in AssetLoader.killerTypes)
                {
                    if (killType == current)
                    {
                        physicalKiller += physicalList[i];
                        magicalKiller += magicalList[i];
                        physicalKillerCount++;
                        magicalKillerCount++;
                        break;
                    }
                    i++;
                }
            }

            if (physicalKillerCount > 1) physicalKiller /= physicalKillerCount;
            if (magicalKillerCount > 1) magicalKiller /= magicalKillerCount;
        }

        protected virtual bool checkDamageResists(AttackType attackType)
        {
            if (GeneralMitigation >= 1 || CoverMitigation >= 1 || DefendMitigation >= 1)
            {
                //Console.WriteLine(Name + " resisted damage!");
                return true;
            }

            switch (attackType)
            {
                case AttackType.PHYSICAL_ATTACK:
                    if (PhysicalMitigation >= 1)
                    {
                        //Console.WriteLine(Name + " resisted physical damage!");
                        return true;
                    }
                    break;
                case AttackType.MAGICAL_ATTACK:
                    if (MagicalMitigation >= 1)
                    {
                        //Console.WriteLine(Name + " resisted magical damage!");
                        return true;
                    }
                    break;
                case AttackType.HYBRID_ATTACK:
                    if (PhysicalMitigation >= 1 && MagicalMitigation >= 1)
                    {
                        //Console.WriteLine(Name + " resisted hybrid damage!");
                        return true;
                    }
                    break;
            }
            return false;
        }

        protected virtual double calculateElementalMod(List<ElementalType> elements, out string message)
        {
            double elementalMod = 0.0;
            List<double> elementalMods = new List<double>();
            foreach (ElementalType element in elements)
            {
                switch (element)
                {
                    case ElementalType.FIRE: elementalMods.Add(1.0 - FireResist + FireImperil); break;
                    case ElementalType.ICE: elementalMods.Add(1.0 - IceResist + IceImperil); break;
                    case ElementalType.THUNDER: elementalMods.Add(1.0 - ThunderResist + ThunderImperil); break;
                    case ElementalType.WATER: elementalMods.Add(1.0 - WaterResist + WaterImperil); break;
                    case ElementalType.WIND: elementalMods.Add(1.0 - WindResist + WindImperil); break;
                    case ElementalType.EARTH: elementalMods.Add(1.0 - EarthResist + EarthImperil); break;
                    case ElementalType.LIGHT: elementalMods.Add(1.0 - LightResist + LightImperil); break;
                    case ElementalType.DARK: elementalMods.Add(1.0 - DarkResist + DarkImperil); break;
                }
            }
            message = "";
            if (elementalMods.Count > 0)
            {
                foreach (double result in elementalMods)
                {
                    if (result < 1e-4) continue;
                    else elementalMod += result;
                }
                elementalMod /= elementalMods.Count;
                if (elementalMod <= 1e-4) message = " has resisted elemental damage!";
            }
            else
            {
                elementalMod = 1.0 - ElementlessResist;
                if(elementalMod <= 1e-4) message = " has resisted elementless damage!";
            }
            return elementalMod;
        }

        protected virtual double checkElementalAbsorb(List<ElementalType> elements)
        {
            foreach (ElementalType element in elements)
            {
                switch (element)
                {
                    case ElementalType.FIRE: if (FireAbsorb > 0) return -1.0; break;
                    case ElementalType.ICE: if (IceAbsorb > 0) return -1.0; break;
                    case ElementalType.THUNDER: if (ThunderAbsorb > 0) return -1.0; break;
                    case ElementalType.WATER: if (WaterAbsorb > 0) return -1.0; break;
                    case ElementalType.WIND: if (WindAbsorb > 0) return -1.0; break;
                    case ElementalType.EARTH: if (EarthAbsorb > 0) return -1.0; break;
                    case ElementalType.LIGHT: if (LightAbsorb > 0) return -1.0; break;
                    case ElementalType.DARK: if (DarkAbsorb > 0) return -1.0; break;
                }
            }
            if (elements.Count < 1 && ElementlessAbsorb > 0) return -1.0;
            return 1.0;
        }

        protected virtual double calculateDamageTaken(DamageType damageType, AttackType attackType, bool esperAttack, double physicalKiller, double magicalKiller, double weaponMod, double atk, double mag,
            double ignore, double levelCorrection, double finalMod, double skillMod, double elementalMod, double weight)
        {
            double finalDamage = 0.0;
            double magDamage = 0.0;
            double atkDamage = 0.0;

            double physicalMitigation = 1.0 - PhysicalMitigation;
            double magicalMitigation = 1.0 - MagicalMitigation;
            double hybridMitigation = 1.0 - ((PhysicalMitigation + MagicalMitigation) / 2);
            double generalMitigation = 1.0 - GeneralMitigation;
            double coverMitigation = 1.0 - CoverMitigation;
            double defendMitigation = 1.0 - DefendMitigation;

            bool noDamage = false;

            double disease = 1.0;
            if (Diseased) disease = 0.9;
            double currentWeaponMod = 1.0;
            double physicalKillerMod = 1.0;
            double magicalKillerMod = 1.0;

            if (!esperAttack)
            {
                physicalKillerMod += physicalKiller;
                magicalKillerMod += magicalKiller;
                currentWeaponMod = weaponMod;
            }

            switch (damageType)
            {
                case DamageType.PHYSICAL_DAMAGE: atkDamage = atk * atk * weaponMod * physicalKillerMod * levelCorrection / (Def * (1.0 - ignore) * disease); break;
                case DamageType.MAGICAL_DAMAGE: magDamage = mag * mag * magicalKillerMod * levelCorrection / (Spr * (1.0 - ignore) * disease); break;
                case DamageType.EVOKE_DAMAGE: magDamage = ((atk * atk) + (mag * mag)) * levelCorrection / (Spr * 2 * (1.0 - ignore) * disease); break;
                case DamageType.HYBRID_DAMAGE:
                    atkDamage = atk * atk * weaponMod * physicalKillerMod * levelCorrection / (Def * (1.0 - ignore) * disease);
                    magDamage = mag * mag * physicalKillerMod * levelCorrection / (Spr * (1.0 - ignore) * disease);
                    break;
                case DamageType.FIXED_DAMAGE:
                    atkDamage = 1;
                    finalMod = 1;
                    break;
                case DamageType.HP_DAMAGE:
                    atkDamage = MaxHP;
                    finalMod = 1;
                    break;
            }

            atkDamage *= elementalMod * skillMod;
            magDamage *= elementalMod * skillMod;

            switch (attackType)
            {
                case AttackType.PHYSICAL_ATTACK:
                    finalDamage = (atkDamage + magDamage) * physicalMitigation;
                    if (physicalMitigation < 1e-4) noDamage = true;
                    break;
                case AttackType.MAGICAL_ATTACK:
                    finalDamage = (atkDamage + magDamage) * magicalMitigation;
                    if (magicalMitigation < 1e-4) noDamage = true;
                    break;
                case AttackType.HYBRID_ATTACK:
                    finalDamage = ((atkDamage + magDamage) / 2) * hybridMitigation;
                    if (hybridMitigation < 1e-4) noDamage = true;
                    break;
                case AttackType.FIXED_ATTACK:
                    finalDamage = atkDamage + magDamage;
                    break;
            }

            if (damageType == DamageType.EVOKE_DAMAGE) finalDamage *= magicalMitigation;


            finalDamage *= generalMitigation * coverMitigation * defendMitigation * finalMod;// * chainBonus * weight;
            if (generalMitigation < 1e-4 || coverMitigation < 1e-4 || defendMitigation < 1e-4) noDamage = true;

            double absorber = getTotalDoubleStat("absorber", "");
            if (absorber > 1e-4)
            {
                int threshold = 0;
                switch (damageType)
                {
                    case DamageType.PHYSICAL_DAMAGE: threshold = (int)(getTotalDoubleStat("absorbPhysical", "") * 100); break;
                    case DamageType.MAGICAL_DAMAGE: threshold = (int)(getTotalDoubleStat("absorbMagical", "") * 100); break;
                    case DamageType.HYBRID_DAMAGE: threshold = (int)((getTotalDoubleStat("absorbPhysical", "") + getTotalDoubleStat("absorbMagical", "")) * 50); break;
                }
                if (threshold > AssetLoader.random.Next(threshold)) finalDamage *= (1.0 - absorber);
            }

            if (finalDamage <= 1 && !noDamage) finalDamage = 1;
            return finalDamage;
        }

        public virtual double takeDamage(AttackQueue queue, int id)
        {
            string output = "";
            chainFrame = 0;
            if (unitState == UnitState.UNIT_DEAD || unitState == UnitState.UNIT_REMOVED || unitState == UnitState.UNIT_AIRBORNE || unitState == UnitState.UNIT_HIDING) return 0.0;
            if (Petrified)
            {
                Console.WriteLine(Name + " cannot be damaged once petrified!");
                return 0.0;
            }

            if (queue.currentFrame < 1)
            {
                switch (queue.abilityType)
                {
                    case AbilityType.ESPER_ABILITY: esperHitters.Add(queue.id - 1); break;
                    case AbilityType.LIMIT_BURST: limitHitters.Add(queue.id - 1); break;
                    case AbilityType.ITEM_ABILITY: if (queue.elements.Count > 0) elementalItemHitters.Add(queue.id - 1); break;
                }

                switch (queue.attackType)
                {
                    case AttackType.HYBRID_ATTACK: physicalHitters.Add(queue.id - 1); break;
                    case AttackType.MAGICAL_ATTACK: magicalHitters.Add(queue.id - 1); break;
                    case AttackType.PHYSICAL_ATTACK:
                        if (queue.abilityName.Equals("Attack")) autoAttackHitters.Add(queue.id - 1);
                        physicalHitters.Add(queue.id - 1);
                        break;
                }
            }

            foreach(ElementalType element in queue.elements)
            {
                switch(element)
                {
                    case ElementalType.FIRE:    fireHitters.Add(queue.id - 1);      break;
                    case ElementalType.ICE:     iceHitters.Add(queue.id - 1);       break;
                    case ElementalType.THUNDER: thunderHitters.Add(queue.id - 1);   break;
                    case ElementalType.WATER:   waterHitters.Add(queue.id - 1);     break;
                    case ElementalType.WIND:    windHitters.Add(queue.id - 1);      break;
                    case ElementalType.EARTH:   earthHitters.Add(queue.id - 1);     break;
                    case ElementalType.LIGHT:   lightHitters.Add(queue.id - 1);     break;
                    case ElementalType.DARK:    darkHitters.Add(queue.id - 1);      break;
                }
            }

            if((queue.self.IsPlayer != isPlayer && (queue.calculatedDamage.Count < 1 || queue.calculatedDamage[id] <= 1e-4)) ||
                (queue.self.IsPlayer == isPlayer && (queue.calculatedDamageAllies.Count < 1 || queue.calculatedDamageAllies[id] <= 1e-4)))
            {
                Console.WriteLine(Name + " resisted damage.");
                return 0.0;
            }
            else if (queue.drainType != DrainType.MP_DRAIN )
            {
                if (whoIsChaining == queue.id)
                {
                    output += "Streak Ended...";
                    chainBonus = 1;
                    totalChainCount = 1;
                }
                else if (++totalChainCount > 1)
                {
                    output += chainCalculations(queue);
                    if (!queue.dualWieldPlus && chainBonus > 4.0) chainBonus = 4.0;
                }
                CombatMode.toSpark = true;
            }

            //if (queue.abilityName.Equals("Evocation - Light of Ruin")) Console.WriteLine("Bug!!!");
            //if (Name.Equals("Family Man")) Console.WriteLine("Bug!!!");
            //if (GameEngine.debug) Console.WriteLine("Frame Count - " + queue.currentFrame.ToString() + "; Weight - " + queue.weight[queue.currentFrame].ToString());
            double finalDamage = chainBonus * queue.weight[queue.currentFrame];
            if (queue.self.IsPlayer == isPlayer) finalDamage *= queue.calculatedDamageAllies[id];
            else finalDamage *= queue.calculatedDamage[id];

            string output2 = "";
            switch(queue.drainType)
            {
                case DrainType.HP_DRAIN:
                    double drainHP = setHp(finalDamage, true, out output2) * queue.drainScale;
                    double enemyHp = queue.self.getDoubleStat("hp");
                    enemyHp += drainHP;
                    if (enemyHp > queue.self.MaxHP) enemyHp = queue.self.MaxHP;
                    output += output2 + "\n" + queue.self.Name + " has been healed by " + Math.Floor(drainHP).ToString() + " HP!";
                    queue.self.setDoubleStat("hp", enemyHp);
                    break;
                case DrainType.MP_DRAIN:
                    double mp = getDoubleStat("mp");
                    double drainMP = finalDamage;
                    if (finalDamage > mp)
                    {
                        drainMP = mp;
                        mp = 0;
                    }
                    else mp -= drainMP;
                    output += Name + " has been drained by " + Math.Floor(drainMP).ToString() + " MP! ";
                    setDoubleStat("mp", mp);
                    drainMP *= queue.drainScale;
                    double enemyMp = queue.self.getDoubleStat("mp");
                    enemyMp += drainMP;
                    if (enemyMp > queue.self.MaxMP) enemyMp = queue.self.MaxMP;
                    output += queue.self.Name + " has been healed for " + Math.Floor(drainMP).ToString() + " MP!";
                    queue.self.setDoubleStat("mp", enemyMp);
                    break;
                default:
                    setHp(finalDamage, false, out output2);
                    output += output2;
                    break;
            }

            previousElements = queue.elements;
            whoIsChaining = queue.id;

            if ((Confused || Sleeping) && (queue.attackType == AttackType.PHYSICAL_ATTACK && finalDamage > 1e-4))
                removeADebuff(4, null, new List<StatusType>() { StatusType.CONFUSE, StatusType.SLEEP }, null);

            Console.WriteLine(output);
            return finalDamage;
        }

        protected virtual void takeDamageOverTime(DebuffQueue queue)
        {
            chainFrame = 0;
            if (unitState == UnitState.UNIT_DEAD)
            {
                Console.WriteLine(Name + " is already dead!");
                return;
            }
            if (Petrified)
            {
                Console.WriteLine(Name + " cannot be damaged once petrified!");
                return;
            }

            if (checkDamageResists(queue.attackType)) return;

            string elementMessage = "";
            double elementalMod = calculateElementalMod(queue.elements, out elementMessage);
            if (elementalMod <= 1e-4)
            {
                //Console.WriteLine(Name + elementMessage);
                return;
            }

            double physicalKiller = 0.0, magicalKiller = 0.0;
            double skillMod = queue.skillMod;

            if (queue.hex) skillMod *= countDebuffs() + 1;

            getKillerMods(out physicalKiller, out magicalKiller, queue.physicalKillers, queue.magicalKillers);

            double finalDamage = calculateDamageTaken(queue.damageType, queue.attackType, queue.abilityType == AbilityType.ESPER_ABILITY,  physicalKiller, magicalKiller, queue.weaponMod, queue.atkDamage,
                queue.magDamage, queue.ignore, queue.levelCorrection, queue.finalMod, skillMod, elementalMod, 1) * checkElementalAbsorb(queue.elements);

            string output = "";
            setHp(finalDamage, false, out output);

            if ((Confused || Sleeping) && (queue.attackType == AttackType.PHYSICAL_ATTACK && finalDamage > 1e-4))
                removeADebuff(4, null, new List<StatusType>() { StatusType.CONFUSE, StatusType.SLEEP }, null);
            Console.WriteLine(output);
        }

        protected virtual double setHp(double damage, bool drain, out string output)
        {
            output = "";
            double drainAmount = 0.0;
            double hp = getDoubleStat("hp");
            double bp = getDoubleStat("bp");
            if(damage < -1.0)
            {
                output = name + " has been healed for " + Math.Round(-damage).ToString() + " damage!";
                hp -= damage;
                if (hp >= getDoubleStat("maxHp")) hp = getDoubleStat("maxHp");
                return 0.0;
            }
            if(damage > hp + bp){
                int roll = AssetLoader.random.Next(100);
                foreach(Passive passive in unitData.Passives)
                {
                    double fatalThreshold = MaxHP * passive.getSimpleDoubleStat("fatalThreshold");
                    int fatalChance = passive.getSimpleIntStat("fatalChance");
                    int cheatDeath = getIntStat("cheatDeath");
                    if (fatalThreshold > 1e-4 && hp + bp >= fatalThreshold && roll < fatalChance && cheatDeath < passive.getSimpleIntStat("cheatDeath"))
                    {
                        setIntStat("cheatDeath", ++cheatDeath);
                        bp = 0;
                        hp = 1;
                        output = name + " lost " + Math.Round(damage).ToString() + " HP, but survived a fatal blow!";
                        if (drain) drainAmount = damage;
                        setDoubleStat("hp", hp);
                        setDoubleStat("bp", bp);
                        return drainAmount;
                    }
                }
            }
            if (bp > 1e-4)
            {
                bp -= damage;
                if (bp <= 1e-4)
                {
                    output = "Barrier absorbed " + Math.Round(damage).ToString() + " damage, but it broke! " + name + " lost " + Math.Round(-bp).ToString() + " HP!";
                    hp += bp;
                    if (drain) drainAmount = -bp;
                    bp = 0;
                    setIntStat("barrierDuration", 0);
                }
                else output = "Barrier absorbed " + Math.Round(damage).ToString() + " damage for " + name + "!";
            }
            else
            {
                if (drain)
                {
                    output = name + " has been drained by " + Math.Round(damage).ToString() + " HP!";
                    drainAmount = damage;
                }
                else output = name + " took " + Math.Round(damage).ToString() + " damage!";
                hp -= damage;
            }
            if (!DataName.Equals("Training Dummy"))
            {
                if (hp <= 1e-4) hp = unitIsDead();
                setDoubleStat("hp", hp);
                setDoubleStat("bp", bp);
            }
            return drainAmount;
        }

        public virtual void setUnitState(UnitState state) { unitState = state; }

        protected virtual double unitIsDead()
        {
            string output = "";
            double result = 0.0;
            if (unitState == UnitState.UNIT_DEAD || unitState == UnitState.UNIT_REMOVED || unitState == UnitState.UNIT_DYING)
                return result;
            //if (DataName.Equals("Terror Knight Beast Parade")) unitState = UnitState.UNIT_DYING;
            //else unitState = UnitState.UNIT_DEAD;
            else unitState = UnitState.UNIT_DYING;
            if (getIntStat("undying") > 0) return result;
            if (getBoolStat("reraise"))
            {
                result = MaxHP * getDoubleStat("reraiseHp");
                raiseUnit(result, out output);
                setBoolStat("reraise", false);
                setDoubleStat("reraiseHp", 0.0);
            }
            else
            {
                output = Name + " died.";
                //This is for the sacrifice mechanic
                if(isPlayer) Party.deathCount++;
            }
            while (buffs.Count > 0){
                removeBuff(buffs[0]);
                buffs.RemoveAt(0);
            }
            while (debuffs.Count > 0){
                removeDebuff(debuffs[0]);
                debuffs.RemoveAt(0);
            }
            foreach (string stack in listOfStacks) setDoubleStat(stack, 0.0);
            listOfStacks.Clear();

            setIntStat("cheatDeath", 0);
            setBoolStat("poisoned", false);
            setBoolStat("silenced", false);
            setBoolStat("blinded", false);
            setBoolStat("sleeping", false);
            setBoolStat("confused", false);
            setBoolStat("paralysed", false);
            setBoolStat("petrified", false);
            setBoolStat("diseased", false);
            setBoolStat("charmed", false);
            setBoolStat("stopped", false);
            setBoolStat("berserked", false);
            setDoubleStat("baseAtkPenalty", 0.0);
            setDoubleStat("baseMagPenalty", 0.0);
            setDoubleStat("baseDefPenalty", 0.0);
            setDoubleStat("baseSprPenalty", 0.0);
            setDoubleStat("baseAtkBonus", 0.0);
            setDoubleStat("baseMagBonus", 0.0);
            setDoubleStat("baseDefBonus", 0.0);
            setDoubleStat("baseSprBonus", 0.0);
            /*setDoubleStat("fireResist", 0.0);
            setDoubleStat("iceResist", 0.0);
            setDoubleStat("thunderResist", 0.0);
            setDoubleStat("waterResist", 0.0);
            setDoubleStat("windResist", 0.0);
            setDoubleStat("earthResist", 0.0);
            setDoubleStat("lightResist", 0.0);
            setDoubleStat("darkResist", 0.0);
            setDoubleStat("fireImperil", 0.0);
            setDoubleStat("iceImperil", 0.0);
            setDoubleStat("thunderImperil", 0.0);
            setDoubleStat("waterImperil", 0.0);
            setDoubleStat("windImperil", 0.0);
            setDoubleStat("earthImperil", 0.0);
            setDoubleStat("lightImperil", 0.0);
            setDoubleStat("darkImperil", 0.0);*/

            output += " All buffs and debuffs removed!";
            Console.WriteLine(output);
            return result;
        }

        public bool checkWeapon(EquipmentType target, List<Weapon> weaponSet)
        {
            foreach (Weapon weapon in weaponSet)
                if (weapon.EquipData.EquipType == target)
                    return true;
            return false;
        }

        public double getWeaponMod(int i)
        {
            if (weaponSet.Count > 0)
            {
                if (weaponSet.Count > 1)
                {
                    if (weaponSet[0].EquipData.EquipType == EquipmentType.LSHIELD || weaponSet[0].EquipData.EquipType == EquipmentType.HSHIELD)
                        return (double)weaponSet[1].EquipData.getIntStat("minWeaponMod") * 0.01 + (double)AssetLoader.random.Next(weaponSet[1].EquipData.getIntStat("rangeWeaponMod")) * 0.01;
                    if (weaponSet[1].EquipData.EquipType == EquipmentType.LSHIELD || weaponSet[1].EquipData.EquipType == EquipmentType.HSHIELD)
                        return (double)weaponSet[0].EquipData.getIntStat("minWeaponMod") * 0.01 + (double)AssetLoader.random.Next(weaponSet[0].EquipData.getIntStat("rangeWeaponMod")) * 0.01;
                    return (double)weaponSet[i].EquipData.getIntStat("minWeaponMod") * 0.01 + (double)AssetLoader.random.Next(weaponSet[i].EquipData.getIntStat("rangeWeaponMod")) * 0.01;
                }
                if (weaponSet[0].EquipData.EquipType == EquipmentType.LSHIELD || weaponSet[0].EquipData.EquipType == EquipmentType.HSHIELD) return 1.0;
                return (double)weaponSet[0].EquipData.getIntStat("minWeaponMod") * 0.01 + (double)AssetLoader.random.Next(weaponSet[0].EquipData.getIntStat("rangeWeaponMod")) * 0.01;
            }
            return 1.0;
        }

        public void raiseUnit(double hp, out string error)
        {
            error = "";
            setDoubleStat("hp", hp);
            unitState = UnitState.UNIT_ALIVE;
            setBoolStat("actionTaken", true);
            if (isPlayer) error = Name + " has been raised from the dead with " + Math.Floor(hp).ToString() + " HP!";
            //Reapplies start of battle effects
            List<string> list = new List<string>();
            list.AddRange(findExternalAbility("startOfBattle"));
            int i = 0;
            string error2 = "";
            foreach (string abilityName in list)
                prepareAbility(i++, false, false, 0, out error2, AssetLoader.abilitiesData[abilityName], CombatMode.enemies, Party.currentParty);
        }

        public void restoreResource(ProtectQueue queue)
        {
            string error = "";
            double bp = getDoubleStat("bp");
            double hp = getDoubleStat("hp");
            double maxHp = getDoubleStat("maxHp");
            double mp = getDoubleStat("mp");
            double maxMp = getDoubleStat("maxMp");
            double lb = getDoubleStat("lb");
            double extraLimit = 0.0;
            switch (queue.defensiveType)
            {
                case DefensiveType.RAISE:
                    if (unitState == UnitState.UNIT_DEAD)
                    {
                        if (queue.limitBurst > 0) extraLimit = queue.limitScale * queue.limitBurst;
                        hp += maxHp * (queue.healEffect + extraLimit);
                        if (hp > maxHp) hp = maxHp;
                        raiseUnit(hp, out error);
                    }
                    break;
                case DefensiveType.BARRIER:
                    bp = queue.healEffect;
                    Console.WriteLine(Name + " now has " + Math.Floor(bp).ToString() + " BP!");
                    setDoubleStat("bp", bp);
                    setIntStat("barrierDuration", 4);
                    break;
                case DefensiveType.HP_RESTORE:
                    hp += queue.healEffect;
                    if (hp > maxHp) hp = maxHp;
                    Console.WriteLine(Name + " has been healed for " + Math.Floor(queue.healEffect).ToString() + " HP!");
                    setDoubleStat("hp", hp);
                    break;
                case DefensiveType.MP_RESTORE:
                    mp += queue.healEffect;
                    if (mp > maxMp) mp = maxMp;
                    Console.WriteLine(Name + " has been healed for " + Math.Floor(queue.healEffect).ToString() + " MP!");
                    setDoubleStat("mp", mp);
                    break;
                case DefensiveType.LB_RESTORE:
                    lb += queue.healEffect;
                    if (lb > unitData.LbCost) lb = unitData.LbCost;
                    Console.WriteLine(Name + " has received " + Math.Floor(queue.healEffect).ToString() + " LB points!");
                    setDoubleStat("lb", lb);
                    break;
                case DefensiveType.EP_RESTORE:
                    Party.ep += (int)queue.healEffect;
                    if (Party.ep > Party.MAX_ESPER_POINTS) Party.ep = Party.MAX_ESPER_POINTS;
                    Console.WriteLine(Name + " has generated " + Math.Floor(queue.healEffect).ToString() + " Esper Orbs!");
                    break;
                case DefensiveType.HP_RESTORE_PERCENT:
                    if (queue.limitBurst > 0) extraLimit = queue.limitScale * queue.limitBurst;
                    hp += maxHp * (queue.healEffect + extraLimit);
                    if (hp > maxHp) hp = maxHp;
                    Console.WriteLine(Name + " has been healed for " + Math.Floor(maxHp * queue.healEffect).ToString() + " HP!");
                    setDoubleStat("hp", hp);
                    break;
                case DefensiveType.MP_RESTORE_PERCENT:
                    if (queue.limitBurst > 0) extraLimit = queue.limitScale * queue.limitBurst;
                    mp += maxMp * (queue.healEffect + extraLimit);
                    if (mp > maxMp) mp = maxMp;
                    Console.WriteLine(Name + " has been healed for " + Math.Floor(maxMp * queue.healEffect).ToString() + " MP!");
                    setDoubleStat("mp", mp);
                    break;
                case DefensiveType.LB_RESTORE_PERCENT:
                    lb += unitData.LbCost * queue.healEffect;
                    if (lb > unitData.LbCost) lb = unitData.LbCost;
                    Console.WriteLine(Name + " has received " + Math.Floor(unitData.LbCost * queue.healEffect).ToString() + " LB points!");
                    setDoubleStat("lb", lb);
                    break;
                case DefensiveType.ENTRUST:
                    double lbPercent = queue.self.LB / queue.self.unitData.LbCost;
                    lb += unitData.LbCost * lbPercent;
                    if (lb > unitData.LbCost) lb = unitData.LbCost;
                    Console.WriteLine(Name + " has received " + Math.Floor(unitData.LbCost * queue.healEffect).ToString() + " LB points from " + queue.self.Name + "!");
                    setDoubleStat("lb", lb);
                    queue.self.setDoubleStat("lb", 0);
                    break;
                case DefensiveType.REMOVE_DEBUFF:
                    removeADebuff(0, queue.paramTypes, queue.statusTypes, queue.elementalTypes);
                    Console.WriteLine(Name + " is now free from all debuffs!");
                    break;
                case DefensiveType.REMOVE_PARAMETER: removeADebuff(3, queue.paramTypes, queue.statusTypes, queue.elementalTypes); break;
                case DefensiveType.REMOVE_ALL_PARAMETERS:
                    removeADebuff(1, queue.paramTypes, queue.statusTypes, queue.elementalTypes);
                    Console.WriteLine(Name + " is no longer parameter broken!");
                    break;
                case DefensiveType.REMOVE_STATUS: removeADebuff(4, queue.paramTypes, queue.statusTypes, queue.elementalTypes); break;
                case DefensiveType.REMOVE_ALL_STATUS:
                    removeADebuff(2, queue.paramTypes, queue.statusTypes, queue.elementalTypes);
                    Console.WriteLine(Name + " is now free from all status ailments!");
                    break;
                case DefensiveType.REMOVE_IMPERIL: removeADebuff(6, queue.paramTypes, queue.statusTypes, queue.elementalTypes); break;
                case DefensiveType.REMOVE_ALL_IMPERILS:
                    removeADebuff(5, queue.paramTypes, queue.statusTypes, queue.elementalTypes);
                    Console.WriteLine(Name + " is no longer imperiled!");
                    break;
            }
        }

        private bool findAParam(DebuffComponent debuff, List<ParameterType> list, List<ParameterType> targets)
        {
            bool flag = false;
            foreach (ParameterType param1 in list)
                foreach (ParameterType param2 in targets)
                    if(param1 == param2)
                    {
                        flag = debuff.removeParameter(param2);
                        break;
                    }
            return flag;
        }

        private bool findAStatus(DebuffComponent debuff, List<StatusType> list, List<StatusType> targets)
        {
            bool flag = false;
            foreach (StatusType stat1 in list)
                foreach (StatusType stat2 in targets)
                    if (stat1 == stat2)
                    {
                        flag = debuff.removeStatus(stat2);
                        break;
                    }
            return flag;
        }

        private bool findAnImperil(DebuffComponent debuff, List<ElementalType> list, List<ElementalType> targets)
        {
            bool flag = false;
            foreach (ElementalType elem1 in list)
                foreach (ElementalType elem2 in targets)
                    if (elem1 == elem2)
                    {
                        flag = debuff.removeImperil(elem2);
                        break;
                    }
            return flag;
        }

        private void removeADebuff(int removeType, List<ParameterType> paramTypes, List<StatusType> statusTypes, List<ElementalType> elementalTypes)
        {
            int debuffCount = 0;
            int i = 0;
            while (i < debuffs.Count)
            {
                bool flag = debuffs[i].Dispelable;
                switch(removeType){
                    case 1: flag = debuffs[i].Dispelable && debuffs[i].DebuffType == DebuffType.PARAMETER_DEBUFF; break;
                    case 2: flag = debuffs[i].Dispelable && debuffs[i].DebuffType == DebuffType.STATUS_DEBUFF; break;
                    case 3: flag = debuffs[i].Dispelable && findAParam(debuffs[i], new List<ParameterType>(debuffs[i].ParameterTypes), paramTypes); break;
                    case 4: flag = debuffs[i].Dispelable && findAStatus(debuffs[i], new List<StatusType>(debuffs[i].StatusTypes), statusTypes); break;
                    case 5: flag = debuffs[i].Dispelable && debuffs[i].DebuffType == DebuffType.ELEMENTAL_DEBUFF; break;
                    case 6: flag = debuffs[i].Dispelable && findAnImperil(debuffs[i], new List<ElementalType>(debuffs[i].ElementalTypes), elementalTypes); break;
                }
                if (flag)
                {
                    //removeDebuff(debuffs[i]);
                    debuffs.RemoveAt(i);
                    debuffCount++;
                }
                else i++;
            }
            foreach (DebuffComponent newDebuff in debuffs) checkDebuff(newDebuff, true);
            if (debuffCount > 0) Console.WriteLine("Removed " + debuffCount.ToString() + " debuff component/s from " + Name + "!");
            //Hard removes statuses after checking for existing debuffs
            switch(removeType)
            {
                case 1:
                    setDoubleStat("baseAtkPenalty", 0.0);
                    setDoubleStat("baseMagPenalty", 0.0);
                    setDoubleStat("baseDefPenalty", 0.0);
                    setDoubleStat("baseSprPenalty", 0.0);
                    break;
                case 2:
                    setBoolStat("poisoned", false);
                    setBoolStat("silenced", false);
                    setBoolStat("blinded", false);
                    setBoolStat("sleeping", false);
                    setBoolStat("confused", false);
                    setBoolStat("paralysed", false);
                    setBoolStat("petrified", false);
                    setBoolStat("diseased", false);
                    setBoolStat("charmed", false);
                    setBoolStat("stopped", false);
                    setBoolStat("berserked", false);
                    break;
                case 3:
                    foreach (ParameterType param in paramTypes)
                        switch (param)
                        {
                            case ParameterType.ATK_PARAMETER:
                                if (getDoubleStat("baseAtkPenalty") > 1e-4)
                                {
                                    setDoubleStat("baseAtkPenalty", 0.0);
                                    Console.WriteLine(Name + " is no longer ATK broken!");
                                }
                                break;
                            case ParameterType.MAG_PARAMETER:
                                if (getDoubleStat("baseMagPenalty") > 1e-4)
                                {
                                    setDoubleStat("baseMagPenalty", 0.0);
                                    Console.WriteLine(Name + " is no longer DEF broken!");
                                }
                                break;
                            case ParameterType.DEF_PARAMETER:
                                if (getDoubleStat("baseDefPenalty") > 1e-4)
                                {
                                    setDoubleStat("baseDefPenalty", 0.0);
                                    Console.WriteLine(Name + " is no longer MAG broken!");
                                }
                                break;
                            case ParameterType.SPR_PARAMETER:
                                if (getDoubleStat("baseSorPenalty") > 1e-4)
                                {
                                    setDoubleStat("baseSprPenalty", 0.0);
                                    Console.WriteLine(Name + " is no longer SPR broken!");
                                }
                                break;
                        }
                    break;
                case 4:
                    foreach (StatusType status in statusTypes)
                        switch (status)
                        {
                            case StatusType.POISON:
                                if (getBoolStat("poisoned"))
                                {
                                    setBoolStat("poisoned", false);
                                    Console.WriteLine(Name + " is now free from poison effects!");
                                }
                                break;
                            case StatusType.SILENCE:
                                if (getBoolStat("silenced"))
                                {
                                    setBoolStat("silenced", false);
                                    Console.WriteLine(Name + " is now free from silence effects!");
                                }
                                break;
                            case StatusType.BLIND:
                                if (getBoolStat("blinded"))
                                {
                                    setBoolStat("blinded", false);
                                    Console.WriteLine(Name + " is now free from blind effects!");
                                }
                                break;
                            case StatusType.SLEEP:
                                if (getBoolStat("sleeping"))
                                {
                                    setBoolStat("sleeping", false);
                                    Console.WriteLine(Name + " is now free from sleep effects!");
                                }
                                break;
                            case StatusType.CONFUSE:
                                if (getBoolStat("confused"))
                                {
                                    setBoolStat("confused", false);
                                    Console.WriteLine(Name + " is now free from confuse effects!");
                                }
                                break;
                            case StatusType.PARALYSE:
                                if (getBoolStat("paralysed"))
                                {
                                    setBoolStat("paralysed", false);
                                    Console.WriteLine(Name + " is now free from paralyse effects!");
                                }
                                break;
                            case StatusType.PETRIFY:
                                if (getBoolStat("petrified"))
                                {
                                    setBoolStat("petrified", false);
                                    Console.WriteLine(Name + " is now free from petrify effects!");
                                }
                                break;
                            case StatusType.DISEASE:
                                if (getBoolStat("diseased"))
                                {
                                    setBoolStat("diseased", false);
                                    Console.WriteLine(Name + " is now free from disease effects!");
                                }
                                break;
                            case StatusType.CHARM:
                                if (getBoolStat("charmed"))
                                {
                                    setBoolStat("charmed", false);
                                    Console.WriteLine(Name + " is now free from charm effects!");
                                }
                                break;
                            case StatusType.STOP:
                                if (getBoolStat("stopped"))
                                {
                                    setBoolStat("stopped", false);
                                    Console.WriteLine(Name + " is now free from stop effects!");
                                }
                                break;
                            case StatusType.BERSERK:
                                if (getBoolStat("berserked"))
                                {
                                    setBoolStat("berserked", false);
                                    Console.WriteLine(Name + " is now free from berserk effects!");
                                }
                                break;
                        }
                    break;
                case 5:
                    setDoubleStat("fireImperil", 0.0);
                    setDoubleStat("iceImperil", 0.0);
                    setDoubleStat("thunderImperil", 0.0);
                    setDoubleStat("waterImperil", 0.0);
                    setDoubleStat("windImperil", 0.0);
                    setDoubleStat("earthImperil", 0.0);
                    setDoubleStat("lightImperil", 0.0);
                    setDoubleStat("darkImperil", 0.0);
                    break;
                case 6:
                    foreach (ElementalType element in elementalTypes)
                    {
                        switch (element)
                        {
                            case ElementalType.FIRE:
                                if (getDoubleStat("fireImperil") > 1e-4)
                                {
                                    setDoubleStat("fireImperil", 0.0);
                                    Console.WriteLine(Name + " is now free from fire imperil!");
                                }
                                break;
                            case ElementalType.ICE:
                                if (getDoubleStat("iceImperil") > 1e-4)
                                {
                                    setDoubleStat("iceImperil", 0.0);
                                    Console.WriteLine(Name + " is now free from ice imperil!");
                                }
                                break;
                            case ElementalType.THUNDER:
                                if (getDoubleStat("thunderImperil") > 1e-4)
                                {
                                    setDoubleStat("thunderImperil", 0.0);
                                    Console.WriteLine(Name + " is now free from thunder imperil!");
                                }
                                break;
                            case ElementalType.WATER:
                                if (getDoubleStat("waterImperil") > 1e-4)
                                {
                                    setDoubleStat("waterImperil", 0.0);
                                    Console.WriteLine(Name + " is now free from water imperil!");
                                }
                                break;
                            case ElementalType.WIND:
                                if (getDoubleStat("windImperil") > 1e-4)
                                {
                                    setDoubleStat("windImperil", 0.0);
                                    Console.WriteLine(Name + " is now free from wind imperil!");
                                }
                                break;
                            case ElementalType.EARTH:
                                if (getDoubleStat("earthImperil") > 1e-4)
                                {
                                    setDoubleStat("earthImperil", 0.0);
                                    Console.WriteLine(Name + " is now free from earth imperil!");
                                }
                                break;
                            case ElementalType.LIGHT:
                                if (getDoubleStat("lightImperil") > 1e-4)
                                {
                                    setDoubleStat("lightImperil", 0.0);
                                    Console.WriteLine(Name + " is now free from light imperil!");
                                }
                                break;
                            case ElementalType.DARK:
                                if (getDoubleStat("darkImperil") > 1e-4)
                                {
                                    setDoubleStat("darkImperil", 0.0);
                                    Console.WriteLine(Name + " is now free from dark imperil!");
                                }
                                break;
                        }
                    }
                    break;
            }
        }

        public void gainLB(double crystal)
        {
            double lb = getDoubleStat("lb");
            lb += crystal * (1.0 + LBFillRate + getTotalDoubleStat("lbFillRate", ""));
            if (lb >= unitData.LbCost) lb = unitData.LbCost;
            setDoubleStat("lb", lb);
        }

        private bool checkChainingNames(string name)
        {
            switch(name)
            {
                case "Onion Slice": return true;
                case "Onion Cutter": return true;
                case "High-Voltage Twin Blade": return true;
                case "Dazzling Twin Blade": return true;
                case "Tempestous Twin Blade": return true;
                case "Ruthless Blade": return true;
                case "Flash - Dazzle": return true;
                case "Destruction": return true;
                case "Ultimate Destruction": return true;
                case "Aeolian Onslaught": return true;
                default: return false;
            }
        }

        private void loadOffensiveComponent(bool counter, int i, int target, string name, OffensiveComponent offensive, AbilityType abilityType, List<Unit> victims, List<Unit> allies)
        {
            double atkDamage = 0.0, magDamage = 0.0, fixedDamage = 0.0;

            List<double> physicalKillers = new List<double>();
            List<double> magicalKillers = new List<double>();

            atkDamage = 0.0;
            magDamage = 0.0;
            fixedDamage = 0.0;

            switch (offensive.AttackType)
            {
                case AttackType.PHYSICAL_ATTACK: CombatMode.Physical_Hits++; break;
                case AttackType.MAGICAL_ATTACK: CombatMode.Magical_Hits++; break;
                case AttackType.HYBRID_ATTACK: CombatMode.Physical_Hits++; break;
            }

            switch (offensive.DamageType)
            {
                case DamageType.HP_DAMAGE: fixedDamage = calculateFixedDamage(offensive.FrameData, offensive.Effect); break;
                case DamageType.FIXED_DAMAGE: fixedDamage = calculateFixedDamage(offensive.FrameData, offensive.Effect); break;
                case DamageType.EVOKE_DAMAGE: calculateEvokeDamage(offensive.ScalingTypes, out atkDamage, out magDamage); break;
                case DamageType.PHYSICAL_DAMAGE:
                    if (abilityType == AbilityType.ESPER_ABILITY) atkDamage = calculateEsperDamage(true);
                    else atkDamage = calculateAtkDamage(offensive.ScalingTypes, (i % 2));
                    break;
                case DamageType.MAGICAL_DAMAGE:
                    if (abilityType == AbilityType.ESPER_ABILITY) magDamage = calculateEsperDamage(false);
                    else magDamage = calculateMagDamage(offensive.ScalingTypes);
                    break;
                case DamageType.HYBRID_DAMAGE:
                    if (abilityType == AbilityType.ESPER_ABILITY)
                    {
                        atkDamage = calculateEsperDamage(true);
                        magDamage = calculateEsperDamage(false);
                    }
                    else
                    {
                        atkDamage = calculateAtkDamage(offensive.ScalingTypes, (i % 2));
                        magDamage = calculateMagDamage(offensive.ScalingTypes);
                    }
                    break;
            }

            foreach (string killer in AssetLoader.killerStrings)
            {
                physicalKillers.Add(getTotalDoubleStat("killerPhysical", killer) + getDoubleStat("killerPhysical" + killer));
                magicalKillers.Add(getTotalDoubleStat("killerMagical", killer) + getDoubleStat("killerMagical" + killer));
            }

            AttackQueue queue = new AttackQueue();
            //queue.globalFrame = CombatMode.frameRate;
            queue.dualWieldPlus = unitData.getIntStatAdvanced("dualWieldPlus", "", this) > 0;
            queue.accuracy = getTotalIntStat("accuracy", "tdhBoost");
            queue.abilityType = abilityType;
            queue.self = this;
            queue.abilityName = name;
            queue.isPlayer = isPlayer;
            queue.lbCrystal = 1;
            if (name == "Attack" || name == "Warden" || name == "Holy Sword: Retribution" || name == "Cloud Attack")
                queue.lbCrystal = unitData.LBPerAttack;
            queue.checkpoint = 0;
            queue.currentFrame = 0;
            queue.id = chainId;
            if (offensive.Duplicate) queue.id += Party.currentParty.Count;
            queue.target = target;
            queue.limitBurst = lbLevel - 1;
            queue.weight = new List<double>(offensive.Weight);
            queue.frameData = new List<int>(offensive.FrameData);
            queue.elements = new List<ElementalType>(offensive.ElementalTypes);
            queue.targetType = offensive.TargetType;
            queue.attackType = offensive.AttackType;
            queue.drainType = offensive.DrainType;
            queue.drainScale = offensive.Scale;
            int castDelay = 16;
            if (checkChainingNames(name)) castDelay = 8;
            if (counter) queue.delay = castDelay * (i + 1) + offensive.CastTime * i + getIntStat("timedDelay");
            else
            {
                queue.currentCastDelay = -castDelay * (i + 1);
                castFrame = offensive.CastTime;
                //Console.WriteLine("castDelay - " + queue.currentCastDelay.ToString() + "; castFrame - " + castFrame.ToString());
            }

            //if (GameEngine.debug) Console.WriteLine("i - " + i.ToString() + "; delay - " + queue.delay.ToString() + "; castDelay - " + castDelay.ToString() + "; castTime - " + offensive.CastTime.ToString());
            //if (CombatMode.gameMode != GameMode.PRACTICE) queue.delay += getIntStat("hitResult");
            double skillMod = offensive.Effect + getDoubleStat(name+"Buff") + getTotalDoubleStat("damageModifier", name) + getDamageModifierBuff(name, offensive.LimitScale, offensive.Scale);
            if (abilityType == AbilityType.LIMIT_BURST) skillMod = (skillMod + offensive.LimitScale * (lbLevel - 1)) * (1 + getTotalDoubleStat("lbDamage", "") + getDoubleStat("lbDamage"));
            if (offensive.Jump) skillMod *= (1.0 + getTotalDoubleStat("jumpDamage", name));
            if(offensive.Sacrifice > 0)
            {
                int sacrificeCount = Party.deathCount;
                if(sacrificeCount > offensive.Sacrifice) sacrificeCount = offensive.Sacrifice;
                skillMod = skillMod * sacrificeCount;
                if(skillMod <= 1) skillMod = 1;
            }
            double weaponMod = getWeaponMod((i % 2));
            double levelCorrection = 1 + (double)unitLevel / UnitData.MAX_UNIT_LEVEL_6_STARS;
            double finalMod = (0.85 + (double)AssetLoader.random.Next(15) * 0.01);
            if (abilityType == AbilityType.ESPER_ABILITY)
            {
                skillMod = offensive.Effect * offensive.Scale;
                finalMod *= 1.6;
            }
            if (offensive.AttackType == AttackType.PHYSICAL_ATTACK || offensive.AttackType == AttackType.HYBRID_ATTACK)
            {
                foreach (Weapon weapon in weaponSet)
                    foreach (ElementalType type in weapon.EquipData.ElementalTypes)
                    {
                        bool flag = true;
                        foreach (ElementalType comparison in offensive.ElementalTypes)
                            if (comparison == type)
                            {
                                flag = false;
                                break;
                            }
                        if (flag) queue.elements.Add(type);
                    }
                foreach (ElementalType type in imbueList)
                {
                    bool flag = true;
                    foreach (ElementalType comparison in offensive.ElementalTypes)
                        if (comparison == type)
                        {
                            flag = false;
                            break;
                        }
                    if (flag) queue.elements.Add(type);
                }
            }

            if (offensive.TargetType == TargetType.RANDOM_ENEMY || offensive.TargetType == TargetType.ALL_ENEMIES || offensive.TargetType == TargetType.SINGLE_ENEMY)
            {
                foreach (Unit victim in victims)
                    queue.calculatedDamage.Add(victim.calculatingDamageTaken(weaponMod, atkDamage, magDamage, offensive.Ignore, levelCorrection, finalMod, skillMod, abilityType, offensive.AttackType,
                        offensive.DamageType, queue.elements, physicalKillers, magicalKillers));
            }
            else
            {
                foreach (Unit victim in allies)
                    queue.calculatedDamageAllies.Add(victim.calculatingDamageTaken(weaponMod, atkDamage, magDamage, offensive.Ignore, levelCorrection, finalMod, skillMod, abilityType,
                        offensive.AttackType, offensive.DamageType, queue.elements, physicalKillers, magicalKillers));
            }
            
            CombatMode.activeQueue.Add(queue);
        }

        public double calculatingDamageTaken(double weaponMod, double atkDamage, double magDamage, double ignore, double levelCorrection, double finalMod, double skillMod, AbilityType abilityType,
            AttackType attackType, DamageType damageType, List<ElementalType> elements, List<double> physicalKillers, List<double> magicalKillers)
        {
            if (unitState == UnitState.UNIT_DEAD || unitState == UnitState.UNIT_REMOVED || unitState == UnitState.UNIT_AIRBORNE || unitState == UnitState.UNIT_HIDING) return 0.0;
            if (Petrified)
            {
                Console.WriteLine(Name + " cannot be damaged once petrified!");
                return 0.0;
            }

            if (checkDamageResists(attackType)) return 0.0;

            string elementMessage = "";
            double elementalMod = calculateElementalMod(elements, out elementMessage);
            if (elementalMod <= 1e-4)
            {
                Console.WriteLine(Name + elementMessage);
                return 0.0;
            }

            double physicalKiller = 0.0, magicalKiller = 0.0;

            getKillerMods(out physicalKiller, out magicalKiller, physicalKillers, magicalKillers);

            //if (GameEngine.debug) Console.WriteLine("Frame Count - " + queue.currentFrame.ToString() + "; Weight - " + queue.weight[queue.currentFrame].ToString());
            return calculateDamageTaken(damageType, attackType, abilityType == AbilityType.ESPER_ABILITY, physicalKiller, magicalKiller, weaponMod, atkDamage, magDamage, ignore, levelCorrection, finalMod,
                skillMod, elementalMod, 1) * checkElementalAbsorb(elements);
        }

        private void loadDefensiveComponent(bool counter, int i, int target, string name, DefensiveComponent defensive)
        {
            double baseEffect = defensive.Effect;
            if (defensive.Sacrifice > 0)
            {
                int sacrificeCount = Party.deathCount;
                if (sacrificeCount > defensive.Sacrifice) sacrificeCount = defensive.Sacrifice;
                baseEffect *= sacrificeCount;
            }
            double effect = baseEffect;
            switch (defensive.DefensiveType)
            {
                case DefensiveType.BARRIER: effect = calculateHealing(defensive.ScalingTypes, baseEffect, defensive.Scale, defensive.FrameData.Count, 1); break;
                case DefensiveType.HP_RESTORE: effect = calculateHealing(defensive.ScalingTypes, baseEffect, defensive.Scale, defensive.FrameData.Count, 1); break;
                case DefensiveType.MP_RESTORE: effect = calculateHealing(defensive.ScalingTypes, baseEffect, defensive.Scale, defensive.FrameData.Count, 1); break;
                case DefensiveType.LB_RESTORE: effect = (int)baseEffect + AssetLoader.random.Next((int)defensive.Scale); break;
                case DefensiveType.EP_RESTORE: effect = (int)baseEffect + AssetLoader.random.Next((int)defensive.Scale); break;
            }
            ProtectQueue queue = new ProtectQueue();
            //queue.globalFrame = CombatMode.frameRate;
            queue.abilityName = name;
            queue.isPlayer = isPlayer;
            queue.checkpoint = 0;
            queue.currentFrame = 0;
            queue.id = chainId;
            queue.target = target;
            queue.limitBurst = lbLevel - 1;
            queue.limitScale = defensive.LimitScale;
            queue.frameData = new List<int>(defensive.FrameData);
            int castDelay = 16;
            if (checkChainingNames(name)) castDelay = 8;
            if (counter) queue.delay = castDelay * (i + 1) + defensive.CastTime * i + getIntStat("timedDelay");
            else
            {
                queue.currentCastDelay = -castDelay * (i + 1);
                castFrame = defensive.CastTime;
            }
            
            queue.targetType = defensive.TargetType;
            queue.defensiveType = defensive.DefensiveType;
            queue.paramTypes = new List<ParameterType>(defensive.ParameterTypes);
            queue.statusTypes = new List<StatusType>(defensive.StatusTypes);
            queue.healEffect = effect;
            queue.self = this;
            CombatMode.activeQueue.Add(queue);
        }

        public void loadBuffComponent(bool counter, int i, int target, string name, BuffComponent buff)
        {
            BuffQueue queue = new BuffQueue();
            //queue.globalFrame = CombatMode.frameRate;
            queue.regenEffect = 0.0;
            if (buff.ScalingTypes.Count > 0) queue.regenEffect = calculateHealing(buff.ScalingTypes, buff.Effect, buff.Scale, 1, buff.Duration);
            queue.abilityName = name;
            queue.isPlayer = isPlayer;
            queue.checkpoint = 0;
            queue.currentFrame = 0;
            queue.id = chainId;
            queue.target = target;
            queue.limitBurst = lbLevel - 1;
            buff.limitBurst = lbLevel - 1;
            queue.frameData = new List<int>(buff.FrameData);
            int castDelay = 16;
            if (checkChainingNames(name)) castDelay = 8;
            if (counter) queue.delay = castDelay * (i + 1) + buff.CastTime * i + getIntStat("timedDelay");
            else
            {
                queue.currentCastDelay = -castDelay * (i + 1);
                castFrame = buff.CastTime;
            }
            queue.buff = buff;
            queue.self = this;
            CombatMode.activeQueue.Add(queue);
        }

        private void loadDebuffComponent(bool counter, int i, int target, string name, DebuffComponent debuff, AbilityType abilityType)
        {
            double atkDamage = 0.0, magDamage = 0.0, fixedDamage = 0.0;

            List<double> physicalKillers = new List<double>();
            List<double> magicalKillers = new List<double>();

            switch (debuff.AttackType)
            {
                case AttackType.PHYSICAL_ATTACK: CombatMode.Physical_Hits++; break;
                case AttackType.MAGICAL_ATTACK: CombatMode.Magical_Hits++; break;
                case AttackType.HYBRID_ATTACK: CombatMode.Physical_Hits++; break;
            }

            switch (debuff.DamageType)
            {
                case DamageType.HP_DAMAGE: fixedDamage = calculateFixedDamage(debuff.FrameData, debuff.Effect); break;
                case DamageType.FIXED_DAMAGE: fixedDamage = calculateFixedDamage(debuff.FrameData, debuff.Effect); break;
                case DamageType.PHYSICAL_DAMAGE:
                    if (abilityType == AbilityType.ESPER_ABILITY) atkDamage = calculateEsperDamage(true);
                    else atkDamage = calculateAtkDamage(debuff.ScalingTypes, (i % 2));
                    break;
                case DamageType.MAGICAL_DAMAGE:
                    if (abilityType == AbilityType.ESPER_ABILITY) magDamage = calculateEsperDamage(false);
                    else magDamage = calculateMagDamage(debuff.ScalingTypes);
                    break;
                case DamageType.HYBRID_DAMAGE:
                    if (abilityType == AbilityType.ESPER_ABILITY)
                    {
                        atkDamage = calculateEsperDamage(true);
                        magDamage = calculateEsperDamage(false);
                    }
                    else
                    {
                        atkDamage = calculateAtkDamage(debuff.ScalingTypes, (i % 2));
                        magDamage = calculateMagDamage(debuff.ScalingTypes);
                    }
                    break;
            }

            foreach (string killer in AssetLoader.killerStrings)
            {
                physicalKillers.Add(getTotalDoubleStat("killerPhysical", killer) + getDoubleStat("killerPhysical" + killer));
                magicalKillers.Add(getTotalDoubleStat("killerMagical", killer) + getDoubleStat("killerMagical" + killer));
            }

            DebuffQueue queue = new DebuffQueue();
            //queue.globalFrame = CombatMode.frameRate;
            queue.atkDamage = atkDamage;
            queue.magDamage = magDamage;
            queue.physicalKillers = physicalKillers;
            queue.magicalKillers = magicalKillers;
            queue.skillMod = debuff.Effect + getTotalDoubleStat("damageModifier", name) + getDamageModifierBuff(name, debuff.LimitScale, debuff.Scale);
            if (abilityType == AbilityType.LIMIT_BURST) queue.skillMod = (queue.skillMod + debuff.LimitScale * (lbLevel - 1)) * (1 + getTotalDoubleStat("lbDamage", ""));
            queue.weaponMod = getWeaponMod((i % 2));
            queue.levelCorrection = 1 + (double)unitLevel / UnitData.MAX_UNIT_LEVEL_6_STARS;
            queue.finalMod = 0.85 + (double)AssetLoader.random.Next(15) * 0.01;
            queue.abilityName = name;
            queue.isPlayer = isPlayer;
            queue.checkpoint = 0;
            queue.currentFrame = 0;
            queue.id = chainId;
            queue.target = target;
            queue.limitBurst = lbLevel - 1;
            debuff.limitBurst = lbLevel - 1;
            queue.hex = debuff.Hex;
            queue.damageType = debuff.DamageType;
            queue.attackType = debuff.AttackType;
            queue.elements = new List<ElementalType>(debuff.ElementalTypes);
            queue.frameData = new List<int>(debuff.FrameData);
            int castDelay = 16;
            if (checkChainingNames(name)) castDelay = 8;
            if (counter) queue.delay = castDelay * (i + 1) + debuff.CastTime * i + getIntStat("timedDelay");
            else
            {
                queue.currentCastDelay = -castDelay * (i + 1);
                castFrame = debuff.CastTime;
            }
            queue.debuff = debuff;
            queue.self = this;
            CombatMode.activeQueue.Add(queue);
        }

        private bool loadComboComponent(out string error, ComboComponent combo, int target, List<Unit> victims, List<Unit> allies)
        {
            error = "";
            int casts = combo.MaxCast;
            List<Ability> comboAbilities = new List<Ability>();
            List<string> comboList = new List<string>(combo.List);
            foreach (Ability current in unitData.Abilities)
            {
                if (current.AbilityType == combo.Condition || (combo.Condition == AbilityType.ALL_MAGIC && (current.AbilityType == AbilityType.BLACK_MAGIC ||
                    current.AbilityType == AbilityType.WHITE_MAGIC || current.AbilityType == AbilityType.GREEN_MAGIC)))
                {
                    if(comboList.Count > 0)
                    {
                        foreach(string value in comboList)
                            if(current.Name.Equals(value))
                            {
                                comboAbilities.Add(current);
                                break;
                            }
                    }
                    else comboAbilities.Add(current);
                }
            }
            if (comboAbilities.Count < 1)
            {
                error = "This unit does not have any valid abilities to combo with!";
                return false;
            }
            double mpCost = -1.0;
            foreach (Ability current in comboAbilities)
                if (mpCost < 0.0 || current.Cost < mpCost)
                    mpCost = current.Cost;
            if (mpCost * casts > MP)
            {
                error = "Not enough MP to perform " + casts.ToString() + " abilities!";
                return false;
            }
            comboAbilities.Clear();
            bool mainFlag = true;
            while (mainFlag)
            {
                mpCost = 0;
                Console.WriteLine("Combo Ability    - select combo ability to use\n\"list-abilities\" - list abilities to combo with\n\"cancel\"         - cancels ability combo\n");
                for (int current = 0; current < casts; current++)
                {
                    Console.WriteLine("Select ability #" + (current + 1).ToString() + "!");
                    bool flag = true;
                    while (flag)
                    {
                        string input = Console.ReadLine().ToLower();
                        string abilityName = "";
                        string error2 = "";
                        if (input.Equals("list-abilities")) listAbilities(false, true, combo.Condition, comboList);
                        else if (input.Equals("cancel"))
                        {
                            error = "Ability combo cancelled!";
                            flag = mainFlag = false;
                            return false;
                        }
                        else if (checkAbility(input, combo.Condition, out abilityName, allies, comboList, out error2) > 1)
                        {
                            if (cdAbilityAlreadyUsed(abilityName, comboAbilities))
                                Console.WriteLine("You cannot use the same cooldown ability more than once! Try again!");
                            else
                            {
                                mpCost += AssetLoader.abilitiesData[abilityName].Cost; //AssetLoader.abilitiesData[abilityName].Cost;
                                comboAbilities.Add(AssetLoader.abilitiesData[abilityName]);
                                flag = false;
                                break;
                            }
                        }
                        if (!error2.Equals("")) Console.WriteLine(error2);
                    }
                }
                if (mpCost > MP)
                {
                    Console.WriteLine("This combo does not have enough MP to use! Try again!");
                    comboAbilities.Clear();
                }
                else mainFlag = false;
            }
            int currentAbility = 0;
            foreach (Ability abilityCombo in comboAbilities)
                prepareAbility(currentAbility++, true, false, target, out error, abilityCombo, victims, allies);
            return true;
        }

        private void loadRandomComponent(out string error, bool dualWield, bool multicasting, RandomComponent randComp, int target, List<Unit> victims, List<Unit> allies)
        {
            error = "";
            int roll = AssetLoader.random.Next(100);
            int odds = 0;
            int i = 0;
            foreach(int stat in randComp.Odds){
                odds += stat;
                if(roll < odds){
                    useAbility(0, dualWield, multicasting, false, target, out error, AssetLoader.abilitiesData[randComp.Abilities[i]], victims, allies);
                    break;
                }
                i++;
            }
        }

        protected void selectTimedDelay()
        {
            int target = 0;
            bool flag = true;
            Console.WriteLine("Set the timer delay between 0 and 300 to time your abilities!");
            while (flag)
            {
                if (int.TryParse(Console.ReadLine(), out target))
                {
                    if (target >= 0 && target < 301) flag = false;
                    else Console.WriteLine("Invalid delay!");
                }
                else Console.WriteLine("Please type in an integer between 0 and 300!");
            }
            setIntStat("timedDelay", target);
        }

        private List<Unit> checkAvailableTargets(TargetType type, List<Unit> targets, int healType, bool displayOutput)
        {
            int target = 0;
            List<Unit> available = new List<Unit>();
            foreach (Unit current in targets)
            {
                switch(healType)
                {
                    case 1:
                        if(!current.Petrified && current.CurrentState == UnitState.UNIT_DEAD && !(type == TargetType.OTHER_ALLY && current == this))
                        {
                            if (displayOutput) Console.WriteLine("#" + target.ToString() + " - " + current.Name.ToString());
                            available.Add(current);
                            target++;
                        }
                        break;
                    case 2:
                        if (current.CurrentState == UnitState.UNIT_ALIVE && !(type == TargetType.OTHER_ALLY && current == this))
                        {
                            if(displayOutput) Console.WriteLine("#" + target.ToString() + " - " + current.Name.ToString());
                            available.Add(current);
                            target++;
                        }
                        break;
                    default:
                        if (!current.Petrified && current.CurrentState == UnitState.UNIT_ALIVE && !(type == TargetType.OTHER_ALLY && current == this))
                        {
                            if(displayOutput) Console.WriteLine("#" + target.ToString() + " - " + current.Name.ToString());
                            available.Add(current);
                            target++;
                        }
                        break;
                }
            }
            return available;
        }

        private int chooseTarget(TargetType type, List<Unit> targets, int healType)
        {
            int target = 0;
            List<Unit> available = checkAvailableTargets(type, targets, healType, true);
            if (available.Count < 1) return -1;
            else if (available.Count > 1)
            {
                bool flag = true;
                Console.WriteLine("Choose a target between 0 and " + (available.Count - 1).ToString() + "!");
                while (flag)
                {
                    if (int.TryParse(Console.ReadLine(), out target))
                    {
                        if (target >= 0 && target < available.Count) flag = false;
                        else Console.WriteLine("Invalid target!");
                    }
                    else Console.WriteLine("Please type in an integer between 0 and " + (available.Count - 1).ToString() + "!");
                }
            }
            int i = 0;
            foreach (Unit current in targets)
            {
                if (current == available[target]) return i;
                i++;
            }
            return -1;
        }

        public bool prepareAbility(int abilityCount, bool multicasting, bool counter, int target, out string error, Ability ability, List<Unit> victims, List<Unit> allies)
        {
            error = "";
            if(isPlayer && CombatMode.combatState == CombatState.SELECTING_ABILITIES)
                Console.WriteLine(name + " will use " + ability.DisplayName + "!");
            bool abilityUsed = true;
            switch (ability.CostType)
            {
                case CostType.EP: Party.epRefund = ability.Cost; break;
                case CostType.LB: lbRefund += ability.Cost; break;
                case CostType.MP:
                    double cost = ability.Cost * (1.0 - getTotalDoubleStat("mpCost", ""));
                    mpRefund += cost;
                    break;
            }
            int castCount = 1;
            if (ability.Repeat > 1) castCount = ability.Repeat;
            if (ability.RepeatRand > 0) castCount += AssetLoader.random.Next(ability.RepeatRand);

            bool combo = false;
            AttackType attackType = AttackType.NONE;
            for (int i = 0; i < castCount; i++)
            {
                bool alreadySelected = false;
                foreach (AbilityComponent component in ability.AbilityComponents)
                {
                    if (isPlayer && !alreadySelected && !counter)
                    {
                        int healType = 0;
                        if (component.TargetType == TargetType.OTHER_ALLY || component.TargetType == TargetType.SINGLE_ALLY)
                        {
                            if (component is DefensiveComponent)
                            {
                                DefensiveComponent def = component as DefensiveComponent;
                                if (def.DefensiveType == DefensiveType.RAISE) healType = 1;
                                else if (def.DefensiveType == DefensiveType.REMOVE_ALL_STATUS || (def.DefensiveType == DefensiveType.REMOVE_STATUS && def.StatusTypes.Contains(StatusType.PETRIFY)))
                                    healType = 2;
                            }
                            target = chooseTarget(component.TargetType, allies, healType);
                            alreadySelected = true;
                        }
                        else if (component.TargetType == TargetType.SINGLE_ENEMY)
                        {
                            target = chooseTarget(component.TargetType, victims, healType);
                            alreadySelected = true;
                        }
                    }
                    if (component.ActionType == ActionType.COMBO)
                    {
                        abilityUsed = loadComboComponent(out error, component as ComboComponent, target, victims, allies);
                        combo = true;
                    }
                    if (component.ActionType == ActionType.OFFENSIVE) attackType = (component as OffensiveComponent).AttackType;
                }
                if(!combo)
                {
                    AbilityCommand command = new AbilityCommand();
                    command.abilityName = ability.Name;
                    command.abilityType = ability.AbilityType;
                    command.attackType = attackType;
                    command.id = chainId;
                    command.target = target;
                    command.dualWield = false;
                    command.multiCast = multicasting;
                    abilityCommands.Add(command);
                    if (!command.multiCast && command.abilityType != AbilityType.LIMIT_BURST && (command.attackType == AttackType.PHYSICAL_ATTACK || command.attackType == AttackType.HYBRID_ATTACK) &&
                        weaponSet.Count == 2 && !(checkWeapon(EquipmentType.LSHIELD, weaponSet) || checkWeapon(EquipmentType.HSHIELD, weaponSet)))
                    {
                        AbilityCommand command2 = new AbilityCommand();
                        command2.abilityName = ability.Name;
                        command2.abilityType = ability.AbilityType;
                        command2.attackType = attackType;
                        command2.id = chainId;
                        command2.target = target;
                        command2.dualWield = true;
                        command2.multiCast = multicasting;
                        abilityCommands.Add(command2);
                    }
                    abilityUsed = true;
                }
            }
            return abilityUsed;
        }

        public bool useAbility(int abilityCount, bool dualWield, bool multicasting, bool counter, int target, out string error, Ability ability, List<Unit> victims, List<Unit> allies)
        {
            error = "";
            Console.WriteLine(name + " uses " + ability.DisplayName + "!");
            bool abilityUsed = true;
            if (!dualWield)
            {
                switch (ability.CostType)
                {
                    case CostType.EP: Party.ep -= ability.Cost; break;
                    case CostType.LB: setDoubleStat("lb", getDoubleStat("lb") - ability.Cost); break;
                    case CostType.MP:
                        double cost = ability.Cost * (1.0 - getTotalDoubleStat("mpCost", ""));
                        setDoubleStat("mp", getDoubleStat("mp") - cost);
                        break;
                }
                if (ability.Cooldown > 0)
                {
                    abilityRefund = ability.DisplayName;
                    setIntStat(ability.DisplayName, ability.Cooldown);
                }
            }
            if(ability.AbilityType == AbilityType.ITEM_ABILITY) Party.itemUsed(ability.Name);

            foreach (AbilityComponent component in ability.AbilityComponents)
            {
                switch (component.ActionType)
                {
                    case ActionType.OFFENSIVE:
                        OffensiveComponent offensive = component as OffensiveComponent;
                        loadOffensiveComponent(counter, abilityCount, target, ability.DisplayName, offensive, ability.AbilityType, victims, allies);
                        break;
                    case ActionType.DEFENSIVE: loadDefensiveComponent(counter, abilityCount, target, ability.DisplayName, component as DefensiveComponent); break;
                    case ActionType.BUFFING:
                        loadBuffComponent(counter, abilityCount, target, ability.DisplayName, component as BuffComponent);
                        if (ability.DisplayName.Equals("Defend"))
                        {
                            List<string> list = findExternalAbility("guarding");
                            foreach (string abilityName in list)
                                useAbility(abilityCount, dualWield, multicasting, counter, 0, out error, AssetLoader.abilitiesData[abilityName], victims, allies);
                        }
                        break;
                    case ActionType.DEBUFFING: loadDebuffComponent(counter, abilityCount, target, ability.DisplayName, component as DebuffComponent, ability.AbilityType); break;
                    //case ActionType.COMBO: abilityUsed = loadComboComponent(out error, component as ComboComponent, target, victims, allies); break;
                    case ActionType.RANDOM: loadRandomComponent(out error, dualWield, multicasting, component as RandomComponent, target, victims, allies); break;
                }
                foreach (BuffComponent buff in buffs)
                {
                    if (buff.Unlocks.Count > 0 && buff.Unlocks[0] == ability.Name && buff.Charge > 0)
                    {
                        buff.discharge();
                        break;
                    }
                }
            }
            return abilityUsed;
        }

        private bool cdAbilityAlreadyUsed(string name, List<Ability> abilityList)
        {
            int cooldown = AssetLoader.abilitiesData[name].Cooldown;
            if(cooldown > 0)
                foreach(Ability ability in abilityList)
                    if(ability.Name.Equals(name))
                        return true;
            return false;
        }

        private int checkCost(string message, double currentCost, double minimumCost, out string error)
        {
            error = "";
            if (currentCost >= minimumCost) return 2;
            else
            {
                error += message;
                return 1;
            }
        }

        public bool checkHPThreshold(string passiveName)
        {
            //getTotalDoubleStat("direThreshold", "") * MaxHP >= HP;
            double mod = unitData.getHpThreshold(passiveName, this);
            if(mod > 1e-4)
                return mod * MaxHP >= HP;
            foreach (Weapon weapon in weaponSet)
            {
                foreach (Passive passive in weapon.EquipPassives)
                    if (passive.Name.Equals(passiveName))
                        return passive.getSimpleDoubleStat("direThreshold") * MaxHP >= HP;
                foreach (Passive passive in weapon.EquipData.EquipPassives)
                    if (passive.Name.Equals(passiveName))
                        return passive.getSimpleDoubleStat("direThreshold") * MaxHP >= HP;
            }
            foreach (Equipment equip in equipmentSet)
                foreach (Passive passive in equip.EquipData.EquipPassives)
                    if (passive.Name.Equals(passiveName))
                        return passive.getSimpleDoubleStat("direThreshold") * MaxHP >= HP;
            foreach (Materia materia in materiaSet)
                if (materia.PassiveEffect != null && materia.PassiveEffect.Name.Equals(passiveName))
                    return materia.PassiveEffect.getSimpleDoubleStat("direThreshold") * MaxHP >= HP;
            if (esper != null)
            {
                foreach (Passive passive in esper.EquipPassives)
                    if (passive.Name.Equals(passiveName))
                        return passive.getSimpleDoubleStat("direThreshold") * MaxHP >= HP;
            }
            return false;
        }

        public bool abilityIsInnate(string input)
        {
            foreach (Ability ability in unitData.Abilities)
                if (ability.DisplayName.Equals(input))
                    return true;
            return false;
        }

        public bool checkAbilityName(string input)
        {
            foreach (Ability ability in unitData.Abilities)
                if (ability.DisplayName.Equals(input))
                    return true;
            foreach (Equipment equip in weaponSet)
                foreach (Ability ability in equip.EquipData.EquipAbilities)
                    if (ability.DisplayName.Equals(input))
                        return true;
            foreach (Equipment equip in equipmentSet)
                foreach (Ability ability in equip.EquipData.EquipAbilities)
                    if (ability.DisplayName.Equals(input))
                        return true;
            foreach (Materia materia in materiaSet)
                if (materia.AbilityEffect != null && materia.AbilityEffect.DisplayName.Equals(input))
                    return true;
            if(esper != null)
                foreach (Ability ability in esper.Abilities)
                    if (ability.DisplayName.Equals(input))
                        return true;
            return false;
        }

        private bool checkCombo(AbilityType selectedType, AbilityType targetType, string name, List<string> list)
        {
            bool flag = !(targetType == AbilityType.NORMAL_ABILITY || targetType == AbilityType.LIMIT_BURST || targetType == AbilityType.ESPER_ABILITY) &&
                (selectedType != targetType && !(targetType == AbilityType.ALL_MAGIC && (selectedType == AbilityType.BLACK_MAGIC ||
                selectedType == AbilityType.WHITE_MAGIC || selectedType == AbilityType.GREEN_MAGIC)));
            if (!flag && list.Count > 0)
            {
                flag = true;
                foreach (string value in list)
                    if (name.Equals(value))
                        flag = false;
            }
            return flag;
        }

        public int checkAnAbility(int i, Ability ability, string input, AbilityType abilityType, out string abilityName, List<Unit> allies, List<string> comboList, out string error)
        {
            error = "";
            abilityName = "";
            if (ability.DisplayName.ToLower().Equals(input))
            {
                abilityName = ability.Name;
                if (!checkAbilityCondition(!abilityIsInnate(abilityName), i, abilityName, ability.Condition, ability.Requirement, true, out error))
                    return 1;

                if (Silenced && (ability.AbilityType == AbilityType.BLACK_MAGIC || ability.AbilityType == AbilityType.WHITE_MAGIC ||
                    ability.AbilityType == AbilityType.GREEN_MAGIC))
                {
                    error += Name + " has been silenced and cannot use magic!";
                    return 1;
                }
                if(checkCombo(ability.AbilityType, abilityType, abilityName, comboList))
                {
                    error += "Cannot use this ability for the combo!";
                    return 1;
                }
                if (ability.Cooldown > 0 && getIntStat(ability.DisplayName) > 0)
                {
                    error += "Ability is on a cooldown.";
                    return 1;
                }
                if (ability.Name != "Angelic Feather" && ability.Name != "Angelic Feather+")
                {
                    foreach (AbilityComponent comp in ability.AbilityComponents)
                    {
                        DefensiveComponent def = comp as DefensiveComponent;
                        ComboComponent combo = comp as ComboComponent;
                        int healType = 0;
                        if(combo != null && (combo.Condition == AbilityType.BLACK_MAGIC || combo.Condition == AbilityType.WHITE_MAGIC || combo.Condition == AbilityType.GREEN_MAGIC
                                || combo.Condition == AbilityType.ALL_MAGIC) && Silenced)
                        {
                            error += Name + " has been silenced and cannot use any number of magic spells!";
                            return 1;
                        }
                        if (def != null)
                        {
                            if (def.DefensiveType == DefensiveType.RAISE) healType = 1;
                            else if (def.DefensiveType == DefensiveType.REMOVE_ALL_STATUS ||
                                (def.DefensiveType == DefensiveType.REMOVE_STATUS && def.StatusTypes.Contains(StatusType.PETRIFY)))
                                healType = 2;
                        }
                        List<Unit> available = checkAvailableTargets(comp.TargetType, allies, healType, false);
                        if ((comp.TargetType == TargetType.OTHER_ALLIES || comp.TargetType == TargetType.OTHER_ALLY) && available.Count < 1)
                        {
                            error += "Ability does not have valid targets!";
                            return 1;
                        }
                    }
                }
                switch (ability.CostType)
                {
                    case CostType.LB: return checkCost("Not enough limit burst crystals!", LB - lbRefund, ability.Cost, out error);
                    case CostType.EP: return checkCost("Not enough esper orbs!", Party.ep - Party.epRefund, ability.Cost, out error);
                    case CostType.MP: return checkCost("Not enough mp!", MP - mpRefund, ability.Cost * (1.0 - getTotalDoubleStat("mpCost", "")), out error);
                }
                return 2;
            }
            return 0;
        }

        public int checkAbility(string input, AbilityType abilityType, out string abilityName, List<Unit> allies, List<string> comboList, out string error)
        {
            error = "";
            abilityName = "";
            int checkpoint, i = 0;
            int flag = 0;
            foreach(Ability ability in unitData.Abilities){
                checkpoint = checkAnAbility(i, ability, input, abilityType, out abilityName, allies, comboList, out error);
                if (checkpoint > 0)
                {
                    flag = checkpoint;
                    break;
                }
                i++;
            }
            if (flag > 0) return flag;
            i = 0;
            foreach (BuffComponent buff in buffs)
            {
                if (buff.BuffType == BuffType.GAIN_ABILITY)
                {
                    foreach (string currentName in buff.AbilitiesList)
                    {
                        Ability ability = AssetLoader.abilitiesData[currentName];
                        checkpoint = checkAnAbility(i, ability, input, abilityType, out abilityName, allies, comboList, out error);
                        if (checkpoint > 0)
                        {
                            flag = checkpoint;
                            break;
                        }
                        i++;
                    }
                }
                if (flag > 0) break;
            } 
            if (flag > 0) return flag;
            i = 0;
            foreach (Equipment equip in weaponSet)
                foreach (Ability ability in equip.EquipData.EquipAbilities)
                {
                    checkpoint = checkAnAbility(i, ability, input, abilityType, out abilityName, allies, comboList, out error);
                    if (checkpoint > 0)
                    {
                        flag = checkpoint;
                        break;
                    }
                    i++;
                }
            if (flag > 0) return flag;
            i = 0;
            foreach (Equipment equip in equipmentSet)
                foreach (Ability ability in equip.EquipData.EquipAbilities)
                {
                    checkpoint = checkAnAbility(i, ability, input, abilityType, out abilityName, allies, comboList, out error);
                    if (checkpoint > 0)
                    {
                        flag = checkpoint;
                        break;
                    }
                    i++;
                }
            if (flag > 0) return flag;
            i = 0;
            foreach (Materia materia in materiaSet)
            {
                if (materia.AbilityEffect != null)
                {
                    checkpoint = checkAnAbility(i, materia.AbilityEffect, input, abilityType, out abilityName, allies, comboList, out error);
                    if (checkpoint > 0)
                    {
                        flag = checkpoint;
                        break;
                    }
                }
                i++;
            }
            if (flag > 0) return flag;
            i = 0;
            if (getTotalIntStat("evokeAllEspers", "") > 0)
            {
                foreach (Character toon in Party.currentParty)
                {
                    flag = toon.checkEsper(input, abilityType, out abilityName, allies, comboList, out error);
                    if (flag > 0) break;
                }
            }
            else flag = checkEsper(input, abilityType, out abilityName, allies, comboList, out error);
            if (flag == 0) error += "Ability does not exist!";
            return flag;
        }

        public int checkEsper(string input, AbilityType abilityType, out string abilityName, List<Unit> allies, List<string> comboList, out string error)
        {
            abilityName = "";
            error = "";
            int i = 0;
            if (esper != null)
                foreach (Ability ability in esper.Abilities)
                {
                    int checkpoint = checkAnAbility(i, ability, input, abilityType, out abilityName, allies, comboList, out error);
                    if (checkpoint > 0) return checkpoint;
                    i++;
                }
            return 0;
        }

        public bool checkEquipment(string requirement)
        {
            if(requirement.Equals("Crimson Saber Special"))
                return checkEquipment("Crimson Saber II") || checkEquipment("Crimson Saber III");
            if (requirement.Equals("Purple Lightning Special"))
                return checkEquipment("Purple Lightning II") || checkEquipment("Purple Lightning III");
            if (requirement.Equals("Reincarnation Special"))
                return checkEquipment("Reincarnation II") || checkEquipment("Reincarnation III");
            foreach (Equipment equip in weaponSet)
                if (equip.EquipData.DisplayName.Equals(requirement))
                    return true;
            foreach (Equipment equip in equipmentSet)
                if (equip.EquipData.DisplayName.Equals(requirement))
                    return true;
            foreach (Materia materia in materiaSet)
                if (materia.DisplayName.Equals(requirement))
                    return true;
            return false;
        }

        private bool checkName(string target)
        {
            switch (target)
            {
                case "rainCondition": return name.Equals("Rain") || name.Equals("Awakened Rain");
                case "lasswellCondition": return name.Equals("Lasswell") || name.Equals("Pyro Glacial Lasswell");
                case "finaCondition": return name.Equals("Fina") || name.Equals("Lotus Mage Fina");
                case "lidCondition": return name.Equals("Lid") || name.Equals("Heavenly Technician Lid");
                case "nicholCondition": return name.Equals("Nichol") || name.Equals("Maritime Strategist Nichol");
                case "jakeCondition": return name.Equals("Jake") || name.Equals("Nameless Gunner Jake");
                case "sakuraCondition": return name.Equals("Sakura") || name.Equals("Blossom Sage Sakura");
            }
            return name.Equals(target);
        }

        private bool checkUnlocks(string requirement, IList<string> unlocks)
        {
            foreach (string unlock in unlocks)
            {
                switch (requirement)
                {
                    case "bloomstoneUnlock": if (unlock.Equals("Curing Bloomstone") || unlock.Equals("Resilient Bloomstone")) return true; break;
                    case "healstoneUnlock": if (unlock.Equals("Empowering Healstone") || unlock.Equals("Greater Healstone")) return true; break;
                    case "windstoneUnlock": if (unlock.Equals("Clearing Windstone") || unlock.Equals("Razorsharp Windstone")) return true; break;
                    case "earthstoneUnlock": if (unlock.Equals("Fortifying Earthstone") || unlock.Equals("Trembling Earthstone")) return true; break;
                    case "lightstoneUnlock": if (unlock.Equals("Illuminating Lightstone") || unlock.Equals("Disintegrating Lightstone")) return true; break;
                    case "firestoneUnlock": if (unlock.Equals("Radiant Firestone") || unlock.Equals("Burning Firestone")) return true; break;
                    case "growthstoneUnlock": if (unlock.Equals("Enhancing Growthstone") || unlock.Equals("Volatile Growthstone")) return true; break;
                    case "rainbowstoneUnlock": if (unlock.Equals("Chromatic Rainbowstone") || unlock.Equals("Flashing Rainbowstone")) return true; break;
                    case "jobLevelUpUnlocks": if (unlock.Equals("Job level up: Level 3") || unlock.Equals("Job level up: Job Master ***")) return true; break;
                    default: if (unlock.Equals(requirement)) return true; break;
                }
            }
            return false;
        }
        
        private bool checkAbilityCondition(bool equipment, int i, string abilityName, string condition, string requirement, bool displayOutput, out string error)
        {
            error = "";
            if (!equipment && unitLevel < unitData.LevelRequired[i])
            {
                if (displayOutput) error = "Unit level is too low to use this ability!";
                return false;
            }
            bool abilityLocked = true;
            switch (condition)
            {
                case "unitCondition":
                    if (!checkName(requirement))
                    {
                        if (displayOutput) error = "Only " + requirement + " can use this ability!";
                        return false;
                    }
                    break;
                case "notPassiveCondition":
                    if (!unitData.fulfilsPassiveCondition(i, condition, requirement, this))
                    {
                        if (displayOutput) error = "An upgraded version of this ability exists!";
                        return false;
                    }
                    break;
                case "passiveCondition":
                    if (!unitData.fulfilsPassiveCondition(i, condition, requirement, this))
                    {
                        if (displayOutput) error = "Ability must be unlocked by having " + requirement + " first!";
                        return false;
                    }
                    break;
                case "notEquipCondition":
                    if(checkEquipment(requirement))
                    {
                        if (displayOutput) error = "An upgraded version of this ability exists!";
                        return false;
                    }
                    break;
                case "equipCondition":
                    if(!checkEquipment(requirement))
                    {
                        if (displayOutput) error = "Ability must be unlocked by equipping " + requirement + " first!";
                        return false;
                    }
                    break;
                case "abilityUnlock":
                    foreach (BuffComponent buff in buffs)
                        if (buff.BuffType == BuffType.UNLOCKS && buff.Unlocks.Count > 0)
                        {
                            foreach (string unlock in buff.Unlocks)
                                if(unlock.Equals(abilityName))
                                {
                                    abilityLocked = false;
                                    break;
                                }
                            
                        }
                    if (abilityLocked)
                    {
                        if (displayOutput) error = "Ability must be unlocked first!";
                        return false;
                    }
                    break;
                case "notAbilityUnlock":
                    //TO DO
                    foreach (BuffComponent buff in buffs)
                        if (buff.BuffType == BuffType.UNLOCKS && buff.Unlocks.Count > 0 && checkUnlocks(requirement, buff.Unlocks))
                        {
                            abilityLocked = false;
                            break;
                        }
                    if (!abilityLocked)
                    {
                        if (displayOutput) error = "A stronger version of this ability is being used!";
                        return false;
                    }
                    break;
            }
            return true;
        }

        public bool canPerformAction()
        {
            if (getBoolStat("readyState")) return false;
            if (getBoolStat("actionTaken")) return false;
            if (getBoolStat("sleeping")) return false;
            if (getBoolStat("confused")) return false;
            if (getBoolStat("paralysed")) return false;
            if (getBoolStat("petrified")) return false;
            if (getBoolStat("charmed")) return false;
            if (getBoolStat("stopped")) return false;
            if (getBoolStat("berserked")) return false;
            return true;
        }

        private void listAnAbility(bool equipment, bool displayCooldown, bool combo, List<string> comboList, int i, Ability ability, AbilityType type, List<string> consoleList)
        {
            string output = "";
            if (checkAbilityCondition(equipment, i, ability.Name, ability.Condition, ability.Requirement, false, out output) &&
                !(combo && checkCombo(ability.AbilityType, type, ability.Name, comboList)))
            {
                string finalString = "";
                string cooldown = "";
                if (displayCooldown)
                {
                    if (ability.Cooldown > 0)
                    {
                        cooldown = ability.Cooldown.ToString();
                        int current = getIntStat(ability.DisplayName);
                        if (current < 1) finalString = ability.DisplayName + " - Ready to be used!";
                        else finalString = ability.DisplayName + " - " + current + " out of " + cooldown + " turns remaining!";
                        if (!consoleList.Contains(finalString)) consoleList.Add(finalString);
                    }
                }
                else
                {
                    string cost = "";
                    if (ability.CostType != CostType.NONE) cost = "Cost: " + ability.Cost.ToString() + ability.CostType.ToString() + ", ";
                    if (ability.Cooldown > 0)
                    {
                        if (ability.OnFirstTurn) cooldown = "Cooldown: " + ability.Cooldown.ToString() + " turns, can be used on turn 1, ";
                        else cooldown = "Cooldown: " + ability.Cooldown.ToString() + " turns, cannot be used on turn 1, ";
                    }
                    finalString = ability.DisplayName + " - " + cost + cooldown + ability.Description;
                    if (!consoleList.Contains(finalString)) consoleList.Add(finalString);
                }
            }
        }

        protected void listAbilities(bool cooldown, bool combo, AbilityType type, List<string> comboList)
        {
            List<string> consoleList = new List<string>();
            int i = 0;
            foreach(Ability ability in unitData.Abilities)
            {
                listAnAbility(false, cooldown, combo, comboList, i, ability, type, consoleList);
                i++;
            }
            i = 0;
            foreach (BuffComponent buff in buffs)
                if (buff.BuffType == BuffType.GAIN_ABILITY)
                    foreach (string currentName in buff.AbilitiesList)
                    {
                        Ability ability = AssetLoader.abilitiesData[currentName];
                        listAnAbility(false, cooldown, combo, comboList, i, ability, type, consoleList);
                        i++;
                    }
            i = 0;
            foreach (Equipment equip in weaponSet)
                foreach (Ability ability in equip.EquipData.EquipAbilities)
                {
                    listAnAbility(true, cooldown, combo, comboList, i, ability, type, consoleList);
                    i++;
                }
            i = 0;
            foreach (Equipment equip in equipmentSet)
                foreach (Ability ability in equip.EquipData.EquipAbilities)
                {
                    listAnAbility(true, cooldown, combo, comboList, i, ability, type, consoleList);
                    i++;
                }
            i = 0;
            foreach (Materia materia in materiaSet)
            {
                if (materia.AbilityEffect != null) listAnAbility(true, cooldown, combo, comboList, i, materia.AbilityEffect, type, consoleList);
                i++;
            }
            if (getTotalIntStat("evokeAllEspers", "") > 0)
                foreach (Character toon in Party.currentParty)
                    toon.listEsper(cooldown, combo, type, comboList, consoleList);
            else listEsper(cooldown, combo, type, comboList, consoleList);

            if(cooldown) Console.WriteLine("\nCooldown Abilities for " + Name + ":\n");
            else Console.WriteLine("\nAbilities for " + Name + ":\n");
            foreach (string description in consoleList) Console.WriteLine(description);
        }

        public int listEsper(bool cooldown, bool combo, AbilityType type, List<string> comboList, List<string> consoleList)
        {
            int i = 0;
            if (esper != null)
                foreach (Ability ability in esper.Abilities)
                {
                    listAnAbility(true, cooldown, combo, comboList, i, ability, type, consoleList);
                    i++;
                }
            return 0;
        }

        private string displayParameterStatus(string parameter, int bonus, int penalty, bool empty)
        {
            string parameterString = "";
            if (bonus > 1e-4 || penalty > 1e-4)
            {
                if (!empty) parameterString += ", ";
                parameterString += parameter;
                if (bonus > 1e-4)
                {
                    parameterString += " +" + bonus.ToString();
                    if (!(parameter.Equals("HP Regen") || parameter.Equals("MP Regen") || parameter.Equals("LB Regen")))
                        parameterString += "%";
                }
                if (penalty > 1e-4) parameterString += " -" + penalty.ToString() + "%";
            }
            return parameterString;
        }
        private string displayAilmentStatus(string parameter, bool afflicted, bool empty)
        {
            string statusString = "";
            if (afflicted)
            {
                if (!empty) statusString += ", ";
                statusString += parameter;
            }
            return statusString;
        }

        private int countDebuffs()
        {
            int count = 0;
            if (BaseAtkPenalty > 1e-4) count++;
            if (BaseMagPenalty > 1e-4) count++;
            if (BaseDefPenalty > 1e-4) count++;
            if (BaseSprPenalty > 1e-4) count++;
            if (FireImperil > 1e-4) count++;
            if (IceImperil > 1e-4) count++;
            if (ThunderImperil > 1e-4) count++;
            if (WaterImperil > 1e-4) count++;
            if (WindImperil > 1e-4) count++;
            if (EarthImperil > 1e-4) count++;
            if (LightImperil > 1e-4) count++;
            if (DarkImperil > 1e-4) count++;
            if (Blinded) count++;
            if (Silenced) count++;
            if (Paralysed) count++;
            if (Sleeping) count++;
            if (Confused) count++;
            if (Petrified) count++;
            if (Diseased) count++;
            if (Stopped) count++;
            if (Charmed) count++;
            if (Berserked) count++;
            return count;
        }

        public void displayStats(int i)
        {
            string extraLine = "";
            string parameterLine = displayParameterStatus("Atk", (int)(BaseAtkBonus * 100), (int)(BaseAtkPenalty * 100), true);
            parameterLine += displayParameterStatus("Mag", (int)(BaseMagBonus * 100), (int)(BaseMagPenalty * 100), parameterLine.Equals(""));
            parameterLine += displayParameterStatus("Def", (int)(BaseDefBonus * 100), (int)(BaseDefPenalty * 100), parameterLine.Equals(""));
            parameterLine += displayParameterStatus("Spr", (int)(BaseSprBonus * 100), (int)(BaseSprPenalty * 100), parameterLine.Equals(""));
            parameterLine += displayParameterStatus("HP Regen", (int)(getDoubleStat("hpRecoverBuff")), 0, parameterLine.Equals(""));
            parameterLine += displayParameterStatus("MP Regen", (int)(getDoubleStat("mpRecoverBuff")), 0, parameterLine.Equals(""));
            parameterLine += displayParameterStatus("LB Regen", (int)(getDoubleStat("lbRecoverBuff")), 0, parameterLine.Equals(""));
            parameterLine += displayParameterStatus("HP% Regen", (int)(getDoubleStat("hpRecoverPercentBuff") * 100), 0, parameterLine.Equals(""));
            parameterLine += displayParameterStatus("MP% Regen", (int)(getDoubleStat("mpRecoverPercentBuff") * 100), 0, parameterLine.Equals(""));
            parameterLine += displayParameterStatus("LB% Regen", (int)(getDoubleStat("lbRecoverPercentBuff") * 100), 0, parameterLine.Equals(""));
            string elementalLine = displayParameterStatus("Fi", (int)(getDoubleStat("fireResist") * 100), (int)(FireImperil * 100), true);
            elementalLine += displayParameterStatus("Ic", (int)(getDoubleStat("iceResist") * 100), (int)(IceImperil * 100), elementalLine.Equals(""));
            elementalLine += displayParameterStatus("Th", (int)(getDoubleStat("ThunderResist") * 100), (int)(ThunderImperil * 100), elementalLine.Equals(""));
            elementalLine += displayParameterStatus("Wa", (int)(getDoubleStat("waterResist") * 100), (int)(WaterImperil * 100), elementalLine.Equals(""));
            elementalLine += displayParameterStatus("Wi", (int)(getDoubleStat("windResist") * 100), (int)(WindImperil * 100), elementalLine.Equals(""));
            elementalLine += displayParameterStatus("Ea", (int)(getDoubleStat("earthResist") * 100), (int)(EarthImperil * 100), elementalLine.Equals(""));
            elementalLine += displayParameterStatus("Li", (int)(getDoubleStat("lightResist") * 100), (int)(LightImperil * 100), elementalLine.Equals(""));
            elementalLine += displayParameterStatus("Da", (int)(getDoubleStat("darkResist") * 100), (int)(DarkImperil * 100), elementalLine.Equals(""));
            string ailmentLine = displayAilmentStatus("Poisoned", Poisoned, true);
            ailmentLine += displayAilmentStatus("Blinded", Blinded, ailmentLine.Equals(""));
            ailmentLine += displayAilmentStatus("Silenced", Silenced, ailmentLine.Equals(""));
            ailmentLine += displayAilmentStatus("Paralysed", Paralysed, ailmentLine.Equals(""));
            ailmentLine += displayAilmentStatus("Sleeping", Sleeping, ailmentLine.Equals(""));
            ailmentLine += displayAilmentStatus("Confused", Confused, ailmentLine.Equals(""));
            ailmentLine += displayAilmentStatus("Petrified", Petrified, ailmentLine.Equals(""));
            ailmentLine += displayAilmentStatus("Diseased", Diseased, ailmentLine.Equals(""));
            ailmentLine += displayAilmentStatus("Stopped", Stopped, ailmentLine.Equals(""));
            ailmentLine += displayAilmentStatus("Charmed", Charmed, ailmentLine.Equals(""));
            ailmentLine += displayAilmentStatus("Berserked", Berserked, ailmentLine.Equals(""));
            string mitigateLine = displayAilmentStatus("Provoke", getDoubleStat("targetChance") > 1e-4, true);
            mitigateLine += displayAilmentStatus("All Cover", AllCover > 1e-4, mitigateLine.Equals(""));
            mitigateLine += displayAilmentStatus("P Cover", PhysicalCover > 1e-4, mitigateLine.Equals(""));
            mitigateLine += displayAilmentStatus("M Cover", MagicalCover > 1e-4, mitigateLine.Equals(""));
            mitigateLine += displayParameterStatus("Dmg", 0, (int)(GeneralMitigation * 100), mitigateLine.Equals(""));
            mitigateLine += displayParameterStatus("P Dmg", 0, (int)(PhysicalMitigation * 100), mitigateLine.Equals(""));
            mitigateLine += displayParameterStatus("M Dmg", 0, (int)(MagicalMitigation * 100), mitigateLine.Equals(""));
            string breakResistLine = displayParameterStatus("Atk Res", getIntStat("atkBreakResist"), 0, true);
            breakResistLine += displayParameterStatus("Mag Res", getIntStat("magBreakResist"), 0, breakResistLine.Equals(""));
            breakResistLine += displayParameterStatus("Def Res", getIntStat("defBreakResist"), 0, breakResistLine.Equals(""));
            breakResistLine += displayParameterStatus("Spr Res", getIntStat("sprBreakResist"), 0, breakResistLine.Equals(""));
            string ailmentResistLine = displayParameterStatus("Po Res", getIntStat("poisonResist"), 0, true);
            ailmentResistLine += displayParameterStatus("Bl Res", getIntStat("blindResist"), 0, ailmentResistLine.Equals(""));
            ailmentResistLine += displayParameterStatus("Si Res", getIntStat("silenceResist"), 0, ailmentResistLine.Equals(""));
            ailmentResistLine += displayParameterStatus("Pa Res", getIntStat("paralyseResist"), 0, ailmentResistLine.Equals(""));
            ailmentResistLine += displayParameterStatus("Sl Res", getIntStat("sleepResist"), 0, ailmentResistLine.Equals(""));
            ailmentResistLine += displayParameterStatus("Co Res", getIntStat("confuseResist"), 0, ailmentResistLine.Equals(""));
            ailmentResistLine += displayParameterStatus("Pe Res", getIntStat("petrifyResist"), 0, ailmentResistLine.Equals(""));
            ailmentResistLine += displayParameterStatus("Di Res", getIntStat("diseaseResist"), 0, ailmentResistLine.Equals(""));
            ailmentResistLine += displayParameterStatus("St Res", getIntStat("stopResist"), 0, ailmentResistLine.Equals(""));
            ailmentResistLine += displayParameterStatus("Ch Res", getIntStat("charmResist"), 0, ailmentResistLine.Equals(""));
            ailmentResistLine += displayParameterStatus("Be Res", getIntStat("berserkResist"), 0, ailmentResistLine.Equals(""));
            string otherLine = displayAilmentStatus("Reraise", getBoolStat("reraise"), true);
            foreach (ElementalType test in imbueList)
                switch(test)
                {
                    case ElementalType.FIRE:
                        if (!otherLine.Equals("")) otherLine += ", ";
                        otherLine += "Fi Imbue";
                        break;
                    case ElementalType.ICE:
                        if (!otherLine.Equals("")) otherLine += ", ";
                        otherLine += "Ic Imbue";
                        break;
                    case ElementalType.THUNDER:
                        if (!otherLine.Equals("")) otherLine += ", ";
                        otherLine += "Th Imbue";
                        break;
                    case ElementalType.WATER:
                        if (!otherLine.Equals("")) otherLine += ", ";
                        otherLine += "Wa Imbue";
                        break;
                    case ElementalType.WIND:
                        if (!otherLine.Equals("")) otherLine += ", ";
                        otherLine += "Wi Imbue";
                        break;
                    case ElementalType.EARTH:
                        if (!otherLine.Equals("")) otherLine += ", ";
                        otherLine += "Ea Imbue";
                        break;
                    case ElementalType.LIGHT:
                        if (!otherLine.Equals("")) otherLine += ", ";
                        otherLine += "Li Imbue";
                        break;
                    case ElementalType.DARK:
                        if (!otherLine.Equals("")) otherLine += ", ";
                        otherLine += "Da Imbue";
                        break;
                }

            if (!parameterLine.Equals("")) extraLine += "\n" + parameterLine;
            if (!elementalLine.Equals("")) extraLine += "\n" + elementalLine;
            if (!ailmentLine.Equals("")) extraLine += "\n" + ailmentLine;
            if (!mitigateLine.Equals("")) extraLine += "\n" + mitigateLine;
            if (!breakResistLine.Equals("")) extraLine += "\n" + breakResistLine;
            if (!ailmentResistLine.Equals("")) extraLine += "\n" + ailmentResistLine;
            if (!otherLine.Equals("")) extraLine += "\n" + otherLine;
            if (isPlayer)
            {
                Console.WriteLine("#" + i.ToString() + " ... " + Name +
                    " - HP: " + Math.Floor(getDoubleStat("hp")).ToString() + "/" + Math.Floor(MaxHP).ToString() +
                    ", BP: " + Math.Floor(getDoubleStat("bp")).ToString() +
                    ", MP: " + Math.Floor(getDoubleStat("mp")).ToString() + "/" + Math.Floor(MaxMP).ToString() +
                    ", LB: " + Math.Floor(getDoubleStat("lb")).ToString() + "/" + Math.Floor(unitData.LbCost).ToString() +
                    extraLine);
            }
            else
            {
                Console.WriteLine("#" + i.ToString() + " ... " + Name +
                    " - HP: " + (Math.Floor(getDoubleStat("hp") / MaxHP * 100)).ToString() +
                    "%, MP: " + (Math.Floor(getDoubleStat("mp") / MaxMP * 100)).ToString() +
                    "%" + extraLine);
            }
        }

        public void displayExtensiveStats()
        {
            Console.WriteLine("\n== " + Name + " == Lv. " + unitLevel.ToString() + " ==\n\n" + 
                "HP: " + Math.Floor(getDoubleStat("hp")).ToString() + "/" + Math.Floor(MaxHP).ToString() +
                "\nMP: " + Math.Floor(getDoubleStat("mp")).ToString() + "/" + Math.Floor(MaxMP).ToString() +
                "\n\nFire Resist: " + Math.Round((FireResist - FireImperil)*100).ToString() +
                "%\nIce Resist: " + Math.Round((IceResist - IceImperil) * 100).ToString() +
                "%\nThunder Resist: " + Math.Round((ThunderResist - ThunderImperil) * 100).ToString() +
                "%\nWater Resist: " + Math.Round((WaterResist - WaterImperil) * 100).ToString() +
                "%\nWind Resist: " + Math.Round((WindResist - WindImperil) * 100).ToString() +
                "%\nEarth Resist: " + Math.Round((EarthResist - EarthImperil) * 100).ToString() +
                "%\nLight Resist: " + Math.Round((LightResist - LightImperil) * 100).ToString() +
                "%\nDark Resist: " + Math.Round((DarkResist - DarkImperil) * 100).ToString() +
                "%\n\nPoison Resist: " + PoisonResist.ToString() +
                "%\nBlind Resist: " + BlindResist.ToString() +
                "%\nSilence Resist: " + SilenceResist.ToString() +
                "%\nSleep Resist: " + SleepResist.ToString() +
                "%\nParalyse Resist: " + ParalyseResist.ToString() +
                "%\nConfuse Resist: " + ConfuseResist.ToString() +
                "%\nPetrify Resist: " + PetrifyResist.ToString() +
                "%\nDisease Resist: " + DiseaseResist.ToString() + "%\n");
        }
    }
}
