﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    public class Omega : Enemy
    {

        public Omega(string name, int lbLevel, int id, int actions, UnitData unitData) : base(name, lbLevel, id, actions, unitData)
        {
            
        }

        public override bool takeAction(string input, List<Unit> characters, List<Unit> allies)
        {
            setBoolStat("actionTaken", true);
            if (unitData.Abilities.Count < 1) return false;

            actions = 8;
            if (HP <= MaxHP * 0.5) actions += 3;
            int i = 0;
            string error = "";

            if (CombatMode.enemyTurn == 1) prepareAbility(0, false, false, 0, out error, AssetLoader.abilitiesData["Atomic Ray"], characters, allies);
            else
            {
                prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Flame Burst"], characters, allies);
                int turnAbility = (CombatMode.enemyTurn - 1) % 10;
                switch (turnAbility)
                {
                    case 1: prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Recoil"], characters, allies); break;
                    case 2: prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Earthquake"], characters, allies); break;
                    case 3: prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Wave Cannon"], characters, allies); break;
                    case 4: prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Encircle"], characters, allies); break;
                    case 5: prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Recoil"], characters, allies); break;
                    case 6: prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Wave Cannon"], characters, allies); break;
                    case 7: prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Atomic Ray"], characters, allies); break;
                    case 8: prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Earthquake"], characters, allies); break;
                    case 9: prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Encircle"], characters, allies); break;
                    default: prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Atomic Ray"], characters, allies); break;
                }
                if (HP <= MaxHP * 0.5 && (CombatMode.enemyTurn - 2) % 3 == 0) prepareAbility(i++, false, false, 0, out error, AssetLoader.abilitiesData["Earthquake"], characters, allies);
                else if ((CombatMode.enemyTurn - 2) % 3 == 0) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Rainbow Wind"], characters, allies);
                while (i < actions) prepareAbility(i++, false, false, getTarget(characters), out error, AssetLoader.abilitiesData["Attack"], characters, allies);
            }
            return true;
        }
    }
}
