﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalFantasyConsoleExvius
{
    public class TownData
    {
        private string name;
        private string intro;
        private List<string> shopItems;
        private Dictionary<string, string> talkData;

        public TownData(string name, string intro, List<string> shopItems, Dictionary<string, string> talkData)
        {
            this.name = name;
            this.intro = intro;
            this.shopItems = shopItems;
            this.talkData = talkData;
        }

        public string Name { get { return name; } }
        public string Intro { get { return intro; } }
        public IList<string> ShopItems { get { return shopItems.AsReadOnly(); } }

	    public int getTalkDataSize(){ return talkData.Count; }
        public string getTalkDataKey(int i) { return talkData.Keys.ToList()[i]; }
        public string getTalkDataValue(int i) { return talkData.Values.ToList()[i]; }
        public string getTalkDataValue(string key) { return talkData[key]; }
    }
}
